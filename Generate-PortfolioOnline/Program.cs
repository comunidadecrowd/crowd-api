﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Domain.Util;
using Backend.Crowd.Infrasctructure.Database.Context;
using Backend.Crowd.Infrasctructure.Database.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generate_PortfolioOnline
{
    class Program
    {
        private static IServiceFreelancer _serviceFreelancer;

        static void Main(string[] args)
        {
            var crowdContext = new CrowdContext();
            var repository = new RepositoryBase<Freelancer>(crowdContext);
            _serviceFreelancer = new ServiceFreelancer(repository);

            var freelancers = _serviceFreelancer.GetAll(f => String.IsNullOrEmpty(f.PortfolioOnLine) && f.Prospect.Value == false);

            foreach (var free in freelancers)
            {
                if (String.IsNullOrEmpty(free.PortfolioOnLine))
                    CreatePortfolioOnline(free);
            }
        }

        private static void CreatePortfolioOnline(Freelancer freelancer)
        {
            var portfolio = String.Join("/", Validador.RemoveSpecialCharacters(freelancer.Title), Validador.RemoveSpecialCharacters(freelancer.Name));

            var duplicado = _serviceFreelancer.GetByExpression(f => f.PortfolioOnLine == portfolio);

            if (duplicado != null)
                portfolio += String.Concat("_", Validador.GenerateCheckDigit(portfolio));

            freelancer.PortfolioOnLine = portfolio;

            _serviceFreelancer.Update(freelancer);
        }
    }
}
