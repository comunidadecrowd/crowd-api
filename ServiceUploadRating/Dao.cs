﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;

namespace SheetsQuickstart
{
    internal class Dao
    {
        private string _connectionString;

        public Dao()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["Connection"].ToString();
        }

        public int GetUserIdByEmail(string email)
        {
            IEnumerable<int> ids = new List<int>();
            using (var cn = new SqlConnection(_connectionString))
            {
                ids = cn.Query<int>(@"Select u.id from crowd_users u where email like @email", new { email });
            }

            if (!ids.Any())
                return 0;

            return ids.First();
        }

        internal int GetfreelancerbyEmailOrName(string email, string name)
        {
            IEnumerable<int> ids = new List<int>();
            using (var cn = new SqlConnection(_connectionString))
            {
                ids = cn.Query<int>(@"Select f.id from crowd_freelancer f where email = @email or name = @name", new { email, name });
            }

            if (!ids.Any())
                return 0;

            return ids.First();
        }

        internal void SaveRatingFreelancer(RatingFreelancer rating)
        {

            using (var cn = new SqlConnection(_connectionString))
            {
                var id = cn.Execute(@"
                           INSERT INTO [dbo].[crowd_freelancer_rating]
                           ([user_id]
                           ,[freelancer_id]
                           ,[responsability]
                           ,[agility]
                           ,[quality]
                           ,[comment]
                           ,[active]
                           ,[created_at]
                           ,[task_id])
                     VALUES
                            (@UserId, 
                            @FreelancerId,
                            @Responsability,
                            @Agility,
                            @Quality,
                            @Comment,
                            @Active,
                            @CreatedAt,
                            @TaskId
                            )", rating);
            }
        }
    }
}