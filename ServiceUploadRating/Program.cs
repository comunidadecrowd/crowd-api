﻿
using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;

namespace SheetsQuickstart
{
    class Program
    {
        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/sheets.googleapis.com-dotnet-quickstart.json
        static string[] Scopes = { SheetsService.Scope.Spreadsheets };
        static string ApplicationName = "Crowd Rating";

        static void Main(string[] args)
        {
            // downloaded json file with private key
            var credential = GoogleCredential.FromStream(new FileStream("Crowd-a539abde0718.json", FileMode.Open)).CreateScoped(Scopes);


            //UserCredential credential;

            //using (var stream =
            //    new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            //{
            //    string credPath = System.Environment.GetFolderPath(
            //        System.Environment.SpecialFolder.Personal);
            //    credPath = Path.Combine(credPath, ".credentials/sheets.googleapis.com-dotnet-quickstart.json");

            //    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
            //        GoogleClientSecrets.Load(stream).Secrets,
            //        Scopes,
            //        "user",
            //        CancellationToken.None,
            //        new FileDataStore(credPath, true)).Result;
            //    Console.WriteLine("Credential file saved to: " + credPath);
            //}

            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            // Define request parameters.
            String spreadsheetId = ConfigurationManager.AppSettings["IdPlanilha"].ToString();

            String range = "Freelancer!A2:L";

            var request = service.Spreadsheets.Values.Get(spreadsheetId, range);

            // Prints the names and majors of students in a sample spreadsheet:
            // https://docs.google.com/spreadsheets/d/1ERwjroKdEwCpD_vL7SW47z-UDQq775DTk0js2g9kxZc/edit
            var response = request.Execute();

            if (response.Values != null && response.Values.Count > 0)
            {
                var x = 2;
                List<ValueRange> data = new List<ValueRange>();
                foreach (var row in response.Values)
                {
                    if (row.Count < 12 || row[11].ToString() != "Ok")
                    {
                        var resposta = SaveFreelancer(row);

                        var saverange = String.Format("Freelancer!L{0}", x);

                        var oblist = new List<object>() { resposta };
                        var values = new List<IList<object>> { oblist };

                        data.Add(new ValueRange { Range = saverange, Values = values });
                    }
                    x++;
                }

                BatchUpdateValuesRequest body = new BatchUpdateValuesRequest { ValueInputOption = "USER_ENTERED", Data = data };
                var update = service.Spreadsheets.Values.BatchUpdate(body, spreadsheetId);
                BatchUpdateValuesResponse result = update.Execute();


            }
        }

        private static string SaveFreelancer(IList<object> row)
        {
            var retorno = new StringBuilder();
            var _dao = new Dao();

            var userid = _dao.GetUserIdByEmail(row[9].ToString());

            if (userid == 0)
                retorno.AppendLine("Usuário não encontrado");

            var freelancerid = _dao.GetfreelancerbyEmailOrName(row[3].ToString(), row[2].ToString());

            if (freelancerid == 0)
                retorno.AppendLine("Freelancer não encontrado");

            if (String.IsNullOrEmpty(row[4].ToString()))
                retorno.AppendLine("Nota de Qualidade não encontrada");

            if (String.IsNullOrEmpty(row[5].ToString()))
                retorno.AppendLine("Nota de Responsabilidade não encontrada");

            if (String.IsNullOrEmpty(row[6].ToString()))
                retorno.AppendLine("Nota de Agilidade não encontrada");

            if (retorno.Length == 0)
            {
                var rating = new RatingFreelancer
                {
                    Active = true,
                    UserId = userid,
                    FreelancerId = freelancerid,
                    Quality = Int32.Parse(row[4].ToString()),
                    Responsability = Int32.Parse(row[5].ToString()),
                    Agility = Int32.Parse(row[6].ToString()),
                    Comment = row.Count > 10 ? row[10].ToString() : String.Empty,
                    CreatedAt = DateTime.Now,
                    TaskId = String.IsNullOrEmpty(row[0].ToString()) ? 0 : Int32.Parse(row[0].ToString()) 
                };

                try
                { 

                _dao.SaveRatingFreelancer(rating);

                }
                catch (SqlException ex)
                {
                    if (ex.Number == 2601)
                        retorno.Append("Registro já existente no banco.");
                    else
                        retorno.Append(ex.Message);
                }

                if (retorno.Length == 0)
                    retorno.Append("Ok");
            };

            return retorno.ToString();
        }
    }
}