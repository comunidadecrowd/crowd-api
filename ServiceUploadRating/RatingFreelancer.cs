﻿using System;

namespace SheetsQuickstart
{
    public class RatingFreelancer
    {
        public int UserId { get; set; }
        public int FreelancerId { get; set; }
        public int Responsability { get; set; }
        public int Agility { get; set; }
        public int Quality { get; set; }
        public string Comment { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int TaskId { get; set; }
    }
}