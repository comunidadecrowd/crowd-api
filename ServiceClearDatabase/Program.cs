﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Infrasctructure.Database.Context;
using Backend.Crowd.Infrasctructure.Database.Repositories;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceClearDatabase
{
    public class Program
    {
        public static ServiceBase<Customer> _serviceCustomer { get; private set; }
        public static IServiceFreelancer _serviceFreelancer { get; private set; }
        public static Logger _serviceLog { get; private set; }

        static void Main(string[] args)
        {
            var crowdContext = new CrowdContext();
            var repositoryCustomer = new RepositoryBase<Customer>(crowdContext);
            var repositoryFreelancer = new RepositoryBase<Freelancer>(crowdContext);

            _serviceCustomer = new ServiceBase<Customer>(repositoryCustomer);
            _serviceFreelancer = new ServiceFreelancer(repositoryFreelancer);
            _serviceLog = new LoggerConfiguration()
            .ReadFrom.AppSettings()
            .CreateLogger();

            ClearCustomer();
            ClearFreelancer();

        }

        private static void ClearCustomer()
        {
            var customers = _serviceCustomer.GetAll(c => c.Active && c.Prospect.HasValue && c.Prospect.Value);
            
            foreach (var customer in customers)
            {
               
            }


        }

        private static void ClearFreelancer()
        {
            throw new NotImplementedException();
        }

       
    }
}
