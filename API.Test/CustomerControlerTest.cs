﻿using Algolia.API;
using API.Controllers;
using API.Models.Portfolio;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using Moq;
using NUnit.Framework;
using System.Web.Http;
using System;
using Backend.Crowd.Domain.Interfaces.Repositories;
using System.Net.Http;
using System.Collections.Generic;
using Intercom;
using Backend.Crowd.Domain.Util.Network;
using AutoMapper;
using API.Models.Customer;

namespace API.Test
{
    [TestFixture]
    public class CustomerControllerTest : BaseTest
    {
        private CustomerController controller;

        [SetUp]
        public void SetUp()
        {
            var serviceCustomer = new Mock<IServiceBase<Customer>>();
            var serviceFreelancer = new Mock<IServiceFreelancer>();

            serviceFreelancer.Setup(moq => moq.GetById(
                                                It.IsAny<long>(),
                                                It.IsAny<bool>()))
                                                .Returns(new Freelancer { Id = 1 });

            serviceFreelancer.Setup(moq => moq.GetByCode(It.Is<string>(c => c.Equals("1")))).Returns(new Freelancer());

            var serviceState = new Mock<IServiceBase<State>>();
            var serviceCity = new Mock<IServiceBase<City>>();
            var serviceCustomerTracking = new Mock<IServiceBase<Customer.CustomerTracking>>();
            var serviceProject = new Mock<IServiceBase<Project>>();
            var serviceActionLog = new Mock<IServiceBase<User.ActionLog>>();
            var serviceLog = new Mock<Serilog.ILogger>();

            controller = new CustomerController(
                serviceCustomer.Object,
                serviceFreelancer.Object,
                serviceState.Object,
                serviceCity.Object,
                serviceUser.Object,
                serviceCustomerTracking.Object,
                serviceProject.Object,
                serviceIntercom.Object,
                serviceActionLog.Object,
                serviceEmail.Object,
                serviceMap,
                serviceLog.Object
                );

            GenerateToken(controller,false);

        }

        [Test]
        [TestCase("Trade","User","55129941864","teste@teste.com",1,null,null,true)]
        [TestCase(null, "User", "55129941864", "teste@teste.com", 1, null, null, true)]
        public void TestPreCadastro
            (string trade,string name,string phone,string email,
            PlanType plantype,string referrer,string promocode,bool acceptterms)
        {
            var vm = new PreCadastroCustomerViewModel {
                Trade = trade,
                Name = name,
                Phone = phone,
                Email = email,
                PlanType = plantype,
                Referrer = referrer,
                PromoCode = promocode,
                AcceptTerms = acceptterms
            };

            var response = controller.PreCadastro(vm);

            if (trade == null)
            {
                Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.BadRequest);
            }
            else
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}
