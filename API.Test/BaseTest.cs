﻿using API.App_Start;
using API.Controllers;
using AutoMapper;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Repositories;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Util.Network;
using Intercom;
using Moq;
using System;
using System.Linq.Expressions;
using System.Web.Http;

namespace API.Test
{
    public class BaseTest
    {
        public IMapper serviceMap;
        public Mock<IServiceUser> serviceUser;
        public Mock<ICommunication> serviceIntercom;
        public Mock<IEmail> serviceEmail;

        public const string email = "teste@teste.com";
        
        public BaseTest()
        {
            ConfigMapper();

            ConfigIntercom();

            ConfigEmail();

            ConfigUser();
            
        }

        private void ConfigEmail()
        {
            serviceEmail = new Mock<IEmail>();
        }

        private void ConfigIntercom()
        {
            serviceIntercom = new Mock<ICommunication>();

            var contact = new Intercom.Data.Contact
            {
                name = "Teste"
            };

            serviceIntercom.Setup(
                u => u.SendNewLead(It.IsAny<Intercom.Data.Contact>()))
                    .Returns(contact);
        }

        private void ConfigMapper()
        {
            serviceMap = new MapperConfiguration(cfg =>
            {

                cfg.AddProfile(new MapProfile());

            }).CreateMapper();
        }

        private void ConfigUser()
        {
            serviceUser = new Mock<IServiceUser>();

            var user = new User
            {
                Id = 1,
                IdFreelancer = 1,
                Name = "Teste",
                Email = "teste@teste.com",
                Freelancer = new Freelancer
                {
                    Id = 1
                }
            };

            serviceUser.Setup(
               u => u.GetByExpression(
                                It.Is<Expression<Func<User, bool>>>(e => ExpressionCompare(e, "unique")),
                                It.IsAny<bool>()))
                                .Returns(user);

            serviceUser.Setup(u => u.Authentication(It.IsAny<string>())).Returns(user);

            serviceUser.Setup(u => u.Authentication(It.IsAny<string>(),It.IsAny<string>())).Returns(user);
        }

        private bool ExpressionCompare(Expression<Func<User, bool>> e, string expr)
        {
            return e.ToString().Contains(expr);
        }

        protected void GenerateToken(BaseAPIController controller, bool Customer)
        {
            var user = new User()
            {
                 Id = 1,
                 Email = "teste@teste.com"      
            };

            LoggedUser loggeduser;

            var token = BaseAPIController.CreateJwtToken(user, out loggeduser);

            controller.Configuration = new HttpConfiguration();
            controller.Request = new System.Net.Http.HttpRequestMessage();
            controller.Request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
        }
    }
}