﻿using Algolia.API;
using API.Controllers;
using API.Models.Portfolio;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using Moq;
using NUnit.Framework;
using System.Web.Http;
using System;
using Backend.Crowd.Domain.Interfaces.Repositories;
using System.Net.Http;
using System.Collections.Generic;

namespace API.Test
{
    [TestFixture]
    public class FreelancerControlerTest : BaseTest
    {
        private FreelancerController controller;

        [SetUp]
        protected void SetUp()
        {
            var serviceFreelancer = new Mock<IServiceFreelancer>();
            var serviceState = new Mock<IServiceBase<State>>();
            var serviceCity = new Mock<IServiceBase<City>>();
            var serviceRating = new Mock<IServiceBase<Freelancer.RatingFreelancer>>();
            var serviceTasks = new Mock<IServiceBase<Tasks>>();
            var searchservice = new Mock<ISearch>();

            serviceFreelancer.Setup(moq => moq.GetById(
                                                It.IsAny<long>(), 
                                                It.IsAny<bool>()))
                                                .Returns(new Freelancer { Id = 1 });

            serviceFreelancer.Setup(moq => moq.GetByCode(It.Is<string>(c => c.Equals("1")))).Returns(new Freelancer());

            var repositoryPortfolio = new Mock<IRepositoryBase<Freelancer.Portfolio>>();

            repositoryPortfolio.Setup(r => r.Add(It.IsAny<Freelancer.Portfolio>())).Callback((Freelancer.Portfolio p) => { p.Id = 1; } );

            repositoryPortfolio.Setup(r => r.GetById(It.Is<long>(c => c.Equals(1)),It.IsAny<bool>())).Returns(new Freelancer.Portfolio { Id = 1,  FreelancerId = 1, ClientName = "Teste", Description = "Teste", Media = "Teste",Type = FileType.IMAGE });

            var servicePortfolio = new ServiceBase<Freelancer.Portfolio>(repositoryPortfolio.Object);

            controller = new FreelancerController(
                serviceFreelancer.Object,
                serviceState.Object,
                serviceCity.Object,
                serviceRating.Object,
                serviceUser.Object,
                servicePortfolio,
                serviceTasks.Object,
                searchservice.Object
                );

            GenerateToken(controller,false);

        }

        

        [Test]
        [TestCase(FileType.VIDEO)]
        public void TestAddPortfolio(FileType type)
        {
            var vm = new PortfolioViewModel
            {
                Title = "Um Projeto Qualquer",
                Media = "youtube.com/?v=cdt7v32b82m",
                FreelancerId = 1,
                Type = type,
                ClientName = "Um Cliente Avulso",
                Description = "Lorem Ipsum dolor sit ameth lorem Ipsum dolor sit ameth",
                Thumb = "teste/teste"
            };

            var response = controller.AddPortfolio(vm);

            PortfolioGetViewModel portfolio;
            response.TryGetContentValue(out portfolio);

            Assert.IsInstanceOf<PortfolioGetViewModel>(portfolio);
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.AreEqual(portfolio.Id, 1);
        }

        [Test]
        public void TestListPortfolio()
        {
            var response = controller.ListPortfolio("1");

            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Test]
        public void TestGetPortfolio()
        {
            var response = controller.GetPortfolio(1);

            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Test]
        public void TestRemovePortfolio()
        {
            var response = controller.RemovePortfolio(1);

            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Test]
        public void TestOrderPortfolio()
        {
            var vm = new List<PortfolioOrderViewModel>();

            vm.Add(new PortfolioOrderViewModel {  Order = 0 , PortfolioId = 1 });

            var response = controller.OrderPortfolio(vm);

            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Test]
        [Ignore("In development")]
        public void TestAttachPortfolioFile()
        {
            var response = controller.AttachPortfolioFile();

            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}
