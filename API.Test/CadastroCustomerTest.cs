﻿using Algolia.API;
using API.App_Start;
using API.Controllers;
using API.Models;
using API.Models.Customer;
using API.Models.Login;
using AutoMapper;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Util.Network;
using Intercom;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Test
{
    [TestFixture]
    public class CadastroCustomerTest 
    {
        private CustomerController controller;

        private UserController userController;

        private List<Customer> ListCustomer;

        private List<User> ListUser;

        private List<Intercom.Data.Contact> ListContact;

        private List<Intercom.Data.Company> ListCompany;

        [SetUp]
        protected void SetUp()
        {
            ListCustomer = new List<Customer>();
            ListUser = new List<User>();
            ListContact = new List<Intercom.Data.Contact>();
            ListCompany = new List<Intercom.Data.Company>();

            var serviceCustomer = new Mock<IServiceBase<Customer>>();

            var serviceFreelancer = new Mock<IServiceFreelancer>();

            var serviceUser = new Mock<IServiceUser>();
            var serviceIntercom = new Mock<ICommunication>();
            var serviceEmail = new Mock<IEmail>();
            var serviceMap = new MapperConfiguration(cfg =>
            {

                cfg.AddProfile(new MapProfile());

            }).CreateMapper();

            serviceCustomer.Setup(moq => moq.Add(It.IsAny<Customer>())).Callback<Customer>(Customersave);

            serviceUser.Setup(moq => moq.Add(It.IsAny<User>())).Callback<User>(Usersave);

            serviceUser.Setup(moq => moq.Authentication(It.IsAny<string>())).Returns<string>(UserAuthentication);

            serviceUser.Setup(moq => moq.GetByExpression(
                                            It.IsAny<Expression<Func<User, bool>>>(),It.IsAny<bool>()
                                            ))
                                            .Returns<User>(UserbyExpression);

            serviceIntercom.Setup(moq => moq.SendCompany(It.IsAny<Intercom.Data.Company>())).Returns<Intercom.Data.Company>(IntercomCompanysave);

            serviceIntercom.Setup(moq => moq.SendNewLead(It.IsAny<Intercom.Data.Contact>())).Returns<Intercom.Data.Contact>(IntercomContactsave);

            serviceFreelancer.Setup(moq => moq.GetByExpression(
                                                It.Is<Expression<Func<Freelancer, bool>>>(e => e.ToString().Contains("email")),
                                                false)).Returns(new Freelancer { Id = 1 });

            serviceFreelancer.Setup(moq => moq.GetByCode(It.Is<string>(c => c.Equals("1")))).Returns(new Freelancer());

            var serviceState = new Mock<IServiceBase<State>>();
            var serviceCity = new Mock<IServiceBase<City>>();
            var serviceCustomerTracking = new Mock<IServiceBase<Customer.CustomerTracking>>();
            var serviceProject = new Mock<IServiceBase<Project>>();
            var serviceActionLog = new Mock<IServiceBase<User.ActionLog>>();
            var serviceLog = new Mock<Serilog.ILogger>();

            controller = new CustomerController(
                serviceCustomer.Object,
                serviceFreelancer.Object,
                serviceState.Object,
                serviceCity.Object,
                serviceUser.Object,
                serviceCustomerTracking.Object,
                serviceProject.Object,
                serviceIntercom.Object,
                serviceActionLog.Object,
                serviceEmail.Object,
                serviceMap,
                serviceLog.Object
                );

            var serviceSkill = new Mock<IServiceBase<Freelancer.Skill>>();
            var serviceSegment = new Mock<IServiceBase<Freelancer.Segment>>();
            var serviceCategory = new Mock<IServiceBase<Freelancer.Category>>();
            var serviceFreelancerExperience = new Mock<IServiceBase<Freelancer.Experience>>();
            var serviceRating = new Mock<IServiceBase<Freelancer.RatingFreelancer>>();
            var serviceFreelancerAwards = new Mock<IServiceBase<Freelancer.Award>>();
            var serviceLanguage = new Mock<IServiceBase<Freelancer.Language>>();
            var serviceTasks = new Mock<IServiceBase<Tasks>>();
            var servicePortfolio = new Mock<IServiceBase<Freelancer.Portfolio>>();
            var searchservice = new Mock<ISearch>();

            userController = new UserController(
                serviceUser.Object,
                serviceFreelancer.Object,
                serviceSkill.Object,
                serviceSegment.Object,
                serviceCity.Object,
                serviceState.Object,
                serviceCategory.Object,
                serviceFreelancerExperience.Object,
                serviceRating.Object,
                serviceFreelancerAwards.Object,
                serviceCustomer.Object,
                serviceLanguage.Object,
                serviceActionLog.Object,
                serviceTasks.Object,
                servicePortfolio.Object,
                searchservice.Object,
                serviceIntercom.Object,
                serviceEmail.Object,
                serviceMap,
                serviceLog.Object
                );

            controller.Configuration = new HttpConfiguration();
            controller.Request = new HttpRequestMessage();
        }

        private User UserbyExpression(User arg)
        {
            return ListUser.First();
        }

        private User UserAuthentication(string email)
        {
            User user = ListUser.Where(e => e.Email == email).FirstOrDefault();

            user.Customer = ListCustomer.Where(c => c.Id == user.IdCustomer).FirstOrDefault();

            return user;
        }

        private Intercom.Data.Company IntercomCompanysave(Intercom.Data.Company company)
        {
            ListCompany.Add(company);

            return company;
        }

        private Intercom.Data.Contact IntercomContactsave(Intercom.Data.Contact contact)
        {
            ListContact.Add(contact);

            return contact;
        }

        private void Usersave(User user)
        {
            ListUser.Add(user);
        }

        private void Customersave(Customer customer)
        {
            customer.Id = 1;
            ListCustomer.Add(customer);
        }

        [Test]
        [TestCase("Trade", "User", "55129941864", "teste@teste.com", 1, null, null, true)]
        public void CaminhoFelizTest(string trade,string name,string phone,string email,int plantype,
            string referrer, string promocode,bool acceptterms)
        {
            var vm = new PreCadastroCustomerViewModel
            {
                Trade = trade,
                Name = name,
                Phone = phone,
                Email = email,
                PlanType = (PlanType)plantype,
                Referrer = referrer,
                PromoCode = promocode,
                AcceptTerms = acceptterms
            };

            var response = controller.PreCadastro(vm);

            LoginResponseViewModel login;
            response.TryGetContentValue(out login);

            Assert.IsInstanceOf<LoginResponseViewModel>(login);

            Assert.IsTrue(response.IsSuccessStatusCode);

            Assert.AreEqual(true, login.loggedUser.Prospect);

            Assert.IsNotEmpty(ListCustomer);

            Assert.IsNotEmpty(ListUser);

            Assert.IsNotEmpty(ListCompany);

            Assert.IsNotEmpty(ListContact);

            var customer = ListCustomer.First();

            Assert.IsFalse(customer.Active);

            Assert.IsNotEmpty(customer.EmailResponsible);

            Assert.IsNotEmpty(customer.NameResponsible);

            Assert.IsNotEmpty(customer.Phone);

            Assert.IsNotEmpty(customer.Trade);

            Assert.Greater((int)customer.PlanType, 0);

            var loginvm = new LoginViewModel
            {
                Email = vm.Email
            };

            var resposta = userController.Login(loginvm);



        }
    }
}
