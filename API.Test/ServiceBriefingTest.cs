﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Repositories;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Test
{
    [TestFixture]
    public class ServiceBriefingTest
    {
        IServiceBriefing _serviceBriefing;

        public ServiceBriefingTest()
        {
            var repository = new Mock<IRepositoryBriefing>();
            _serviceBriefing = new ServiceBriefing(repository.Object);
        }

        [Test]
        [TestCase("11 983335995", "11 < strong style = \"background: grey;color: wheat;\" >< i class=\" fa-exclamation-triangle\"></i> Conteúdo removido por violar termos de uso da CROWD</strong>")]
        [TestCase("11 98333 5995", "11 < strong style = \"background: grey;color: wheat;\" >< i class=\" fa-exclamation-triangle\"></i> Conteúdo removido por violar termos de uso da CROWD</strong>")]
        [TestCase("<p>(11) 983335995", "<p>(11) < strong style = \"background: grey;color: wheat;\" >< i class=\" fa-exclamation-triangle\"></i> Conteúdo removido por violar termos de uso da CROWD</strong>")]
        [TestCase("(11) 983335995", "(11) < strong style = \"background: grey;color: wheat;\" >< i class=\" fa-exclamation-triangle\"></i> Conteúdo removido por violar termos de uso da CROWD</strong>")]
        [TestCase("(11)983335995", "(11)< strong style = \"background: grey;color: wheat;\" >< i class=\" fa-exclamation-triangle\"></i> Conteúdo removido por violar termos de uso da CROWD</strong>")]
        [TestCase("11 98333-5995", "11 < strong style = \"background: grey;color: wheat;\" >< i class=\" fa-exclamation-triangle\"></i> Conteúdo removido por violar termos de uso da CROWD</strong>")]
        [TestCase("<html>(11) 983335995", "<html>(11) < strong style = \"background: grey;color: wheat;\" >< i class=\" fa-exclamation-triangle\"></i> Conteúdo removido por violar termos de uso da CROWD</strong>")]
        [TestCase("luciano@crowd.br.com", "< strong style = \"background: grey;color: wheat;\" >< i class=\" fa-exclamation-triangle\"></i> Conteúdo removido por violar termos de uso da CROWD</strong>")]
        public void TestMethod(string text,string responsetext)
        {
            var briefing = new Briefing { Text = text };

            _serviceBriefing.ClearTextBriefing(briefing);

            Assert.AreEqual(responsetext,briefing.Text);
        }
    }
}
