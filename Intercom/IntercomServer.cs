﻿using Intercom.Clients;
using Intercom.Core;
using Intercom.Data;
using Intercom.Exceptions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intercom
{
    public class IntercomServer : ICommunication
    {
        const string sSource = "CrowdService";
        const string sLog = "Application";

        private readonly Authentication _authentication;

        public IntercomServer(string accessToken)
        {
            _authentication = new Authentication(accessToken);
        }

        public User UpdateUser(User user)
        {
            UsersClient usersClient = new UsersClient(_authentication);
            
            user = usersClient.Update(user);
           
            return user;
        }

       

        public void SendTrialEmail(int userId, DateTime dateTrial, bool trial)
        {
            // Create UsersClient instance
            UsersClient usersClient = new UsersClient(_authentication);
           
            User user = usersClient.View(new User() { user_id = userId.ToString() });

            //user.companies = new List<Company>(); //{ new Company() { company_id = "new_company" } };
            user.custom_attributes["trial"] = trial;
            user.custom_attributes["trial-expire"] = dateTrial;

            user = usersClient.Update(user);

        }

        public void DeleteLead(string id)
        {
            ContactsClient usersClient = new ContactsClient(_authentication);

            usersClient.Delete(id);
        }

        public Users ListUsers()
        {
            UsersClient usersClient = new UsersClient(_authentication);

            return usersClient.List();
        }

        public Contacts ListLeads()
        {
            ContactsClient usersClient = new ContactsClient(_authentication);

            return usersClient.List();
        }

        public Companies ListCompanies()
        {
            CompanyClient usersClient = new CompanyClient(_authentication);

            return usersClient.List();
        }

        public Contacts ListLeads(Dictionary<String, String> parameters)
        {
            ContactsClient usersClient = new ContactsClient(_authentication);

            return usersClient.List(parameters);
        }

        public Users ListUsers(Dictionary<String, String> parameters)
        {
            UsersClient usersClient = new UsersClient(_authentication);

            return usersClient.List(parameters);
        }

        public Companies ListCompanies(Dictionary<String, String> parameters)
        {
            CompanyClient usersClient = new CompanyClient(_authentication);

            return usersClient.List(parameters);
        }

        public void SendEvent(int userId, string eventName)
        {
            // Create UsersClient instance
            EventsClient eventClient = new EventsClient(_authentication);
            
            Event evento = new Event
            {
                created_at = DateTimeOffset.Now.ToUnixTimeSeconds(),
                user_id = userId.ToString(),
                event_name = eventName
            };

            eventClient.Create(evento);           
        }

        

        public Contact SendNewLead(Data.Contact contact)
        {
            ContactsClient contactsClient = new ContactsClient(_authentication);
 
            var contacts = contactsClient.List(contact.email).contacts;

            if (contacts.Count() > 0)
            {
                contact.id = contacts.First().id;
                contact = contactsClient.Update(contact);
            }
            else
                contact = contactsClient.Create(contact);

            return contact;
        }

        public void DeleteUser(string id)
        {
            UsersClient client = new UsersClient(_authentication);

            client.Delete(id);
        }

        public void DeleteUser(User user)
        {
            UsersClient client = new UsersClient(_authentication);

            client.Delete(user);
        }

        public Company SendCompany(Company company)
        {
            CompanyClient client = new CompanyClient(_authentication);
            Company result;
            try
            {
                result = client.View(company.id);
                result = client.Update(company);
            }
            catch
            {
                result = client.Create(company);
            }
           
            return result;
        }

        public void StartConversation(AdminConversationMessage message)
        {
            message.type = "email";
            AdminConversationsClient client = new AdminConversationsClient(_authentication);
            
            client.Create(message);
            
        }

        public User GetUserById(string id)
        {
            User user = new User();
            // Create UsersClient instance
            UsersClient usersClient = new UsersClient(_authentication);

            try
            {
                user = usersClient.View(id);
            }
            catch
            {
                return null;
            }

            return user;
        }

       

        public User GetUser(string email)
        {
            User user = new User();
            // Create UsersClient instance
            UsersClient usersClient = new UsersClient(_authentication);

            try
            {
                user = usersClient.View(new User() { email = email });
            }
            catch
            {
                return null;
            }

            return user;
        }

        public User GetUser(int userId)
        {

            User user = new User();
            // Create UsersClient instance
            UsersClient usersClient = new UsersClient(_authentication);

            try
            {
                user = usersClient.View(new User() { user_id = userId.ToString() });
            }
            catch
            {
                return null;
            }

            return user;
        }

        public Admin GetAdmin(string email)
        {
            Admin admin = new Admin();
            // Create UsersClient instance
            AdminsClient usersClient = new AdminsClient(_authentication);
           
            admin = usersClient.List().admins.First(a => a.email == email);

            return admin;
        }

        public void UpdateCompany(Company company)
        {
            CompanyClient usersClient = new CompanyClient(_authentication);

            usersClient.Update(company);
        }

        public Company GetCompany(int companyId)
        {
            Company user = new Company();
            // Create UsersClient instance
            CompanyClient usersClient = new CompanyClient(_authentication);

            try
            {
                user = usersClient.View(new Company() { company_id = companyId.ToString() });
            }
            catch (ApiException ex)
            {
                return null;
            }
            
            return user;
        }

        public User SendUser(User user)
        {
            UsersClient client = new UsersClient(_authentication);
            try
            {
                user = client.View(new User() { email = user.email });
            }
            catch
            {
                user = client.Create(user);
            }

            return user;
        }

        public void ChangeLeadtoUser(User user)
        {
            ContactsClient contactsClient = new ContactsClient(_authentication);
            UsersClient usersclient = new UsersClient(_authentication);

            var contacts = contactsClient.List(user.email).contacts;

            if (contacts.Any())
            {
                var contact = contacts.First();
                var newuser = this.GetUser(user.email);
                if (newuser == null)
                    newuser = user;
                   
                contactsClient.Convert(contact,newuser);
               
            }    
        }

        public Contact GetLead(string email)
        {
            ContactsClient contactsClient = new ContactsClient(_authentication);

            var contacts = contactsClient.List(email).contacts;

            if (contacts.Count() > 0)
                return contacts.First();

            return null;
        }
    }
}
