﻿using Intercom.Data;
using System;

namespace Intercom
{
    public interface ICommunication
    {
        void SendTrialEmail(int userId, DateTime dateTrial, bool trial);
        Contact SendNewLead(Contact contact);
        Company SendCompany(Company company);
        void StartConversation(AdminConversationMessage message);
        User GetUser(string email);
        User GetUser(int userId);
        Admin GetAdmin(string email);
        Company GetCompany(int companyId);
        User SendUser(User user);
        void ChangeLeadtoUser(User user);
        Contact GetLead(string email);
        User UpdateUser(User user);
        void UpdateCompany(Company company);
    }
}