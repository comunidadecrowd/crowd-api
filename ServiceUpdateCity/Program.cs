﻿using Newtonsoft.Json;
using ServiceUpdateCity.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceUpdateCity
{
    public static class Extension
    {
        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property)
        {
            return items.GroupBy(property).Select(x => x.First());
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var dao = new Dao();
            List<Ibge> cidades = new List<Ibge>();
            using (StreamReader r = new StreamReader(@"text\new.json"))
            {
                string json = r.ReadToEnd();
                cidades = JsonConvert.DeserializeObject<List<Ibge>>(json);
            }

            var estados = cidades.DistinctBy(c => c.iduf).Select(u => new UF { id = u.iduf, sigla = u.uf });

            estados.ToList().ForEach(dao.SaveState);

            cidades.ToList().ForEach(dao.SaveCity);


           

    }
    }
}
