﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceUpdateCity.Model
{
    public class Ibge
    {
        public int id { get; set; }
        public string nome { get; set; }
        public int iduf { get; set; }
        public string uf { get; set; }
    }
}
