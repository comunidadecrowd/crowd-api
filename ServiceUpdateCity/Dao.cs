﻿using Dapper;
using ServiceUpdateCity.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;

namespace ServiceUpdateCity
{
    internal class Dao
    {
        private string _connectionString;

        public Dao()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["Connection"].ToString();
        }

        internal void SaveState(UF uf)
        {

            using (var cn = new SqlConnection(_connectionString))
            {
                var id = cn.Execute(@"
                           INSERT INTO [dbo].[crowd_state_new]
                           ([id]
                           ,[uf]
                           )
                     VALUES
                            (@Id, 
                            @Uf
                            )", new { Id = uf.id, Uf = uf.sigla });
            }
        }

        internal void SaveCity(Ibge city)
        {

            using (var cn = new SqlConnection(_connectionString))
            {
                var id = cn.Execute(@"
                           INSERT INTO [dbo].[crowd_city_new]
           ([id]
           ,[name]
           ,[id_state])
     VALUES
           (@Id
           ,@Name
           ,@Stateid)", new { Id = city.id, Name = city.nome, Stateid = city.iduf });
            }
        }
    }
}