﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Infrasctructure.Database.Context;
using Backend.Crowd.Infrasctructure.Database.Repositories;
using Google.Cloud.Language.V1;
using Google.Cloud.Vision.V1;
using Google.Protobuf.Collections;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Backend.Crowd.Domain.Entities.WebInformation;

namespace ServiceGoogleApi
{
    class Program
    {
        private static ServiceFreelancer _serviceFreelancer;
        private static ServiceBase<GoogleEntity> _serviceEntity;
        private static ServiceBase<Backend.Crowd.Domain.Entities.Sentence> _serviceSentence;
        private static ServiceBase<Face> _serviceFace;
        private static ServiceBase<Label> _serviceLabel;
        private static ServiceBase<SafeSearch> _serviceSafeSearch;
        private static ServiceBase<Backend.Crowd.Domain.Entities.Sentiment> _serviceSentiment;
        private static ServiceBase<WebInformation> _serviceWebInformation;
        private static LanguageServiceClient _clientLanguage;
        private static ImageAnnotatorClient _clientVision;

        static void Main(string[] args)
        {
            var freelancerId = 1;

            if (args.Length > 0 && Int32.TryParse(args[0], out freelancerId))
                freelancerId = Int32.Parse(args[0]);
                
            Authenticate();

            var crowdContext = new CrowdContext();
            var repository = new RepositoryBase<Freelancer>(crowdContext);
            var repositoryEntity = new RepositoryBase<GoogleEntity>(crowdContext);
            var repositorySentence = new RepositoryBase<Backend.Crowd.Domain.Entities.Sentence>(crowdContext);
            var repositoryFace = new RepositoryBase<Face>(crowdContext);
            var repositoryLabel = new RepositoryBase<Label>(crowdContext);
            var repositorySentiment = new RepositoryBase<Backend.Crowd.Domain.Entities.Sentiment>(crowdContext);
            var repositorySafeSearch = new RepositoryBase<SafeSearch>(crowdContext);
            var repositoryWebInformation = new RepositoryBase<WebInformation>(crowdContext);

            _serviceFreelancer = new ServiceFreelancer(repository);
            _serviceEntity = new ServiceBase<GoogleEntity>(repositoryEntity);
            _serviceSentence = new ServiceBase<Backend.Crowd.Domain.Entities.Sentence>(repositorySentence);
            _serviceFace = new ServiceBase<Face>(repositoryFace);
            _serviceLabel = new ServiceBase<Label>(repositoryLabel);
            _serviceSafeSearch = new ServiceBase<SafeSearch>(repositorySafeSearch);
            _serviceSentiment = new ServiceBase<Backend.Crowd.Domain.Entities.Sentiment>(repositorySentiment);
            _serviceWebInformation = new ServiceBase<WebInformation>(repositoryWebInformation);
            _clientLanguage = LanguageServiceClient.Create();
            _clientVision = ImageAnnotatorClient.Create();

            var freelancers = _serviceFreelancer.GetAll(f=> f.Prospect == false && f.Id > freelancerId && f.Status == StatusUserType.Active && !f.SentimentId.HasValue).OrderBy(f=> f.Id).Where(f=> !String.IsNullOrEmpty(f.Description));

            foreach (var freela in freelancers)
            {
                if (freela.Entities.Any() || freela.Sentiment != null
                   || freela.SafeSearch != null || freela.Faces.Any() || freela.Labels.Any()
                   || freela.WebInformation != null)

                    DeleteFreela(freela);


                Console.WriteLine($"Freelancer : {freela.Id} - {freela.Name}");
                try
                {
                    AnalyzeSentiment(freela);
                    AnalyzeEntity(freela);
                }
                catch (RpcException ex)
                {
                    Console.WriteLine($"Freelancer : {freela.Id} - {freela.Name} - InvalidLanguage");
                }
                //AnalyzeVision(freela);

                _serviceFreelancer.Update(freela);
            }
        }

        private static void AnalyzeVision(Freelancer freela)
        {
            if (freela.Users.Any() && !String.IsNullOrEmpty(freela.Users.First().Photo))
            {
                var path = new Uri(String.Concat(ConfigurationManager.AppSettings["WebUrl"], freela.Users.First().Photo));
                var image = Image.FetchFromUri(path);
                AnalizeFaces(freela, image);
                AnalizeLabels(freela, image);
                AnalizeSafeSearch(freela, image);
                AnalizeWebInformation(freela, image);
            };
        }

        private static void DeleteFreela(Freelancer freela)
        {
            freela.Entities.ToList().ForEach(ent =>
            {
                freela.Entities.Remove(ent);
                _serviceEntity.Remove(ent);
            });

            if (freela.Sentiment != null)
                freela.Sentiment.Sentences.ToList().ForEach(ent =>
                {
                    freela.Sentiment.Sentences.Remove(ent);
                    _serviceSentence.Remove(ent);
                });

            freela.Faces.ToList().ForEach(ent =>
            {
                freela.Faces.Remove(ent);
                _serviceFace.Remove(ent);
            });
            freela.Labels.ToList().ForEach(ent =>
            {
                freela.Labels.Remove(ent);
                _serviceLabel.Remove(ent);
            });

            if (freela.WebInformation != null)
                  _serviceWebInformation.Remove(freela.WebInformation);
            freela.WebInformation = null;
            if (freela.SafeSearch != null)
                _serviceSafeSearch.Remove(freela.SafeSearch);
            freela.SafeSearch = null;
            if (freela.Sentiment != null)
                _serviceSentiment.Remove(freela.Sentiment);
            freela.Sentiment = null;


            _serviceFreelancer.Update(freela);

        }

        private static void AnalizeFaces(Freelancer freela,Image image)
        {
            var response = _clientVision.DetectFaces(image);

            var retorno = new List<Face>();
            response.ToList().ForEach(f => {
                var face = new Face
                {
                    DetectionConfidence = Convert.ToDecimal(f.DetectionConfidence),
                    AngerLikelihood = (Backend.Crowd.Domain.Entities.Enum.Likelihood)f.AngerLikelihood,
                    BlurredLikelihood = (Backend.Crowd.Domain.Entities.Enum.Likelihood)f.BlurredLikelihood,
                    HeadwearLikelihood = (Backend.Crowd.Domain.Entities.Enum.Likelihood)f.HeadwearLikelihood,
                    JoyLikelihood = (Backend.Crowd.Domain.Entities.Enum.Likelihood)f.JoyLikelihood,
                    SorrowLikelihood = (Backend.Crowd.Domain.Entities.Enum.Likelihood)f.SorrowLikelihood,
                    SurpriseLikelihood = (Backend.Crowd.Domain.Entities.Enum.Likelihood)f.SurpriseLikelihood,
                    UnderExposedLikelihood = (Backend.Crowd.Domain.Entities.Enum.Likelihood)f.UnderExposedLikelihood,
                    FreelancerId = freela.Id
                };
      
                retorno.Add(face);
            });

            freela.Faces = retorno;
        }

        private static void AnalizeLabels(Freelancer freela, Image image)
        {
            var response = _clientVision.DetectLabels(image);

            var retorno = new List<Label>();
            response.ToList().ForEach(f => {
                var label = new Label
                {
                   Score = Convert.ToDecimal(f.Score),
                   Description = f.Description,
                   FreelancerId = freela.Id
                };

                retorno.Add(label);
            });

            freela.Labels = retorno;
        }

        private static void AnalizeSafeSearch(Freelancer freela, Image image)
        {
            var response = _clientVision.DetectSafeSearch(image);

            var retorno = new SafeSearch
            {
                Adult = (Backend.Crowd.Domain.Entities.Enum.Likelihood)response.Adult,
                Medical = (Backend.Crowd.Domain.Entities.Enum.Likelihood)response.Medical,
                Spoof = (Backend.Crowd.Domain.Entities.Enum.Likelihood)response.Spoof,
                Violence = (Backend.Crowd.Domain.Entities.Enum.Likelihood)response.Violence,
                FreelancerId = freela.Id
            };
              
            freela.SafeSearch = retorno;
        }

        private static void AnalizeWebInformation(Freelancer freela, Image image)
        {
            var response = _clientVision.DetectWebInformation(image);

            var retorno = new WebInformation
            {
                FreelancerId = freela.Id
            };

            retorno.WebEntities = new List<WebEntity>();

            response.WebEntities.ToList().ForEach(f =>
                retorno.WebEntities.Add(new WebEntity
                {
                    Score = Convert.ToDecimal(f.Score),
                    Description = f.Description
                }));

            var full = new List<WebFull>();

            response.FullMatchingImages.ToList().ForEach(f =>
                full.Add(new WebFull
                {
                    Score = Convert.ToDecimal(f.Score),
                    Url = f.Url
                }));

            retorno.FullMatchingImages = full;

            var parcial = new List<WebPartial>();

            response.PartialMatchingImages.ToList().ForEach(f =>
                parcial.Add(new WebPartial
                {
                    Score = Convert.ToDecimal(f.Score),
                    Url = f.Url
                }));

            retorno.PartialMatchingImages = parcial;

            var pages = new List<WebPage>();

            response.PagesWithMatchingImages.ToList().ForEach(f =>
                pages.Add(new WebPage
                {
                    Score = Convert.ToDecimal(f.Score),
                    Url = f.Url
                }));

            retorno.PagesWithMatchingImages = pages;

            freela.WebInformation = retorno;
        }

        private static void AnalyzeEntity(Freelancer freela)
        {
            Document document = Document.FromPlainText(freela.Description);
            AnalyzeEntitiesResponse responseentities = _clientLanguage.AnalyzeEntities(document);

            var retorno = new List<GoogleEntity>();
            responseentities.Entities.ToList().ForEach(e => {
                var entity = new GoogleEntity
                {
                    Name = e.Name,
                    Type = (GoogleEntityType)e.Type,
                    Salience = Convert.ToDecimal(e.Salience),
                    FreelancerId = freela.Id
                };
                _serviceEntity.Add(entity);
                retorno.Add(entity);
            });

            freela.Entities = retorno;
        }

        private static void AnalyzeSentiment(Freelancer freela)
        {
            Document document = Document.FromPlainText(freela.Description);
            AnalyzeSentimentResponse response = _clientLanguage.AnalyzeSentiment(document);
            
            var retorno = new Backend.Crowd.Domain.Entities.Sentiment
            {
                FreelancerId = freela.Id,
                Freelancer = freela,
                Language = response.Language,
                Magnitude = Convert.ToDecimal(response.DocumentSentiment.Magnitude),
                Score = Convert.ToDecimal(response.DocumentSentiment.Score),
                Sentences = CreateSentences(response.Sentences)
            };

            freela.Sentiment = retorno;
        }

        private static List<Backend.Crowd.Domain.Entities.Sentence> CreateSentences(RepeatedField<Google.Cloud.Language.V1.Sentence> sentences)
        {
            var retorno = new List<Backend.Crowd.Domain.Entities.Sentence>();

            foreach (var sentence in sentences)
            {
                var item = new Backend.Crowd.Domain.Entities.Sentence {
                    Content = sentence.Text.Content,
                    BeginOffset = sentence.Text.BeginOffset,
                    Magnitude = Convert.ToDecimal(sentence.Sentiment.Magnitude),
                    Score = Convert.ToDecimal(sentence.Sentiment.Score)
                };

                retorno.Add(item);
            }

            return retorno;
        }

        private static void Authenticate()
        {
            var name = "GOOGLE_APPLICATION_CREDENTIALS";
            var value = @"crowd-eb68ce264f39.json";

            System.Environment.SetEnvironmentVariable(name, value);
        }
    }
}
