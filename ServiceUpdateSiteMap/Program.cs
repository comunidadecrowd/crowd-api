﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Domain.Util.Sitemap;
using Backend.Crowd.Infrasctructure.Database.Context;
using Backend.Crowd.Infrasctructure.Database.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ServiceUpdateSiteMap
{
    class Program
    {
        private static IServiceFreelancer _serviceFreelancer;

        static void Main(string[] args)
        {
            var crowdContext = new CrowdContext();
            var repository = new RepositoryBase<Freelancer>(crowdContext);
            _serviceFreelancer = new ServiceFreelancer(repository);


            var fs = new StreamWriter(ConfigurationManager.AppSettings["File"], false);

            XmlSerializer xs = new XmlSerializer(typeof(Sitemap));

            var sitemap = new Sitemap();

            sitemap.Locations = GetFreelancersLocations();

            xs.Serialize(fs, sitemap);
        }

        private static List<Location> GetFreelancersLocations()
        {
            return _serviceFreelancer.GetAll(f => f.Status == Backend.Crowd.Domain.Entities.Enum.StatusUserType.Active).Select(f => new Location
            {
                Url = String.Concat(ConfigurationManager.AppSettings["Server"], f.PortfolioOnLine, "/Full"),
                LastModified = DateTime.UtcNow.AddDays(-1)
            }).ToList();
        }
    }
}
