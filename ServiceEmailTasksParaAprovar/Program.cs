﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Domain.Util.Network;
using Backend.Crowd.Infrasctructure.Database.Context;
using Backend.Crowd.Infrasctructure.Database.Repositories;
using Intercom;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceEmailTasksParaAprovar
{
    class Program
    {
        private static ServiceBase<Tasks> _serviceTask;
        private static IntercomServer _serviceIntercom;
        private static Email _email;

        static void Main(string[] args)
        {
            var crowdContext = new CrowdContext();
            var repository = new RepositoryBase<Tasks>(crowdContext);
            _serviceTask = new ServiceBase<Tasks>(repository);
            _serviceIntercom = new IntercomServer(ConfigurationManager.AppSettings["IntercomToken"]);
           

            var tasks = _serviceTask.GetAll(t => t.Active && t.Status == Backend.Crowd.Domain.Entities.Enum.TaskStatus.WAITING && t.User.Customer.Id > 1).ToList();

            tasks.GroupBy(t => t.IdUser,t => t, (key, p) => new { Id = key, p.First().User, Tasks = String.Join(",",p.Select(z=> z.Title)) }).ToList().ForEach(SendEmail);
        }

        private static void SendEmail(dynamic users)
        {
            var datafechamento = DateTime.Now.Day < 15 ? 15 : 30;
            var datarecebimento = datafechamento == 15 ? "final do mês" : "próximo dia 15";
            var variables = new Dictionary<string, string>
            {   { "NAME", users.User.Name },
                {"DATA_FECHAMENTO", datafechamento.ToString()  },
                {"DATA_RECEBIMENTO", datarecebimento  },
                {"TASK_NAME", users.Tasks }
            };
            ITemplate email = new TemplateEmail();
            _email = new Email(_serviceIntercom, email);

            try
            {
                _email.NewSend(users.Id.ToString(), ConfigurationManager.AppSettings["Sender"], "user", "Não se esqueça de aprovar as suas tarefas pendentes", "TaskAprovada", variables);
            }
            catch
            {

            }
        }
    }
}
