﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceUpdateIndex
{
    class Program
    {
        static void Main(string[] args)
        {
            var apiaddress = ConfigurationManager.AppSettings["CrowdAPI"].ToString();
            var client = new RestClient(apiaddress);

            var request = new RestRequest("Professional/UpdateIndex", Method.POST);

            IRestResponse response = client.Execute(request);



            EventLog.WriteEntry("ServiceUpdateIndex", "Update Index Executado", EventLogEntryType.Information);

        }
    }
}
