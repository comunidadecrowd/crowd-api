﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Urlshortener.v1;
using Google.Apis.Urlshortener.v1.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleApi
{
    public class UrlShortener
    {
        private readonly UrlshortenerService _service;

        public UrlShortener(string baseUrl)
        {
            var credential = GoogleCredential.FromStream(new FileStream(baseUrl, FileMode.Open)).CreateScoped(UrlshortenerService.Scope.Urlshortener);
            _service = new UrlshortenerService(new BaseClientService.Initializer
            {
                ApplicationName = "Crowd",
                HttpClientInitializer = credential
            });
        }

        public string Insert(string urlToShorten)
        {
            Url response = _service.Url.Insert(new Url { LongUrl = urlToShorten }).Execute();

            return response.Id;
        }

    }
}
