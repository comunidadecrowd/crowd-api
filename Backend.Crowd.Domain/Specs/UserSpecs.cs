﻿using Backend.Crowd.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace Backend.Crowd.Domain.Specs
{
    public static class UserSpecs
    {
        public static Expression<Func<User, bool>> GetByEmail(string email)
        {
            return x => x.Email == email;
        }

        public static Expression<Func<User, bool>> GetByPhone(string phone)
        {
            return x => x.Phone == phone;
        }

        public static Expression<Func<User, bool>> GetByEmailOrPhone(string email, string phone)
        {
            return x => x.Phone == phone || x.Email == email;
        }
    }
}
