﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Repositories;
using Backend.Crowd.Domain.Interfaces.Services;

namespace Backend.Crowd.Domain.Services
{
    public class ServiceBase<TEntity> : IServiceBase<TEntity> where TEntity : BaseEntity
    {
        internal IRepositoryBase<TEntity> _repository { get; set; }

        public ServiceBase(IRepositoryBase<TEntity> repository)
        {
            _repository = repository;
        }

        public virtual void Add(TEntity obj)
        {
            obj.CreatedAt = DateTime.Now;
            _repository.Add(obj);
        }

        public virtual void Update(TEntity obj)
        {
            obj.UpdatedAt = DateTime.Now;
            _repository.Update(obj);
        }

        public virtual void Remove(TEntity obj)
        {
            _repository.Remove(obj);
        }

        public virtual TEntity GetById(long id, bool lazyLoadEnabled = true)
        {
            return _repository.GetById(id, lazyLoadEnabled);
        }

        public virtual TEntity GetById(Guid id, bool lazyLoadEnabled = true)
        {
            return _repository.GetById(id, lazyLoadEnabled);
        }

        public virtual TEntity GetByExpression(Expression<Func<TEntity, bool>> predicate, bool lazyLoadEnabled = true, params string[] includes)
        {
            return _repository.GetByExpression(predicate, lazyLoadEnabled, includes);
        }

        public virtual IEnumerable<TEntity> GetAll(bool lazyLoadEnabled = true, params string[] includes)
        {
            return _repository.GetAll(lazyLoadEnabled, includes);
        }

        public virtual IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate = null,
            Expression<Func<TEntity, object>> order = null, bool ascending = true, int skipRecords = 0,
            int takeRecords = 0, bool lazyLoadEnabled = true, params string[] includes)
        {
            return _repository.GetAll(predicate, order, ascending, skipRecords, takeRecords, lazyLoadEnabled, includes);
        }

        public virtual IEnumerable<TEntity> GetAll(ref int totalRecords, Expression<Func<TEntity, bool>> predicate = null,
            Expression<Func<TEntity, object>> order = null, bool ascending = true, int skipRecords = 0,
            int takeRecords = 0, bool lazyLoadEnabled = true, params string[] includes)
        {
            return _repository.GetAll(ref totalRecords, predicate, order, ascending, skipRecords, takeRecords, lazyLoadEnabled, includes);
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}
