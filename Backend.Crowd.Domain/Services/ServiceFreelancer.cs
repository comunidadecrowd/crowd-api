﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Interfaces.Repositories;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Backend.Crowd.Domain.Services
{
    public class ServiceFreelancer : ServiceBase<Freelancer>, IServiceFreelancer
    {
        private readonly new IRepositoryBase<Freelancer> _repository;

        public ServiceFreelancer(IRepositoryBase<Freelancer> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public IEnumerable<Freelancer> GetAll(int page, int amount, ref int totalRecords)
        {

            return GetAll(ref totalRecords, null, null, false, ((page * amount) - amount), amount);
        }

        public IEnumerable<Freelancer> Search(string query, string city, int minValueHour, int maxValueHour, int categoriaId, int[] skillId, int availabilityId, int segmentId, int page, int amount, ref int totalRecords, params string[] includes)
        {
            Expression<Func<Freelancer, bool>> queryFunc = x => x.Price >= minValueHour && x.Price <= maxValueHour;

            if (!String.IsNullOrEmpty(query))
            {
                Expression<Func<Freelancer, bool>> queryFuncTemp = x => (x.Name.Contains(query) || x.Email.Contains(query) || x.Title.Contains(query) || x.PortfolioURL.Contains(query));
                queryFunc = queryFunc.And(queryFuncTemp);
            }

            if (!string.IsNullOrEmpty(city))
            {
                Expression<Func<Freelancer, bool>> cityFuncTemp = x => x.City.Equals(city);
                queryFunc = queryFunc.And(cityFuncTemp);
            }

            if (categoriaId > 0)
            {
                Expression<Func<Freelancer, bool>> categoryFuncTemp = x => x.CategoryId == categoriaId;
                queryFunc = queryFunc.And(categoryFuncTemp);
            }

            if (availabilityId > 0)
            {
                Expression<Func<Freelancer, bool>> availabilityFuncTemp = x => x.Availability == (AvailabilityType)availabilityId;
                queryFunc = queryFunc.And(availabilityFuncTemp);
            }

            if (skillId != null && skillId.Length > 0)
            {
                Expression<Func<Freelancer, bool>> skillFuncTemp = x => x.Skills.Any(y => skillId.Contains(y.Id));
                queryFunc = queryFunc.And(skillFuncTemp);
            }

            if (segmentId > 0)
            {
                Expression<Func<Freelancer, bool>> segmentFuncTemp = x => x.Segments.Any(y => y.Id == segmentId);
                queryFunc = queryFunc.And(segmentFuncTemp);
            }

            return GetAll(ref totalRecords, queryFunc, null, false, ((page * amount) - amount), amount, true, includes);
        }

        public Freelancer GetByCode(string code)
        {
            return _repository.GetByExpression(f => f.Code.ToString() == code);
        }

        public Freelancer GetByEmail(string email)
        {
            return _repository.GetByExpression(x => x.Email == email);
        }

        public Freelancer GetByPhone(string phone)
        {
            return _repository.GetByExpression(x => x.Phone == Validador.ClearPhone(phone));
        }

        public Freelancer GetByIdAndEmail(int id, string email)
        {
            return _repository.GetByExpression(x => x.Id != id && x.Email == email);
        }

        public Freelancer GetByIdAndPhone(int id, string phone)
        {
            phone = Validador.ClearPhone(phone);
            return _repository.GetByExpression(x => x.Id != id && x.Phone == phone);
        }

        public Tuple<bool, bool> GetByEmailOrPhone(string email, string phone)
        {
            bool existEmail = GetByEmail(email) != null ? true : false;
            bool existPhone = GetByPhone(phone) != null ? true : false;

            var response = new Tuple<bool, bool>(existEmail, existPhone);

            return response;
        }

        public Tuple<bool, bool> GetByEmailOrPhone(int id, string email, string phone)
        {
            bool existEmail = GetByIdAndEmail(id, email) != null ? true : false;
            bool existPhone = GetByIdAndPhone(id, phone) != null ? true : false;

            var response = new Tuple<bool, bool>(existEmail, existPhone);

            return response;
        }
    }
}
