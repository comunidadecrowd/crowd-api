﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Repositories;

namespace Backend.Crowd.Domain.Services
{
    public class ServiceTracking : ServiceBase<Tracking>
    {
        private readonly new IRepositoryBase<Tracking> _repository;

        public ServiceTracking(IRepositoryBase<Tracking> repository)
            :base(repository)
        {
            _repository = repository;
        }
    }
}
