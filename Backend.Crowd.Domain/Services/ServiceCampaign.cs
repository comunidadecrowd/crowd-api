﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Repositories;
using Backend.Crowd.Domain.Interfaces.Services;

namespace Backend.Crowd.Domain.Services
{

    public class ServiceCampaign : ServiceBase<Campaign>, IServiceCampaign
    {
        private readonly new IRepositoryBase<Campaign> _repository;

        public ServiceCampaign(IRepositoryBase<Campaign> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}