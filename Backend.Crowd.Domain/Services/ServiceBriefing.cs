﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;
using System.Linq.Expressions;
using Backend.Crowd.Domain.Interfaces.Repositories;
using Backend.Crowd.Domain.Interfaces.Services;
using System.Text.RegularExpressions;
using RestSharp.Validation;
using Backend.Crowd.Domain.Util;
using System.Globalization;

namespace Backend.Crowd.Domain.Services
{
    public class ServiceBriefing : ServiceBase<Briefing>, IServiceBriefing
    {
        private readonly IRepositoryBriefing _repositoryBriefing;

        public ServiceBriefing(IRepositoryBriefing repository) : base(repository)
        {
            _repositoryBriefing = repository;
        }

        public IEnumerable<BriefingList> GetBriefing(User user)
        {
            switch (user.Role)
            {
                case Entities.Enum.RoleType.MASTER:
                    return GetListMaster();
                case Entities.Enum.RoleType.FREELANCER:
                    return GetListFreelancer(user.IdFreelancer.Value);
                case Entities.Enum.RoleType.EMPLOYEE:
                case Entities.Enum.RoleType.CLIENT:
                    return GetListClient(user.IdCustomer.Value);
            }

            return null;
        }

        public Briefing GetBriefing(string token)
        {
            return _repositoryBriefing.GetByExpression(b => b.TokenFreelancers.Any(bf => bf.Token.ToString() == token));
        }

        private IEnumerable<BriefingList> GetListClient(int idCustomer)
        {
            string query = String.Format(@"Select b.id,b.excerpt,b.title,b.active,b.created_at as CreatedAt,b.updated_at as UpdatedAt,b.id_project as IdProject,p.color,p.name as nameProjeto,u.name as userName,c.logo as photo,b.id_user as IdUser,Cast(0 as bit) As [Read],b.hunting as Hunting from crowd_briefings b
                                inner join crowd_users u on b.id_user = u.id
                                inner join crowd_customers c on c.id = u.id_customer
                                inner join crowd_projects p on b.id_project = p.id
                                where c.id = {0}
                                order by b.created_at", idCustomer.ToString());

            return _repositoryBriefing.GetList(query);
        }

        private IEnumerable<BriefingList> GetListFreelancer(int idFreelancer)
        {
            string query = String.Format(@"Select b.id,b.excerpt,b.title,b.active,b.created_at as CreatedAt,b.updated_at as UpdatedAt,b.id_project as IdProject,p.color,p.name as nameProjeto,u.name as userName,c.logo as photo,b.id_user as IdUser,isnull(rb.[read],0) as [Read] from crowd_briefings b
                                inner join crowd_users u on b.id_user = u.id
                                inner join crowd_customers c on c.id = u.id_customer
                                inner join crowd_projects p on b.id_project = p.id
								inner join crowd_briefings_freelancers bf on bf.briefing_id = b.id
								inner join crowd_freelancer f on f.id = bf.freelancer_id
								left join crowd_read_briefing rb on rb.briefing_id = b.id and rb.freelancer_id = f.id 
                                where f.id = {0}
                                order by b.created_at", idFreelancer.ToString());

            return _repositoryBriefing.GetList(query);
        }

        private IEnumerable<BriefingList> GetListMaster()
        {
            string query = @"Select b.id,b.excerpt,b.title,b.active,b.created_at as CreatedAt,b.updated_at as UpdatedAt,b.id_project as IdProject,p.color,p.name as nameProjeto,u.name as userName,c.logo as photo,b.id_user as IdUser,Cast(0 as bit) as [Read],b.hunting as Hunting from crowd_briefings b
                                inner join crowd_users u on b.id_user = u.id
                                inner join crowd_customers c on c.id = u.id_customer
                                inner join crowd_projects p on b.id_project = p.id
                                order by b.created_at";

            return _repositoryBriefing.GetList(query);
        }

        public void ClearTextBriefing(Briefing briefing)
        {
            briefing.Text = Validador.ClearText(briefing.Text);
        }


        public void UpdateBriefingFreelancers(Briefing briefing)
        {
            int idBriefing = briefing.Id;
            _repositoryBriefing.ExecuteCommand("DELETE FROM crowd_briefings_freelancers WHERE briefing_id = " + idBriefing);

            foreach (Freelancer freelancer in briefing.Freelancers)
            {
                int[] values = {
                    freelancer.Id,
                    briefing.Id
                };

                _repositoryBriefing.ExecuteCommand("INSERT INTO crowd_briefings_freelancers VALUES (" + string.Join(",", values) + ")");
            }

            _repositoryBriefing.ExecuteCommand("DELETE FROM crowd_briefings_freelancers_token WHERE briefing_id = " + idBriefing);

            foreach (Briefing.TokenFreelancer token in briefing.TokenFreelancers)
            {
                _repositoryBriefing.ExecuteCommand("INSERT INTO " +
                    "crowd_briefings_freelancers_token (" +
                        "freelancer_id, " +
                        "briefing_id, " +
                        "token, " +
                        "status, " +
                        "active, " +
                        "created_at, " +
                        "updated_at, " +
                        "url, " +
                        "see_at, " +
                        "vizualizations, " +
                        "auto_insert " +
                    ") VALUES (" +
                        token.FreelancerId + ", " +
                        token.BriefingId + ", " +
                        "'" + token.Token.ToString() + "', " +
                        Convert.ToInt32(token.Status) + ", " +
                        Convert.ToInt32(token.Active) + ", " +
                        "'"  + DateTime.Now.ToString("G", CultureInfo.InvariantCulture) + "', " +
                        "null, " +
                        "'" + token.UrlShortener + "', " +
                        "null, " +
                        token.Visualizations + ", " +
                        "null" +
                    ")"
                );
            }
        }

        public Briefing Buscar(int id)
        {
            return _repositoryBriefing.Buscar(id);
        }
    }
}