﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Repositories;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Util;
using Backend.Crowd.Domain.Util.Network;
using Backend.Crowd.Domain.Util.Security;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace Backend.Crowd.Domain.Services
{
    public class ServiceUser : ServiceBase<User>, IServiceUser
    {
        private readonly new IRepositoryBase<User> _repository;
        private readonly IEmail _sendEmail;

        public ServiceUser(IRepositoryBase<User> repository, IEmail email)
            : base(repository)
        {
            _repository = repository;
            _sendEmail = email;
        }

        public User Authentication(string name, string password)
        {
            password = Criptografy.GetMD5Hash(password);
            return GetByExpression(x => x.Email.Equals(name) && x.Password.Equals(password));
        }

        public User Authentication(string name)
        {
            return GetByExpression(x => x.Email.Equals(name));
        }

        public override void Add(User obj)
        {
            if (!string.IsNullOrEmpty(obj.Password))
                obj.Password = Criptografy.GetMD5Hash(obj.Password);

            base.Add(obj);
        }

        public override void Update(User obj)
        {
            //if (!String.IsNullOrEmpty(obj.Password))
            //    obj.Password = Criptografy.GetMD5Hash(obj.Password);

            base.Update(obj);
        }

        public Guid? RecoverPassword(String email)
        {
            var user = GetByExpression(x => x.Email.Equals(email));
            if (user != null && user.Id > 0)
            {
                if (user.RecoverPassword.HasValue)
                    return user.RecoverPassword.Value;
                user.RecoverPassword = Guid.NewGuid();
                Update(user);
                return user.RecoverPassword.Value;
            }
            return null;
        }

        public bool ChangePassword(String email, String newPassword, String guid)
        {
            var user = GetByExpression(x => x.Email.Equals(email) && x.RecoverPassword.HasValue
                && x.RecoverPassword.Value.ToString().Equals(guid));
            if (user != null && user.Id > 0)
            {
                user.RecoverPassword = null;
                user.Password = Criptografy.GetMD5Hash(newPassword);
                Update(user);
                return true;
            }
            return false;
        }

        public bool ChangePassword(int id, String newPassword)
        {
            var user = GetByExpression(x => x.Id == id);
            if (user != null && user.Id > 0)
            {
                user.RecoverPassword = null;
                user.Password = Criptografy.GetMD5Hash(newPassword);
                Update(user);
                return true;
            }
            return false;
        }

        public User GetByEmail(string email)
        {
            var user = this.GetByExpression(e => e.Email == email, false, "Customer", "Freelancer");

            return user;

        }

        public void SendConfirmationEmail(User user)
        {
            var variables = new Dictionary<string, string> {
                { "CONFIRMEMAIL", String.Concat(ConfigurationManager.AppSettings["EmailconfirmadoURL"],"?hash=",user.ConfirmEmail.ToString()) },
                { "EMAIL", user.Email },
                { "NAME", user.Name }
            };

            _sendEmail.Send(user.Email, user.Name, "Obrigado por entrar para a Crowd", user.IsCustomer ? "customer-pre-cadastro" : "profissional-pre-cadastro", variables);

            //_sendEmail.NewSend(user.IdLead, ConfigurationManager.AppSettings["noreplyEmail"], "contact", "Obrigado por entrar para a Crowd",user.IsCustomer ? "PreCadastro" : "PreCadastroFreelancer", variables);

        }

        public User GetByPhone(string phone)
        {
            return _repository.GetByExpression(x => x.Phone == Validador.ClearPhone(phone));
        }

        public User GetByIdAndEmail(int id, string email)
        {
            return _repository.GetByExpression(x => x.Id != id && x.Email == email);
        }

        public User GetByIdAndPhone(int id, string phone)
        {
            phone = Validador.ClearPhone(phone);
            return _repository.GetByExpression(x => x.Id != id && x.Phone == phone);
        }

        public Tuple<bool, bool> GetByEmailOrPhone(string email, string phone)
        {
            bool existEmail = GetByEmail(email) != null ? true : false;
            bool existPhone = GetByPhone(phone) != null ? true : false;

            var response = new Tuple<bool, bool>(existEmail, existPhone);

            return response;
        }

        public Tuple<bool, bool> GetByEmailOrPhone(int id, string email, string phone)
        {
            bool existEmail = GetByIdAndEmail(id, email) != null ? true : false;
            bool existPhone = GetByIdAndPhone(id, phone) != null ? true : false;

            var response = new Tuple<bool, bool>(existEmail, existPhone);

            return response;
        }
    }
}
