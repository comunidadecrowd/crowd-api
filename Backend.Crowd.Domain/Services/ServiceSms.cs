﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Repositories;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Util.Network;
using System.Collections.Generic;
using Twilio;
using Twilio.Exceptions;
using Twilio.Rest.Api.V2010.Account;

namespace Backend.Crowd.Domain.Services
{
    public class ServiceSms : ServiceBase<Sms>, IServiceSms
    {
        private TemplateSms _templateBuilder;
        private const string _accountSid = "AC27412e037e1589853ff5118e835e090d";
        private const string _authToken = "31c3b82a7ed98936179fbca10874b5ad";
        private const string _from = "+18338310053";

        public ServiceSms(IRepositoryBase<Sms> repository) : base(repository)
        {
            _repository = repository;
            _templateBuilder = new TemplateSms();
        }

        public void Create(int usuario,string from,string to,string template, Dictionary<string,string> variables)
        {
            TwilioClient.Init(_accountSid, _authToken);

            try
            {
                var message = MessageResource.Create(
                body: _templateBuilder.Generate(template, variables),
                from: new Twilio.Types.PhoneNumber(_from),
                to: new Twilio.Types.PhoneNumber($"+55{to}"),
                pathAccountSid: _accountSid
                );

                var obj = new Sms
                {
                    From = from,
                    Body = _templateBuilder.Generate(template, variables),
                    Status = Entities.Enum.SmsStatus.Created,
                    To = to,
                    UserId = usuario,
                    SmsId = message.Sid
                };

                base.Add(obj);
            }
            catch (TwilioException te)
            {

                //throw;
            }
        }
    }
}
