﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class Argument : BaseEntity
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public Tracking Tracking { get; set; }
        public int TrackingId { get; set; }
    }
}
