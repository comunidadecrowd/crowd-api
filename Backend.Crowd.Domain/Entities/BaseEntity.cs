﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            CreatedAt = DateTime.Now;
            Active = true;
        }

        public int Id { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }

    
        public static class StringExt
        {
            public static bool ContainsInsensitive(this string source, string search)
            {
                return (new CultureInfo("pt-BR").CompareInfo).IndexOf(source, search, CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace) >= 0;
            }

            public static bool StartInsensitive(this string source, string search)
            {
                return (new CultureInfo("pt-BR").CompareInfo).IsPrefix(source, search, CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace);
            }
    }
}
