﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class WebInformation : BaseEntity
    {
        public virtual ICollection<WebEntity> WebEntities { get; set;}

        public virtual ICollection<WebFull> FullMatchingImages { get; set; }

        public virtual ICollection<WebPartial> PartialMatchingImages { get; set; }

        public virtual ICollection<WebSimilar> VisuallySimilarImages { get; set; }

        public virtual ICollection<WebPage> PagesWithMatchingImages { get; set; }

        public int FreelancerId { get; set; }

        //public virtual Freelancer Freelancer { get; set; }

        public class WebEntity 
        {
            public int Id { get; set; }
            public decimal Score { get; set; }
            public string Description { get; set; }
            public int WebInformationId { get; set; }
            public virtual WebInformation WebInformation { get; set; }
        }

        public class WebFull : WebImage
        {

        }

        public class WebPartial : WebImage
        {

        }

        public class WebSimilar : WebImage
        {

        }

        public class WebPage : WebImage
        {
        }

        public class WebImage
        {
            public int Id { get; set; }
            public decimal Score { get; set; }
            public string Url { get; set; }
            public int WebInformationId { get; set; }
            public virtual WebInformation WebInformation {get; set;}
        }

        
    }
}
