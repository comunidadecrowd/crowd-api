﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class BriefingList : BaseEntity
    {
        public string Title { get; set; }
        public string Excerpt { get; set; }
        public int IdProject { get; set; }
        public string Color { get; set; }
        public string NameProjeto { get; set; }
        public string UserName { get; set; }
        public string Photo { get; set; }
        public int IdUser { get; set; }
        public bool Read { get; set; }
        public bool? Hunting { get; set; }
    }
}
