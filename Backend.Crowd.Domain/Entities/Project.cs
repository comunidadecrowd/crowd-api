﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class Project : BaseEntity
    {
        public Project()
        {

        }

        public string Name { get; set; }
        public string Color { get; set; }
        public int TaskCount { get; set; }
        public int IdCustomer { get; set; }


        public virtual Customer Customer { get; set; }

    }
}
