﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class SafeSearch : BaseEntity
    {
        public Likelihood Adult { get; set; }
        public Likelihood Spoof { get; set; }
        public Likelihood Medical { get; set; }
        public Likelihood Violence { get; set; }

        public int FreelancerId { get; set; }

        //public virtual Freelancer Freelancer { get; set; }
    }
}
