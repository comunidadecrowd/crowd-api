﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class Face : BaseEntity
    {
        public decimal DetectionConfidence { get; set; }

        public Likelihood SorrowLikelihood { get; set; }
        public Likelihood JoyLikelihood { get; set; }
        public Likelihood AngerLikelihood { get; set; }
        public Likelihood UnderExposedLikelihood { get; set; }
        public Likelihood BlurredLikelihood { get; set; }
        public Likelihood HeadwearLikelihood { get; set; }
        public Likelihood SurpriseLikelihood { get; set; }

        public int FreelancerId { get; set; }

        //public virtual Freelancer Freelancer { get; set; }

    }
}
