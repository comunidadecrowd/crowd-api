﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class Search : BaseEntity
    { 
        public int UserId { get; set; }
        public string Tag { get; set; }

        public virtual User User { get; set; }
    }
}
