﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class LoggedUser
    {
        public string Email { get; set; }
        public int Id { get; set; }
        public RoleType Role { get; set; }
        public string Photo { get; set; }
        public string Name { get; set; }
        public string CustomerLogo { get; set; }
        public string Customer { get; set; }
        public string CustomerPlan { get; set; }
        public object IdCustomer { get; set; }
        public string IdFreelancer { get; set; }
        public string Code { get; set; }
        public object RoleCompany { get; set; }
        public bool AcceptTerms { get; set; }
        public string defaultURL { get; set; }
        public bool? Prospect { get; set; }
        public bool? Trial { get; set; }
        public DateTime? TrialDate { get; set; }
        public bool? Paid { get; set; }
    }
}
