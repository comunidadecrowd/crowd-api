﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Backend.Crowd.Domain.Entities
{
    public class MessageTasks : BaseEntity
    {
        public MessageTasks()
        {
            Attachs = new HashSet<MessageTaskAttachs>();
        }

        public int? UserId { get; set; }
        public int? FreelancerId { get; set; }
        public int TaskId { get; set; }
        public String Text { get; set; }
        public bool Read { get; set; }

        public virtual Tasks Task { get; set; }
        public virtual User User { get; set; }
        public virtual Freelancer Freelancer { get; set; }

        public virtual ICollection<MessageTaskAttachs> Attachs { get; set; }
    }
}
