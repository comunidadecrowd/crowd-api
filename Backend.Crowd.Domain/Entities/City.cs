﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class City : BaseEntity
    {
        public string Name { get; set; }
        public int StateId { get; set; }

        public virtual State State { get; set; }
   
    }
}
