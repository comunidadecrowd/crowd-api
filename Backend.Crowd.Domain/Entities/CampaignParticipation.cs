﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class CampaignParticipation : BaseEntity
    {

        public int CampaignId { get; set; }
        public int FreelancerId { get; set; }
        public string Text { get; set; }
        
        public virtual Freelancer Freelancer { get; set; }
        public virtual Campaign Campaign { get; set; }
    }

    
}
