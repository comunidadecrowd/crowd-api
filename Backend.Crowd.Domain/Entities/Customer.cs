﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities.Enum;

namespace Backend.Crowd.Domain.Entities
{
    public class Customer : BaseEntity
    {
        public Customer()
        {
            Prospect = false;
        }

        public String Name { get; set; }
        public String Trade { get; set; }

        public string Logo { get; set; }
        public string CNPJ { get; set; }
        public int QtyPersons { get; set; }

        public int? StateId { get; set; }
        public int? CityId { get; set; }

        public string NameResponsible { get; set; }
        public string EmailResponsible { get; set; }

        public virtual State State { get; set; }
        public virtual City City { get; set; }

        public string InscricaoEstadual { get; set; }
        public string InscricaoMunicipal { get; set; }

        public string Address { get; set; }
        public string Number { get; set; }
        public string Complement { get; set; }
        public string CEP { get; set; }

        public bool? Prospect { get; set; }

        public string Phone { get; set; }

        public DateTime? LastChangeActivation { get; set; }

        public DateTime? LastChangePlan { get; set; }

        public PlanType PlanType { get; set; }

        public virtual ICollection<RatingCustomer> Ratings { get; set; }   
        
        public string Referrer { get; set; }

        public bool? Trial { get; set; }

        public DateTime? TrialDate { get; set; }

        public string PaymentToken { get; set; }

        public string PaymentSubscription { get; set; }

        public string PromoCode { get; set; }

        public virtual PaymentCustomer Payment { get; set; }


public class CustomerTracking : BaseEntity
        {
            public int UserId { get; set; }
            public int FreelancerId { get; set; }
            public string FieldRequested { get; set; }
            public string SearchTerm { get; set; }

            public PlanType PlanType { get; set; }

            public virtual User User { get; set; }
            public virtual Freelancer Freelancer { get; set; }
        }

        public class RatingCustomer : BaseEntity
        {
            public int CustomerId { get; set; }
            public int UserId { get; set; }
            public int Approval { get; set; }
            public int Communicate { get; set; }
            public int Professionalism { get; set; }
            public String Comment { get; set; }

            public int? TaskId { get; set; }

            public virtual Customer Customer { get; set; }
            public virtual User User { get; set; }

            public virtual Tasks Task { get; set; }
        }

        public class PaymentCustomer : BaseEntity
        {
            public string SubscriptionId { get; set; }
            public CustomerPaymentStatus Status { get; set; }
            public DateTime? Expires { get; set; }
            public virtual Customer Customer { get; set; }
            public int CustomerId { get; set; }
            public virtual ICollection<CustomerInvoices> Invoices { get; set; }
            public virtual ICollection<CustomerPaymentLog> Logs { get; set; }
        }

        public class CustomerInvoices
        {
            public string Id { get; set; }
            public DateTime DueDate { get; set; }
            public CustomerPaymentStatus Status { get; set; }
            public decimal? Total { get; set; }
            public string SubscriptionId { get; set; }
        }

        public class CustomerPaymentLog
        {
            public string Id { get; set; }
            public string Description { get; set; }
            public string Notes { get; set; }
            public DateTime CreatedAt { get; set; }
            public string SubscriptionId { get; set; }
        }
    }

    
}
