﻿using Backend.Crowd.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Util.Security;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Crowd.Domain.Entities
{
    public class User : BaseEntity
    {
        public int? IdCustomer { get; set; }
        public int? IdFreelancer { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string NewPassword
        {
            get { return Password; }
            set
            {
                if (!string.IsNullOrEmpty(value)) Password = Criptografy.GetMD5Hash(value);
            }
        }
        public Guid? RecoverPassword { get; set; }
        public RoleType Role { get; set; }
        public string Photo { get; set; }
        public string Phone { get; set; }

        public string RoleCompany { get; set; }

        public virtual Customer Customer { get; set; }
        //[ScriptIgnore]
        public virtual Freelancer Freelancer { get; set; }

        public bool AcceptTerms { get; set; }
        public DateTime AcceptTermsAt { get; set; }

        public DateTime? ConfirmEmailAt { get; set; }

        public Guid? ConfirmEmail { get; set; }
        public bool? IsResetting { get; set; }

        [NotMapped]
        public string IdLead { get; set; }

        [NotMapped]
        public bool IsProspect
        {
            get
            {
                if (IdCustomer > 0 && Customer != null && Customer.Prospect.HasValue)
                    return Customer.Prospect.Value;
                else if (Freelancer != null && Freelancer.Prospect.HasValue)
                    return Freelancer.Prospect.Value;
                else return false;
            }
        }

        [NotMapped]
        public bool IsCustomer
        {
            get
            {
                return (IdCustomer > 0);
            }
        }

        [NotMapped]
        public DateTime ActiveChange { get; set; }

        public class ActionLog : BaseEntity
        {
            public int UserId { get; set; }
            public ActionLogType Type { get; set; }

            public virtual User User { get; set; }
        }
    }
}
