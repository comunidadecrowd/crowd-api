﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class Sms : BaseEntity
    {
        public int UserId { get; set; }
        public virtual User User { get; set;}
        public string Body { get; set; }
        public DateTime? SentAt { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string SmsId { get; set; }
        public SmsStatus Status { get; set; }
        public string SourceId { get; set; }
    }
}
