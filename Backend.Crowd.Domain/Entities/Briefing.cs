﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;

namespace Backend.Crowd.Domain.Entities
{
    public class Briefing : BaseEntity
    {
        public int IdUser { get; set; }
        public int? IdProject { get; set; }
        public string Title { get; set; }
        public string Excerpt { get; set; }
        public string Text { get; set; }
        public bool Sended { get; set; }
        public bool? Hunting { get; set; }
        public Guid? Token { get; set; }

        //[ForeignKey("Freelancer_Id")]
        //public int FreelancerId { get; set; }
        //public int Freelancer_Id { get; set; }

        public virtual User User { get; set; }
        public virtual Project Project { get; set; }
        public virtual ICollection<Freelancer> Freelancers { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<Complement> Complements { get; set; }
        public virtual ICollection<Attach> Attachs { get; set; }
        public virtual ICollection<ReadBriefing> Read { get; set; }
        public virtual ICollection<TokenFreelancer> TokenFreelancers { get; set; }

        public Briefing()
        {
            Freelancers = new HashSet<Freelancer>();
            Messages = new HashSet<Message>();
            Read = new HashSet<ReadBriefing>();
            Complements = new HashSet<Complement>();
            Attachs = new HashSet<Attach>();
            TokenFreelancers = new HashSet<TokenFreelancer>();
        }

        public class TokenFreelancer : BaseEntity
        {
            public int BriefingId { get; set; }
            public int FreelancerId { get; set; }
            public Guid? Token { get; set; }
            public SmsBriefingStatus Status { get; set; }
            public string UrlShortener { get; set; }
            public DateTime? SeeAt { get; set; }
            public int Visualizations { get; set; }
        }

        public class Attach : BaseEntity
        {
            public Attach()
            {
            }

            public int briefingId { get; set; }
            public string attachUrl { get; set; }
            public string attachName { get; set; }

            public virtual Briefing Briefing { get; set; }
        }

        public class Complement : BaseEntity
        {
            public int BriefingId { get; set; }
            public int? TaskId { get; set; }
            public string Text { get; set; }

            public virtual Briefing Briefing { get; set; }
            public virtual Tasks Task { get; set; }
            public virtual ICollection<ComplementAttach> Attachs { get; set; }
        }

        public class ReadBriefing : BaseEntity
        {

            public int FreelancerId { get; set; }
            public int BriefingId { get; set; }

            public bool Read { get; set; }

            public Briefing Briefings { get; set; }
            public virtual Freelancer Freelancer { get; set; }

        }

        public class Propose : BaseEntity
        {
            public int? BriefingId { get; set; }
            public int? TaskId { get; set; }
            public int FreelancerId { get; set; }
            public decimal Price { get; set; }
            public int DeadlineDays { get; set; }

            public bool Contracted { get; set; }
            public int? UserResponsibleId { get; set; }

            public virtual Briefing Briefing { get; set; }
            public virtual Tasks Task { get; set; }
            public virtual Freelancer Freelancer { get; set; }

            public virtual User UserResponsible { get; set; }
        }

        public class Message : BaseEntity
        {
            public int? UserId { get; set; }
            public int? FreelancerId { get; set; }
            public int BriefingId { get; set; }
            public String Text { get; set; }
            public bool Read { get; set; }

            public virtual Briefing Briefings { get; set; }
            public virtual User User { get; set; }
            public virtual Freelancer Freelancers { get; set; }
        }
    }
}
