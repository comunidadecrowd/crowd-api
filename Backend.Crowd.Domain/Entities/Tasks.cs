﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Backend.Crowd.Domain.Entities.Enum;

namespace Backend.Crowd.Domain.Entities
{
    public class Tasks : BaseEntity
    {
        public Tasks()
        {
            Messages = new HashSet<MessageTasks>();
            Attachs = new HashSet<TasksAttach>();
            Status = TaskStatus.TO_BE_DONE;
        }
        
        public string Title { get; set; }
        public string Description { get; set; }

        public TaskStatus Status { get; set; }
        public DateTime? DeliveryAt { get; set; }
        public DateTime? ApprovalAt { get; set; }
        public DateTime? DueAt { get; set; }
        public decimal? Price { get; set; }

        public virtual User User { get; set; }
        public int IdUser { get; set; }

        public virtual ICollection<MessageTasks> Messages { get; set; }
        public virtual ICollection<TasksAttach> Attachs { get; set; }
        public virtual ICollection<Briefing.Complement> Complements { get; set; }

        public int IdProject { get; set; }
        public virtual Project Project { get; set; }

        public int? IdBriefing { get; set; }
        public virtual Briefing Briefing { get; set; }

        public int IdFreelancer { get; set; }
        public virtual Freelancer Freelancer { get; set; }

        public bool? FreelancerReview { get; set; }
        public bool? CustomerReview { get; set; }
    }
}
