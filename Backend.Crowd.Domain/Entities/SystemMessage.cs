﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class SystemMessage : BaseEntity
    {
        public SystemMessage()
        {
            Read = new HashSet<ReadSystemMessage>();
            Users = new HashSet<User>();
        }

        public string Text { get; set; }
        public string Excerpt { get; set; }
        public string Title { get; set; }
        public virtual User UserSent { get; set; }
        public int IdUserSent { get; set; }

        public virtual ICollection<ReadSystemMessage> Read { get; set; }

        public bool ToAllFreelancers { get; set; }
        public bool ToAllCustomers { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public class ReadSystemMessage : BaseEntity
        {
            public int UserId { get; set; }
            public int SystemMessageId { get; set; }

            public bool Read { get; set; }

            public SystemMessage SystemMessage { get; set; }
            public virtual User User { get; set; }

        }
    }
}
