﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class GoogleEntity : BaseEntity
    {
        public int FreelancerId { get; set; }

        public string Name { get; set; }

        public GoogleEntityType Type { get; set; }

        public decimal Salience { get; set; }
    }
}
