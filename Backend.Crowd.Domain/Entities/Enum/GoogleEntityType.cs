﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities.Enum
{
    public enum GoogleEntityType
    {
        //
        // Resumo:
        //     Unknown
        Unknown = 0,
        //
        // Resumo:
        //     Person
        Person = 1,
        //
        // Resumo:
        //     Location
        Location = 2,
        //
        // Resumo:
        //     Organization
        Organization = 3,
        //
        // Resumo:
        //     Event
        Event = 4,
        //
        // Resumo:
        //     Work of art
        WorkOfArt = 5,
        //
        // Resumo:
        //     Consumer goods
        ConsumerGood = 6,
        //
        // Resumo:
        //     Other types
        Other = 7
    }
}
