﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities.Enum
{
    //
    // Resumo:
    //     A bucketized representation of likelihood, which is intended to give clients
    //     highly stable results across model upgrades.
    public enum Likelihood
    {
        //
        // Resumo:
        //     Unknown likelihood.
        Unknown = 0,
        //
        // Resumo:
        //     It is very unlikely that the image belongs to the specified vertical.
        VeryUnlikely = 1,
        //
        // Resumo:
        //     It is unlikely that the image belongs to the specified vertical.
        Unlikely = 2,
        //
        // Resumo:
        //     It is possible that the image belongs to the specified vertical.
        Possible = 3,
        //
        // Resumo:
        //     It is likely that the image belongs to the specified vertical.
        Likely = 4,
        //
        // Resumo:
        //     It is very likely that the image belongs to the specified vertical.
        VeryLikely = 5
    }
}
