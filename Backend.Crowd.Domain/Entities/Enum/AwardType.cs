﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities.Enum
{
    public enum AwardType
    {
        PREMIUM = 1,
        CERTIFICATION = 2,
    }
}
