﻿namespace Backend.Crowd.Domain.Entities.Enum
{
    public enum BePayType
    {
        Cartao = 1,
        Boleto = 2
    }
}