﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities.Enum
{
    public enum PaymentType
    {
        CREDITCARD = 1,
        BOLETO = 2,
        DEBITO = 3
    }
}
