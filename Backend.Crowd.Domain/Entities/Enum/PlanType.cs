﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities.Enum
{
    public enum PlanType
    {
        FREE = 1,
        SMART = 2,
        PRO = 3,
        ENTERPRISE = 4
    }
}
