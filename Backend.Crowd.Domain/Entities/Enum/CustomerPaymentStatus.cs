﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities.Enum
{
    public enum CustomerPaymentStatus
    {
        pending = 1,
        paid = 2,
        canceled = 3,
        partially_paid = 4,
        refunded = 5,
        expired= 6,
        authorized=7,
        in_protest=8,
        chargeback=9
    }
}
