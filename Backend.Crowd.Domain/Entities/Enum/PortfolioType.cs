﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities.Enum
{
    public enum PortfolioType
    {
        IMAGE = 1,
        VIDEO = 2
    }
}
