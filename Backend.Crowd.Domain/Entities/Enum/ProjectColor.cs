﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities.Enum
{
    public class ProjectColor
    {
        private ProjectColor(string value) { Value = value; }

        public string Value { get; set; }

        public static ProjectColor Red { get { return new ProjectColor("#d6494b"); } }
        public static ProjectColor Pink { get { return new ProjectColor("#fd57d0"); } }
        public static ProjectColor LightPink { get { return new ProjectColor("#fab4b4"); } }
        public static ProjectColor Purple { get { return new ProjectColor("#6d45bc"); } }
        public static ProjectColor LightPurple { get { return new ProjectColor("#a58add"); } }
        public static ProjectColor GreyPurple { get { return new ProjectColor("#d2c5ec"); } }
        public static ProjectColor Blue { get { return new ProjectColor("#37a9b7"); } }
        public static ProjectColor LightBlue { get { return new ProjectColor("#77d6e1"); } }
        public static ProjectColor XLightBlue { get { return new ProjectColor("#baeaef"); } }
        public static ProjectColor Orange { get { return new ProjectColor("#e98f2e"); } }
        public static ProjectColor Yellow { get { return new ProjectColor("#f9cd48"); } }
        public static ProjectColor LightYellow { get { return new ProjectColor("#f8e59b"); } }
    }
}
