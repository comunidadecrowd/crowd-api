﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities.Enum
{
    public enum MentionType
    {
        //
        // Resumo:
        //     Unknown
        Unknown = 0,
        //
        // Resumo:
        //     Proper name
        Proper = 1,
        //
        // Resumo:
        //     Common noun (or noun compound)
        Common = 2
    }
}
