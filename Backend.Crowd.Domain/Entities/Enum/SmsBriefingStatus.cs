﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities.Enum
{
    public enum SmsBriefingStatus
    {
        Ok = 1,
        Ocupado = 2,
        Desinteressado = 3
    }
}
