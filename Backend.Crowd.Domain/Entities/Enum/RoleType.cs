﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities.Enum
{
    public enum RoleType
    {
        MASTER = 1, 
        FREELANCER = 2,
        EMPLOYEE = 3,
        CLIENT = 4
    }
}
