﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities.Enum
{
    public enum WebsiteType
    {
        Facebook = 1,
        Twitter = 2,
        Dribbble = 3,
        Instagram = 4,
        Linkedin = 5,
        Behance = 6,
        Github = 7
    }
}
