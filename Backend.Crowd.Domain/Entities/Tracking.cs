﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class Tracking : BaseEntity
    {

        public int IdUser { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string Token { get; set; }
        public string State { get; set; }
        public List<Argument> Arguments { get; set; }
        public string Environment { get; set; }
    }
}
