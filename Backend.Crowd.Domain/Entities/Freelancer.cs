﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities.Enum;

namespace Backend.Crowd.Domain.Entities
{
    public class Freelancer : BaseEntity
    {
        public int CategoryId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String Email { get; set; }
        public String Phone { get; set; }
        public String Country { get; set; }
        public String Title { get; set; }
        public String Skype { get; set; }
        public decimal Price { get; set; }
        public String PortfolioURL { get; set; }
        public int? StateId { get; set; }
        public int? CityId { get; set; }
        public double Rating
        {
            get;
            /*{               

                var ratingsCount = Ratings.Count();

                var QualityRating = ratingsCount == 0 ? 0 : Math.Round((double)Ratings.Select(x => x.Quality).Sum() / ratingsCount);
                var ResponsabilityRating = ratingsCount == 0 ? 0 : Math.Round((double)Ratings.Select(x => x.Responsability).Sum() / ratingsCount);
                var AgilityRating = ratingsCount == 0 ? 0 : Math.Round((double)Ratings.Select(x => x.Agility).Sum() / ratingsCount);
                var Rating = ratingsCount == 0 ? 0 : ((QualityRating + ResponsabilityRating + AgilityRating) / 3);
                return Rating;
            }*/
            set; /*{
                
            }*/
        }
        public AvailabilityType Availability { get; set; }
        public StatusUserType Status { get; set; }

        public Guid Code { get; set; }

        public DateTime? Birthday { get; set; }
        public string EmailSecondary { get; set; }
        public string Referrer { get; set; }

        public virtual State State { get; set; }
        public virtual City City { get; set; }
        public virtual Category Categorys { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Briefing> Briefings { get; set; }
        public virtual ICollection<Skill> Skills { get; set; }
        public virtual ICollection<Segment> Segments { get; set; }
        public virtual ICollection<RatingFreelancer> Ratings { get; set; }
        public virtual ICollection<Experience> Experiences { get; set; }
        public virtual ICollection<Award> Awards { get; set; }
        public virtual ICollection<Portfolio> Portfolios { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
        public virtual ICollection<Link> Links { get; set; }
        public virtual ICollection<Language> Languages { get; set; }
        public FileType? CoverType { get; set; }

        public string Cover { get; set; }
        public string PromoCode { get; set; }
        public bool? Prospect { get; set; }

        public bool Alocado { get; set; }
        public string PortfolioOnLine { get; set; }

        public int? SentimentId { get; set; }
        public virtual Sentiment Sentiment { get; set; }
        public virtual ICollection<GoogleEntity> Entities { get; set; }
        public virtual ICollection<Face> Faces { get; set; }
        public virtual ICollection<Label> Labels { get; set; }
        public int? SafeSearchId { get; set; }
        public virtual SafeSearch SafeSearch { get; set; }
        public int? WebInformationId { get; set; }
        public virtual WebInformation WebInformation { get; set; }

        public Freelancer()
        {
            Status = StatusUserType.Active;
            Users = new HashSet<User>();
            Briefings = new HashSet<Briefing>();
            Skills = new HashSet<Skill>();
            Segments = new HashSet<Segment>();
            Ratings = new HashSet<RatingFreelancer>();
            Experiences = new HashSet<Experience>();
            Awards = new HashSet<Award>();
            Portfolios = new HashSet<Portfolio>();
            Payments = new HashSet<Payment>();
            Links = new HashSet<Link>();
            Languages = new HashSet<Language>();
            Entities = new List<GoogleEntity>();
            Alocado = false;
        }

        /**
         * Below are auxiliary freelance class 
         **/

        public class Award : BaseEntity
        {
            public string Title { get; set; }
            public DateTime Date { get; set; }
            public string URL { get; set; }
            public int FreelancerId { get; set; }
            public AwardType Type { get; set; }

            public virtual Freelancer Freelancer { get; set; }
        }

        public class Category : BaseEntity
        {
            public string Name { get; set; }
        }

        public class Segment : BaseEntity
        {
            public string Name { get; set; }
        }

        public class Skill : BaseEntity
        {
            public String Name { get; set; }
        }

        public class Payment : BaseEntity
        {
            public int FreelancerId { get; set; }
            public string Bank { get; set; }
            public string FullName { get; set; }
            public string Agency { get; set; }
            public string Account { get; set; }
            public string CPF { get; set; }

            public virtual Freelancer Freelancer { get; set; }
        }

        public class Link : BaseEntity
        {
            public int FreelancerId { get; set; }
            public WebsiteType Website { get; set; }
            public string URL { get; set; }

            public virtual Freelancer Freelancer { get; set; }
        }

        public class Language : BaseEntity
        {
            public int FreelancerId { get; set; }
            public string Value { get; set; }

            public virtual Freelancer Freelancer { get; set; }
        }

        public class RatingFreelancer : BaseEntity
        {
            public int FreelancerId { get; set; }
            public int UserId { get; set; }
            public int Responsability { get; set; }
            public int Agility { get; set; }
            public int Quality { get; set; }
            public String Comment { get; set; }

            public int? TaskId { get; set; }

            public virtual Freelancer Freelancer { get; set; }
            public virtual User User { get; set; }

            public virtual Tasks Task { get; set; }
        }

        public class Experience : BaseEntity
        {
            public int FreelancerId { get; set; }
            public string Role { get; set; }
            public string Company { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public string Description { get; set; }

            public virtual Freelancer Freelancer { get; set; }
        }

        public class Portfolio : BaseEntity
        {
            public int FreelancerId { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public FileType Type { get; set; }
            public string URL { get; set; }
            public string ClientName { get; set; }
            public string Media { get; set; }

            public int Order { get; set; }

            public virtual Freelancer Freelancer { get; set; }
            public string Thumb { get; set; }
        }
    }
}
