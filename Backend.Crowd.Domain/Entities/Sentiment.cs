﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class Sentiment : BaseEntity
    {
        public int FreelancerId { get; set; }

        public decimal Magnitude { get; set; }
        public decimal Score { get; set; }

        public string Language { get; set; }

        public virtual List<Sentence> Sentences { get; set; }

        public virtual Freelancer Freelancer { get; set; }
    }

    public class Sentence : BaseEntity
    {
        public string Content { get; set; }
        public int BeginOffset { get; set; }

        public decimal Magnitude { get; set; }
        public decimal Score { get; set; }

        public int SentimentId { get; set; }
    }
}
