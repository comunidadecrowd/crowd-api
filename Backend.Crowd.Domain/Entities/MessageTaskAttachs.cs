﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class MessageTaskAttachs : BaseEntity
    {
        public int MessageTasksId { get; set; }
        public string URL { get; set; }
        public string Name { get; set; }
    }
}
