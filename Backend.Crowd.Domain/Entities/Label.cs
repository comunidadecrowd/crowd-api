﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class Label : BaseEntity
    {
        public string Description { get; set; }
        
        public decimal Score { get; set; }

        public int FreelancerId { get; set; }

        //public virtual Freelancer Freelancer { get; set; }
    }
}
