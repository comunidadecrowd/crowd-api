﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class Campaign : BaseEntity
    {
        public string Title { get; set; }
        public string Text { get; set; }

        public int LimitParticipation { get; set; }

    }

    
}
