﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class ComplementAttach : BaseEntity
    {
        public ComplementAttach()
        {
        }

        public int complementId { get; set; }
        public string attachUrl { get; set; }
        public string attachName { get; set; }
    }
}
