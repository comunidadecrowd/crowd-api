﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Entities
{
    public class State : BaseEntity
    {
        public string UF { get; set; }
        public string Name { get; set; }

        //public virtual ICollection<City> Cities { get; set; }
    }
}
