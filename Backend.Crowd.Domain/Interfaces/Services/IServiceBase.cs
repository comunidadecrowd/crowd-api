﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Domain.Interfaces.Services
{
    public interface IServiceBase<TEntity> where TEntity : BaseEntity
    {
        void Add(TEntity obj);

        void Update(TEntity obj);

        void Remove(TEntity obj);

        TEntity GetById(long id, bool lazyLoadEnabled = true);

        TEntity GetById(Guid id, bool lazyLoadEnabled = true);

        TEntity GetByExpression(Expression<Func<TEntity, bool>> predicate,
            bool lazyLoadEnabled = true, params string[] includes);

        IEnumerable<TEntity> GetAll(bool lazyLoadEnabled = true, params string[] includes);

        IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate = null,
            Expression<Func<TEntity, dynamic>> order = null, bool ascending = true, int skipRecords = 0,
            int takeRecords = 0, bool lazyLoadEnabled = true, params string[] includes);

        IEnumerable<TEntity> GetAll(ref int totalRecords, Expression<Func<TEntity, bool>> predicate = null,
            Expression<Func<TEntity, dynamic>> order = null, bool ascending = true, int skipRecords = 0,
            int takeRecords = 0, bool lazyLoadEnabled = true, params string[] includes);

        void Dispose();

    }
}
