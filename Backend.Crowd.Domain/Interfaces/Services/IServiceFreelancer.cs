﻿using Backend.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Backend.Crowd.Domain.Interfaces.Services
{
    public interface IServiceFreelancer : IServiceBase<Freelancer>
    {
        IEnumerable<Freelancer> GetAll(int page, int amount, ref int totalRecords);

        IEnumerable<Freelancer> Search(String query, String city, int minValueHour, int maxValueHour, int categoriaId, int[] skillId, int availabilityId, int segmentId, int page, int amount, ref int totalRecords, params string[] includes);

        Freelancer GetByCode(string code);
        Freelancer GetByEmail(string email);
        Freelancer GetByPhone(string phone);
        Freelancer GetByIdAndEmail(int id, string email);
        Freelancer GetByIdAndPhone(int id, string phone);
        Tuple<bool, bool> GetByEmailOrPhone(string email, string phone);
        Tuple<bool, bool> GetByEmailOrPhone(int id, string email, string phone);
    }
}
