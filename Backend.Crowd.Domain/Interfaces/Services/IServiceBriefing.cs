﻿using Backend.Crowd.Domain.Entities;
using System.Collections.Generic;

namespace Backend.Crowd.Domain.Interfaces.Services
{
    public interface IServiceBriefing : IServiceBase<Briefing>
    {
        IEnumerable<BriefingList> GetBriefing(User user);
        Briefing GetBriefing(string token);
        void ClearTextBriefing(Briefing briefing);
        void UpdateBriefingFreelancers(Briefing briefing);
        Briefing Buscar(int id);
    }
}
