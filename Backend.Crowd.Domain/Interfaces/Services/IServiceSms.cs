﻿using Backend.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Interfaces.Services
{
    public interface IServiceSms : IServiceBase<Sms>
    {
        void Create(int usuario, string from, string to, string template, Dictionary<string,string> variables);
    }
}
