﻿using Backend.Crowd.Domain.Entities;
using System;

namespace Backend.Crowd.Domain.Interfaces.Services
{
    public interface IServiceUser : IServiceBase<User>
    {
        User Authentication(String name, String password);

        User Authentication(String name);

        Guid? RecoverPassword(String email);

        bool ChangePassword(String email, String newPassword, String guid);

        bool ChangePassword(int id, String newPassword);

        User GetByEmail(string email);
        User GetByPhone(string phone);
        User GetByIdAndEmail(int id, string email);
        User GetByIdAndPhone(int id, string phone);
        Tuple<bool, bool> GetByEmailOrPhone(string email, string phone);
        Tuple<bool, bool> GetByEmailOrPhone(int id, string email, string phone);

        void SendConfirmationEmail(User user);
    }
}
