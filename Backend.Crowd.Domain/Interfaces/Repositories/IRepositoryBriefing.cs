﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Repositories;
using System.Collections.Generic;

namespace Backend.Crowd.Domain.Interfaces.Repositories
{
    public interface IRepositoryBriefing : IRepositoryBase<Briefing>
    {
        IEnumerable<BriefingList> GetList(string query,params object[] parameters);
        void ExecuteCommand(string query);
        Briefing Buscar(int id);
    }
}