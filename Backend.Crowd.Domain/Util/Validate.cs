﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Backend.Crowd.Domain.Util
{
    public static class Validador
    {
        public static bool isCPFCNPJ(string cpfcnpj, bool vazio)
        {
            if (string.IsNullOrEmpty(cpfcnpj))
                return vazio;
            else
            {
                int[] d = new int[14];
                int[] v = new int[2];
                int j, i, soma;
                string Sequencia, SoNumero;

                SoNumero = Regex.Replace(cpfcnpj, "[^0-9]", string.Empty);

                //verificando se todos os numeros são iguais
                if (new string(SoNumero[0], SoNumero.Length) == SoNumero) return false;

                // se a quantidade de dígitos numérios for igual a 11
                // iremos verificar como CPF
                if (SoNumero.Length == 11)
                {
                    for (i = 0; i <= 10; i++) d[i] = Convert.ToInt32(SoNumero.Substring(i, 1));
                    for (i = 0; i <= 1; i++)
                    {
                        soma = 0;
                        for (j = 0; j <= 8 + i; j++) soma += d[j] * (10 + i - j);

                        v[i] = (soma * 10) % 11;
                        if (v[i] == 10) v[i] = 0;
                    }
                    return (v[0] == d[9] & v[1] == d[10]);
                }
                // se a quantidade de dígitos numérios for igual a 14
                // iremos verificar como CNPJ
                else if (SoNumero.Length == 14)
                {
                    Sequencia = "6543298765432";
                    for (i = 0; i <= 13; i++) d[i] = Convert.ToInt32(SoNumero.Substring(i, 1));
                    for (i = 0; i <= 1; i++)
                    {
                        soma = 0;
                        for (j = 0; j <= 11 + i; j++)
                            soma += d[j] * Convert.ToInt32(Sequencia.Substring(j + 1 - i, 1));

                        v[i] = (soma * 10) % 11;
                        if (v[i] == 10) v[i] = 0;
                    }
                    return (v[0] == d[12] & v[1] == d[13]);
                }
                // CPF ou CNPJ inválido se
                // a quantidade de dígitos numérios for diferente de 11 e 14
                else return false;
            }
        }

        public static string GenerateCheckDigit(string cusip)
        {
            int sum = 0;
            char[] digits = cusip.ToUpper().ToCharArray();
            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ*@#";

            for (int i = 0; i < digits.Length; i++)
            {
                int val;
                if (!int.TryParse(digits[i].ToString(), out val))
                    val = alphabet.IndexOf(digits[i]) + 10;

                if ((i % 2) != 0)
                    val *= 2;

                val = (val % 10) + (val / 10);

                sum += val;
            }

            int check = (10 - (sum % 10)) % 10;

            return check.ToString();
        }

        public static string RemoveSpecialCharacters(string str)
        {
            str = str.RemoverAcentuacao();
            str = str.Replace("/", " ");
            str = Regex.Replace(str, "[^a-zA-Z0-9_ ]+", "", RegexOptions.Compiled).Replace(" ","-").Replace("--","-").Replace("--", "-");
            if (str.StartsWith("-"))
                str = str.Remove(0,1);

            return str;
        }

        public static string RemoverAcentuacao(this string text)
        {
            return new string(text
                .Normalize(NormalizationForm.FormD)
                .Where(ch => char.GetUnicodeCategory(ch) != UnicodeCategory.NonSpacingMark)
                .ToArray());
        }

        public static string ClearPhone(string phone)
        {
            return phone
                .Replace("(","")
                .Replace(")","")
                .Replace(",","")
                .Replace(".","")
                .Replace("-","")
                .Replace(" ","");
        }

        public static string ClearText(string text)
        {
            Regex regExp = new Regex(@"(?!<[^>]*>)\d{4,5}(.?)\d{4,5}");

            if (regExp.IsMatch(text))
            {
                foreach (Match match in regExp.Matches(text))
                {
                    text = text.Remove(match.Index, match.Length);
                    text = text.Insert(match.Index, "<strong style = \"background: grey;color: wheat;\"><i class=\"fa fa-exclamation-triangle\"></i> Informações de contato disponíveis no perfil do profissional</strong>");
                }
            }

            Regex regMail = new Regex("(?(" + '\u0022' + ")(" + '\u0022' + @".+?(?<!\\)" + '\u0022' + @"@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\\{\}\|~\w])*)(?<=[0-9a-z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))");

            if (regMail.IsMatch(text.ToLower()))
            {
                foreach (Match match in regMail.Matches(text.ToLower()))
                {
                    text = text.Remove(match.Index, match.Length);
                    text = text.Insert(match.Index, "<strong style = \"background: grey;color: wheat;\"><i class=\"fa fa-exclamation-triangle\"></i> Informações de contato disponíveis no perfil do profissional</strong>");
                }
            }

            return text;
        }
    }
}
