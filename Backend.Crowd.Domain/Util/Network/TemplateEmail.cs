﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Backend.Crowd.Domain.Util.Network
{
    public class TemplateEmail : ITemplate
    {
        Hashtable _section;
        internal readonly string _dir;

        public TemplateEmail()
        {
            _section = (Hashtable)ConfigurationManager.GetSection("HtmlTemplates");
            _dir = _section["Dir"].ToString();
        }

        public virtual string Generate(string template,Dictionary<string,string> parameters)
        {
            string body;
            using (StreamReader reader = new StreamReader(Path.Combine(_dir,_section[template].ToString())))
            {
                body = reader.ReadToEnd();
                if (parameters != null)
                    foreach (var key in parameters.Keys)
                        body = body.Replace(String.Concat("{",key,"}"), parameters[key]);
            }

            return body;
        }
    }
}
