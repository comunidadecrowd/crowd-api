﻿using Intercom;
using Mandrill;
using Mandrill.Models;
using Mandrill.Requests.Messages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;



namespace Backend.Crowd.Domain.Util.Network
{
    public class Email : IEmail
    {
        private readonly ICommunication _serviceIntercom;
        private readonly ITemplate _templateEmail;

        public Email()
        {

        }

        public Email(ICommunication serviceIntercom, ITemplate templateEmail)
        {
            this._serviceIntercom = serviceIntercom;
            this._templateEmail = templateEmail;
        }

        public void Send(string to, string name, string subject, string template,
            Dictionary<string, string> variables = null)
        {
            var mandrill = new MandrillApi(ConfigurationManager.AppSettings["MandrillKey"]);
            var mandrillRecipients = new List<EmailAddress>();

            mandrillRecipients.Add(new EmailAddress() { Email = to, Name = name });

            var email = new EmailMessage
            {
                To = mandrillRecipients,
                FromEmail = ConfigurationManager.AppSettings["MandrillFrom"],
                FromName = ConfigurationManager.AppSettings["MandrillFromName"],
                Subject = subject,
            };

            email.AddRecipientVariable(to, "NOME", name);

            if (variables != null)
                foreach (var variable in variables.Keys)
                    if (!variable.Equals("NOME"))
                        email.AddRecipientVariable(to, variable, variables[variable]);
            mandrill.SendMessageTemplate(new SendMessageTemplateRequest(email, template, null));
        }

        public void NewSend(string to, string from, string toType, string subject, string template,
            Dictionary<string, string> variables = null)
        {
            var admin = _serviceIntercom.GetAdmin(from);

            var message = new Intercom.Data.AdminConversationMessage(
                from: new Intercom.Data.AdminConversationMessage.From(admin.id),
                to: new Intercom.Data.AdminConversationMessage.To(type: toType, user_id: to),
                message_type: Intercom.Data.AdminConversationMessage.MessageType.EMAIL,
                template: Intercom.Data.AdminConversationMessage.MessageTemplate.PLAIN,
                subject: subject,
                body: @_templateEmail.Generate(template, variables)
                );

            _serviceIntercom.StartConversation(message);
        }
    }
}
