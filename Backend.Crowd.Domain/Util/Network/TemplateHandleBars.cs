﻿using HandlebarsDotNet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Util.Network
{
    public class TemplateHandleBars : ITemplateEmail
    {
        Hashtable _section;
        internal readonly string _dir;

        public TemplateHandleBars()
        {
            _section = (Hashtable)ConfigurationManager.GetSection("HtmlTemplates");
            _dir = _section["Dir"].ToString();
        }
        public string Generate(string template, Dictionary<string, string> parameters)
        {
            var source = File.ReadAllText(String.Concat(_dir, _section[template].ToString()));
            var handleBars = Handlebars.Compile(source);

            return handleBars(parameters);
        }
    }
}
