﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace Backend.Crowd.Domain.Util.Network
{
    public class TemplateSms : ITemplate
    {
        Hashtable _section;
        internal readonly string _dir;

        public TemplateSms()
        {
            _section = (Hashtable)ConfigurationManager.GetSection("SmsTemplates");
            _dir = _section["Dir"].ToString();
        }

        public virtual string Generate(string template,Dictionary<string,string> parameters)
        {
            string body;
            var path = String.Concat(_dir, "/", _section[template].ToString());
            using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath(path)))
            {
                body = reader.ReadToEnd();
                if (parameters != null)
                    foreach (var key in parameters.Keys)
                        body = body.Replace(String.Concat("{",key,"}"), parameters[key]);
            }

            return body;
        }
    }
}
