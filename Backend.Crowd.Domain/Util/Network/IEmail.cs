﻿using System.Collections.Generic;

namespace Backend.Crowd.Domain.Util.Network
{
    public interface IEmail
    {
        void NewSend(string to, string from, string toType, string subject, string template, Dictionary<string, string> variables = null);
        void Send(string to, string name, string subject, string template, Dictionary<string, string> variables = null);
    }
}