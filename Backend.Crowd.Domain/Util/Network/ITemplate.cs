﻿using System.Collections.Generic;

namespace Backend.Crowd.Domain.Util.Network
{
    public interface ITemplate
    {
        string Generate(string template, Dictionary<string, string> parameters);
    }
}