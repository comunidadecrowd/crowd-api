﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Domain.Util
{
    public class PropertiesRule
    {
        public struct Rule
        {
            public string propertyName;
            public string propetyType;
            public Func<object, object> ruleFunction;
        };
        List<string> propertiesToApply;
        List<Rule> rules;
        public PropertiesRule()
        {
            propertiesToApply = new List<string>();
            rules = new List<Rule>();
        }
        public void AddRule(Rule rule)
        {
            propertiesToApply.Add(rule.propertyName);
            rules.Add(rule);
        }
        public string[] GetPropertiesRange()
        {
            return propertiesToApply.ToArray();
        }
        public object ApplyRule(object instance, PropertyInfo prop)
        {
            return rules[propertiesToApply.IndexOf(prop.Name)].ruleFunction(prop.GetValue(instance));
        }

        public static object nullString(object value)
        {
            return null;
        }

        public static object longStringToShortString(object value)
        {
            return value.ToString().Split(' ')[0] + " .";
        }

        public static object shortenName(object value)
        {
            var splited_name = value.ToString().Split(' ');
            return splited_name[0] + " " + splited_name[splited_name.Length-1][0] + ".";
        }

        public static object decimalToZero(object value)
        {
            return Decimal.Zero;
        }
    }

    
}
