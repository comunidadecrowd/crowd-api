﻿using Backend.Crowd.Domain.Entities;
using System.Collections.Generic;
using System.Configuration;

namespace ServiceSMS
{
    internal class SmsPost
    {

        public int IdUser { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Template { get; set; }
        public KeyValuePair<string, string>[] Variables { get; set; }

        public SmsPost(Customer customer,string template)
        {
            IdUser = 1;
            From = "Crowd";

            Template = template;
            To = customer.Phone;
            Variables = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>( "primeiro nome", customer.NameResponsible.IndexOf(' ') == -1 ? customer.NameResponsible : customer.NameResponsible.Substring(0,customer.NameResponsible.IndexOf(' '))),
                new KeyValuePair<string, string>( "crowd", ConfigurationManager.AppSettings["CrowdWeb"].ToString())
            }.ToArray();
        }

        public SmsPost(Freelancer freelancer, string template)
        {
            IdUser = 1;
            From = "Crowd";

            Template = template;
            To = freelancer.Phone;
            Variables = new List <KeyValuePair<string, string>>
            {
                 new KeyValuePair<string, string>( "primeiro nome", freelancer.Name.IndexOf(' ') == -1 ? freelancer.Name : freelancer.Name.Substring(0,freelancer.Name.IndexOf(' '))),
                 new KeyValuePair<string, string>( "crowd", ConfigurationManager.AppSettings["CrowdWeb"].ToString())
            }.ToArray();
        }
    }
}