﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Infrasctructure.Database.Context;
using Backend.Crowd.Infrasctructure.Database.Repositories;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceSMS
{
    class Program
    {
        static void Main(string[] args)
        {
            var crowdContext = new CrowdContext();

            //Lead Empresa 1 dia
            //SendCustomer(crowdContext, 1);

            //Lead Empresa 5 dias
            SendCustomer(crowdContext, 5);

            //Lead Freelancer 1 dia
            SendFreelancer(crowdContext, 1);

            //Lead Empresa 5 dias
            SendFreelancer(crowdContext, 5);

        }

        private static void SendFreelancer(CrowdContext context, int days)
        {
            var repositoryFreelancer = new RepositoryBase<Freelancer>(context);
            var serviceFreelancer = new ServiceFreelancer(repositoryFreelancer);

            string template = String.Format("lead-freelancer{0}", days);

            var today = DateTime.Now.AddDays(-days).Date;


            List<Freelancer> freelancers = serviceFreelancer.GetAll(c => c.Prospect.Value == true).Where(c => c.CreatedAt.Date == today).ToList();
            if (today.DayOfWeek == DayOfWeek.Monday)
            {
                freelancers.AddRange(serviceFreelancer.GetAll(c => c.Prospect.Value == true).Where(c => c.CreatedAt.Date == today.AddDays(-1) || c.CreatedAt.Date == today.AddDays(-2)));
            }

            freelancers.ForEach(c => SendSms(new SmsPost(c, template)));
        }

        private static void SendCustomer(CrowdContext context, int days)
        {
            var repositoryCustomer = new RepositoryBase<Customer>(context);
            var serviceCustomer = new ServiceBase<Customer>(repositoryCustomer);

            string template = String.Format("lead-empresa{0}", days);

            var today = DateTime.Now.AddDays(-days).Date;

            List<Customer> customers = serviceCustomer.GetAll(c => c.Prospect.Value==true).Where(c => c.CreatedAt.Date == today).ToList();

            if (today.DayOfWeek == DayOfWeek.Monday)
            {
                customers.AddRange(serviceCustomer.GetAll(c => c.Prospect.Value==true).Where(c => c.CreatedAt.Date == today.AddDays(-1) || c.CreatedAt.Date == today.AddDays(-2)));
            }

            customers.ForEach(c => SendSms(new SmsPost(c, template)));
        }

        private static void SendSms(SmsPost sms)
        {
            var apiaddress = ConfigurationManager.AppSettings["CrowdAPI"].ToString();
            var client = new RestClient(apiaddress);

            var request = new RestRequest("Sms", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Accept", "application/json");
            request.AddHeader("crowd", ConfigurationManager.AppSettings["SMSToken"].ToString());
            request.AddJsonBody(sms);

            IRestResponse response = client.Execute(request);

            if (!response.IsSuccessful)
                Console.WriteLine(response.ErrorMessage);
        }
    }
}
