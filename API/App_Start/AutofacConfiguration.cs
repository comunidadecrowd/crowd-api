﻿using Autofac;
using Autofac.Integration.WebApi;
using Backend.Crowd.Domain.Interfaces.Repositories;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Domain.Util;
using Backend.Crowd.Infrasctructure.Database.Context;
using Backend.Crowd.Infrasctructure.Database.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Algolia.API;
using Autofac.Core;
using CorePayment;
using Iugu;
using Intercom;
using Backend.Crowd.Domain.Util.Network;
using AutoMapper;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Serilog;
using AutofacSerilogIntegration;
using System.Data.Entity;

namespace API.App_Start
{
    public static class AutofacConfiguration
    {
        public static void Init()
        {
            
         
            

            var builder = new ContainerBuilder();

            

            //register your configuration as a single instance
            builder.Register(c => new MapperConfiguration(cfg =>
            {
               
                    cfg.AddProfile(new MapProfile());

            })).AsSelf().SingleInstance();

            //register your mapper
            builder.Register(c => c.Resolve<MapperConfiguration>().CreateMapper(c.Resolve)).As<IMapper>().InstancePerLifetimeScope();

            Log.Logger = new LoggerConfiguration()
            .ReadFrom.AppSettings()
            .CreateLogger();


            List<Parameter> parameterssearch =
                new List<Parameter>
                {
                    new NamedParameter("appid", ConfigurationManager.AppSettings["AlgoliaId"]),
                    new NamedParameter("apikey", ConfigurationManager.AppSettings["AlgoliaApiKey"]),
                    new NamedParameter("indexname", ConfigurationManager.AppSettings["AlgoliaIndex"])
                };

            List<Parameter> parameterspayment =
                new List<Parameter>
                {
                    new NamedParameter("apikey", ConfigurationManager.AppSettings["PaymentApiKey"]),
                };

            List<Parameter> parameterscommunicator =
                new List<Parameter>
                {
                    new NamedParameter("accessToken", ConfigurationManager.AppSettings["IntercomToken"]),
                };

            builder.RegisterApiControllers(typeof(Startup).Assembly);
            builder.RegisterType<CrowdContext>().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(RepositoryBase<>)).As(typeof(IRepositoryBase<>)).InstancePerRequest();
            builder.RegisterType<RepositoryBriefing>().As<IRepositoryBriefing>().InstancePerRequest();
            builder.RegisterGeneric(typeof(ServiceBase<>)).As(typeof(IServiceBase<>)).InstancePerRequest();
            builder.RegisterType(typeof(ServiceFreelancer)).As(typeof(IServiceFreelancer)).InstancePerRequest();
            builder.RegisterType(typeof(ServiceUser)).As(typeof(IServiceUser)).InstancePerRequest();
            builder.RegisterType(typeof(ServiceSms)).As(typeof(IServiceSms)).InstancePerRequest();
            builder.RegisterType(typeof(ServiceBriefing)).As(typeof(IServiceBriefing)).InstancePerRequest();
            builder.RegisterType<AlgoliaSearch>().As<ISearch>().WithParameters(parameterssearch).InstancePerRequest();
            builder.RegisterType<IuguServer>().As<IPayment>().WithParameters(parameterspayment).InstancePerRequest();
            builder.RegisterType<IntercomServer>().As<ICommunication>().WithParameters(parameterscommunicator).InstancePerRequest();
            builder.RegisterType<TemplateEmail>().As<ITemplate>().InstancePerRequest();
            builder.RegisterType<Email>().As<IEmail>().InstancePerRequest();
            builder.RegisterType<ServiceTracking>().PropertiesAutowired();
            //builder.RegisterType<TrackingAttribute>().PropertiesAutowired();
            builder.RegisterLogger();
            builder.RegisterType<RepositoryBase<Tracking>>().As<IRepositoryBase<Tracking>>().InstancePerLifetimeScope();
            builder.RegisterWebApiFilterProvider(GlobalConfiguration.Configuration);
            // Add your registrations

            var container = builder.Build();

            //return container;

            // Set the dependency resolver for Web API.
            var webApiResolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = webApiResolver;
        }
    }
}