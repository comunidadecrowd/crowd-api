using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using System.Web.Http.Cors;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RollbarDotNet;
using System.Configuration;
using API.App_Start;

namespace API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.

            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;

            //config.SuppressDefaultHostAuthentication();

            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            var jsonMF = new JsonMediaTypeFormatter();
            jsonMF.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            jsonMF.SerializerSettings.Formatting = Formatting.Indented;
            jsonMF.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            jsonMF.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            jsonMF.SerializerSettings.Converters.Add(new StringEnumConverter());
            jsonMF.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;

            //config.Formatters.RemoveAt(0);
            //config.Formatters.Insert(0, jsonMF);
            config.Formatters.Add(jsonMF);

            config.Formatters.JsonFormatter.SerializerSettings.DateFormatString = "dd/MM/yyyy HH:mm";


            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("multipart/form-data"));

            config.Filters.Add(new RollbarExceptionFilter());

        }
    }
}
