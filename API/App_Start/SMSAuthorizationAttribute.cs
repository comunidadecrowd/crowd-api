﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace API
{
    public class SMSAuthorizationAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {

            IEnumerable<string> values;
            string key = string.Empty;
            if (actionContext.Request.Headers.TryGetValues("crowd", out values))
            {
                key = values.First();
            }
            
                

             if (key != ConfigurationManager.AppSettings["SMSToken"].ToString() )
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);

            base.OnAuthorization(actionContext);
        }
    }
}