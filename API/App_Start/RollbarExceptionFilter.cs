﻿using System;
using RollbarDotNet;
using System.Web.Http.Filters;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using Exception = RollbarDotNet.Exception;

namespace API
{
    public class RollbarExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (!(context.Exception is OperationCanceledException))
            {
                Rollbar.PersonData(() => null);
                try
                {
                    var handler = new JwtSecurityTokenHandler();
                    var jsonToken = handler.ReadToken(context.Request.Headers.Authorization.Parameter) as JwtSecurityToken;
                    var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;
                    var id = jsonToken.Claims.First(claim => claim.Type == "nameid").Value;

                    var person = new Person(id);
                    person.Email = unique_name;
                    person.UserName = unique_name;

                    Rollbar.PersonData(() => person);
                }
                catch
                {

                }
                finally
                {
                    var custom = new Dictionary<string, object>();

                    custom.Add("Method", context.Request.Method);
                    custom.Add("RequestUri", context.Request.RequestUri);
                    custom.Add("Host", context.Request.Headers.Host);
                    custom.Add("RequestBody", context.Request.Content);
                    try
                    {
                        Rollbar.Report(context.Exception, ErrorLevel.Error, custom);
                    }
                    catch
                    {

                    }
                }
            }
        }
    }



}