﻿using AutoMapper;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<Customer, Intercom.Data.Company>()
                            .ForMember(d => d.company_id, opt => opt.MapFrom(s => s.Id))
                            .ForMember(d => d.name, opt => opt.MapFrom(s => s.Trade))
                            .ForMember(d => d.plan, opt => opt.MapFrom(s => new Intercom.Data.Plan
                            {
                                id = s.PlanType.ToString(),
                                name = Enum.GetName(typeof(PlanType), s.PlanType)
                            }))
                            .ForAllOtherMembers(opt=> opt.Ignore());
                           

            CreateMap<User, Intercom.Data.Contact>()
                .ForMember(d => d.name,opt => opt.MapFrom(c=> c.Name))
                .ForMember(d => d.phone, opt => opt.MapFrom(c => c.Phone))
                .ForMember(d => d.email, opt => opt.MapFrom(c => c.Email))
                .ForMember(d=> d.custom_attributes, opt => opt.UseValue(new Dictionary<string, object>()))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<User, Intercom.Data.User>()
                .ForMember(d => d.user_id, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.name, opt => opt.MapFrom(c => c.Name))
                .ForMember(d => d.phone, opt => opt.MapFrom(c => c.Phone))
                .ForMember(d => d.email, opt => opt.MapFrom(c => c.Email))
                .ForMember(d => d.last_request_at, opt => opt.UseValue(DateTimeOffset.Now.ToUnixTimeSeconds()))
                .ForMember(d => d.avatar, opt => opt.MapFrom(s => new Intercom.Data.Avatar() { image_url = s.Photo }))
                .ForMember(d => d.custom_attributes, opt => opt.MapFrom(s => new Dictionary<string, object>
                                                                                {
                                                                                                { "role", s.Role },
                                                                                                { "active", s.Active.ToString() },
                                                                                                { "photo", s.Photo },
                                                                                                { "roleCompany", s.RoleCompany },
                                                                                                { "acceptTerms", s.AcceptTerms.ToString() },
                                                                                                { "hash", s.ConfirmEmail.ToString() },
                                                                                                { "activechange", s.ActiveChange }
                                                                                }))
             .ForAllOtherMembers(opt => opt.Ignore());

        }
    }
}