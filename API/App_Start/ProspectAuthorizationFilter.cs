﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace API
{
    public class ProspectAuthorizationAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var handler = new JwtSecurityTokenHandler();

            var jsonToken = handler.ReadToken(actionContext?.Request?.Headers?.Authorization?.Parameter) as JwtSecurityToken;
            var claim = jsonToken?.Claims?.First(c => c.Type == "Prospect");
            if (claim != null)
            {
                var prospect = Convert.ToBoolean(claim.Value);

                if (prospect)
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.PreconditionFailed, "User in prospect");
            }

            base.OnAuthorization(actionContext);
        }
    }
}