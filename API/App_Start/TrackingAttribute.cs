﻿using API.Models;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace API.App_Start
{
    public class TrackingAttribute : Attribute, IActionFilter
    {

        public ServiceTracking ServiceTracking { get; set; }
        public ServiceUser ServiceUser { get; set; }

        public TrackingAttribute()
        {

        }

        public bool AllowMultiple => true;

        public Task<HttpResponseMessage> ExecuteActionFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            var result = continuation();

            result.Wait();


            var track = new Tracking
            {
                Action = actionContext.ActionDescriptor.ActionName,
                Controller = actionContext.ControllerContext.ControllerDescriptor.ControllerName,
                Arguments = Process((ITracking)actionContext.ControllerContext.Controller, actionContext.ActionDescriptor.ActionName, actionContext.ActionArguments)
            };

            if (actionContext.Request.Headers.Authorization != null)
                track.Token = actionContext.Request.Headers.Authorization.Parameter;

            try
            {
                var claims = GetClaim(actionContext.Request);
                var user = claims["nameid"];
                track.IdUser = Int32.Parse(user);
            }
            catch (Exception ex)
            {
                track.IdUser = 0;
            }

            if (actionContext.Request.Headers.Contains("state"))
            {
                track.State = actionContext.Request.Headers.GetValues("state").FirstOrDefault();
            }

            GetEnvironment(actionContext, track);

            //var retorno = result.Result.Content.ReadAsFormDataAsync();

            ServiceTracking.Add(track);

            return result;
        }

        private static void GetEnvironment(HttpActionContext actionContext, Tracking track)
        {
            if (actionContext.Request.RequestUri.Authority.ToLower().StartsWith("beta"))
                track.Environment = "beta";
            else if (actionContext.Request.RequestUri.Authority.ToLower().StartsWith("homolog"))
                track.Environment = "homolog";
            else if (actionContext.Request.RequestUri.Authority.ToLower().StartsWith("localhost"))
                track.Environment = "dev";
            else
                track.Environment = "producao";
        }

        private List<Argument> Process(ITracking controller,string action,Dictionary<string, object> actionArguments)
        {
            var arguments = new List<Argument>();

            arguments.AddRange(controller.Processor(action, actionArguments));

            return arguments;
        }

        private Dictionary<String,String> GetClaim(HttpRequestMessage request)
        {
            var handler = new JwtSecurityTokenHandler();

            var jsonToken = handler.ReadToken(request.Headers.Authorization.Parameter) as JwtSecurityToken;
            return jsonToken.Claims.ToDictionary(c => c.Type,c=> c.Value);
        }
    }
}