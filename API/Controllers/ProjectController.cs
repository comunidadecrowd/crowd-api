using API.Models;
using API.Models.Projects;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    [Authorize]
    [RoutePrefix("Project")]
    public class ProjectController : BaseAPIController
    {
        private IServiceBase<Project> _serviceProject;
        private IServiceBase<Tasks> _serviceTask;
        private IServiceBase<Customer> _serviceCustomer;
        private IServiceUser _serviceUser;


        public ProjectController()
        {
        }

        public ProjectController(IServiceBase<Project> serviceProject, IServiceBase<Customer> serviceCustomer,
            IServiceUser serviceUser, IServiceBase<Tasks> serviceTask)
        {
            _serviceProject = serviceProject;
            _serviceCustomer = serviceCustomer;
            _serviceUser = serviceUser;
            _serviceTask = serviceTask;
        }

        [HttpPost()]
        [Route("Add")]
        public HttpResponseMessage Add(ProjectAddViewModel model)
        {
            var user = _serviceUser.GetById(model.IdUser);
            string[] colors = {
                "red",
                "pink",
                "lightpink",
                "purple",
                "lightpurple",
                "greypurple",
                "blue",
                "lightblue",
                "xlightblue",
                "orange",
                "yellow",
                "lightyellow"
            };

            if (user == null || user.Customer == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "Client doesn't exist");

            var rnd = new Random();
            int colorindex = rnd.Next(0, colors.Length);

            var project = new Project()
            {
                Name = model.Name,
                Color = colors[colorindex],
                IdCustomer = (int)user.IdCustomer
            };
            _serviceProject.Add(project);

            model.Id = project.Id;

            return Request.CreateResponse(HttpStatusCode.OK, new { model });
        }

        [HttpGet()]
        //[AllowAnonymous]
        [Route("List")]
        public IHttpActionResult List(int userId, bool status)
        {
            var user = _serviceUser.GetById(userId);

            if (user == null || user.Customer == null)
                return Ok();

            IEnumerable<Project> projects;

            projects = _serviceProject.GetAll(x => x.Active == status && x.IdCustomer == user.Customer.Id, ascending: false, lazyLoadEnabled: false);

            projects.ToList().ForEach(x =>
            {
                if (user.Role == RoleType.FREELANCER)
                    x.TaskCount = _serviceTask.GetAll(y => y.IdProject == x.Id && y.Messages.Any(z => !z.Read && z.User == null && z.FreelancerId == user.IdFreelancer)).Count();
                else if (user.Role == RoleType.CLIENT || user.Role == RoleType.EMPLOYEE)
                    x.TaskCount = _serviceTask.GetAll(y => y.IdProject == x.Id && y.Messages.Any(z => !z.Read && z.UserId != userId && z.Task.IdUser == userId)).Count();
                else
                    x.TaskCount = _serviceTask.GetAll(y => y.IdProject == x.Id && y.Messages.Any(z => !z.Read)).Count();

                x.Customer = null;
            });

            return Ok(projects.OrderByDescending(p => p.CreatedAt));
        }

        [HttpPost()]
        [Route("Edit")]
        public HttpResponseMessage Edit(ProjectAddViewModel model)
        {
            var user = _serviceUser.GetById(model.IdUser);

            if (user == null || user.Customer == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "Client doesn't exist");

            var project = _serviceProject.GetById(model.Id);
            project.Name = model.Name;

            _serviceProject.Update(project);

            return Request.CreateResponse(HttpStatusCode.OK, new { model });
        }

        [HttpGet()]
        [Route("Get")]
        public IHttpActionResult Get(int projectId)
        {
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
            var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;
            var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

            var project = _serviceProject.GetById(projectId, false);

            if (requestUser.IdCustomer != project.IdCustomer && requestUser.Role != RoleType.MASTER)
                return Content(HttpStatusCode.Forbidden, "You don't have permission to access this project.");

            return Ok(project);
        }


        [HttpPost]
        [Route("ChangeStatus")]
        public HttpResponseMessage ChangeStatus(int projectId, bool status)
        {
            HttpResponseMessage response = null;
            if (ModelState.IsValid)
            {
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
                var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;
                var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

                var project = _serviceProject.GetById(projectId);

                if (requestUser.IdCustomer != project.IdCustomer && requestUser.Role != RoleType.MASTER)
                    return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "You don't have permission to access this project.");

                project.Active = status;

                _serviceProject.Update(project);

                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpPost]
        [Route("EditName")]
        public HttpResponseMessage EditName(int project_id, string name)
        {
            HttpResponseMessage response = null;


            if (ModelState.IsValid)
            {
                var project = _serviceProject.GetById(project_id);
                var handler = new JwtSecurityTokenHandler();

                var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
                var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

                var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

                if (project == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Project does not exist");

                project.Name = name;
                _serviceProject.Update(project);

                response = Request.CreateResponse(HttpStatusCode.OK);

            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }




    }
}
