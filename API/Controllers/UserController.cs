using API.Models;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Domain.Entities;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using API.Security;
using System.IdentityModel.Protocols.WSTrust;
using Backend.Crowd.Domain.Interfaces.Repositories;
using System.Web;
using System.IO;
using Backend.Crowd.Domain.Util;
using System.Drawing;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Util.Security;
using Backend.Crowd.Domain.Util.Network;
using Backend.Crowd.Domain.Interfaces.Services;
using System.Configuration;
using System.Reflection;
using Algolia.API;
using API.Models.Portfolio;
using API.Models.Tasks;
using Intercom;
using AutoMapper;
using API.Models.Freelancer;
using FluentValidation.Results;
using API.Models.freelancer;
using API.App_Start;
using API.Models.User;

namespace API.Controllers
{
    [RoutePrefix("User")]
    public class UserController : BaseAPIController, ITracking
    {

        private new IServiceUser _serviceUser;
        private IServiceFreelancer _serviceFreelancer;
        private IServiceBase<Freelancer.Skill> _serviceSkill;
        private IServiceBase<Freelancer.Segment> _serviceSegment;
        private IServiceBase<City> _serviceCity;
        private IServiceBase<State> _serviceState;
        private IServiceBase<Freelancer.Category> _serviceCategory;
        private IServiceBase<Freelancer.Experience> _serviceFreelancerExperience;
        private IServiceBase<Freelancer.Award> _serviceFreelancerAwards;
        private IServiceBase<Freelancer.RatingFreelancer> _serviceRating;
        private IServiceBase<Customer> _serviceCustomer;

        private IServiceBase<User.ActionLog> _serviceActionLog;

        private IServiceBase<Freelancer.Language> _serviceLanguage;

        private IServiceBase<Tasks> _serviceTasks;

        private IServiceBase<Freelancer.Portfolio> _servicePortfolio;

        private readonly ISearch _searchservice;

        private ICommunication _serviceIntercom;

        private IMapper _map;

        private IEmail _sendEmail;

        public UserController()
        {
        }

        public UserController(
            IServiceUser serviceUser,
            IServiceFreelancer serviceFreelancer,
            IServiceBase<Freelancer.Skill> serviceSkill,
            IServiceBase<Freelancer.Segment> serviceSegment,
            IServiceBase<City> serviceCity,
            IServiceBase<State> serviceState,
            IServiceBase<Freelancer.Category> serviceCategory,
            IServiceBase<Freelancer.Experience> serviceFreelancerExperience,
            IServiceBase<Freelancer.RatingFreelancer> serviceRating,
            IServiceBase<Freelancer.Award> serviceFreelancerAwards,
            IServiceBase<Customer> serviceCustomer,
            IServiceBase<Freelancer.Language> serviceLanguage,
            IServiceBase<User.ActionLog> serviceActionLog,
            IServiceBase<Tasks> serviceTasks,
            IServiceBase<Freelancer.Portfolio> servicePortfolio,
            ISearch searchservice,
            ICommunication serviceIntercom,
            IEmail sendEmail,
            IMapper map,
            Serilog.ILogger log
        ) : base(serviceUser, serviceActionLog, log)
        {
            _serviceUser = serviceUser;
            _serviceFreelancer = serviceFreelancer;
            _serviceSkill = serviceSkill;
            _serviceSegment = serviceSegment;
            _serviceCity = serviceCity;
            _serviceState = serviceState;
            _serviceCategory = serviceCategory;
            _serviceFreelancerExperience = serviceFreelancerExperience;
            _serviceRating = serviceRating;
            _serviceFreelancerAwards = serviceFreelancerAwards;
            _serviceCustomer = serviceCustomer;
            _serviceLanguage = serviceLanguage;
            _serviceActionLog = serviceActionLog;
            _serviceTasks = serviceTasks;
            _servicePortfolio = servicePortfolio;
            _searchservice = searchservice;
            _serviceIntercom = serviceIntercom;
            _map = map;
            _sendEmail = sendEmail;
        }


        /// <summary>
        /// Login User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Login")]
        public HttpResponseMessage Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                _log.Debug("Login: {@Model}", model);
                _log.Information("Login: {email}", model.Email);

                var user = _serviceUser.GetByEmail(model.Email);

                var freelancer = _serviceFreelancer.GetByExpression(x => x.Email == model.Email);

                //if (!string.IsNullOrEmpty(user.Password) && !user.IsProspect)
                //{
                //    // Confirmar
                //    var erro = String.Format
                //    (
                //        $"Um email foi enviado para <strong>{user.Email.ToLower()}</strong> " +
                //        "com as instruções para recuperação da senha."
                //    );

                //    _log.Error("Login: Error: {erro}", erro);

                //    return Request.CreateResponse(HttpStatusCode.ExpectationFailed, erro);
                //}
                if (user == null && freelancer == null)
                {
                    var erro = "Usuario nao existe.";

                    _log.Error("Login: Error: {erro}", erro);

                    return Request.CreateResponse(HttpStatusCode.Unauthorized, erro);
                    // nao tem usuario mas tem freelancer
                }
                else if (user == null && freelancer != null)
                {
                    var erro = "Freelancer sem usuário.";

                    _log.Error("Login: Error: {erro}", erro);

                    user = CreateUserfromFreelancer(freelancer);

                    return Request.CreateResponse(HttpStatusCode.Unauthorized, erro);

                }
                else if (user.IsCustomer && !user.Customer.Active)
                {
                    var erro = "User inativo.";

                    _log.Error("Login: Error: {erro}", erro);

                    return Request.CreateResponse(HttpStatusCode.Unauthorized, erro);
                }
                // usuario inativo
                else if (!user.Active)
                {
                    var erro = "User inativo.";

                    _log.Error("Login: Error: {erro}", erro);

                    return Request.CreateResponse(HttpStatusCode.Unauthorized, erro);

                }
                else if (!user.IsProspect && String.IsNullOrEmpty(model.Password))
                {
                    return Request.CreateResponse(HttpStatusCode.Found, new { Photo = user.Photo, Name = user.Name });
                }
                // is prospect sem senha
                else if (user.IsProspect && String.IsNullOrEmpty(model.Password))
                {
                    // Verificar
                    var contact = _map.Map<User, Intercom.Data.Contact>(user);

                    if (!user.IsCustomer)
                        contact.custom_attributes.Add("IdFreelancer", user.IdFreelancer);

                    contact = _serviceIntercom.SendNewLead(contact);

                    _serviceUser.SendConfirmationEmail(user);

                    var erro = String.Format("Um email foi enviado para <strong>{0}</strong> com as instruções para recuperação da senha.", user.Email);

                    _log.Error("Login: Error: {erro}", erro);

                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed, erro);
                }
                else if ((!user.IsCustomer) && freelancer.Status == StatusUserType.Disabled && (freelancer.Prospect == false))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Usuário é inativo");
                }

                //Autenticação
                var autentica = _serviceUser.Authentication(model.Email, model.Password);

                if (autentica == null)
                {
                    var erro = "Senha Inválida.";

                    _log.Error("Login: Error: {erro}", erro);

                    return Request.CreateResponse(HttpStatusCode.Unauthorized, erro);
                }

                LoggedUser loggedUser;

                var redirect = String.Empty;
                if (user.IsProspect)
                    redirect = user.Role == RoleType.FREELANCER ? ConfigurationManager.AppSettings["freelancerProspectPath"] : ConfigurationManager.AppSettings["customerProspectPath"];
                else
                    redirect = user.Role == RoleType.FREELANCER ? ConfigurationManager.AppSettings["freelancerPath"] : ConfigurationManager.AppSettings["customerPath"];


                var token = CreateJwtToken(user, out loggedUser, redirect);

                _serviceActionLog.Add(new User.ActionLog()
                {
                    Type = ActionLogType.LOGIN,
                    UserId = user.Id
                });

                var retorno = new { loggedUser, token };

                var retornominimo = new { id = loggedUser.Id, IdCustomer = loggedUser.IdCustomer, IdFreelancer = loggedUser.IdFreelancer, Prospect = loggedUser.Prospect, Plan = loggedUser.CustomerPlan };

                _log.Debug("Login: Retorno: {@retorno}", retorno);
                _log.Information("Login: Retorno: {@retornominimo}", retornominimo);


                return Request.CreateResponse(HttpStatusCode.OK, retorno);
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Modelo Inválido.");
        }

        private User CreateUserfromFreelancer(Freelancer freelancer)
        {
            var newUser = new User()
            {
                Active = true,
                CreatedAt = DateTime.Now,
                Email = freelancer.Email,
                IdFreelancer = freelancer.Id,
                Name = freelancer.Name,
                Role = RoleType.FREELANCER,
                AcceptTerms = true,
                AcceptTermsAt = DateTime.Now,
                ConfirmEmail = new Guid(),
                Phone = freelancer.Phone
            };

            freelancer.Prospect = false;

            _serviceFreelancer.Update(freelancer);

            _serviceUser.Add(newUser);

            var intercomuser = Mapper.Map<User, Intercom.Data.User>(newUser);

            intercomuser.custom_attributes.Add("idFreelancer", newUser.IdFreelancer);

            _serviceIntercom.SendUser(intercomuser);

            _serviceUser.SendConfirmationEmail(newUser);

            return newUser;
        }

        [AllowAnonymous]
        [Route("PreCadastro")]
        public HttpResponseMessage PreCadastro(PreCadastroFreelancerViewModel model)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Modelo Inválido");

            model.Phone = Validador.ClearPhone(model.Phone);

            PreCadastroFreelancerViewModelValidator validator = new PreCadastroFreelancerViewModelValidator(_serviceUser);
            ValidationResult validations = validator.Validate(model);

            if (!validations.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, validations.Errors.Aggregate("", (a, v) => a += v.ErrorMessage));

            var freelancer = new Freelancer()
            {
                Name = model.Name,
                Phone = model.Phone,
                Email = model.Email,
                CategoryId = model.Category.Id,
                Referrer = model.Referrer,
                PromoCode = model.PromoCode,
                Status = StatusUserType.Disabled,
                Active = true,
                Code = Guid.NewGuid(),
                Prospect = true
            };

            _serviceFreelancer.Add(freelancer);

            var user = new User()
            {
                Email = model.Email,
                Name = model.Name,
                IdFreelancer = freelancer.Id,
                Role = RoleType.FREELANCER,
                Active = true,
                ConfirmEmail = Guid.NewGuid(),
                AcceptTerms = model.AcceptTerms,
                AcceptTermsAt = DateTime.Now,
                Phone = model.Phone
            };

            _serviceUser.Add(user);

            var contact = _map.Map<User, Intercom.Data.Contact>(user);

            contact.custom_attributes.Add("IdFreelancer", freelancer.Id);

            contact = _serviceIntercom.SendNewLead(contact);

            user.IdLead = contact.user_id;

            _serviceUser.SendConfirmationEmail(user);

            return Request.CreateResponse(HttpStatusCode.OK, CreateLogin(model.Email, null, ConfigurationManager.AppSettings["freelancerProspectPath"]));
        }

        [HttpPost]
        [Route("Register")]
        public HttpResponseMessage Register(FreelancerViewModel model)
        {
            var httpRequest = HttpContext.Current.Request;
            var freelancerExist = _serviceFreelancer.GetByExpression(x => x.Email.Equals(model.Email));
            var userExist = _serviceUser.GetByExpression(x => x.Email.Equals(model.Email));

            if (!string.IsNullOrEmpty(model.Email))
            {
                var checkUserEmailAndPhone = _serviceUser.GetByEmailOrPhone(model.Email, model.Phone);
                var checkFreelancerEmailAndPhone = _serviceFreelancer.GetByEmailOrPhone(model.Email, model.Phone);

                if (checkUserEmailAndPhone.Item1 || checkUserEmailAndPhone.Item2 ||
                   checkFreelancerEmailAndPhone.Item1 || checkFreelancerEmailAndPhone.Item2)
                {
                    return Request.CreateResponse
                    (
                        HttpStatusCode.Forbidden,
                        checkUserEmailAndPhone.Item1 || checkFreelancerEmailAndPhone.Item1 ? "Este email está sendo usado por outro freelancer" :
                        checkUserEmailAndPhone.Item2 || checkFreelancerEmailAndPhone.Item2 ? "Este telefone está sendo usado por outro freelancer" :
                        "Este email e telefone estão sendo usados por outro freelancer"
                    );
                }
            }

            if ((freelancerExist == null || freelancerExist.Id == 0) && userExist == null)
            {
                var freelancer = new Freelancer();
                if (freelancerExist != null)
                {
                    freelancer = freelancerExist;
                }
                else
                {
                    freelancer.Active = false;
                }

                freelancer.Availability = model.Availability;
                freelancer.Name = model.Name;
                freelancer.Email = model.Email;
                freelancer.Active = true;
                freelancer.CityId = model.City.Id;
                freelancer.StateId = model.State.Id;

                freelancer.City = _serviceCity.GetById(model.City.Id);
                freelancer.State = _serviceState.GetById(model.State.Id);

                freelancer.Country = model.Country;
                freelancer.Price = decimal.Parse(model.Price.ToString());
                freelancer.Phone = model.Phone;
                freelancer.Title = model.Title;
                freelancer.PortfolioURL = model.Portfolio;
                freelancer.Description = model.Description;
                freelancer.Skype = model.Skype;
                freelancer.CategoryId = model.CategoryId;
                freelancer.Categorys = _serviceCategory.GetById(model.CategoryId);

                freelancer.Birthday = model.Birthday;
                freelancer.EmailSecondary = model.EmailSecondary;

                freelancer.Code = Guid.NewGuid();

                freelancer.Referrer = model.Referrer;

                Array.ForEach(model.SegmentsIds, x =>
                {
                    var segment = _serviceSegment.GetById(x);
                    if (freelancer.Segments.All(s => s.Id != segment.Id))
                        freelancer.Segments.Add(segment);
                });

                Array.ForEach(model.SkillsIds, x =>
                {
                    var skill = _serviceSkill.GetById(x);
                    if (freelancer.Skills.All(s => s.Id != skill.Id))
                        freelancer.Skills.Add(skill);
                });

                if (freelancerExist == null)
                    _serviceFreelancer.Add(freelancer);
                else
                    _serviceFreelancer.Update(freelancer);


                var user = new User
                {
                    IdFreelancer = freelancer.Id,
                    Role = RoleType.FREELANCER,
                    Name = freelancer.Name,
                    Email = freelancer.Email,
                    AcceptTerms = model.AcceptTerms,
                    AcceptTermsAt = DateTime.Now,
                    Active = true,
                    ConfirmEmail = Guid.NewGuid()
                };

                string folder = "/Content/images/user/";
                string path = HttpContext.Current.Server.MapPath("~/Content/images/user/");
                if (!string.IsNullOrEmpty(model.PhotoBase64))
                {
                    byte[] bytes = Convert.FromBase64String(model.PhotoBase64);
                    string filename = Guid.NewGuid().ToString();
                    path += filename + "." + model.PhotoExtension;
                    File.WriteAllBytes(path, bytes);
                    user.Photo = folder + filename + "." + model.PhotoExtension;
                }

                _serviceUser.Add(user);

                //Add Index
                _searchservice.Update(new FreelancerPoco(freelancer));

                var contact = Mapper.Map<User, Intercom.Data.Contact>(user);

                if (contact.custom_attributes != null)
                    contact.custom_attributes = new Dictionary<string, object>();

                contact.custom_attributes.Add("IDFREELANCER", freelancer.Id);

                _serviceIntercom.SendNewLead(contact);

                Email sendEmail = new Email();
                //sendEmail.Send(model.Email, model.Name, "Seja muito bem vindo a Crowd!", "new-freelancer");
                sendEmail.Send(model.Email, model.Name, "Seja muito bem vindo a Crowd!", "confirm-email",
                    new Dictionary<string, string> {
                        { "CONFIRMEMAIL", user.ConfirmEmail.ToString() },
                        { "EMAIL", user.Email }
                    });


                return Request.CreateResponse(HttpStatusCode.OK);

            }
            return Request.CreateResponse(HttpStatusCode.Unauthorized, "User already exist");
        }

        [Route("Get")]
        public FreelancerViewModel Get(string code)
        {
            var freelancer = _serviceFreelancer.GetByExpression(x => x.Code.ToString() == code);

            if (freelancer == null)
                throw new Exception("Freelancer does not exist");

            var handler = new JwtSecurityTokenHandler();

            var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
            var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

            var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

            var city = new CityViewModel();
            var state = new StateViewModel();

            if (freelancer.City != null)
            {
                city = new CityViewModel()
                {
                    Id = freelancer.City.Id,
                    Name = freelancer.City.Name
                };
            }

            if (freelancer.State != null)
            {
                state = new StateViewModel()
                {
                    Id = freelancer.State.Id,
                    UF = freelancer.State.UF
                };
            }

            var skills = new List<SkillsViewModel>();

            freelancer.Skills.ToList().ForEach(item =>
            {
                skills.Add(new SkillsViewModel()
                {
                    Id = item.Id,
                    Name = item.Name
                });
            });

            var segments = new List<SegmentsViewModel>();
            freelancer.Segments.ToList().ForEach(segement => segments.Add(
                new SegmentsViewModel()
                {
                    Id = segement.Id,
                    Name = segement.Name
                }
            ));

            var experiences = new List<FreelancerExperienceViewModel>();

            freelancer.Experiences.ToList().OrderBy(d => d.Id).ToList().ForEach(item =>
             {
                 experiences.Add(new FreelancerExperienceViewModel()
                 {
                     Id = item.Id,
                     Company = item.Company,
                     Description = item.Description,
                     EndDate = item.EndDate,
                     FreelancerId = item.FreelancerId,
                     Role = item.Role,
                     StartDate = item.StartDate
                 });
             });

            var awards = new List<FreelancerAwardsViewModel>();

            freelancer.Awards.ToList().ForEach(item =>
            {
                awards.Add(new FreelancerAwardsViewModel()
                {
                    Id = item.Id,
                    URL = item.URL,
                    Date = item.Date,
                    Title = item.Title,
                    FreelancerId = item.FreelancerId,
                    Type = item.Type
                });
            });

            var category = new CategoriesViewModel()
            {
                Id = freelancer.CategoryId,
                Name = freelancer.Categorys.Name
            };

            var ratings = freelancer.Ratings.Where(x => x.Active == true);
            var ratingsCount = ratings.Count();

            var comments = new List<string>();
            ratings.ToList().ForEach(rate =>
            {
                comments.Add(rate.Comment);
            });

            var lastTasks = new List<TasksViewModel>();
            var tasks = _serviceTasks.GetAll(x => x.IdFreelancer == freelancer.Id && x.Status == TaskStatus.DONE).OrderByDescending(x => x.UpdatedAt).Take(10);
            tasks.ToList().ForEach(item =>
            {
                lastTasks.Add(new TasksViewModel()
                {
                    DeliveryAt = item.DeliveryAt.HasValue ? String.Format("{0:s}", item.DeliveryAt.Value) : null,
                    Description = item.Description,
                    Id = item.Id,
                    Project = new Models.Projects.ProjectViewModel()
                    {
                        Id = item.Project.Id,
                        Color = item.Project.Color,
                        Name = item.Project.Name
                    },
                    Title = item.Title,
                    User = new UserBriefingViewModel()
                    {
                        Id = item.User.Id,
                        Name = item.User.Name,
                        Photo = item.User.Photo,
                        Customer = new CustomerViewModel()
                        {
                            Name = item.User.Customer.Name,
                            Trade = item.User.Customer.Trade,
                            Logo = item.User.Customer.Logo
                        }
                    }

                });
            });

            var galeriaPortfolio = new List<PortfolioGaleriaViewModel>();
            int order = 0;
            freelancer.Portfolios.OrderByDescending(d => d.CreatedAt).Take(12).ToList().ForEach(item =>
            {
                order++;

                galeriaPortfolio.Add(new PortfolioGaleriaViewModel()
                {
                    Title = item.Title,
                    Type = item.Type,
                    URL = item.URL,
                    StartSlide = order
                });
            });

            var cover = new CoverViewModel();

            if (!String.IsNullOrEmpty(freelancer.Cover))
            {
                cover.Type = freelancer.CoverType ?? FileType.NOTSET;
                cover.URL = freelancer.Cover;
            }

            if (freelancer.Prospect == false && String.IsNullOrEmpty(freelancer.PortfolioOnLine))
            {
                CreatePortfolioOnline(freelancer);
            }

            var response = new FreelancerViewModel()
            {
                Id = Criptografy.Encrypt(freelancer.Id.ToString(), ConfigurationManager.AppSettings["cryptoPass"]),
                Code = freelancer.Code.ToString(),
                Availability = freelancer.Availability,
                Alocado = freelancer.Alocado,
                Name = freelancer.Name,
                Email = freelancer.Email,
                City = city,
                State = state,
                Country = freelancer.Country,
                Price = freelancer.Price,
                Phone = freelancer.Phone,
                Title = freelancer.Title,
                Portfolio = freelancer.PortfolioURL,
                Description = freelancer.Description,
                Skype = freelancer.Skype,
                CategoryId = freelancer.CategoryId,
                Skills = skills,
                Segments = segments,
                Category = category,
                FacebookUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Facebook) != null
                ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Facebook).URL : String.Empty,
                TwitterUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Twitter) != null
                ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Twitter).URL : String.Empty,
                DribbbleUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Dribbble) != null
                ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Dribbble).URL : String.Empty,
                InstagramUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Instagram) != null
                ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Instagram).URL : String.Empty,
                LinkedinUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Linkedin) != null
                ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Linkedin).URL : String.Empty,
                BehanceUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Behance) != null
                ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Behance).URL : String.Empty,
                GithubUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Github) != null
                ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Github).URL : String.Empty,
                Experiences = experiences,
                Comments = comments,
                Photo = freelancer.Users.Count() != 0 ? freelancer.Users.First().Photo : "",
                Active = freelancer.Active,
                Languages = String.Join(",", freelancer.Languages.Select(x => x.Value).ToArray()),
                Available = freelancer.Status == StatusUserType.Active ? true : false,
                Awards = awards,
                Birthday = freelancer.Birthday,
                EmailSecondary = freelancer.EmailSecondary,
                LastTasks = lastTasks,
                GaleriaPortfolio = galeriaPortfolio,
                Cover = cover,
                PortfolioOnline = String.Format("portfolio-online/{0}", freelancer.PortfolioOnLine)
            };


            response.QualityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Quality).Sum() / ratingsCount);
            response.ResponsabilityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Responsability).Sum() / ratingsCount);
            response.AgilityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Agility).Sum() / ratingsCount);
            response.Rating = ratingsCount == 0 ? 0 : ((response.QualityRating + response.ResponsabilityRating + response.AgilityRating) / 3);

            if (requestUser.Customer != null && requestUser.Customer.PlanType == PlanType.FREE)
            {
                string[] removeProperties = new string[] {
                    "Email",
                    "Skype",
                    "Phone",
                    "Name",
                    "FacebookUrl",
                    "TwitterUrl",
                    "DribbbleUrl",
                    "InstagramUrl",
                    "LinkedinUrl",
                    "BehanceUrl",
                    "GithubUrl",
                    "Portfolio"
                };

                PropertiesRule myRules = new PropertiesRule();
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Email", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Skype", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Phone", propetyType = "string", ruleFunction = PropertiesRule.nullString });

                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Name", propetyType = "string", ruleFunction = PropertiesRule.shortenName });

                myRules.AddRule(new PropertiesRule.Rule { propertyName = "FacebookUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "TwitterUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "DribbbleUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "InstagramUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "LinkedinUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "BehanceUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "GithubUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Portfolio", propetyType = "string", ruleFunction = PropertiesRule.nullString });

                foreach (PropertyInfo prop in response.GetType().GetProperties())
                {
                    if (prop.CanWrite)
                    {
                        if (myRules.GetPropertiesRange().Contains(prop.Name))
                        {
                            prop.SetValue(response, myRules.ApplyRule(response, prop));

                            //if (prop.PropertyType.Name.ToLower().IndexOf("decimal")> -1)
                            //    prop.SetValue(item, Decimal.Zero);
                        }
                    }
                }
            }


            return response;
        }

        private void CreatePortfolioOnline(Freelancer freelancer)
        {
            var portfolio = String.Join("/", Validador.RemoveSpecialCharacters(freelancer.Title), Validador.RemoveSpecialCharacters(freelancer.Name));

            var duplicado = _serviceFreelancer.GetByExpression(f => f.PortfolioOnLine == portfolio && f.Id != freelancer.Id);

            if (duplicado != null)
                portfolio += String.Concat("_", Validador.GenerateCheckDigit(portfolio));

            freelancer.PortfolioOnLine = portfolio;

            _serviceFreelancer.Update(freelancer);
        }

        [Authorize]
        [HttpPost]
        [Route("Edit")]
        public HttpResponseMessage Edit(FreelancerEditViewModel model)
        {
            var httpRequest = HttpContext.Current.Request;
            var freelancerId = int.Parse(Criptografy.Decrypt(model.Id, ConfigurationManager.AppSettings["cryptoPass"]));
            var freelancer = _serviceFreelancer.GetById(freelancerId);
            var user = _serviceUser.GetByExpression(x => x.IdFreelancer == freelancerId);

            if (freelancer != null || user != null)
            {
                var checkUserEmailAndPhone = _serviceUser.GetByEmailOrPhone(user.Id, model.Email, model.Phone);
                var checkFreelancerEmailAndPhone = _serviceFreelancer.GetByEmailOrPhone(freelancer.Id, model.Email, model.Phone);

                if (checkUserEmailAndPhone.Item1 || checkUserEmailAndPhone.Item2 ||
                   checkFreelancerEmailAndPhone.Item1 || checkFreelancerEmailAndPhone.Item2)
                {
                    return Request.CreateResponse
                    (
                        HttpStatusCode.BadRequest,
                        checkUserEmailAndPhone.Item1 || checkFreelancerEmailAndPhone.Item1 ? "Este email está sendo usado por outro freelancer" :
                        checkUserEmailAndPhone.Item2 || checkFreelancerEmailAndPhone.Item2 ? "Este telefone está sendo usado por outro freelancer" :
                        "Este email e telefone estão sendo usados por outro freelancer"
                    );
                }

                if (freelancer.Active != model.Active)
                {
                    var userinter = _serviceIntercom.GetUser(freelancer.Users.First().Id);

                    if (!model.Active)
                        userinter.custom_attributes["ocupado"] = DateTime.Now.ToLocalTime();
                    else
                        userinter.custom_attributes["ocupado"] = "";

                    _serviceIntercom.UpdateUser(userinter);
                }
                freelancer.Availability = model.Availability;
                freelancer.Name = model.Name;
                freelancer.Email = model.Email;
                freelancer.EmailSecondary = model.EmailSecondary;
                if (freelancer.Active != model.Active)
                    user.ActiveChange = DateTime.Now;
                freelancer.Active = model.Active;
                if (model.City != null && model.City.Id > 0)
                    freelancer.CityId = model.City.Id;
                if (model.State != null && model.State.Id > 0)
                    freelancer.StateId = model.State.Id;
                freelancer.Country = model.Country;
                freelancer.Price = decimal.Parse(model.Price.ToString());
                freelancer.Phone = model.Phone;
                freelancer.Title = model.Title;
                freelancer.PortfolioURL = model.Portfolio;
                freelancer.Description = model.Description;
                freelancer.Skype = model.Skype;
                freelancer.CategoryId = model.CategoryId;

                if (freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Instagram) != null)
                    freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Instagram).URL = model.InstagramUrl;
                else if (!string.IsNullOrEmpty(model.InstagramUrl))
                {
                    freelancer.Links.Add(new Freelancer.Link()
                    {
                        FreelancerId = freelancer.Id,
                        URL = model.InstagramUrl,
                        Website = WebsiteType.Instagram
                    });
                }

                if (freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Linkedin) != null)
                    freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Linkedin).URL = model.LinkedinUrl;
                else if (!string.IsNullOrEmpty(model.LinkedinUrl))
                {
                    freelancer.Links.Add(new Freelancer.Link()
                    {
                        FreelancerId = freelancer.Id,
                        URL = model.LinkedinUrl,
                        Website = WebsiteType.Linkedin
                    });
                }

                if (freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Twitter) != null)
                    freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Twitter).URL = model.TwitterUrl;
                else if (!string.IsNullOrEmpty(model.TwitterUrl))
                {
                    freelancer.Links.Add(new Freelancer.Link()
                    {
                        FreelancerId = freelancer.Id,
                        URL = model.TwitterUrl,
                        Website = WebsiteType.Twitter
                    });
                }


                if (freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Dribbble) != null)
                    freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Dribbble).URL = model.DribbbleUrl;
                else if (!string.IsNullOrEmpty(model.DribbbleUrl))
                {
                    freelancer.Links.Add(new Freelancer.Link()
                    {
                        FreelancerId = freelancer.Id,
                        URL = model.TwitterUrl,
                        Website = WebsiteType.Dribbble
                    });
                }


                if (freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Facebook) != null)
                    freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Facebook).URL = model.FacebookUrl;
                else if (!string.IsNullOrEmpty(model.FacebookUrl))
                {
                    freelancer.Links.Add(new Freelancer.Link()
                    {
                        FreelancerId = freelancer.Id,
                        URL = model.FacebookUrl,
                        Website = WebsiteType.Facebook
                    });
                }


                if (freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Behance) != null)
                    freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Behance).URL = model.BehanceUrl;
                else if (!string.IsNullOrEmpty(model.BehanceUrl))
                {
                    freelancer.Links.Add(new Freelancer.Link()
                    {
                        FreelancerId = freelancer.Id,
                        URL = model.BehanceUrl,
                        Website = WebsiteType.Behance
                    });
                }

                //freelancer.Status = model.Available ? StatusUserType.Active : StatusUserType.Disabled;

                freelancer.Birthday = model.Birthday;

                freelancer.Segments.Clear();
                freelancer.Skills.Clear();

                if (model.SegmentsIds != null)
                    Array.ForEach(model.SegmentsIds, x => { freelancer.Segments.Add(_serviceSegment.GetById(x)); });

                if (model.SkillsIds != null)
                    Array.ForEach(model.SkillsIds, x => { freelancer.Skills.Add(_serviceSkill.GetById(x)); });

                /* Experience */
                _serviceFreelancer.Update(freelancer);

                freelancer.Languages.ToList().ForEach(lng =>
                {
                    freelancer.Languages.Remove(lng);
                    _serviceLanguage.Remove(lng);
                });

                _serviceFreelancer.Update(freelancer);

                Array.ForEach(model.Languages.Split(','), x =>
                {
                    freelancer.Languages.Add(
                        new Freelancer.Language()
                        {
                            FreelancerId = freelancer.Id,
                            Freelancer = freelancer,
                            Value = x.Trim()
                        }
                    );
                });

                _serviceFreelancer.Update(freelancer);

                var experiencies = new List<Freelancer.Experience>();
                var auxExp = new List<FreelancerExperienceViewModel>();

                freelancer.Experiences.ToList().ForEach(item =>
                {
                    auxExp.Add(new FreelancerExperienceViewModel()
                    {
                        Company = item.Company,
                        Description = item.Description,
                        Role = item.Role,
                        EndDate = item.EndDate,
                        StartDate = item.StartDate,
                        FreelancerId = freelancer.Id,
                        Id = item.Id
                    });
                });

                var experiencesToRemove = auxExp.Except(model.Experiences, new Comparer()).ToList();
                //freelancer.Experiences.Where(x => model.Experiences.Any(i => i.Id != x.Id && i.Id != 0));

                experiencesToRemove.ToList().ForEach(exp =>
                {
                    _serviceFreelancerExperience.Remove(_serviceFreelancerExperience.GetById(exp.Id));
                });

                model.Experiences.ForEach(item =>
                {
                    if (!string.IsNullOrEmpty(item.Id.ToString()) && item.Id != 0)
                    {

                        var experienceItem = _serviceFreelancerExperience.GetById(item.Id);

                        experienceItem.Company = item.Company;
                        experienceItem.Description = item.Description;
                        experienceItem.Role = item.Role;
                        experienceItem.EndDate = item.EndDate;
                        experienceItem.StartDate = item.StartDate;
                        _serviceFreelancerExperience.Update(experienceItem);
                    }
                    else
                    {
                        freelancer.Experiences.Add(new Freelancer.Experience()
                        {
                            Company = item.Company,
                            Description = item.Description,
                            Role = item.Role,
                            EndDate = item.EndDate,
                            StartDate = item.StartDate,
                            FreelancerId = freelancer.Id
                        });
                    }
                });

                if (freelancer.Prospect == false)
                {
                    CreatePortfolioOnline(freelancer);
                }

                _serviceFreelancer.Update(freelancer);

                /* Awards */
                var awards = new List<Freelancer.Award>();
                var auxAwards = new List<FreelancerAwardsViewModel>();

                freelancer.Awards.ToList().ForEach(item =>
                {
                    auxAwards.Add(new FreelancerAwardsViewModel()
                    {
                        Date = item.Date,
                        Title = item.Title,
                        URL = item.URL,
                        FreelancerId = freelancer.Id,
                        Id = item.Id,
                        Type = item.Type
                    });
                });

                var awardsToRemove = auxAwards.Except(model.Awards, new ComparerAwards()).ToList();

                awardsToRemove.ToList().ForEach(awrd =>
                {
                    _serviceFreelancerAwards.Remove(_serviceFreelancerAwards.GetById(awrd.Id));
                });

                model.Awards.ForEach(item =>
                {
                    if (!string.IsNullOrEmpty(item.Id.ToString()) && item.Id != 0)
                    {
                        var awardItem = _serviceFreelancerAwards.GetById(item.Id);
                        awardItem.Title = item.Title;
                        awardItem.Date = item.Date;
                        awardItem.URL = item.URL;
                        awardItem.Type = item.Type;
                        _serviceFreelancerAwards.Update(awardItem);
                    }
                    else
                    {
                        freelancer.Awards.Add(new Freelancer.Award()
                        {
                            Title = item.Title,
                            Date = item.Date,
                            URL = item.URL,
                            FreelancerId = freelancer.Id,
                            Type = item.Type
                        });
                    }
                });

                if (model.Cover != null)
                {
                    if (model.Cover.Type == FileType.IMAGE)
                    {

                        freelancer.CoverType = FileType.IMAGE;
                        freelancer.Cover = GetImage(model.Cover.PhotoBase64, model.Cover.PhotoExtension, "/Content/images/cover");
                    }
                    else
                    {
                        freelancer.CoverType = FileType.VIDEO;
                        freelancer.Cover = model.Cover.URL;
                    }
                }

                user.IdFreelancer = freelancer.Id;
                user.Role = RoleType.FREELANCER;
                user.Name = freelancer.Name;
                user.Email = freelancer.Email;
                user.Phone = freelancer.Phone;

                FreelancerProspectValidator validator = new FreelancerProspectValidator(_serviceFreelancer);
                ValidationResult validations = validator.Validate(freelancer);

                UserProspectValidator validatoruser = new UserProspectValidator();
                ValidationResult validationUser = validatoruser.Validate(user);

                if (!string.IsNullOrEmpty(model.Password))
                    user.Password = Criptografy.GetMD5Hash(model.Password);

                if (!string.IsNullOrEmpty(model.PhotoBase64))
                {
                    user.Photo = GetImage(model.PhotoBase64, model.PhotoExtension, "/Content/images/user/");
                }

                if (freelancer.Prospect ?? false)
                {
                    if (validations.IsValid && validationUser.IsValid)
                    {
                        freelancer.Prospect = false;
                        freelancer.Status = StatusUserType.Active;

                        //Add Index
                        _searchservice.Update(new FreelancerPoco(freelancer));

                        Intercom.Data.User iuser = _map.Map<Backend.Crowd.Domain.Entities.User, Intercom.Data.User>(user);

                        _serviceIntercom.ChangeLeadtoUser(iuser);
                    }
                }

                _serviceFreelancer.Update(freelancer);

                _serviceUser.Update(user);

                if (!freelancer.Prospect.Value)
                {

                    //Add Index
                    _searchservice.Update(new FreelancerPoco(freelancer));
                }

                if (!validations.IsValid)
                    return Request.CreateResponse(HttpStatusCode.PreconditionFailed, ReturnValidationError(validations.Errors));

                if (!validationUser.IsValid)
                    return Request.CreateResponse(HttpStatusCode.PreconditionFailed, ReturnValidationError(validationUser.Errors));

                LoggedUser loggedUser;
                var token = CreateJwtToken(user, out loggedUser);

                return Request.CreateResponse(new { loggedUser, token });

            }
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        public static string EncryptPassword(string password, string salt)
        {
            using (var sha256 = SHA256.Create())
            {
                var saltedPassword = string.Format("{0}{1}", salt, password);
                var saltedPasswordAsBytes = Encoding.UTF8.GetBytes(saltedPassword);
                return Convert.ToBase64String(sha256.ComputeHash(saltedPasswordAsBytes));
            }
        }

        [HttpPost]
        [Route("ForgotPassword")]
        public HttpResponseMessage ForgotPassword(string email)
        {
            var guid = _serviceUser.RecoverPassword(email);
            if (guid != null)
            {
                var user = _serviceUser.GetByExpression(p => p.Email.Equals(email), true);
                var variables = new Dictionary<string, string> { { "EMAIL", email }, { "ID", guid.ToString() } };
                Email sendEmail = new Email();
                sendEmail.Send(user.Email, user.Name, "Alterar a Senha", "change-password-new", variables);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("ValidateHash")]
        public HttpResponseMessage ValidateHash(string email, string hash)
        {
            var user = _serviceUser.GetByExpression(x => x.Email.Equals(email) && x.RecoverPassword.HasValue
                && x.RecoverPassword.Value.ToString().Equals(hash));

            if (user != null)
                return Request.CreateResponse(HttpStatusCode.OK);

            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }

        [HttpPost]
        [Route("ValidateEmailHash")]
        [AllowAnonymous]
        public HttpResponseMessage ValidateEmailHash(string hash)
        {
            var user = _serviceUser.GetByExpression(x => x.ConfirmEmail.HasValue
                && x.ConfirmEmail.Value.ToString().Equals(hash));

            if (user != null)
            {
                user.ConfirmEmailAt = DateTime.Now;
                _serviceUser.Update(user);
                return Request.CreateResponse(HttpStatusCode.OK, CreateLogin(user.Email, null,
                    (user.IsProspect ?
                    (user.IsCustomer ? ConfigurationManager.AppSettings["customerProspectPath"] : ConfigurationManager.AppSettings["freelancerProspectPath"])
                    :
                    (user.IsCustomer ? ConfigurationManager.AppSettings["customerPathHash"] : ConfigurationManager.AppSettings["freelancerPath"]))));
            }
            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }

        [HttpGet]
        [Authorize]
        [Route("ValidateToken")]
        public HttpResponseMessage ValidateToken()
        {
            var handler = new JwtSecurityTokenHandler();

            var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
            var id = Int32.Parse(jsonToken.Claims.First(claim => claim.Type == "nameid").Value);

            var ultimolog = _serviceActionLog.GetAll(u => u.UserId == id && u.Type == ActionLogType.LOGIN).OrderByDescending(d => d.CreatedAt).First();

            if (ultimolog != null && ultimolog.CreatedAt.AddMinutes(5) < DateTime.Now)
                _serviceActionLog.Add(new User.ActionLog
                {
                    Type = ActionLogType.LOGIN,
                    UserId = id
                });

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("ChangePassword")]
        public HttpResponseMessage ChangePassword(string email, string password, string hash)
        {
            var change = _serviceUser.ChangePassword(email, password, hash);
            if (change)
                return Request.CreateResponse(HttpStatusCode.OK);

            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }

        [Authorize]
        [HttpPost]
        [Route("EditUser")]
        public HttpResponseMessage EditUser(UserEditViewModel model)
        {
            var user = _serviceUser.GetById(model.Id);

            if (user != null)
            {
                var checkUserEmail = _serviceUser.GetByEmail(model.Email) != null ? true : false;
                var checkFreelancerEmail = _serviceFreelancer.GetByEmail(model.Email) != null ? true : false;

                if (checkUserEmail || checkFreelancerEmail)
                {
                    return Request.CreateResponse
                    (
                        HttpStatusCode.BadRequest, "Este email e/ou telefone estão sendo usados por outro freelancer"
                    );
                }

                user.Name = model.Name;
                if (!String.IsNullOrEmpty(model.RoleCompany))
                    user.RoleCompany = model.RoleCompany;

                user.Email = model.Email;

                if (!string.IsNullOrEmpty(model.Password))
                {
                    user.IsResetting = false;
                    user.Password = Criptografy.GetMD5Hash(model.Password);
                }

                if (!string.IsNullOrEmpty(model.PhotoBase64))
                {
                    user.Photo = GetImage(model.PhotoBase64, model.PhotoExtension, "/Content/images/user/");
                }

                _serviceUser.Update(user);

                model.Photo = user.Photo;

                LoggedUser loggedUser;
                var token = CreateJwtToken(user, out loggedUser);

                return Request.CreateResponse(new { loggedUser, token });
            }

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [HttpGet]
        [Route("ConfirmEmail")]
        [Obsolete("Url de confirmacao nao mais usada.")]
        public HttpResponseMessage ConfirmEmail(string email, string hash)
        {
            var response = Request.CreateResponse(HttpStatusCode.Redirect);

            var user = _serviceUser.GetByExpression(x => x.Email.Equals(email) && x.ConfirmEmail.HasValue
                && x.ConfirmEmail.Value.ToString().Equals(hash));

            if (user != null)
            {
                if (user.Active && user.ConfirmEmailAt != null)
                {
                    response.Headers.Location = new Uri(ConfigurationManager.AppSettings["EmailjaconfirmadoURL"]);
                }
                else
                {

                    response.Headers.Location = new Uri(ConfigurationManager.AppSettings["EmailconfirmadoURL"]);
                }
            }
            else
            {

                response.Headers.Location = new Uri(ConfigurationManager.AppSettings["EmailnaoconfirmadoURL"]);
            }

            return response;
        }

        #region ITracking
        public List<Argument> Processor(string action, Dictionary<string, object> actionArguments)
        {
            var arguments = new List<Argument>();
            switch (action)
            {
                case "Get":
                    arguments.Add(new Argument
                    {
                        Name = "code",
                        Type = "String",
                        Value = actionArguments["code"].ToString()
                    });
                    break;
                case "Login":
                    arguments.Add(new Argument
                    {
                        Name = "email",
                        Type = "String",
                        Value = ((LoginViewModel)actionArguments["model"]).Email
                    });
                    break;
            }

            return arguments;
        }
        #endregion
    }
}

