using API.Models.Report;
using ClosedXML.Excel;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Infrasctructure.Database.Context;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Web.Http;
using System.Web.UI.WebControls;

namespace API.Controllers
{
    [Authorize]
    [RoutePrefix("Report")]
    public class ReportController : BaseAPIController
    {
        public IServiceBase<Tasks> _serviceTask;
        private IServiceUser _serviceUser;

        public ReportController(IServiceBase<Tasks> serviceTask, IServiceUser serviceUser)
        {
            _serviceTask = serviceTask;
            _serviceUser = serviceUser;
        }


        private User GetUserOnRequest(HttpRequestMessage request)
        {
            var handler = new JwtSecurityTokenHandler();

            var jsonToken = handler.ReadToken(request.Headers.Authorization.Parameter) as JwtSecurityToken;
            var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;
           
            var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

            return requestUser;
        }


        [HttpGet()]
        [Route("GetCommonValues")]
        public IHttpActionResult GetCommonValues(DateTime? start = null, DateTime? end = null)
        {
            if (start == null)
            {
                start = DateTime.Now.Day < 16
                    ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
                    : new DateTime(DateTime.Now.Year, DateTime.Now.Month, 16);
            }

            if (end == null)
            {
                end = DateTime.Now.Day < 16
                    ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 15)
                    : new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            }

            var requestUser = GetUserOnRequest(Request);

            var now = DateTime.Now;

            var tasks = _serviceTask.GetAll(x => x.Status == TaskStatus.DONE);

            if (requestUser.Role == Backend.Crowd.Domain.Entities.Enum.RoleType.FREELANCER)
            {
                tasks = tasks.Where(x => x.Freelancer.Id == requestUser.Freelancer.Id).ToList();

                var totalReceived = tasks.Sum(x => x.Price ?? 0);

                List<Tasks> tasksToReceive;
                if (now.Day <= 15)
                {
                    var date = new DateTime(now.Year, now.AddMonths(-1).Month, 16);
                    tasksToReceive = tasks.Where(x => x.UpdatedAt != null && x.UpdatedAt.Value >= date).ToList();
                }
                else
                {
                    var date = new DateTime(now.Year, now.Month, 1);
                    tasksToReceive = tasks.Where(x => x.UpdatedAt != null && x.UpdatedAt.Value >= date).ToList();
                }

                var totalNextReceive = tasksToReceive.Sum(x => x.Price ?? 0);
                totalNextReceive = totalNextReceive - (totalNextReceive * (decimal)0.08);

                totalReceived = totalReceived - (totalReceived * (decimal)0.08);

                return Ok(new
                {
                    totalReceived,
                    totalNextReceive
                });
            }

            if (start > end)
                throw new Exception("Start date cannot be major than end date.");

            tasks = tasks.Where(x => x.UpdatedAt != null && (x.User.Customer.Id == requestUser.Customer.Id && x.UpdatedAt.Value >= start &&
                                                             x.UpdatedAt.Value <= end)).ToList();

            var totalPaid = tasks.Sum(x => x.Price ?? 0);

            return Ok(new
            {
                totalPaid
            });
        }


        [HttpGet()]
        [Route("ListTasksDone")]
        public List<TaskDoneViewModel> ListTasksDone(DateTime start, DateTime end)
        {
            if (start > end)
                throw new Exception("Start date cannot be major than end date.");

            var requestUser = GetUserOnRequest(Request);

            var isfreelancer = (requestUser.Role == RoleType.FREELANCER);

            IEnumerable<Tasks> tasks = GetTask(DateTime.MinValue, DateTime.MaxValue);

            var ret = new List<TaskDoneViewModel>();

            tasks.ToList().ForEach(item =>
            {
                ret.Add(new TaskDoneViewModel()
                {
                    CustomerLogo = item.User.Customer.Logo,
                    CustomerName = item.User.Customer.Name,
                    CustomerTrade = item.User.Customer.Trade,
                    Date = String.Format("{0:s}", item.UpdatedAt.Value),
                    FreelancerName = item.Freelancer.Name,
                    FreelancerPhoto = item.Freelancer.Users.First().Photo,
                    Price = isfreelancer ? (item.Price.Value - (item.Price.Value * (decimal)0.08)) : item.Price.Value,
                    ProjectId = item.Project.Id,
                    ProjectName = item.Project.Name,
                    TaskName = item.Title,
                    Color = item.Project.Color,
                    ResponsiblePhoto = item.User.Photo,
                    ResponsibleName = item.User.Name
                });

            });

            
            return ret.OrderByDescending(t => t.Date).ToList();
        }

        private IEnumerable<Tasks> GetTask(DateTime start, DateTime end)
        {
            var requestUser = GetUserOnRequest(Request);

            var tasks = _serviceTask.GetAll(x => x.Status == Backend.Crowd.Domain.Entities.Enum.TaskStatus.DONE);

            if (requestUser.Role == RoleType.FREELANCER)
            {
                tasks = tasks.Where(x => x.Freelancer.Id == requestUser.Freelancer.Id);
            }
            else if (requestUser.Role != RoleType.MASTER)
            {
                tasks = tasks.Where(x => x.User.Customer.Id == requestUser.Customer.Id);
            }

            tasks = tasks.Where(x => x.UpdatedAt.Value >= start && x.UpdatedAt.Value <= end);
            return tasks;
        }

        [HttpGet()]
        [Route("ListTaksFortnight")]
        public List<TaskFortnightViewModel> ListTaksFortnight(DateTime start, DateTime end)
        {
            if (start > end)
                throw new Exception("Start date cannot be major than end date.");

            List<string> fortnightlies = new List<string>();

            TimeSpan timespan = end - start;

            decimal totaldays = Convert.ToInt32(timespan.TotalDays);
            decimal weekly = Math.Ceiling(totaldays / 15);

            for (DateTime date = start; date.Date <= end.Date; date = date.AddDays(1))
            {
                string q = (date.Day <= 15 ? "1� Quinzena " : "2� Quinzena ") + date.ToString("MM/yyyy");
                if (!fortnightlies.Contains(q))
                {
                    fortnightlies.Add(q);
                }
            }

                        

            var requestUser = GetUserOnRequest(Request);
            var ret = new List<TaskFortnightViewModel>();

            CrowdContext context = new CrowdContext();
            var filterStr = string.Empty;

            if ( requestUser.Role != RoleType.MASTER)
               filterStr = String.Format(" AND {0}",requestUser.Role == RoleType.FREELANCER ? ("A.id_freelancer = " + requestUser.Freelancer.Id)  : ("B.id_customer = " + requestUser.Customer.Id));

            string price = "A.price";

            if (requestUser.Role == RoleType.FREELANCER)
                price = "A.price - cast((A.price * 0.08) AS DECIMAL(10,2))";

            var sqlquery = "SELECT CASE WHEN DAY(A.updated_at) BETWEEN 1 AND 15 THEN CONCAT('1� Quinzena ', Format(A.updated_at,'MM/yyyy'))	ELSE CONCAT('2� Quinzena ', FORMAT(A.updated_at,'MM/yyyy')) END AS weekname, A.id AS id, A.title AS title, " + price + " AS price, A.updated_at AS updated_at, B.color AS color, B.name AS project, DATEPART(wk, A.updated_at) as weeknumber FROM crowd_tasks A INNER JOIN crowd_projects B ON A.id_project = B.id WHERE A.status = 4 AND Convert(CHAR(20),A.updated_at,120) >= '" + start.ToString("yyyy-MM-dd") + " 00:00'" + " AND Convert(CHAR(20),A.updated_at,120) <= '" + end.ToString("yyyy-MM-dd") + " 23:59'" + filterStr + " GROUP BY CASE WHEN DAY(A.updated_at) BETWEEN 1 AND 15 THEN 1 ELSE 2 end, A.id, A.title, A.price, A.updated_at, B.color, B.name ORDER BY weeknumber";

            var tasksGroupedBy = context.Database.SqlQuery<TaskFortnightGroupViewModel>(sqlquery).ToList();

            var items = tasksGroupedBy.GroupBy(x => new { x.weekname, x.updated_at.Month, x.updated_at.Year })
                .Select(group => new
                {
                    Fortnight = group.Key,
                    Tasks = group.ToList()
                });



            fortnightlies.ToList().ForEach(ft =>
            {
                var item = items.Where(x => x.Fortnight.weekname == ft).FirstOrDefault();

                if (item != null)
                {
                    ret.Add(new TaskFortnightViewModel()
                    {
                        Fortnight = item.Fortnight.weekname,
                        Tasks = item.Tasks
                    });
                }
                else
                {
                    ret.Add(new TaskFortnightViewModel()
                    {
                        Fortnight = ft,
                        Tasks = new List<TaskFortnightGroupViewModel>()
                    });
                }
            });



            return ret;
        }


        [HttpGet()]
        [Route("GenerateSheet")]
        public HttpResponseMessage GenerateSheet(DateTime start, DateTime end)
        {
            if (start > end)
                throw new Exception("Start date cannot be major than end date.");

            var tasks = GetTask(start, end);

            var ret = new List<TaskDoneViewModel>();

            tasks.ToList().ForEach(item =>
            {
                ret.Add(new TaskDoneViewModel()
                {
                    CustomerLogo = item.User.Customer.Logo,
                    CustomerName = item.User.Customer.Trade,
                    Date = String.Format("{0:s}", item.UpdatedAt.Value),
                    FreelancerName = item.Freelancer.Name,
                    FreelancerPhoto = item.Freelancer.Users.First().Photo,
                    Price = item.Price.Value,
                    ProjectId = item.Project.Id,
                    ProjectName = item.Project.Name,
                    TaskName = item.Title
                });

            });

            DataTable dt = ToDataTable<TaskDoneViewModel>(ret);

            XLWorkbook wb = new XLWorkbook();
            wb.Worksheets.Add(dt, "WorksheetName");

            MemoryStream stream = new MemoryStream();
            wb.SaveAs(stream);
            stream.Position = 0;

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = "report.xls";
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
            
            return result;            
        }

        [HttpGet()]
        [Route("GeneratePDF")]
        public HttpResponseMessage GeneratePDF(DateTime start, DateTime end)
        {
            if (start > end)
                throw new Exception("Start date cannot be major than end date.");

            var requestUser = GetUserOnRequest(Request);

            var tasks = _serviceTask.GetAll(x => x.Status == Backend.Crowd.Domain.Entities.Enum.TaskStatus.DONE);

            if (requestUser.Role == Backend.Crowd.Domain.Entities.Enum.RoleType.FREELANCER)
            {
                tasks = tasks.Where(x => x.Freelancer.Id == requestUser.Freelancer.Id);
            }
            else
            {
                tasks = tasks.Where(x => x.User.Customer.Id == requestUser.Customer.Id);
            }

            tasks = tasks.Where(x => x.UpdatedAt.Value >= start && x.UpdatedAt.Value <= end);

            var ret = new List<TaskDoneViewModel>();

            tasks.ToList().ForEach(item =>
            {
                ret.Add(new TaskDoneViewModel()
                {
                    CustomerLogo = item.User.Customer.Logo,
                    CustomerName = item.User.Customer.Trade,
                    Date = String.Format("{0:s}", item.UpdatedAt.Value),
                    FreelancerName = item.Freelancer.Name,
                    FreelancerPhoto = item.Freelancer.Users.First().Photo,
                    Price = item.Price.Value,
                    ProjectId = item.Project.Id,
                    ProjectName = item.Project.Name,
                    TaskName = item.Title
                });

            });

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            using (MemoryStream m = new MemoryStream())
            {
                Document doc = new Document(PageSize.A4, 2, 2, 2, 2);
                PdfWriter writer = PdfWriter.GetInstance(doc, m);
                doc.Open();

                PdfPTable pdfTab = new PdfPTable(9); // here 4 is no of column
                pdfTab.HorizontalAlignment = 1; // 0- Left, 1- Center, 2- right
                pdfTab.SpacingBefore = 20f;
                pdfTab.SpacingAfter = 20f;

                pdfTab.AddCell("Customer Logo");
                pdfTab.AddCell("Customer Name");
                pdfTab.AddCell("Date");
                pdfTab.AddCell("Freelancer Name");
                pdfTab.AddCell("Freelancer Photo");
                pdfTab.AddCell("Price");
                pdfTab.AddCell("Project ID");
                pdfTab.AddCell("Project Name");
                pdfTab.AddCell("Task Name");

                foreach (var item in ret)
                {
                    pdfTab.AddCell(item.CustomerLogo);
                    pdfTab.AddCell(item.CustomerName);
                    pdfTab.AddCell(item.Date);
                    pdfTab.AddCell(item.FreelancerName);
                    pdfTab.AddCell(item.FreelancerPhoto);
                    pdfTab.AddCell(item.Price.ToString());
                    pdfTab.AddCell(item.ProjectId.ToString());
                    pdfTab.AddCell(item.ProjectName);
                    pdfTab.AddCell(item.TaskName);
                }

                doc.Add(pdfTab);

                result.Content = new ByteArrayContent(m.GetBuffer());
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "report.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                writer.Flush();
                doc.Close();
            }

            return result;
        }

        public HttpResponseMessage GeneratePDFNew(DateTime start, DateTime end)
        {
            if (start > end)
                throw new Exception("Start date cannot be major than end date.");

            var filename = System.Web.Hosting.HostingEnvironment.MapPath("/Html/crowdReport.html");

            var tasks = GetTask(start, end);

            var ret = new List<TaskDoneViewModel>();

            tasks.ToList().ForEach(item =>
            {
                ret.Add(new TaskDoneViewModel()
                {
                    CustomerLogo = item.User.Customer.Logo,
                    CustomerName = item.User.Customer.Trade,
                    Date = item.UpdatedAt.Value.ToUniversalTime().ToString(),
                    FreelancerName = item.Freelancer.Name,
                    FreelancerPhoto = item.Freelancer.Users.First().Photo,
                    Price = item.Price.Value,
                    ProjectId = item.Project.Id,
                    ProjectName = item.Project.Name,
                    TaskName = item.Title
                });

            });

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            var bytes = File.ReadAllBytes(filename);

            using (MemoryStream m = new MemoryStream(bytes))
            {

                Document doc = new Document(PageSize.A4, 2, 2, 2, 2);
                PdfWriter writer = PdfWriter.GetInstance(doc, m);
                //doc.Open();

                //ICSSResolver cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(false);

                //HtmlPipelineContext context = new HtmlPipelineContext(null);
                //context.SetTagFactory(iTextSharp.tool.xml.html.Tags.GetHtmlTagProcessorFactory());
                //IPipeline pipeline = new CssResolverPipeline(cssResolver, new HtmlPipeline(context, new PdfWriterPipeline(doc, writer)));
                //var worker = new XMLWorker(pipeline, true);
                //var parser = new XMLParser(worker);

                //using (TextReader sr = new StringReader(filename))
                //{
                //    parser.Parse(sr);
                //}
                //doc.Close();
                //byte[] test = m.GetBuffer();

                //result.Content = new ByteArrayContent(m.GetBuffer());
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "report.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                writer.Flush();
                doc.Close();
            }

            return result;
        }

        private static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

    }
}
