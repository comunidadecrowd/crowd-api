using API.Models;
using API.Models.enums;
using API.Models.Portfolio;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Domain.Util;
using Backend.Crowd.Domain.Util.Network;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Algolia.API;

namespace API.Controllers
{
    [Authorize]
    [RoutePrefix("Professional")]
    public class FreelancerController : BaseAPIController
    {
        private IServiceFreelancer _serviceFreelancer;
        private IServiceBase<State> _serviceState;
        private IServiceBase<City> _serviceCity;
        private IServiceBase<Freelancer.RatingFreelancer> _serviceRating;
        private IServiceUser _serviceUser;
        private IServiceBase<Tasks> _serviceTasks;
        private readonly ISearch _searchservice;

        private IServiceBase<Freelancer.Portfolio> _servicePortfolio;


        public FreelancerController()
        {
        }

        public FreelancerController(IServiceFreelancer serviceFreelancer, IServiceBase<State> serviceState,
            IServiceBase<City> serviceCity, IServiceBase<Freelancer.RatingFreelancer> serviceRating, IServiceUser serviceUser,
            IServiceBase<Freelancer.Portfolio> servicePortfolio, IServiceBase<Tasks> serviceTasks,ISearch searchservice)
        {
            _serviceFreelancer = serviceFreelancer;
            _serviceCity = serviceCity;
            _serviceState = serviceState;
            _serviceRating = serviceRating;
            _serviceUser = serviceUser;
            _servicePortfolio = servicePortfolio;
            _serviceTasks = serviceTasks;
            _searchservice = searchservice;
        }

        [AllowAnonymous]
        [Route("UpdateIndex")]
        public HttpResponseMessage UpdateIndex()
        {
            var freelancer = FreelancerPoco.GetDateForSearch(DateTime.Today.AddDays(-5),_serviceFreelancer, _serviceTasks);
            _searchservice.UpdateBatch(freelancer);
            _searchservice.RunBatch();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [AllowAnonymous]
        [Route("RecreateIndex")]
        public HttpResponseMessage RecreateIndex()
        {
            var freelancers = FreelancerPoco.GetAllForSearch(_serviceFreelancer,_serviceTasks);
            _searchservice.Clear();
            _searchservice.AddBatch(freelancers);
            _searchservice.RunBatch();
            
            return Request.CreateResponse(HttpStatusCode.OK);
        }


        [Route("Search")]
        public IHttpActionResult GetSearchFreelancer([FromUri]FreelancerFilterViewModel filter, Order order = Order.CREATED,
            OrderType orderType = OrderType.DESC, int page = 1, int pageSize = 30)
        {
            // create default Lucene search index directory
            if (!Directory.Exists(LuceneSearchController._luceneDir))
                Directory.CreateDirectory(LuceneSearchController._luceneDir);

            // perform Lucene search
            var freelancers = LuceneSearchController.GetAllIndexRecords().ToList();
            var total = _serviceFreelancer.GetAll(lazyLoadEnabled: false).ToList().Count();
            var requestUser = new User();
            try
            {
                var handler = new JwtSecurityTokenHandler();

                var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
                var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

                requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

            }
            catch (Exception ex)
            {
                return Unauthorized();
            }

            if (filter == null ||(
                (filter.availability == 0) && 
                (filter.city == 0) && 
                (filter.maxPrice == 0 ) && 
                (filter.minPrice == 0) && 
                (filter.segment == 0) &&
                (filter.uf == 0) &&
                (filter.segmentsType == null) &&
                (filter.terms == null) ))
            {
                freelancers = freelancers
                    .Where(x => x.CreatedAt <= DateTime.Now.AddDays(-5) && x.Status == StatusUserType.Active).ToList();
            }
            else
            {

                if (filter.terms != null)
                    freelancers = LuceneSearchController.Search(String.Join("|", filter.terms)).ToList();

                if (filter.segmentsType != null)
                {
                    freelancers = freelancers.Where(item => filter.segmentsType.Contains(item.CategoryId)).ToList();
                }

                if (filter.uf != 0)
                {
                    freelancers = freelancers.Where(item => filter.uf == item.State.Id).ToList();
                }

                if (filter.city != 0)
                {
                    freelancers = freelancers.Where(item => filter.city == item.City.Id).ToList();
                }

                if (filter.segment != 0)
                {
                    freelancers = freelancers.Where(item => item.Segments.Any(z => z.Id == filter.segment)).ToList();
                }

                if (filter.maxPrice != 0)
                {
                    freelancers = freelancers.Where(item => filter.maxPrice >= item.Price).ToList();
                }

                if (filter.minPrice != 0)
                {
                    freelancers = freelancers.Where(item => filter.minPrice <= item.Price).ToList();
                }

                if (filter.availability != 0)
                {
                    freelancers = freelancers.Where(item => filter.availability == item.Availability).ToList();
                }

                if (filter.Experience.HasValue)
                    freelancers = filter.Experience.Value ? freelancers.Where(item => item.Experiences.Any()).ToList() : freelancers.Where(item => !item.Experiences.Any()).ToList();

                if (filter.Portfolio.HasValue)
                    freelancers = filter.Portfolio.Value ? freelancers.Where(item => item.Portfolios.Any()).ToList() : freelancers.Where(item => !item.Portfolios.Any()).ToList();

                freelancers = freelancers.Where(x => x.Status == StatusUserType.Active).ToList();
            }

            var totalReturned = freelancers.Count();

            var response = new FreelancerSearchViewModel();
            var freelancersViewModel = new List<FreelancerViewModel>();

            var allSkills = new List<SkillsViewModel>();

            switch (order)
            {
                case Order.CREATED:
                    if (filter == null || (filter.terms == null))
                    {
                        if (orderType == OrderType.ASC)
                            freelancers = freelancers.OrderBy(x => x.Id).ThenBy(x => x.UpdatedAt).ToList();
                        else
                            freelancers = freelancers.OrderByDescending(x => x.Id).ThenByDescending(x => x.UpdatedAt).ToList();
                    }
                    break;

                case Order.PRICE:
                    if (orderType == OrderType.ASC)
                        freelancers = freelancers.OrderBy(x => x.Price).ToList();
                    else
                        freelancers = freelancers.OrderByDescending(x => x.Price).ToList();
                    break;

                case Order.RATING:
                    if (orderType == OrderType.ASC)
                        freelancers = freelancers.OrderBy(x => x.Rating).ToList();
                    else
                        freelancers = freelancers.OrderByDescending(x => x.Rating).ToList();
                    break;
            }


            //Pagination
            var skipCount = (page - 1) * pageSize;
            freelancers = freelancers.Skip(skipCount).Take(pageSize).ToList();

            if (requestUser.Customer.PlanType == PlanType.FREE)
            {
                string[] removeProperties = new string[] {
                    "Email",
                    "Skype",
                    "Phone",
                    "Name",
                    "FacebookUrl",
                    "TwitterUrl",
                    "DribbbleUrl",
                    "InstagramUrl",
                    "LinkedinUrl",
                    "BehanceUrl",
                    "Portfolio"
                };

                PropertiesRule myRules = new PropertiesRule();
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Email", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Skype", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Phone", propetyType = "string", ruleFunction = PropertiesRule.nullString });

                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Name", propetyType = "string", ruleFunction = PropertiesRule.shortenName });

                myRules.AddRule(new PropertiesRule.Rule { propertyName = "FacebookUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "TwitterUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "DribbbleUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "InstagramUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "LinkedinUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "BehanceUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Portfolio", propetyType = "string", ruleFunction = PropertiesRule.nullString });

                freelancers.ToList().ForEach(item =>
                {
                    foreach (PropertyInfo prop in item.GetType().GetProperties())
                    {
                        if (prop.CanWrite)
                        {
                            if (myRules.GetPropertiesRange().Contains(prop.Name))
                            {
                                prop.SetValue(item, myRules.ApplyRule(item, prop));

                                //if (prop.PropertyType.Name.ToLower().IndexOf("decimal")> -1)
                                //    prop.SetValue(item, Decimal.Zero);
                            }
                        }
                    }
                });
            }

            freelancers.ToList().ForEach(item =>
            {
                var user = _serviceUser.GetByExpression(x => x.IdFreelancer == item.Id);
                var state = new StateViewModel()
                {
                    UF = item.State.UF
                };

                var city = new CityViewModel()
                {
                    Name = item.City.Name
                };

                var category = new CategoriesViewModel()
                {
                    Name = item.Categorys.Name
                };

                var freelancerId = Backend.Crowd.Domain.Util.Security.Criptografy.Encrypt(item.Id.ToString(), ConfigurationManager.AppSettings["cryptoPass"]);

                var freelancerItem = new FreelancerViewModel()
                {
                    Id = freelancerId,
                    Code = item.Code.ToString(),
                    Name = item.Name,
                    Title = item.Title,
                    Country = item.Country,
                    Price = item.Price,
                    Portfolio = item.PortfolioURL,
                    Availability = item.Availability,
                    Description = item.Description,
                    City = city,
                    State = state,
                    Category = category
                };

                if (user != null)
                {
                    freelancerItem.Photo = user.Photo;
                    freelancerItem.UserId = user.Id;
                }

                var skills = new List<SkillsViewModel>();
                item.Skills.ToList().ForEach(skill => {
                    var skillViewModel = new SkillsViewModel()
                    {
                        Id = skill.Id,
                        Name = skill.Name
                    };

                    skills.Add(skillViewModel);
                    allSkills.Add(skillViewModel);
                });

                freelancerItem.Skills = skills;

                var segments = new List<SegmentsViewModel>();
                if (user != null)
                {
                    if (user.Freelancer != null)
                    {
                        user.Freelancer.Segments.ToList().ForEach(segement => segments.Add(
                            new SegmentsViewModel()
                            {
                                Id = segement.Id,
                                Name = segement.Name
                            }
                        ));
                    }
                }
                else
                {
                    item.Segments.ToList().ForEach(segement => segments.Add(
                        new SegmentsViewModel()
                        {
                            Id = segement.Id,
                            Name = segement.Name
                        }
                    ));
                }

                freelancerItem.Segments = segments;

                var ratings = _serviceRating.GetAll(true).Where(x => x.FreelancerId == item.Id && x.Active == true);
                var ratingsCount = ratings.Count();

                freelancerItem.QualityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Quality).Sum() / ratingsCount);
                freelancerItem.ResponsabilityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Responsability).Sum() / ratingsCount);
                freelancerItem.AgilityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Agility).Sum() / ratingsCount);
                freelancerItem.Rating = ratingsCount == 0 ? 0 : ((freelancerItem.QualityRating + freelancerItem.ResponsabilityRating + freelancerItem.AgilityRating) / 3);


                freelancersViewModel.Add(freelancerItem);

            });


            response.results = freelancersViewModel;
            response.relatedSkills = new List<SkillsViewModel>();

            var most = allSkills;
            if (filter != null && filter.segmentsType != null)
                most = most.ToList();

            most = most.GroupBy(i => i).OrderByDescending(grp => grp.Count()).Select(grp => grp.Key).ToList().Take(5).ToList();
            response.relatedSkills = most.ToList();


            return Ok(new { response, total, totalReturned });
        }


        [Route("ChangeAvailable")]
        [HttpPost]
        public HttpResponseMessage ChangeAvailable(FreelancerAvailableViewModel model)
        {
            var id = int.Parse(Backend.Crowd.Domain.Util.Security.Criptografy.Decrypt(model.FreelancerId.ToString(), ConfigurationManager.AppSettings["cryptoPass"]));
            var freelancer = _serviceFreelancer.GetById(id);
            freelancer.Status = model.Available ? StatusUserType.Active : StatusUserType.Disabled;

            _serviceFreelancer.Update(freelancer);

            //Add Index
            var lucene = new LuceneSearchController();
            lucene.AddUpdateLuceneIndex(freelancer);

            return Request.CreateResponse(HttpStatusCode.OK);
        }


        [Route("AddBankInfo")]
        [HttpPut]
        public HttpResponseMessage AddBankInfo(FreelancerBankInfoViewModel model)
        {
            var id = int.Parse(Backend.Crowd.Domain.Util.Security.Criptografy.Decrypt(model.Id.ToString(), ConfigurationManager.AppSettings["cryptoPass"]));
            var freelancer = _serviceFreelancer.GetById(id);
            if (freelancer == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Freelancer not found.");

            freelancer.Payments.First().FullName = model.BankFullName;
            freelancer.Payments.First().Agency = model.BankAgency;
            freelancer.Payments.First().Account = model.BankAccount;
            freelancer.Payments.First().CPF = model.CPF;
            freelancer.Payments.First().Bank = model.Bank;
            _serviceFreelancer.Update(freelancer);

            return Request.CreateResponse(HttpStatusCode.OK);
        }


        [Route("GetBankInfo")]
        [HttpGet]
        public HttpResponseMessage GetBankInfo(string freelancerId)
        {
            var id = int.Parse(Backend.Crowd.Domain.Util.Security.Criptografy.Decrypt(freelancerId, ConfigurationManager.AppSettings["cryptoPass"]));
            var freelancer = _serviceFreelancer.GetById(id);
            if (freelancer == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Freelancer not found.");

            var model = new FreelancerBankInfoViewModel()
            {
                Id = freelancerId,
                BankFullName = freelancer.Payments.First().Bank,
                BankAgency = freelancer.Payments.First().Agency,
                BankAccount = freelancer.Payments.First().Account,
                CPF = freelancer.Payments.First().CPF,
                Bank = freelancer.Payments.First().Bank
            };

            return Request.CreateResponse(HttpStatusCode.OK, new { model });
        }

        [Route("SearchByNameOrEmail")]
        [HttpGet]
        public HttpResponseMessage SearchByNameOrEmail(string term)
        {
            var freelancers = LuceneSearchController.Search(term, new string[] { "Name", "Email" }).ToList();

            var ret = new List<FreelancerAutocompleteViewModel>();

            freelancers.ForEach(item =>
            {
                ret.Add(new FreelancerAutocompleteViewModel()
                {
                    Email = item.Email,
                    Id = Backend.Crowd.Domain.Util.Security.Criptografy.Encrypt(item.Id.ToString(), ConfigurationManager.AppSettings["cryptoPass"]),
                    Name = item.Name,
                    Photo = (item.Users != null && item.Users.Count() != 0) ? item.Users.First().Photo : "",
                    Title = item.Title
                });
            });

            return Request.CreateResponse(HttpStatusCode.OK, ret);
        }

        [Route("Tasks/Count")]
        [HttpGet]
        public IHttpActionResult CountTask(int user_Id)
        {
            var user = _serviceUser.GetById(user_Id);
            if (user == null)
                return BadRequest();

            var tasks = _serviceTasks
                .GetAll(x => x.IdFreelancer == user.IdFreelancer && x.Messages.Count > 0 && x.Active).ToList();

            if (user.Role == RoleType.FREELANCER)
                tasks = tasks.Where(x => x.Messages.Any(
                    y => y.FreelancerId == user.IdFreelancer && !y.Read && y.UserId.HasValue)).ToList();

            else
                tasks = tasks.Where(x => x.Messages.Any(y => !y.Read && y.UserId.HasValue)).ToList();

            return Ok(new { Count = tasks.Count() });
        }



        [AllowAnonymous]
        [Route("UpdateGUID")]
        public HttpResponseMessage UpdateGUID()
        {
            var freelancers = _serviceFreelancer.GetAll(true).Where(x => string.IsNullOrEmpty(x.Code.ToString()));

            freelancers.ToList().ForEach(item =>
            {
                item.Code = Guid.NewGuid();
                _serviceFreelancer.Update(item);
            });

            return Request.CreateResponse(HttpStatusCode.OK, freelancers.Count().ToString());
        }


        [AllowAnonymous]
        [Route("ResendConfirmationMail")]
        public HttpResponseMessage ResendConfirmationMail(string email)
        {
            var freelancer = _serviceFreelancer.GetByExpression(x => x.Email == email);

            if (freelancer == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Freelancer does not exist");

            Email sendEmail = new Email();
            //sendEmail.Send(model.Email, model.Name, "Seja muito bem vindo a Crowd!", "new-freelancer");
            sendEmail.Send(freelancer.Email, freelancer.Name, "Seja muito bem vindo a Crowd!", "confirm-email",
                new Dictionary<string, string> {
                        { "CONFIRMEMAIL", freelancer.Users.First().ConfirmEmail.ToString() },
                        { "EMAIL", freelancer.Email }
                });

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private User GetUserOnRequest(HttpRequestMessage request)
        {
            var handler = new JwtSecurityTokenHandler();

            var jsonToken = handler.ReadToken(request.Headers.Authorization.Parameter) as JwtSecurityToken;
            var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

            var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

            return requestUser;
        }

        #region Awards

        #endregion


        #region Portfolio


        [Route("AddPortfolio")]
        [HttpPost]
        public HttpResponseMessage AddPortfolio(PortfolioViewModel model)
        {
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Modelo inv�lido");

            if (string.IsNullOrEmpty(model.Title))
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Title inexistente");

            if (string.IsNullOrEmpty(model.Media))
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Media inexistente");

            var requestUser = GetUserOnRequest(Request);

            var freelancer = _serviceFreelancer.GetById(requestUser.Freelancer.Id);
            if (freelancer == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Freelancer not found.");
            var portfolio = new Freelancer.Portfolio();
            if (!string.IsNullOrEmpty(model.Id.ToString()) && model.Id != 0)
            {
                portfolio = _servicePortfolio.GetById(model.Id);

                if (portfolio == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Porfolio does not exist.");
            }

            var ret = new PortfolioGetViewModel();

            if (!string.IsNullOrEmpty(model.Title) &&
                !string.IsNullOrEmpty(model.Media)
                ) {
                portfolio.Title = model.Title;
                if (model.Type == FileType.IMAGE)
                    portfolio.Media = GenerateNewFile(model.Media,requestUser.Id);
                else
                    portfolio.Media = model.Media;
                portfolio.Type = model.Type;
                portfolio.Description = model.Description;
                portfolio.ClientName = model.ClientName;
                portfolio.FreelancerId = freelancer.Id;
                portfolio.URL = model.URL;
                if (model.Type == FileType.IMAGE)
                    portfolio.Thumb = GenerateNewFile(model.Thumb, requestUser.Id);
                else
                    portfolio.Thumb = model.Thumb;

                if (string.IsNullOrEmpty(model.Id.ToString()) || model.Id == 0)
                {
                    portfolio.Order = 0;
                    _servicePortfolio.Add(portfolio);
                }
                else
                {
                    _servicePortfolio.Update(portfolio);
                }

                ret = new PortfolioGetViewModel()
                {
                    ClientName = portfolio.ClientName,
                    Description = portfolio.Description,
                    FreelancerId = freelancer.Id,
                    Id = portfolio.Id,
                    Media = portfolio.Media,
                    Title = portfolio.Title,
                    Type = portfolio.Type,
                    URL = portfolio.URL
                };
            }

            if (!string.IsNullOrEmpty(model.PortfolioURL))
            {
                freelancer.PortfolioURL = model.PortfolioURL;
                _serviceFreelancer.Update(freelancer);

                ret.PortfolioURL = model.PortfolioURL;
            }

            return Request.CreateResponse(HttpStatusCode.OK, ret);
        }

        private string GenerateNewFile(string media, int id)
        {
            string oldpath = System.Web.Hosting.HostingEnvironment.MapPath(media);
            string newpath = string.Empty;
            if (File.Exists(oldpath))
            {
                string sPath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/images/user/portfolio"),id.ToString());

                if (!Directory.Exists(sPath))
                    Directory.CreateDirectory(sPath);

                newpath = Path.Combine(sPath, Path.GetFileName(media));
           
                File.Move(oldpath, newpath);
            }
           
            return String.Concat("/Content/images/user/portfolio/",id.ToString(),"/",Path.GetFileName(media));
        }

        [Route("ListPortfolio")]
        [HttpGet]
        public HttpResponseMessage ListPortfolio(string code)
        {
            var requestUser = GetUserOnRequest(Request);

            var freelancer = _serviceFreelancer.GetByCode(code);

            if (freelancer == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Freelancer not found.");

            var portfolios = _servicePortfolio.GetAll(x => x.FreelancerId == freelancer.Id).OrderBy(x => x.Order);

            var ret = new List<PortfolioGetViewModel>();

            portfolios.ToList().ForEach(item =>
            {
                ret.Add(new PortfolioGetViewModel()
                {
                    ClientName = item.ClientName,
                    Description = item.Description,
                    FreelancerId = freelancer.Id,
                    Id = item.Id,
                    Media = item.Media,
                    Title = item.Title,
                    Type = item.Type,
                    URL = item.URL,
                    Thumb = item.Thumb
                });
            });

            return Request.CreateResponse(HttpStatusCode.OK, ret);
        }


        
        [Route("GetPortfolio")]
        [HttpGet]
        public HttpResponseMessage GetPortfolio(int portfolio_id)
        {
            var requestUser = GetUserOnRequest(Request);

            var portfolio = _servicePortfolio.GetById(portfolio_id);
            if (portfolio == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Portfolio does not exist.");

            if (portfolio.FreelancerId != requestUser.Freelancer.Id)
                return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "You don't have permission to get this item.");

            var ret = new PortfolioGetViewModel()
            {
                ClientName = portfolio.ClientName,
                Description = portfolio.Description,
                FreelancerId = requestUser.Freelancer.Id,
                Id = portfolio.Id,
                Media = portfolio.Media,
                Title = portfolio.Title,
                Type = portfolio.Type,
                URL = portfolio.URL,
                PortfolioURL = requestUser.Freelancer.PortfolioURL,
                Thumb = portfolio.Thumb
            };

            return Request.CreateResponse(HttpStatusCode.OK, ret);
        }

        
        [Route("RemovePortfolio")]
        [HttpPost]
        public HttpResponseMessage RemovePortfolio([FromBody]int portfolio_id)
        {
            var requestUser = GetUserOnRequest(Request);

            var freelancer = _serviceFreelancer.GetById(requestUser.Freelancer.Id);
            if (freelancer == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Freelancer does not exist.");

            var portfolio = _servicePortfolio.GetById(portfolio_id);
            if (portfolio == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Portfolio does not exist.");

            if (freelancer.Id != portfolio.FreelancerId)
                return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "You don't have permission to remove this item.");

            _servicePortfolio.Remove(portfolio);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("OrderPortfolio")]
        [HttpPost]
        public HttpResponseMessage OrderPortfolio(List<PortfolioOrderViewModel> portfolios)
        {
            var requestUser = GetUserOnRequest(Request);            

            var freelancer = _serviceFreelancer.GetById(requestUser.Freelancer.Id);
            if (freelancer == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Freelancer does not exist.");

            portfolios.ForEach(item =>
            {
                var portfolio = _servicePortfolio.GetById(item.PortfolioId);
                portfolio.Order = item.Order;
                _servicePortfolio.Update(portfolio);                
            });

            return Request.CreateResponse(HttpStatusCode.OK);
        }


        [AllowAnonymous()]
        [Route("AttachPortfolioFile")]
        public HttpResponseMessage AttachPortfolioFile()
        {
            int iUploadedCnt = 0;

            string folder = Path.Combine("/Content/images/user/portfolio/temp/");
            string sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/images/user/portfolio/temp/");

            HttpFileCollection hfc = HttpContext.Current.Request.Files;

            var path = "";
            var filename = "";

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                HttpPostedFile hpf = hfc[iCnt];
                if (hpf.ContentLength > 10485760)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "File size exceed limit of 10MB");
                var fileName = Guid.NewGuid().ToString();
                var fileExtenstion = Path.GetExtension(hpf.FileName);
                if (hpf.ContentLength > 0)
                {
                    if (sPath != null && !Directory.Exists(sPath))
                        Directory.CreateDirectory(sPath);

                    if (!File.Exists(sPath + Path.GetFileName(fileName + fileExtenstion)))
                    {
                        hpf.SaveAs(sPath + fileName + fileExtenstion);
                        filename = fileName + fileExtenstion;
                        path = folder + fileName + fileExtenstion;
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt > 0)
            {
                return Request.CreateResponse(path);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Cannot upload file");
            }
        }

        #endregion

    }
}
