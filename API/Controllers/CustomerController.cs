using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ImageResizer;
using API.Models;
using Backend.Crowd.Domain.Util;
using Backend.Crowd.Domain.Util.Security;
using Backend.Crowd.Domain.Util.Network;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Entities.Enum;
using System.Configuration;
using API.Models.Customer;
using FluentValidation.Results;
using AutoMapper;
using API.Models.freelancer;
using API.Models.User;

namespace API.Controllers
{

    [RoutePrefix("Customer")]
    public class CustomerController : BaseAPIController
    {
        private IServiceBase<Customer> _serviceCustomer;
        private IServiceBase<Customer.CustomerTracking> _serviceCustomerTracking;
        private IServiceBase<State> _serviceState;
        private IServiceBase<City> _serviceCity;
        private IServiceFreelancer _serviceFreelancer;
        private IServiceBase<Project> _serviceProject;
        private Intercom.ICommunication _serviceIntercom;
        private IEmail _sendEmail;
        private IMapper _map;

        public CustomerController()
        {

        }

        public CustomerController(
            IServiceBase<Customer> serviceCustomer,
            IServiceFreelancer serviceFreelancer,
            IServiceBase<State> serviceState,
            IServiceBase<City> serviceCity,
            IServiceUser serviceUser,
            IServiceBase<Customer.CustomerTracking> serviceCustomerTracking,
            IServiceBase<Project> serviceProject,
            Intercom.ICommunication serviceIntercom,
            IServiceBase<User.ActionLog> serviceActionLog,
            IEmail email, IMapper map, Serilog.ILogger log
            ) : base(serviceUser, serviceActionLog, log)
        {
            _serviceCustomer = serviceCustomer;
            _serviceCity = serviceCity;
            _serviceState = serviceState;
            _serviceCustomerTracking = serviceCustomerTracking;
            _serviceFreelancer = serviceFreelancer;
            _serviceIntercom = serviceIntercom;
            _serviceProject = serviceProject;
            _sendEmail = email;
            _map = map;
        }


        [Route("Get")]
        public CustomerViewModel Get(int customer_id)
        {
            var customer = _serviceCustomer.GetById(customer_id);

            var users = _serviceUser.GetAll(u => u.IdCustomer == customer_id && u.Role == RoleType.CLIENT);
            var response = new CustomerViewModel();
            if (customer == null)
                return response;

            response = new CustomerViewModel()
            {
                Id = customer.Id,
                Name = customer.Name,
                Trade = customer.Trade,
                NameResponsible = customer.NameResponsible,
                EmailResponsible = customer.EmailResponsible,
                Logo = customer.Logo,
                Photo = users.First(u => u.IdCustomer == customer.Id).Photo,
                Active = customer.Active,
                Address = customer.Address,
                CNPJ = customer.CNPJ,
                Complement = customer.Complement,
                InscricaoEstadual = customer.InscricaoEstadual,
                InscricaoMunicipal = customer.InscricaoMunicipal,
                Number = customer.Number,
                Prospect = customer.Prospect,
                Phone = customer.Phone,
                PlanType = customer.PlanType,
                CEP = customer.CEP,
                Trial = customer.Trial ?? false,
                TrialDate = customer.TrialDate,
                PassReset = users.First(u => u.IdCustomer == customer.Id).IsResetting,
                RoleCompany = users.First(u => u.IdCustomer == customer.Id).RoleCompany,
                AcceptTerms = users.First(u => u.IdCustomer == customer.Id).AcceptTerms
            };

            if (customer.QtyPersons > 0)
                response.QtyPersons = customer.QtyPersons;

            var city = new CityViewModel();
            var state = new StateViewModel();

            if (customer.City != null)
            {
                response.City = new CityViewModel()
                {

                    Id = customer.City.Id,
                    Name = customer.City.Name,
                    StateId = customer.City.StateId

                };
            }

            if (customer.State != null)
            {
                response.State = new StateViewModel()
                {
                    Id = customer.State.Id,
                    UF = customer.State.UF,
                    Name = customer.State.Name
                };
            }

            return response;
        }


        [Route("Add")]
        public HttpResponseMessage Add(CustomerViewModel model)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Model");

            var customer = new Customer();
            string folder = "/Content/images/customer/";
            User user;

            if (model.Id == 0 || string.IsNullOrEmpty(model.Id.ToString()))
            {
                user = _serviceUser.GetByExpression(x => x.Email == model.EmailResponsible);

                if (user != null)
                    return Request.CreateResponse(HttpStatusCode.Conflict, "Email already exist");

                if (!string.IsNullOrEmpty(model.CNPJ))
                {
                    if (!Validations.Validations.validarCNPJ(model.CNPJ))
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "CNPJ is not valid");

                    var cnpjExist = _serviceCustomer.GetAll(x => x.CNPJ == model.CNPJ);
                    if (cnpjExist.Count() != 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.Conflict, new
                        {
                            message = "CNPJ is in use by another customer. Please contact the admin.",
                            contact = cnpjExist.OrderByDescending(x => x.CreatedAt).First().NameResponsible
                        });
                    }
                }

                customer.Name = model.Name;
                customer.Trade = model.Trade;
                customer.CNPJ = model.CNPJ;
                customer.QtyPersons = model.QtyPersons ?? 0;
                customer.City = _serviceCity.GetById(model.City.Id);
                customer.State = _serviceState.GetById(model.State.Id);
                customer.EmailResponsible = model.EmailResponsible;
                customer.NameResponsible = model.NameResponsible;
                customer.InscricaoEstadual = model.InscricaoEstadual;
                customer.InscricaoMunicipal = model.InscricaoMunicipal;
                customer.Address = model.Address;
                customer.Complement = model.Complement;
                customer.Number = model.Number;
                customer.CEP = model.CEP;

                if (!String.IsNullOrEmpty(model.LogoBase64))
                    customer.Logo = GetImage(model.LogoBase64, model.LogoExtension, folder);
                customer.Phone = model.Phone;
                customer.PlanType = model.PlanType == 0 ? PlanType.FREE : model.PlanType;
                customer.Active = false;
                customer.Trial = false;

                _serviceCustomer.Add(customer);

                var password = System.Web.Security.Membership.GeneratePassword(8, 4);
                var newuser = new User()
                {
                    Email = model.EmailResponsible,
                    Name = model.NameResponsible,
                    Password = password,
                    IdCustomer = customer.Id,
                    Role = RoleType.CLIENT,
                    Active = false,
                    AcceptTerms = model.AcceptTerms,
                    AcceptTermsAt = DateTime.Now,
                    ConfirmEmail = Guid.NewGuid(),
                    RoleCompany = model.RoleCompany
                };

                _serviceUser.Add(newuser);

                var project = new Project
                {
                    Active = true,
                    Color = "blue",
                    UpdatedAt = DateTime.Now,
                    IdCustomer = customer.Id,
                    Name = "Meu Projeto"
                };

                _serviceProject.Add(project);

                var company = _map.Map<Customer, Intercom.Data.Company>(customer);

                company.custom_attributes = new Dictionary<string, object>
                    {
                        { "email", customer.EmailResponsible },
                        { "responsibleId", user.Id },
                        { "responsible", customer.Name }
                    };

                company = _serviceIntercom.SendCompany(company);
                var contact = _map.Map<User, Intercom.Data.Contact>(newuser);

                contact.companies = new List<Intercom.Data.Company>();

                contact.companies.Add(company);

                contact = _serviceIntercom.SendNewLead(contact);

                _sendEmail.Send(user.Email, user.Name, "Seja muito bem vindo a Crowd!", "confirm-email",
                    new Dictionary<string, string> {
                        { "CONFIRMEMAIL", user.ConfirmEmail.ToString() },
                        { "EMAIL", user.Email }
                });

            }
            else
            {
                var existingCustomer = _serviceCustomer.GetById(model.Id);
                customer = existingCustomer;

                customer.Id = model.Id;
                customer.Name = model.Name;
                customer.Trade = model.Trade;
                customer.CNPJ = model.CNPJ;
                customer.QtyPersons = model.QtyPersons ?? 0;
                if (model.City != null && model.City.Id > 0)
                    customer.City = _serviceCity.GetById(model.City.Id);
                if (model.State != null && model.State.Id > 0)
                    customer.State = _serviceState.GetById(model.State.Id);
                customer.EmailResponsible = model.EmailResponsible;
                customer.NameResponsible = model.NameResponsible;
                customer.InscricaoEstadual = model.InscricaoEstadual;
                customer.InscricaoMunicipal = model.InscricaoMunicipal;
                customer.Address = model.Address;
                customer.Complement = model.Complement;
                customer.Number = model.Number;
                customer.CEP = model.CEP;
                customer.Phone = model.Phone;
                customer.Active = model.Active;
                customer.Logo = model.Logo;

                if (customer.PlanType != model.PlanType)
                    customer.LastChangePlan = DateTime.Now;

                customer.PlanType = model.PlanType == 0 ? PlanType.FREE : model.PlanType;

                if (!String.IsNullOrEmpty(model.LogoBase64))
                    customer.Logo = GetImage(model.LogoBase64, model.LogoExtension, folder);

                customer.Trial = model.Trial;

                if (!customer.Prospect.Value && model.Trial)
                    customer.TrialDate = model.TrialDate;

                user = this.RequestTokenUser();

                CustomerProspectValidator validator = new CustomerProspectValidator(_serviceCustomer);
                ValidationResult validations = validator.Validate(customer);

                if (model.PassReset.HasValue && model.PassReset.Value)
                {
                    user.IsResetting = true;
                    user.Password = Criptografy.GetMD5Hash("somoscrowd");
                }

                if (!String.IsNullOrEmpty(model.Password))
                {
                    user.Password = Criptografy.GetMD5Hash(model.Password);
                }

                user.Photo = model.Photo;

                user.Email = model.EmailResponsible;
                user.Name = model.NameResponsible;

                user.IdCustomer = customer.Id;
                user.RoleCompany = model.RoleCompany;

                string folderPhoto = "/Content/images/user/";
                if (!String.IsNullOrEmpty(model.PhotoBase64))
                    user.Photo = GetImage(model.PhotoBase64, model.PhotoExtension, folderPhoto);

                UserProspectValidator validatoruser = new UserProspectValidator();
                ValidationResult validationUser = validatoruser.Validate(user);

                if (customer.Prospect.Value)
                {
                    if (validations.IsValid && validationUser.IsValid)
                    {
                        customer.Prospect = false;
                        customer.Trial = false;
                        //customer.TrialDate = DateTime.Now.AddDays(14);
                        customer.Active = true;

                        var project = new Project
                        {
                            Active = true,
                            Color = "blue",
                            UpdatedAt = DateTime.Now,
                            IdCustomer = customer.Id,
                            Name = "Meu Projeto"
                        };

                        _serviceProject.Add(project);

                        Intercom.Data.User intercomuser = _map.Map<User, Intercom.Data.User>(user);

                        _serviceIntercom.ChangeLeadtoUser(intercomuser);

                        _sendEmail.Send(ConfigurationManager.AppSettings["contactEmail"], "Comunidade Crowd", "Cadastro de nova empresa", "new-interested-customer",
                                          new Dictionary<string, string> {
                                                { "RAZAO", customer.Name },
                                                { "FANTASIA", customer.Trade },
                                                { "CNPJ", customer.CNPJ },
                                                { "RESPONSAVEL", customer.NameResponsible },
                                                { "EMAIL", customer.EmailResponsible },
                                                { "PHONE", customer.Phone },
                                                { "QTD", customer.QtyPersons.ToString() },
                                                { "ADDRESS", customer.Address },
                                                { "NUMBER", customer.Number },
                                                { "COMPLEMENT", string.IsNullOrEmpty(customer.Complement) ? " " : customer.Complement },
                                                { "CITY", customer.City.Name },
                                                { "STATE", customer.State.UF },
                                                { "PLANTYPE", customer.PlanType.ToString() + (customer.Trial == true ? " - Trial" : "") },
                                                { "REFERRER", customer.Referrer ?? "Plataforma" }
                                            });
                    }
                }

                _serviceCustomer.Update(customer);

                try
                {
                    _serviceUser.Update(user);
                }
                catch (Exception)
                {
                    return Request.CreateResponse(HttpStatusCode.Conflict, "Email already exist");
                }

                if (!validations.IsValid)
                    return Request.CreateResponse(HttpStatusCode.PreconditionFailed, ReturnValidationError(validations.Errors));

                if (!validationUser.IsValid)
                    return Request.CreateResponse(HttpStatusCode.PreconditionFailed, ReturnValidationError(validationUser.Errors));

            }

            model.Id = customer.Id;
            model.Logo = customer.Logo;
            model.Active = customer.Active;
            model.Photo = user.Photo;

            return Request.CreateResponse(HttpStatusCode.OK, CreateLogin(user.Email, null, user.IsProspect ? ConfigurationManager.AppSettings["customerProspectPath"] : ConfigurationManager.AppSettings["customerPath"]));
        }

        [Route("Edit")]
        public HttpResponseMessage Edit(CustomerViewModel model)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Model");

            var customer = new Customer();
            string folder = "/Content/images/customer/";
            User user = null;

            if (model.Id == 0 || string.IsNullOrEmpty(model.Id.ToString()))
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Model without Id");

            var existingCustomer = _serviceCustomer.GetById(model.Id);
            customer = existingCustomer;

            customer.Id = model.Id;
            customer.Name = model.Name;
            customer.Trade = model.Trade;
            customer.CNPJ = model.CNPJ;
            customer.QtyPersons = model.QtyPersons ?? 0;
            if (model.City != null && model.City.Id > 0)
                customer.City = _serviceCity.GetById(model.City.Id);
            if (model.State != null && model.State.Id > 0)
                customer.State = _serviceState.GetById(model.State.Id);
            customer.EmailResponsible = model.EmailResponsible;
            customer.NameResponsible = model.NameResponsible;
            customer.InscricaoEstadual = model.InscricaoEstadual;
            customer.InscricaoMunicipal = model.InscricaoMunicipal;
            customer.Address = model.Address;
            customer.Complement = model.Complement;
            customer.Number = model.Number;
            customer.CEP = model.CEP;
            customer.Phone = model.Phone;
            customer.Active = model.Active;
            customer.Logo = model.Logo;

            if (customer.PlanType != model.PlanType)
                customer.LastChangePlan = DateTime.Now;

            customer.PlanType = model.PlanType == 0 ? PlanType.FREE : model.PlanType;

            if (!String.IsNullOrEmpty(model.LogoBase64))
                customer.Logo = GetImage(model.LogoBase64, model.LogoExtension, folder);

            customer.Trial = model.Trial;

            if (!customer.Prospect.Value && model.Trial)
                customer.TrialDate = DateTime.Now.AddDays(14);

            CustomerProspectValidator validator = new CustomerProspectValidator(_serviceCustomer);
            ValidationResult validations = validator.Validate(customer);

            if (model.PassReset.HasValue && model.PassReset.Value)
            {
                user = _serviceUser.GetByEmail(model.EmailResponsible);

                if (user == null)
                {
                    user = _serviceUser.GetByExpression(u => u.IdCustomer == model.Id && u.Role == RoleType.CLIENT);
                }

                if (user == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Customer without Administrator");
                }

                model.EmailResponsible = user.Email;
                model.NameResponsible = user.Name;

                user.IsResetting = true;
                user.Password = Criptografy.GetMD5Hash("somoscrowd");

                try
                {
                    _serviceUser.Update(user);
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.Conflict, ex.Message);
                }
            }


            if (customer.Prospect.Value && user != null)
            {
                if (validations.IsValid)
                {
                    customer.Prospect = false;
                    customer.Trial = false;
                    //customer.TrialDate = DateTime.Now.AddDays(14);
                    customer.Active = true;

                    var project = new Project
                    {
                        Active = true,
                        Color = "blue",
                        UpdatedAt = DateTime.Now,
                        IdCustomer = customer.Id,
                        Name = "Meu Projeto"
                    };

                    _serviceProject.Add(project);

                    Intercom.Data.User intercomuser = _map.Map<User, Intercom.Data.User>(user);

                    _serviceIntercom.ChangeLeadtoUser(intercomuser);

                    _sendEmail.Send(ConfigurationManager.AppSettings["contactEmail"], "Comunidade Crowd", "Cadastro de nova empresa", "new-interested-customer",
                                      new Dictionary<string, string> {
                                                { "RAZAO", customer.Name },
                                                { "FANTASIA", customer.Trade },
                                                { "CNPJ", customer.CNPJ },
                                                { "RESPONSAVEL", customer.NameResponsible },
                                                { "EMAIL", customer.EmailResponsible },
                                                { "PHONE", customer.Phone },
                                                { "QTD", customer.QtyPersons.ToString() },
                                                { "ADDRESS", customer.Address },
                                                { "NUMBER", customer.Number },
                                                { "COMPLEMENT", string.IsNullOrEmpty(customer.Complement) ? " " : customer.Complement },
                                                { "CITY", customer.City.Name },
                                                { "STATE", customer.State.UF },
                                                { "PLANTYPE", customer.PlanType.ToString() + (customer.Trial == true ? " - Trial" : "") },
                                                { "REFERRER", customer.Referrer ?? "Plataforma" }
                                        });
                }
            }

            _serviceCustomer.Update(customer);

            if (!validations.IsValid)
                return Request.CreateResponse(HttpStatusCode.PreconditionFailed, ReturnValidationError(validations.Errors));

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("AddUser")]
        public HttpResponseMessage AddUser(CustomerUserViewModel model)
        {

            var password = model.Password;
            User user = null;
            var existing_user = _serviceUser.GetByExpression(x => x.Email == model.Email);
            var customer = _serviceCustomer.GetById(model.IdCustomer);

            if (customer == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "Cliente não encontrado.");

            if (string.IsNullOrEmpty(model.Id.ToString()))
            {

                if (existing_user != null)
                {
                    return Request.CreateResponse(HttpStatusCode.Conflict, "Email já cadastrado!");
                }

                user = new User()
                {
                    IdCustomer = model.IdCustomer,
                    Name = model.Name,
                    Email = model.Email,
                    RoleCompany = model.RoleCompany,
                    Role = RoleType.EMPLOYEE
                };

            }
            else
            {
                user = existing_user;
            }


            if (!string.IsNullOrEmpty(model.Id.ToString()))
            {
                user.Name = model.Name;
                user.Email = model.Email;
                user.RoleCompany = model.RoleCompany;
                user.Phone = model.Phone;

                user.AcceptTerms = model.AcceptTerms;
                user.AcceptTermsAt = DateTime.Now;

                if (!string.IsNullOrEmpty(password))
                    user.Password = Criptografy.GetMD5Hash(password);

                string folder = "/Content/images/user/";
                string path = HttpContext.Current.Server.MapPath("~/Content/images/user/");
                if (!string.IsNullOrEmpty(model.PhotoBase64))
                {
                    byte[] bytes = Convert.FromBase64String(model.PhotoBase64);
                    string filename = Guid.NewGuid().ToString();
                    path += filename + "." + model.PhotoExtension;
                    File.WriteAllBytes(path, bytes);
                    user.Photo = folder + filename + "." + model.PhotoExtension;
                }

                _serviceUser.Update(user);
            }
            else
            {
                user.Password = password;
                user.Active = true;
                user.ConfirmEmail = Guid.NewGuid();
                user.AcceptTerms = false;
                user.AcceptTermsAt = DateTime.Now;
                _serviceUser.Add(user);
            }

            model.Id = user.Id;
            model.Active = user.Active;
            model.Role = RoleType.EMPLOYEE;

            if (existing_user == null)
            {
                Email sendEmail = new Email();
                sendEmail.Send(user.Email, user.Name, "Seja muito bem vindo a Crowd!", "confirm-email-new-customer-user",
                    new Dictionary<string, string> {
                        { "DE", customer.NameResponsible },
                        { "EMPRESA", customer.Name },
                        { "USER", user.Email },
                        { "PASS", password },
                        { "CONFIRMEMAIL", user.ConfirmEmail.ToString() },
                        { "EMAIL", user.Email }
                });
            }

            return Request.CreateResponse(new { model });
        }

        [Route("ChangeStatusUser")]
        [HttpPost]
        public HttpResponseMessage ChangeStatusUser(int userId, bool active)
        {
            var user = _serviceUser.GetById(userId);
            user.Active = !user.Active;

            _serviceUser.Update(user);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("ChangeStatusCustomer")]
        [HttpPost]
        public HttpResponseMessage ChangeStatusCustomer(int customerId, bool active)
        {
            var customer = _serviceCustomer.GetById(customerId);
            customer.Active = !customer.Active;
            if (active)
                customer.Prospect = false;

            customer.LastChangeActivation = DateTime.Now;

            _serviceCustomer.Update(customer);

            var users = _serviceUser.GetAll(x => x.IdCustomer == customer.Id);

            if (active)
            {
                var password = System.Web.Security.Membership.GeneratePassword(8, 4);
                var user = users.First();
                user.Active = true;
                _serviceUser.ChangePassword(user.Id, password);
                _serviceUser.Update(user);

                _sendEmail.Send(user.Email, user.Name, "Seja muito bem vindo a Crowd!", "new-customer-user",
                    new Dictionary<string, string> {
                        { "DE", customer.NameResponsible },
                        { "EMPRESA", customer.Name },
                        { "USER", user.Email },
                        { "PASS", password } });
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [Route("GetCustomers")]
        public List<CustomerViewModel> GetCustomers()
        {
            var response = new List<CustomerViewModel>();
            try
            {
                var customers = _serviceCustomer.GetAll(true).OrderByDescending(x => x.Id);
                var users = _serviceUser.GetAll(u => u.IdCustomer > 0);

                foreach (var item in customers)
                {
                    var customer = new CustomerViewModel
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Trade = item.Trade,
                        NameResponsible = item.NameResponsible,
                        EmailResponsible = item.EmailResponsible,
                        Logo = item.Logo,
                        Active = item.Active,
                        Address = item.Address,
                        CEP = item.CEP,
                        CNPJ = item.CNPJ,
                        Complement = item.Complement,
                        InscricaoEstadual = item.InscricaoEstadual,
                        InscricaoMunicipal = item.InscricaoMunicipal,
                        Number = item.Number,
                        QtyPersons = item.QtyPersons,
                        Prospect = item.Prospect,
                        Phone = item.Phone,
                        PlanType = item.PlanType,
                        Referrer = item.Referrer,
                        Trial = item.Trial ?? false,
                        TrialDate = item.TrialDate,
                        PassReset = users.First(u => u.IdCustomer == item.Id).IsResetting
                    };

                    if (item.State != null)
                        customer.State = new StateViewModel
                        {
                            Id = item.State.Id,
                            Name = item.State.Name,
                            UF = item.State.UF
                        };

                    if (item.City != null)
                        customer.City = new CityViewModel
                        {
                            Id = item.City.Id,
                            Name = item.City.Name,
                            StateId = item.City.StateId
                        };

                    response.Add(customer);
                };
            }
            catch (Exception ex)
            {
                _log.Error("GetCustomers: Error: {erro}", ex.Message);
            }

            return response;
        }

        [Route("GetCustomerUser")]
        public List<CustomerUserViewModel> GetCustomerUser(int customerId)
        {
            var customersUsers = _serviceUser.GetAll(x => x.IdCustomer == customerId).OrderBy(x => x.Role);

            var response = new List<CustomerUserViewModel>();

            customersUsers.ToList().ForEach(item => response.Add(
                new CustomerUserViewModel()
                {
                    Email = item.Email,
                    RoleCompany = item.RoleCompany,
                    Id = item.Id,
                    Name = item.Name,
                    Photo = item.Photo,
                    Active = item.Active,
                    Role = item.Role,
                    Phone = item.Phone
                }
            ));

            return response;
        }


        [AllowAnonymous()]
        [Route("AttachFile")]
        public HttpResponseMessage AttachFile()
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string folder = "/Content/images/customer/";
            string sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/images/customer/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            var path = "";
            var filename = "";

            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];
                var fileName = Guid.NewGuid().ToString();
                var fileExtenstion = Path.GetExtension(hpf.FileName);
                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists(sPath + Path.GetFileName(fileName + fileExtenstion)))
                    {
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + fileName + fileExtenstion);
                        filename = fileName + fileExtenstion;
                        path = sPath + fileName + fileExtenstion;
                        //ImageBuilder.Current.Build(path, path,
                        //                    new ResizeSettings("width=80"));
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt > 0)
            {
                return Request.CreateResponse(new { folder, filename });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Cannot upload file");
            }
        }

        [AllowAnonymous]
        [Route("PreCadastro")]
        public HttpResponseMessage PreCadastro(PreCadastroCustomerViewModel model)
        {
            model.Phone = Validador.ClearPhone(model.Phone);

            PreCadastroCustomerViewModelValidator validator = new PreCadastroCustomerViewModelValidator(_serviceUser);
            ValidationResult validations = validator.Validate(model);

            if (!validations.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, validations.Errors.Aggregate("", (a, v) => a += v.ErrorMessage));

            var customer = new Customer()
            {
                Trade = model.Trade,
                NameResponsible = model.Name,
                EmailResponsible = model.Email,
                Active = false,
                Prospect = true,
                Phone = model.Phone,
                PlanType = model.PlanType == 0 ? PlanType.FREE : model.PlanType,
                Referrer = model.Referrer,
                PromoCode = model.PromoCode
            };

            _serviceCustomer.Add(customer);

            var user = new User()
            {
                Email = model.Email,
                Name = model.Name,
                IdCustomer = customer.Id,
                Role = RoleType.CLIENT,
                Active = true,
                ConfirmEmail = Guid.NewGuid(),
                AcceptTerms = model.AcceptTerms,
                AcceptTermsAt = DateTime.Now,
                Phone = model.Phone
            };

            _serviceUser.Add(user);

            var company = _map.Map<Customer, Intercom.Data.Company>(customer);

            company.custom_attributes = new Dictionary<string, object>
                    {
                        { "email", customer.EmailResponsible },
                        { "responsible", customer.NameResponsible },
                        { "last_change_plan", customer.LastChangePlan ?? DateTime.Now }
                    };

            company = _serviceIntercom.SendCompany(company);

            var contact = _map.Map<User, Intercom.Data.Contact>(user);

            contact.companies = new List<Intercom.Data.Company>();

            contact.companies.Add(company);

            contact.custom_attributes.Add("IdCustomer", customer.Id);

            contact = _serviceIntercom.SendNewLead(contact);

            user.IdLead = contact.user_id;

            _serviceUser.SendConfirmationEmail(user);

            return Request.CreateResponse(HttpStatusCode.OK, CreateLogin(model.Email, null, ConfigurationManager.AppSettings["customerProspectPath"]));
        }


        [AllowAnonymous]
        [Route("AddInterestedClient")]
        [Obsolete("Substituido por PreCadastro")]
        public HttpResponseMessage AddInterestedClient(CustomerInterestedViewModel model)
        {
            var userExist = _serviceUser.GetByExpression(x => x.Email == model.EmailResponsible);

            if (userExist != null)
                return Request.CreateResponse(HttpStatusCode.Forbidden, "This email is already used");

            if (!string.IsNullOrEmpty(model.CNPJ))
            {
                if (!Validations.Validations.validarCNPJ(model.CNPJ))
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "CNPJ is not valid");

                var cnpjExist = _serviceCustomer.GetAll(x => x.CNPJ == model.CNPJ);
                if (cnpjExist.Count() != 0)
                {
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        "CNPJ is in use by another customer. Please contact the admin."
);
                }
            }

            var customer = new Customer()
            {
                Name = model.Name,
                Trade = model.Trade,
                State = _serviceState.GetById(model.State),
                City = _serviceCity.GetById(model.City),
                Address = model.Address,
                Number = model.Number,
                Complement = model.Complement,
                NameResponsible = model.NameResponsible,
                EmailResponsible = model.EmailResponsible,
                QtyPersons = model.QtyPersons,
                Active = false,
                Prospect = true,
                Phone = model.Phone,
                PlanType = model.PlanType == 0 ? PlanType.FREE : model.PlanType,
                CNPJ = model.CNPJ,
                Referrer = model.Referrer
            };

            if (customer.PlanType == PlanType.FREE && model.Referrer == null)
            {
                customer.PlanType = PlanType.SMART;
                customer.Trial = true;
                customer.TrialDate = DateTime.Now.AddDays(15);
            }

            _serviceCustomer.Add(customer);

            var project = new Project
            {
                Active = true,
                Color = "blue",
                UpdatedAt = DateTime.Now,
                IdCustomer = customer.Id,
                Name = "Meu Projeto"
            };

            _serviceProject.Add(project);

            var password = System.Web.Security.Membership.GeneratePassword(8, 4);
            var user = new User()
            {
                Email = model.EmailResponsible,
                Name = model.NameResponsible,
                Password = password,
                IdCustomer = customer.Id,
                Role = RoleType.CLIENT,
                Active = false,
                ConfirmEmail = Guid.NewGuid(),
                AcceptTerms = model.AcceptTerms,
                AcceptTermsAt = DateTime.Now
            };

            _serviceUser.Add(user);

            var intercomuser = Mapper.Map<User, Intercom.Data.User>(user);

            if (user.IdCustomer > 0)
            {
                var company = _serviceIntercom.GetCompany(user.IdCustomer.Value);

                if (intercomuser.companies == null)
                    intercomuser.companies = new List<Intercom.Data.Company>();

                intercomuser.companies.Add(company);
            }
            else intercomuser.custom_attributes.Add("idFreelancer", user.IdFreelancer);

            _serviceIntercom.SendUser(intercomuser);

            _sendEmail.Send(user.Email, user.Name, "Obrigado por entrar para a Crowd", "prospect-customer");

            _sendEmail.Send(
                ConfigurationManager.AppSettings["contactEmail"], "Comunidade Crowd", "Cadastro de nova empresa", "new-interested-customer",
                new Dictionary<string, string> {
                    { "RAZAO", customer.Name },
                    { "FANTASIA", customer.Trade },
                    { "CNPJ", customer.CNPJ },
                    { "RESPONSAVEL", customer.NameResponsible },
                    { "EMAIL", customer.EmailResponsible },
                    { "PHONE", customer.Phone },
                    { "QTD", customer.QtyPersons.ToString() },
                    { "ADDRESS", customer.Address },
                    { "NUMBER", customer.Number },
                    { "COMPLEMENT", string.IsNullOrEmpty(customer.Complement) ? " " : customer.Complement },
                    { "CITY", customer.City.Name },
                    { "STATE", customer.State.UF },
                    { "PLANTYPE", customer.PlanType.ToString() + (customer.Trial == true ? " - Trial" : "") },
                    { "REFERRER", customer.Referrer ?? "Plataforma" }
                });

            model.Id = customer.Id;
            return Request.CreateResponse(HttpStatusCode.OK, CreateLogin(user.Email, password, ConfigurationManager.AppSettings["customerPath"]));
        }

        [HttpPost]
        [Route("ChangePlan")]
        public HttpResponseMessage ChangePlan(int userId, PlanType planType)
        {
            var user = _serviceUser.GetById(userId);

            if (user == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "This user does not exist.");

            if (user.Customer.PlanType == planType)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "This customer have the same plan selected.");

            var customer = _serviceCustomer.GetById(user.Customer.Id);
            var oldPlan = customer.PlanType;
            customer.PlanType = planType;
            customer.LastChangePlan = DateTime.Now;
            _serviceCustomer.Update(customer);

            var Intcustomer = _map.Map<Customer, Intercom.Data.Company>(customer);

            if (oldPlan != PlanType.FREE && planType == PlanType.FREE)
                Intcustomer.custom_attributes.Add("plano_cancelado", 1);

            _serviceIntercom.SendCompany(Intcustomer);

            //_sendEmail.Send(ConfigurationManager.AppSettings["plataformaEmail"], "Crowd", "Mudança de plano", "customer-change-plan",
            //    new Dictionary<string, string> {
            //        { "EMPLOYEE", user.Name },
            //        { "CUSTOMER", user.Customer.Trade },
            //        { "CURRENT", oldPlan.ToString() },
            //        { "NEW", planType.ToString() },
            //    });

            //if (user.Role == RoleType.EMPLOYEE)
            //{                
            //    _sendEmail.Send(user.Customer.EmailResponsible, user.Customer.NameResponsible, "Mudança de plano", "customer-change-plan",
            //        new Dictionary<string, string> {
            //            { "EMPLOYEE", user.Name },
            //            { "CUSTOMER", user.Customer.Trade },
            //            { "CURRENT", oldPlan.ToString() },
            //            { "NEW", planType.ToString() },
            //        });  
            //}

            return Request.CreateResponse(HttpStatusCode.OK, "Plan changed successfully");
        }

        [HttpPost]
        [Route("TrackField")]
        public HttpResponseMessage TrackField(CustomerTrackingFieldViewModel model)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var freelancerId = int.Parse(Criptografy.Decrypt(model.FreelancerId, ConfigurationManager.AppSettings["cryptoPass"]));
                var freelancer = _serviceFreelancer.GetById(freelancerId);
                var user = _serviceUser.GetById(model.UserId);

                if (freelancer == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Freelancer does not exist");

                if (user == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "User does not exist");

                if (user.Customer != null)
                {
                    var customerTracking = new Customer.CustomerTracking()
                    {
                        FreelancerId = freelancerId,
                        UserId = model.UserId,
                        FieldRequested = model.FieldRequested,
                        SearchTerm = model.SearchTerm,
                        PlanType = user.Customer.PlanType
                    };

                    _serviceCustomerTracking.Add(customerTracking);

                }

                response = Request.CreateResponse(HttpStatusCode.OK);

            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }

    }
}



