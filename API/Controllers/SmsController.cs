﻿using Algolia.API;
using API.Models;
using API.Models.Sms;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Util;
using HandlebarsDotNet;
using Intercom;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("Sms")]
    public class SmsController : BaseAPIController
    {
        private readonly IServiceSms _serviceSms;
        private readonly IServiceBriefing _serviceBriefing;
        private readonly IServiceFreelancer _serviceFreelancer;
        private readonly IServiceBase<Briefing.TokenFreelancer> _serviceTokenFreelancer;
        private readonly IServiceBase<Briefing.Propose> _servicePropose;
        private readonly IServiceBase<Briefing.Message> _serviceMessage;
        private readonly ISearch _searchservice;
        private ICommunication _serviceIntercom;

        public SmsController(   IServiceSms serviceSms, 
                                IServiceBriefing serviceBriefing,
                                IServiceFreelancer serviceFreelancer, 
                                IServiceBase<Briefing.TokenFreelancer> serviceTokenFreelancer,
                                IServiceBase<Briefing.Propose> servicePropose,
                                IServiceBase<Briefing.Message> serviceMessage,
                                ISearch searchservice,
                                ICommunication serviceIntercom)
        {
            _serviceSms = serviceSms;
            _serviceBriefing = serviceBriefing;
            _serviceFreelancer = serviceFreelancer;
            _serviceTokenFreelancer = serviceTokenFreelancer;
            _servicePropose = servicePropose;
            _serviceMessage = serviceMessage;
            _searchservice = searchservice;
            _serviceIntercom = serviceIntercom;
        }

        [HttpGet]
        [Route("")]
        [SMSAuthorization]
        public HttpResponseMessage Get()
        {
            var list = _serviceSms.GetAll(s => s.Active && (s.Status == SmsStatus.Queued || s.Status == SmsStatus.Created) && s.CreatedAt <= DateTime.Now).Select(s => new SmsGetViewModel(s));

            return Request.CreateResponse(HttpStatusCode.OK,list);
        }

        [HttpPost]
        [Route("")]
        [SMSAuthorization]
        public HttpResponseMessage Post(SmsPostViewModel sms)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest,"Modelo Inválido");

            var dic = new Dictionary<string, string>();

            sms.Variables.ForEach(s => dic.Add(s.Key,s.Value));

             _serviceSms.Create(sms.IdUser,sms.From,sms.To,sms.Template,dic);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("Update")]
        public HttpResponseMessage Update(SmsUpdateViewModel model)
        {
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Modelo Inválido");

            var sms = _serviceSms.GetById(model.Id);

            sms.Id = model.Id;
            sms.SmsId = model.Sms;
            sms.Status = (SmsStatus)model.Status;
            sms.SentAt = model.DataEnvio;
            sms.ErrorCode = model.ErrorCode;
            sms.ErrorMessage = model.ErrorMessage;
            sms.SourceId = model.SourceId;

            _serviceSms.Update(sms);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("View")]
        [HttpGet]
        public HttpResponseMessage View([FromUri] string token)
        {
            var briefing = FindBriefing(token);

            string html;

            if (briefing == null)
                html = NotfoundTemplate();
            else if (! briefing.Active)
                html = ArquivedTemplate(briefing);
            else if (_servicePropose.GetAll(x => x.BriefingId == briefing.Id && x.Contracted).Any())
                html = ContractedTemplate(briefing,token);
            else 
                html = SmsTemplate(briefing, token);

            var response = new HttpResponseMessage
            {
                Content = new StringContent(html)
            };

            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }

        private string ContractedTemplate(Briefing briefing, string token)
        {
            var freelancerId = briefing.TokenFreelancers.Where(bf => bf.Token.ToString().ToUpper() == token.ToUpper()).First().FreelancerId;

            var propose = _servicePropose.GetByExpression(x => x.BriefingId == briefing.Id && x.Contracted);

            if (propose.FreelancerId == freelancerId)
                return SelectTemplate(briefing, "~/Templates/sms.selecionado.html");
            
            return SelectTemplate(briefing, "~/Templates/sms.nao-selecionado.html");
        }

        private string NotfoundTemplate()
        {
            var stringpath = "~/Templates/sms.404.html";
            var template = GetTemplate(stringpath);
            string html = template(null);
            return html;
        }

        private string ArquivedTemplate(Briefing briefing)
        {
            var stringpath = "~/Templates/sms.archived.html";
            var template = GetTemplate(stringpath);
            string html = template(new SmsBriefingResponseViewModel(briefing));
            return html;
        }

        private string SelectTemplate(Briefing briefing,string stringpath)
        {

            var template = GetTemplate(stringpath);
            string html = template(new SmsBriefingResponseViewModel(briefing));
            return html;
        }

        private string SmsTemplate(Briefing briefing, string token)
        {
            var tokenfreelancer = briefing.TokenFreelancers.Where(bf => bf.Token.ToString().ToUpper() == token.ToUpper()).First();

            if (tokenfreelancer.Visualizations < 1)
            {
                tokenfreelancer.SeeAt = DateTime.Now;
                tokenfreelancer.Visualizations = 0;
            }

            tokenfreelancer.Visualizations += 1;

            _serviceTokenFreelancer.Update(tokenfreelancer);

            var freelancerId = tokenfreelancer.FreelancerId;

            string baseUrl = Request.RequestUri.Scheme + "://" + Request.RequestUri.Authority;

            var stringpath = "~/Templates/sms.template.html";
            var template = GetTemplate(stringpath);
            string html = template(new SmsBriefingViewModel(briefing, baseUrl, freelancerId));
            return html;
        }

        private Func<object,string> GetTemplate(string stringpath)
        {
            var path = HttpContext.Current.Server.MapPath(stringpath);
            var source = File.ReadAllText(path);
            return Handlebars.Compile(source);
        }

        [Route("BriefingStatus")]
        [HttpPost]
        public HttpResponseMessage BriefingStatus(SmsBriefingStatusViewModel smsBriefingStatus)
        {
            if (smsBriefingStatus.Status == SmsBriefingStatus.Ocupado)
            {
                MarkFreelancerOcupped(smsBriefingStatus.FreelancerId);
            }

            var token = _serviceTokenFreelancer.GetByExpression(t => t.BriefingId == smsBriefingStatus.BriefingId && t.FreelancerId == smsBriefingStatus.FreelancerId);

            token.Status = smsBriefingStatus.Status;

            _serviceTokenFreelancer.Update(token);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private void MarkFreelancerOcupped(int freelancerId)
        {
            var freelancer = _serviceFreelancer.GetById(freelancerId);
            freelancer.Active = false;

            _serviceFreelancer.Update(freelancer);

            var searchfreela = new FreelancerPoco(freelancer);

            _searchservice.Update(searchfreela);

            var user = _serviceIntercom.GetUser(freelancer.Users.First().Id);

            if (user.custom_attributes.ContainsKey("ocupado"))
                user.custom_attributes["ocupado"] = DateTime.Now.ToLocalTime();
            else
                user.custom_attributes.Add("ocupado", DateTime.Now.ToLocalTime());

            _serviceIntercom.UpdateUser(user);
        }

        [Route("BriefingPropose")]
        [HttpPost]
        public HttpResponseMessage BriefingPropose(SmsBriefingProposeViewModel smsBriefingPropose)
        {
            var response = BriefingStatus(smsBriefingPropose);

            if (!response.IsSuccessStatusCode)
                return response;

            var msg = new MessageViewModel
            {
                BriefingId = smsBriefingPropose.BriefingId,
                FreelancerId = smsBriefingPropose.FreelancerId.ToString(),
                DeadlineDays = smsBriefingPropose.DeadlineDays,
                Price = smsBriefingPropose.Price,
                Text = smsBriefingPropose.Text
            };

            var controller = new BriefingController(_serviceBriefing, _serviceFreelancer, _serviceUser, null, _serviceMessage, _servicePropose, null, null, null, null);

            controller.Configuration = this.Configuration;

            controller.AddMessage(msg);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private Briefing FindBriefing(string token)
        {
           return _serviceBriefing.GetBriefing(token);
        }
    }
}
