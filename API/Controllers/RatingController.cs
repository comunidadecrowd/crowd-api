﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Services;
using DocumentFormat.OpenXml;

namespace API.Controllers
{
    [RoutePrefix("")]
    public class RatingController : BaseAPIController
    {

        private readonly IServiceFreelancer _serviceFreelancer;
        private readonly IServiceBase<Customer> _serviceCustomer;
        private readonly IServiceBase<Tasks> _serviceTask;
        private readonly IServiceBase<Freelancer.RatingFreelancer> _serviceFreelancerRating;
        private readonly IServiceBase<Customer.RatingCustomer> _serviceCustomerRating;

        public RatingController()
        {
                
        }

        public RatingController(
            IServiceFreelancer serviceFreelancer,
            IServiceBase<Tasks> serviceTask,
            IServiceBase<Freelancer.RatingFreelancer> serviceFreelancerRating,
            IServiceBase<Customer.RatingCustomer> serviceCustomerRating,
            IServiceBase<Customer> serviceCustomer)
        {
            _serviceFreelancer = serviceFreelancer;
            _serviceCustomer = serviceCustomer;
            _serviceTask = serviceTask;
            _serviceFreelancerRating = serviceFreelancerRating;
            _serviceCustomerRating = serviceCustomerRating;
        }

        [Authorize]
        [Route("Professional/Rating/Task/{id}")]
        public HttpResponseMessage GetFreelancer(int taskid)
        {
            var task = _serviceTask.GetById(taskid);

            if (task == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "task does not exist");

            var rating = new FreelancerRatingTaskViewModel
            {
                Code = task.Freelancer.Code.ToString(),
                CustomerId = task.User.Customer.Id,
                CustomerTrade = task.User.Customer.Trade,
                TaskDueDate =  task.DueAt.Value,
                TaskPropose = task.Price,
                TaskTitle = task.Title,
                Photo = task.User.Photo, 
                Portfolio = task.Freelancer.PortfolioURL
            };

            var ratings = _serviceFreelancerRating.GetAll(true).Where(x => x.FreelancerId == task.IdFreelancer && x.Active).ToList();
           
            rating.RatingCount = ratings.Count;
            rating.QualityCount = ratings.Count(r => r.Quality > 0);
            rating.ResponsabilityCount = ratings.Count(r => r.Responsability > 0);
            rating.AgilityCount = ratings.Count(r => r.Agility > 0);

            rating.Quality = rating.QualityCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Quality).Sum() / rating.QualityCount);
            rating.Responsability = rating.ResponsabilityCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Responsability).Sum() / rating.ResponsabilityCount);
            rating.Agility = rating.AgilityCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Agility).Sum() / rating.AgilityCount);
            rating.Rating = rating.RatingCount == 0 ? 0 : ((rating.Quality + rating.Responsability + rating.Agility) / 3);
            

            return Request.CreateResponse(HttpStatusCode.OK, rating);
        }

        [Authorize]
        [Route("Customer/Rating/Task/{id}")]
        public HttpResponseMessage GetCustomer(int taskid)
        {
            var task = _serviceTask.GetById(taskid);

            if (task == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "task does not exist");

            var rating = new CustomerRatingTaskViewModel
            {
                Code = task.Freelancer.Code.ToString(),
                CustomerId = task.User.Customer.Id,
                CustomerTrade = task.User.Customer.Trade,
                TaskDueDate = task.DueAt.Value,
                TaskPropose = task.Price,
                TaskTitle = task.Title,
                Photo = task.User.Photo
            };

            var ratings = _serviceCustomerRating.GetAll(true).Where(x => x.CustomerId == rating.CustomerId && x.Active).ToList();

            rating.RatingCount = ratings.Count;
            rating.ApprovalCount = ratings.Count(r => r.Approval > 0);
            rating.CommunicateCount = ratings.Count(r => r.Communicate > 0);
            rating.ProfessionalismCount = ratings.Count(r => r.Professionalism > 0);

            rating.Approval = rating.ApprovalCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Approval).Sum() / rating.ApprovalCount);
            rating.Communicate = rating.CommunicateCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Communicate).Sum() / rating.CommunicateCount);
            rating.Professionalism = rating.ProfessionalismCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Professionalism).Sum() / rating.ProfessionalismCount);
            rating.Rating = rating.RatingCount == 0 ? 0 : ((rating.Approval + rating.Communicate + rating.Professionalism) / 3);

            return Request.CreateResponse(HttpStatusCode.OK, rating);
        }

        [Authorize]
        [HttpPost]
        [Route("Professional/{code}/Rating")]
        public HttpResponseMessage PostFreelancer(string code,[FromBody] FreelancerRatingSaveViewModel freelancerRating)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var freelancer = _serviceFreelancer.GetByCode(code);

                var user = RequestTokenUser();

                var task = _serviceTask.GetById(freelancerRating.taskId);

                if (freelancer == null)                                                                                                                                                                         
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Freelancer does not exist");

                if (user == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "User does not exist");

                if (task == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Task does not exist");

                freelancer.Ratings.Add(new Freelancer.RatingFreelancer()
                {
                    FreelancerId = freelancer.Id,
                    TaskId = freelancerRating.taskId,
                    UserId = user.Id,
                    Agility = freelancerRating.Agility,
                    Quality = freelancerRating.Quality,
                    Responsability = freelancerRating.Responsability,
                    Comment = freelancerRating.Comment
                });

                _serviceFreelancer.Update(freelancer);

                task.FreelancerReview = true;

                _serviceTask.Update(task);

                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }

        [Authorize]
        [HttpPost]
        [Route("Customer/{id}/Rating")]
        public HttpResponseMessage PostCustomer(int id, [FromBody] CustomerRatingSaveViewModel customerRating)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var customer = _serviceCustomer.GetById(id);

                var user = RequestTokenUser();

                var task = _serviceTask.GetById(customerRating.taskId);

                if (customer == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Customer does not exist");

                if (user == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "User does not exist");

                if (task == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Task does not exist");

                customer.Ratings.Add(new Customer.RatingCustomer()
                {
                    CustomerId = customer.Id,
                    TaskId = customerRating.taskId,
                    UserId = user.Id,
                    
                    Approval = customerRating.Approval,
                    Communicate = customerRating.Communicate,
                    Professionalism = customerRating.Professionalism,

                    Comment = customerRating.Comment
                    
                });

                _serviceCustomer.Update(customer);

                task.CustomerReview = true;

                _serviceTask.Update(task);

                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


    }
}
