using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Version = Lucene.Net.Util.Version;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Services;
using Lucene.Net.Analysis;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Interfaces.Services;

namespace API.Controllers
{
    public class LuceneSearchController
    {
        public LuceneSearchController()
        {

        }

        public static string _luceneDir = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "lucene_index");
        public static FSDirectory _directoryTemp;
        public static FSDirectory _directory
        {
            get
            {
                if (_directoryTemp == null) _directoryTemp = FSDirectory.Open(new DirectoryInfo(_luceneDir));
                if (IndexWriter.IsLocked(_directoryTemp)) IndexWriter.Unlock(_directoryTemp);
                var lockFilePath = Path.Combine(_luceneDir, "write.lock");
                if (File.Exists(lockFilePath)) File.Delete(lockFilePath);
                return _directoryTemp;
            }
        }

        private void _addToLuceneIndex(Freelancer freelancer, IndexWriter writer)
        {
            var searchQuery = new TermQuery(new Term("Id", freelancer.Id.ToString()));
            writer.DeleteDocuments(searchQuery);

            var doc = new Document();

            doc.Add(new Field("Id", freelancer.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("Code", freelancer.Code.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("UpdatedAt", freelancer.UpdatedAt.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("CreatedAt", freelancer.CreatedAt.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("Name", freelancer.Name, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("Description", string.IsNullOrEmpty(freelancer.Description) ? "null" : freelancer.Description, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("Email", freelancer.Email, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("City", freelancer.City == null ? "" : freelancer.City.Name, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("CityId", freelancer.CityId == null ? "" : freelancer.City.Id.ToString(), Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("State", freelancer.State == null ? "" : freelancer.State.UF, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("StateId", freelancer.StateId == null ? "" : freelancer.State.Id.ToString(), Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("Country", freelancer.Country, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("Title", freelancer.Title, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("Availability", freelancer.Availability == AvailabilityType.Integral ? "1" : "2", Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("Category", freelancer.Categorys.Name, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("CategoryId", freelancer.CategoryId.ToString(), Field.Store.YES, Field.Index.ANALYZED));            
            doc.Add(new Field("Price", freelancer.Price.ToString(), Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("Active", freelancer.Active ? "1" : "0", Field.Store.YES, Field.Index.ANALYZED));

            /*var ratings = _serviceRating.GetAll(true).Where(x => x.FreelancerId == freelancer.Id && x.Active);
            var ratingsCount = ratings.Count();

            

            var QualityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Quality).Sum() / ratingsCount);
            var ResponsabilityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Responsability).Sum() / ratingsCount);
            var AgilityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Agility).Sum() / ratingsCount);

            freelancer.Rating = ratingsCount == 0 ? 0 : ((QualityRating + ResponsabilityRating + AgilityRating) / 3);*/

            double rate = 0;
            if (freelancer.Ratings.Count() != 0)
            {
                var qualityAvg = freelancer.Ratings.Average(x => x.Quality);
                var responsabilityAvg = freelancer.Ratings.Average(x => x.Responsability);
                var agilityAvg = freelancer.Ratings.Average(x => x.Agility);

                rate = (qualityAvg + responsabilityAvg + agilityAvg) / 3;
            }

            doc.Add(new Field("Rating", rate.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));

            doc.Add(new Field("Status", Enum.GetName(typeof (StatusUserType), freelancer.Status), Field.Store.YES, Field.Index.ANALYZED));

            freelancer.Ratings.ToList().ForEach(rating =>
            {
                doc.Add(new Field("Ratings", rating.Id.ToString(), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
            });

            freelancer.Segments.ToList().ForEach(segment =>
            {
                doc.Add(new Field("Segments", segment.Id.ToString(), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
            });
            
            freelancer.Skills.ToList().ForEach(skill =>
            {
                doc.Add(new Field("Skills", skill.Name, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
            });
            
            freelancer.Experiences.ToList().ForEach(experience =>
            {
                doc.Add(new Field("ExperienceId", experience.Id.ToString(), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
                doc.Add(new Field("ExperienceCompany", experience.Company, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
                doc.Add(new Field("ExperienceDescription", experience.Description, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
                doc.Add(new Field("ExperienceRole", experience.Role, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
            });


            freelancer.Awards.ToList().ForEach(award =>
            {
                doc.Add(new Field("AwardId", award.Id.ToString(), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
                doc.Add(new Field("AwardTitle", award.Title, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
            });

            
            freelancer.Portfolios.ToList().ForEach(portfolio =>
            {
                doc.Add(new Field("PortfolioId", portfolio.Id.ToString(), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
                doc.Add(new Field("PortfolioTitle", portfolio.Title, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
                doc.Add(new Field("PortfolioDescription", portfolio.Description, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
                doc.Add(new Field("PortfolioClientName", portfolio.ClientName, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
            });

            writer.AddDocument(doc);
        }

        public void AddUpdateLuceneIndex(IEnumerable<Freelancer> freelancers)
        {
            var analyzer = new StandardAccentAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(_directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                var lucene = new LuceneSearchController();
                foreach (var freelancer in freelancers) lucene._addToLuceneIndex(freelancer, writer);

                analyzer.Close();
                writer.Dispose();
            }
        }

        public void AddUpdateLuceneIndex(Freelancer freelancer)
        {
            var lucene = new LuceneSearchController();
            lucene.AddUpdateLuceneIndex(new List<Freelancer> { freelancer });
        }

        public static void ClearLuceneIndexRecord(int record_id)
        {
            var analyzer = new StandardAccentAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(_directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                var searchQuery = new TermQuery(new Term("Id", record_id.ToString()));
                writer.DeleteDocuments(searchQuery);

                analyzer.Close();
                writer.Dispose();
            }
        }

        public bool ClearLuceneIndex()
        {
            try
            {
                var analyzer = new StandardAccentAnalyzer(Version.LUCENE_30);
                using (var writer = new IndexWriter(_directory, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    writer.DeleteAll();

                    analyzer.Close();
                    writer.Dispose();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }


        public static void Optimize()
        {
            var analyzer = new StandardAccentAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(_directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                analyzer.Close();
                writer.Optimize();
                writer.Dispose();
            }
        }


        private static Freelancer _mapLuceneDocumentToData(Document doc)
        {
            var segments = new List<Freelancer.Segment>();

            doc.GetFields("Segments").ToList().ForEach(item =>
            {
                segments.Add(new Freelancer.Segment()
                {
                    Id = int.Parse(item.StringValue)
                });
            });
            
            var skills = new List<Freelancer.Skill>();

            doc.GetFields("Skills").ToList().ForEach(item =>
            {
                skills.Add(new Freelancer.Skill()
                {
                    Name = item.StringValue,
                });
            });

            var state = new State()
            {
                Id = string.IsNullOrEmpty(doc.Get("StateId")) ? 0 : int.Parse(doc.Get("StateId")),
                UF = doc.Get("State")
            };

            var city = new City()
            {
                Id = string.IsNullOrEmpty(doc.Get("CityId")) ? 0 : int.Parse(doc.Get("CityId")),
                Name = doc.Get("City")
            };
            var category = new Freelancer.Category()
            {
                Name = doc.Get("Category")
            };

            var ratings = new List<Freelancer.RatingFreelancer>();
            doc.GetFields("Ratings").ToList().ForEach(item =>
            {
                ratings.Add(new Freelancer.RatingFreelancer()
                {
                    Id = int.Parse(item.StringValue)
                });
            });

            var freelancer = new Freelancer
            {
                Id = Convert.ToInt32(doc.Get("Id")),
                Code = Guid.Parse(doc.Get("Code")),
                Name = doc.Get("Name"),
                Description = doc.Get("Description"),
                Email = doc.Get("Email"),
                City = city,
                State = state,
                Country = doc.Get("Country"),
                Segments = segments,
                Skills = skills,
                Availability = doc.Get("Availability") == "1" ? AvailabilityType.Integral : AvailabilityType.Parcial,
                Title = doc.Get("Title"),
                Categorys = category,
                CategoryId = int.Parse(doc.Get("CategoryId")),
                Price = decimal.Parse(doc.Get("Price")),
                PortfolioURL = doc.Get("Portfolio"),
                Status = doc.Get("Status").Equals("Active") ? StatusUserType.Active : StatusUserType.Disabled,
                Rating = double.Parse(doc.Get("Rating"))
            };

            var updatedAt = doc.Get("UpdatedAt");
            if (!string.IsNullOrEmpty(updatedAt))
                freelancer.UpdatedAt = DateTime.Parse(updatedAt);

            var createdAt = doc.Get("CreatedAt");
            if (!string.IsNullOrEmpty(createdAt))
                freelancer.CreatedAt = DateTime.Parse(createdAt);

            return freelancer;
        }


        private static IEnumerable<Freelancer> _mapLuceneToDataList(IEnumerable<Document> hits)
        {
            return hits.Select(_mapLuceneDocumentToData).ToList();
        }


        private static IEnumerable<Freelancer> _mapLuceneToDataList(IEnumerable<ScoreDoc> hits,
            IndexSearcher searcher)
        {
            return hits.Select(hit => _mapLuceneDocumentToData(searcher.Doc(hit.Doc))).ToList();
        }


        private static Query parseQuery(string searchQuery, QueryParser parser)
        {
            Query query;
            try
            {
                query = parser.Parse(searchQuery.Trim());
            }
            catch (ParseException)
            {
                query = parser.Parse(QueryParser.Escape(searchQuery.Trim()));
            }
            return query;
        }


        private static IEnumerable<Freelancer> _search(string searchQuery, string[] searchField = null)
        {
            if (string.IsNullOrEmpty(searchQuery.Replace("*", "").Replace("?", ""))) return new List<Freelancer>();

            using (var searcher = new IndexSearcher(_directory, false))
            {
                var hits_limit = 1000;
                var analyzer = new StandardAccentAnalyzer(Version.LUCENE_30);

                if (null != searchField)
                {
                    var parser = new MultiFieldQueryParser
                        (Version.LUCENE_30, searchField, analyzer);
                    var query = parseQuery(searchQuery, parser);
                    var hits = searcher.Search(query, null, hits_limit, Sort.RELEVANCE).ScoreDocs;
                    var results = _mapLuceneToDataList(hits, searcher);
                    analyzer.Close();
                    searcher.Dispose();
                    return results;
                }
                else {
                    var parser = new MultiFieldQueryParser
                        (Version.LUCENE_30, new[] { "Id", "Code", "Name", "Description", "Email", "City", "CityId", "State", "StateId", "Country", "Segments", "Skills", "Title", "Availability", "Category", "CategoryId", "Price", "Portoflio", "Available", "Rating", "ExperienceCompany", "ExperienceDescription", "ExperienceRole", "AwardId", "AwardTitle", "PortfolioId", "PortfolioTitle", "PortfolioDescription", "PortfolioClientName" }, analyzer);

                    var query = parseQuery(searchQuery, parser);
                    var hits = searcher.Search
                    (query, null, hits_limit, Sort.RELEVANCE).ScoreDocs;
                    var results = _mapLuceneToDataList(hits, searcher);
                    analyzer.Close();
                    searcher.Dispose();
                    return results;
                }
            }
        }


        public static IEnumerable<Freelancer> Search(string input, string[] fieldName = null)
        {
            if (string.IsNullOrEmpty(input)) return new List<Freelancer>();

            var terms = input.Trim().Replace("-", " ").Split('|')
                .Where(x => !string.IsNullOrEmpty(x)).Select(x => x.Trim());
            input = string.Join(" ", terms);
            input = RemoveDiacritics(input);
            return _search(input, fieldName);
        }


        public static IEnumerable<Freelancer> SearchDefault(string input, string[] fieldName = null)
        {
            return string.IsNullOrEmpty(input) ? new List<Freelancer>() : _search(input, fieldName);
        }

        public static IEnumerable<Freelancer> GetAllIndexRecords()
        {
            if (!System.IO.Directory.EnumerateFiles(_luceneDir).Any()) return new List<Freelancer>();

            var searcher = new IndexSearcher(_directory, false);
            var reader = IndexReader.Open(_directory, false);
            var docs = new List<Document>();
            var term = reader.TermDocs();
            while (term.Next()) docs.Add(searcher.Doc(term.Doc));
            reader.Dispose();
            searcher.Dispose();
            return _mapLuceneToDataList(docs);
        }

        private static string RemoveDiacritics(string input)
        {
            string stFormD = input.Normalize(NormalizationForm.FormD);
            int len = stFormD.Length;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++)
            {
                System.Globalization.UnicodeCategory uc = System.Globalization.CharUnicodeInfo.GetUnicodeCategory(stFormD[i]);
                if (uc != System.Globalization.UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(stFormD[i]);
                }
            }
            return (sb.ToString().Normalize(NormalizationForm.FormC));
        }
    }

    internal class StandardAccentAnalyzer : StandardAnalyzer
    {
        public StandardAccentAnalyzer(Version matchVersion)
            :base(matchVersion)
        {

        }

        public override TokenStream TokenStream(string fieldName, TextReader reader)
        {
            TokenStream result = base.TokenStream(fieldName, reader);

            result = new ASCIIFoldingFilter(result);

            return result;
        }
    }
}

