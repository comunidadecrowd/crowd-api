﻿using API.Models;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    
    [RoutePrefix("Common")]
    public class CommonController : BaseAPIController
    {
        private IServiceBase<Freelancer.Skill> _serviceSkill;
        private IServiceBase<Freelancer.Segment> _serviceSegment;
        private IServiceBase<Freelancer.Category> _serviceCategory;
        private IServiceBase<State> _serviceState;
        private IServiceBase<City> _serviceCity;

        public CommonController()
        {
        }

        public CommonController(
            IServiceBase<Freelancer.Skill> serviceSkill, 
            IServiceBase<Freelancer.Segment> serviceSegment, 
            IServiceBase<Freelancer.Category> serviceCategory,
            IServiceBase<State> serviceState,
            IServiceBase<City> serviceCity)
        {
            _serviceSkill = serviceSkill;
            _serviceSegment = serviceSegment;
            _serviceCategory = serviceCategory;
            _serviceState = serviceState;
            _serviceCity = serviceCity;
        }


        [Route("Skills")]
        public List<SkillsViewModel> GetSkills()
        {            
            var response = new List<SkillsViewModel>();

            var skills = _serviceSkill.GetAll(true).OrderBy(x => x.Name).ToList();

            skills.ForEach(item => response.Add(new SkillsViewModel()
            {
                Id = item.Id,
                Name = item.Name
            }));            
            
            return response;
        }



        [Route("Segments")]
        public List<SegmentsViewModel> GetSegments()
        {
            var response = new List<SegmentsViewModel>();

            var segments = _serviceSegment.GetAll(true).OrderBy(x => x.Name).ToList();

            segments.ForEach(item => response.Add(new SegmentsViewModel()
            {
                Id = item.Id,
                Name = item.Name
            }));

            return response;
        }


        [Route("Categories")]
        public List<CategoriesViewModel> GetCategories()
        {
            var response = new List<CategoriesViewModel>();

            var segments = _serviceCategory.GetAll(true).Where(c=> c.Active).OrderBy(x => x.Name).ToList();

            segments.ForEach(item => response.Add(new CategoriesViewModel()
            {
                Id = item.Id,
                Name = item.Name
            }));

            return response;
        }

        [Route("States")]
        public List<StateViewModel> GetStates()
        {
            var response = new List<StateViewModel>();

            var states = _serviceState.GetAll(true).OrderBy(x => x.Name).ToList();

            states.ForEach(item => response.Add(new StateViewModel()
            {
                Id = item.Id,
                Name = item.Name,
                UF = item.UF
            }));

            return response;
        }


        [Route("Cities")]
        public List<CityViewModel> GetCities(string city)
        {
            List<CityViewModel> response = _serviceCity
                                .GetAll(true)
                                .Where(item => item.Name.ContainsInsensitive(city))
                                .Take(10)
                                .Select(LoadCityViewModel).OrderBy(s => s.Name).ToList();

            return response.ToList();
        }

        private static CityViewModel LoadCityViewModel(City item)
        {
            return new CityViewModel()
            {
                Id = item.Id,
                Name = String.Format("{0} - {1}", item.State.Name, item.Name),
                StateId = item.StateId
            };
        }
    }
}
