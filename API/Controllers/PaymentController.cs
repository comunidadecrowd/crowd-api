﻿using API.Models;
using API.Models.Freelancer;
using API.Models.Payment;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Domain.Util.Network;
using Backend.Crowd.Domain.Util.Security;
using CorePayment;
using FluentValidation.Results;
using Iugu;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("Payment")]
    public class PaymentController : BaseAPIController
    {
        IPayment _paymentServer;
        IServiceBase<Customer> _serviceCustomer;
        
        public PaymentController(IPayment paymentServer, IServiceBase<Customer> serviceCustomer,IServiceUser serviceUser, IServiceBase<User.ActionLog> serviceActionLog,Serilog.ILogger log) : base(serviceUser,serviceActionLog,log)
        {
            _paymentServer = paymentServer;
            _serviceCustomer = serviceCustomer;
            _serviceUser = serviceUser;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Authorize]
        public HttpResponseMessage Post(PaymentViewModel model)
        {
            var user = this.RequestTokenUser();

            var customer = _serviceCustomer.GetById(model.IdCustomer);

             if (customer == null)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Cliente não cadastrado");

            var customerPayment = new CustomerPayment
            {
                Name = customer.Name,
                Email = customer.EmailResponsible,
                CNPJ = customer.CNPJ,
                Token = customer.PaymentToken,
                Address = customer.Address,
                CEP = customer.CEP,
                Complement = customer.Complement,
                Number = customer.Number,
                Subscription = customer.PaymentSubscription
            };

            var payment = new SendPayment
            {
                Customer = customerPayment,
                Plan = Enum.GetName(typeof(PlanType), model.Plan),
                PaymentType = (CorePayment.PaymentType)model.PaymentType,
                CreditCardToken = model.creditCardToken
            };

            var result = _paymentServer.Send(payment);

            if (result.Status != PaymentStatus.Ok)
                return MessageErrorViewModel.ReturnError(result.StatusCode, result.ErrorCode, result.ErrorMessage);

            customer.PlanType = (PlanType)model.Plan;
            customer.PaymentToken = result.CustomerId;
            customer.PaymentSubscription = result.SubscriptionId;
            customer.Trial = false;

            _serviceCustomer.Update(customer);

            user = _serviceUser.GetById(user.Id);

            return (Request.CreateResponse(HttpStatusCode.OK, CreateLogin(user.Email, null, ConfigurationManager.AppSettings["customerPath"])));
        }

        [HttpPost]
        [Route("Trigger")]
        public HttpResponseMessage PostTrigger()
        {
            var task = Request.Content.ReadAsFormDataAsync();

            task.Wait();

            var formBody = task.Result;

            ITrigger trigger = TriggerPaymentFactory.Instance(formBody);

            if (trigger != null)
            {
                var customer = _serviceCustomer.GetByExpression(c => c.PaymentSubscription == trigger.Id);

                if (customer == null)
                    _log.Information("Subscription nao encontrada : {0}", trigger.Id);
                else
                {
                    trigger.Execute(customer);

                    _serviceCustomer.Update(customer);
                }
            }
            else _log.Information("Event nao monitorado : {0}", formBody["event"]);
            
            this._log.Information(formBody.ToString());

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
