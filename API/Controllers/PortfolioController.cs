using API.Models;
using API.Models.enums;
using API.Models.Portfolio;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Domain.Util;
using Backend.Crowd.Domain.Util.Network;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Algolia.API;
using System.Net.Http.Headers;
using HandlebarsDotNet;

namespace API.Controllers
{

    [RoutePrefix("Portfolio-online")]
    public class PortfolioController : BaseAPIController
    {
        private const char _separator = '-';
        private IServiceFreelancer _serviceFreelancer;
        private IServiceBase<State> _serviceState;
        private IServiceBase<City> _serviceCity;
        private IServiceBase<Freelancer.RatingFreelancer> _serviceRating;
        private IServiceBase<Tasks> _serviceTasks;
        private readonly ISearch _searchservice;

        private IServiceBase<Freelancer.Portfolio> _servicePortfolio;


        public PortfolioController()
        {
        }

        public PortfolioController(IServiceFreelancer serviceFreelancer, IServiceBase<State> serviceState,
            IServiceBase<City> serviceCity, IServiceBase<Freelancer.RatingFreelancer> serviceRating, IServiceUser serviceUser,
            IServiceBase<Freelancer.Portfolio> servicePortfolio, IServiceBase<Tasks> serviceTasks,ISearch searchservice)
        {
            _serviceFreelancer = serviceFreelancer;
            _serviceCity = serviceCity;
            _serviceState = serviceState;
            _serviceRating = serviceRating;
            _serviceUser = serviceUser;
            _servicePortfolio = servicePortfolio;
            _serviceTasks = serviceTasks;
            _searchservice = searchservice;
        }

        [Route("{Title}/{Name}/Full")]
        [HttpGet]
        public HttpResponseMessage Full([FromUri] string title, [FromUri] string name)
        {
            var freelancer = FindFreela(title, name);

            if (freelancer == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            var amphtml = Request.RequestUri.ToString().Replace("/Full", "");

            return Request.CreateResponse(HttpStatusCode.OK,new PortfolioOnLineViewModel(freelancer,amphtml,"",""));
        }

        [Route("{Title}/{Name}")]
        public HttpResponseMessage Get([FromUri] string title,[FromUri] string name)
        {
            var freelancer = FindFreela(title, name);

            if (freelancer == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            var path = HttpContext.Current.Server.MapPath("~/Templates/Freelancer.html");
            var source = File.ReadAllText(path);
            var template = Handlebars.Compile(source);

            string baseUrl = Request.RequestUri.Scheme + "://" + Request.RequestUri.Authority;

            var canonicalbuilder = new UriBuilder(Request.RequestUri);
                canonicalbuilder.Host = ConfigurationManager.AppSettings["WebBaseUrl"].ToString();
                canonicalbuilder.Scheme = ConfigurationManager.AppSettings["WebSchemeUrl"].ToString();

            var html = template(new PortfolioOnLineViewModel(freelancer,Request.RequestUri.ToString(),canonicalbuilder.Uri.ToString() + "Full", baseUrl));

            var response = new HttpResponseMessage();
            response.Content = new StringContent(html);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }

        private Freelancer FindFreela(string title, string name)
        {
            var portfolioonlline = String.Format("{0}/{1}", title, name);

            var freelancers = _serviceFreelancer.GetAll(f => f.PortfolioOnLine == portfolioonlline,null,false,0,0,true,"Users","City","State");

            foreach (var freelancer in freelancers)
            {
                if (freelancers.Count() == 1 || ValidFreela(freelancer, GetDigit(name)))
                    return freelancer;
                else
                    return null;
            }

            return null;
        }

        private string GetDigit(string name)
        {
            var index = name.LastIndexOf('_');

            if (index < 1)
                return String.Empty;

            return name.Substring(index + 1);
        }

        private string GetName(string name)
        {
            var index = name.LastIndexOf('_');

            if (index < 1)
                return name;

            return name.Remove(index);
        }

        private bool ValidFreela(Freelancer freelancer,string digit)
        {
            return Validador.GenerateCheckDigit(freelancer.Code.ToString()) == digit;
        }
    }
}
