﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("SystemMessage")]
    public class SystemMessageController : BaseAPIController
    {

        private IServiceBase<SystemMessage> _serviceSystemMessages;
        private IServiceBase<SystemMessage.ReadSystemMessage> _serviceReadSystemMessage;
        private IServiceUser _serviceUser;

        public SystemMessageController(IServiceUser serviceUser, IServiceBase<SystemMessage> serviceSystemMessages,
            IServiceBase<SystemMessage.ReadSystemMessage> serviceReadSystemMessage)
        {
            _serviceSystemMessages = serviceSystemMessages;
            _serviceUser = serviceUser;
            _serviceReadSystemMessage = serviceReadSystemMessage;
        }

        [HttpPost]
        [Route("Read")]
        public HttpResponseMessage Read(int user_id, int message_id)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var message = _serviceSystemMessages.GetById(message_id);
                var user = _serviceUser.GetById(user_id);

                var read = new SystemMessage.ReadSystemMessage()
                {
                    SystemMessageId = message.Id,
                    Read = true,
                    UserId = user.Id
                };
                _serviceReadSystemMessage.Add(read);

                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }
    }
}
