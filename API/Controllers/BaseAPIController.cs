﻿using Backend.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Util.Security;
using System.Configuration;
using API.Security;
using System.Security.Claims;
using System.IdentityModel.Protocols.WSTrust;
using System.Web;
using System.IO;
using Backend.Crowd.Domain.Entities.Enum;
using API.Models.Login;
using FluentValidation.Results;
using API.Models.Customer;

namespace API.Controllers
{

    //[EnableCors("https://www.becrowd.co", "*", "*")]
    public abstract class BaseAPIController : ApiController
    {
        public IServiceUser _serviceUser;
        public IServiceBase<User.ActionLog> _serviceActionLog;
        public Serilog.ILogger _log;

        protected BaseAPIController()
        {
                
        }

        protected BaseAPIController(IServiceUser serviceUser, IServiceBase<User.ActionLog> serviceActionLog,Serilog.ILogger log)
        {
            _serviceUser = serviceUser;
            _serviceActionLog = serviceActionLog;
            _log = log;
        }

        public static string CreateJwtToken(User user, out LoggedUser loggedUser,string defaultUrl = null)
        {
            var claimList = new List<Claim>()
                {
                    new Claim(ClaimTypes.Name, user.Email),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim("Prospect",user.IsProspect.ToString())
                };

            var tokenHandler = new JwtSecurityTokenHandler();
            var sSKey = new InMemorySymmetricSecurityKey(SecurityConstants.KeyForHmacSha256);

            var jwtToken = tokenHandler.CreateToken(makeSecurityTokenDescriptor(sSKey, claimList));


            var logo = user.Customer != null ? user.Customer.Logo : string.Empty;
            var customer = user.Customer != null ? user.Customer.Trade : string.Empty;
            var plan = user.Customer != null ? user.Customer.PlanType.ToString() : string.Empty;
            var IdFreelancer = Criptografy.Encrypt(user.IdFreelancer.ToString(), ConfigurationManager.AppSettings["cryptoPass"]);
            var Code = user.Freelancer != null ? user.Freelancer.Code.ToString() : string.Empty;

            bool? Prospect = false;
            bool? paid = false;
            if (user.IsCustomer)
            {
                Prospect = user.Customer != null ? user.Customer.Prospect : false;

                if (user.Customer != null )//&& user.Customer.Payments.Any())
                {
                    CustomerProspectValidator valid = new CustomerProspectValidator();
                    paid = valid.PaymentValid(user.Customer);
                } 
            }
            else
                Prospect = user.Freelancer != null ? user.Freelancer.Prospect : false;

            var defaultURL = (defaultUrl != null) ? defaultUrl : (user.Customer != null) ? ConfigurationManager.AppSettings["customerPath"] : ConfigurationManager.AppSettings["freelancerPath"];

            loggedUser = new LoggedUser
            {
                Email = user.Email,
                Id = user.Id,
                Role = user.Role,
                Photo = user.Photo,
                Name = user.Name,
                CustomerLogo = logo,
                Customer = customer,
                CustomerPlan = plan,
                IdCustomer = user.IdCustomer,
                IdFreelancer = IdFreelancer,
                Code = Code,
                RoleCompany = user.RoleCompany,
                AcceptTerms = user.AcceptTerms,
                defaultURL = defaultURL,
                Prospect = Prospect,
                Paid = paid
            };

            if (user.IsCustomer)
            {
                loggedUser.Trial = user.Customer.Trial;
                loggedUser.TrialDate = user.Customer.TrialDate;
            }

            return tokenHandler.WriteToken(jwtToken);
        }

        private static SecurityTokenDescriptor makeSecurityTokenDescriptor(InMemorySymmetricSecurityKey sSKey, List<Claim> claimList)
        {
            var now = DateTime.UtcNow;
            Claim[] claims = claimList.ToArray();
            return new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                TokenIssuerName = SecurityConstants.TokenIssuer,
                AppliesToAddress = SecurityConstants.TokenAudience,
                Lifetime = new Lifetime(now, now.AddMinutes(SecurityConstants.TokenLifetimeMinutes)),
                SigningCredentials = new SigningCredentials(sSKey,
                    "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256",
                    "http://www.w3.org/2001/04/xmlenc#sha256"),
            };
        }

        protected User RequestTokenUser()
        {
            var handler = new JwtSecurityTokenHandler();

            var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
            var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

            var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));
            return requestUser;
        }

        protected string GetImage(string base64, string extension, string folder)
        {

            string path = HttpContext.Current.Server.MapPath(String.Format("~{0}", folder));
            byte[] bytes = Convert.FromBase64String(base64);
            string filename = Guid.NewGuid().ToString();
            path += filename + "." + extension;
            File.WriteAllBytes(path, bytes);

            return folder + filename + "." + extension;
		}
		
        protected LoginResponseViewModel CreateLogin(string email, string password, string path)
        {
            User user;
            if (String.IsNullOrEmpty(password))
                user = _serviceUser.Authentication(email);
            else
                user = _serviceUser.Authentication(email, password);

            LoggedUser loggedUser;

            var token = CreateJwtToken(user, out loggedUser,path);

            _serviceActionLog.Add(new User.ActionLog()
            {
                Type = ActionLogType.LOGIN,
                UserId = user.Id
            });

            return new LoginResponseViewModel ( loggedUser, token );
        }

        protected IList<ValidationErrors> ReturnValidationError(IList<ValidationFailure> errors)
        {
            return errors.ToList().Select(e => new ValidationErrors(e.ErrorMessage.Split(',')[0], e.ErrorMessage.Split(',')[1])).ToList();
        }

        protected IList<ValidationErrors> ReturnValidationError(string field,string error)
        {
            return new List<ValidationErrors> { new ValidationErrors(field, error) };
        }

        public class ValidationErrors
        {
            private string propertyName;
            private string errorMessage;

            public ValidationErrors(string propertyName, string errorMessage)
            {
                this.propertyName = propertyName;
                this.errorMessage = errorMessage;
            }

            public string Id { get { return propertyName; } }
            public string Message { get { return errorMessage; } }
        }
    }
}
