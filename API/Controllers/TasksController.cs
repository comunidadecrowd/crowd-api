using API.Models;
using API.Models.enums;
using API.Models.Propose;
using API.Models.Tasks;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Domain.Util.Network;
using Backend.Crowd.Domain.Util.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    [ProspectAuthorization]
    [RoutePrefix("Task")]
    public class TasksController : BaseAPIController
    {

        private IServiceBase<Tasks> _serviceTasks;
        private IServiceBase<TasksAttach> _serviceTasksAttach;
        private IServiceFreelancer _serviceFreelancer;
        private IServiceBase<MessageTasks> _serviceMessageTasks;
        private IServiceBase<MessageTaskAttachs> _serviceMessageTasksAttach;
        private IServiceBase<Briefing.Propose> _servicePropose;
        private IServiceBase<Project> _serviceProject;
        private readonly IServiceBase<Freelancer.RatingFreelancer> _serviceFreelancerRating;
        private readonly IServiceBase<Customer.RatingCustomer> _serviceCustomerRating;

        public TasksController()
        {
        }

        public TasksController(IServiceBase<Tasks> serviceTasks, IServiceFreelancer serviceFreelancer, IServiceUser serviceUser, IServiceBase<TasksAttach> serviceTasksAttach, 
            IServiceBase<MessageTasks> serviceMessageTasks, IServiceBase<Briefing.Propose> servicePropose, IServiceBase<Project> serviceProject,
            IServiceBase<MessageTaskAttachs> serviceMessageTasksAttach, IServiceBase<Freelancer.RatingFreelancer> serviceFreelancerRating, IServiceBase<Customer.RatingCustomer> serviceCustomerRating)
        {
            _serviceTasks = serviceTasks;
            _serviceFreelancer = serviceFreelancer;
            _serviceUser = serviceUser;
            _serviceTasksAttach = serviceTasksAttach;
            _serviceMessageTasks = serviceMessageTasks;
            _servicePropose = servicePropose;
            _serviceProject = serviceProject;
            _serviceMessageTasksAttach = serviceMessageTasksAttach;
            _serviceCustomerRating = serviceCustomerRating;
            _serviceFreelancerRating = serviceFreelancerRating;
        }

        [HttpPost]
        [Route("Add")]
        public HttpResponseMessage Add(TasksAddViewModel model)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var user = _serviceUser.GetById(model.IdUser);
                var project = _serviceProject.GetById(model.IdProject);
                var freelancerId = int.Parse(model.IdFreelancer);
                var freelancer = _serviceFreelancer.GetById(freelancerId);

                var requestUser = this.RequestTokenUser();

                if (user == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "User does not exist");

                if (project == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Project does not exist");

                if (freelancer == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Freelancer does not exist");

                double rate = 0;

                if(freelancer.Ratings.Count() != 0)
                    rate = (freelancer.Ratings.Average(y => y.Agility) + freelancer.Ratings.Average(y => y.Quality) + freelancer.Ratings.Average(y => y.Responsability)) / 3;
                
                if (requestUser.Customer.PlanType == PlanType.FREE && rate >= 4 && !freelancer.Ratings.Any(r => r.User.IdCustomer == user.IdCustomer))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "You can not add an more 4 star freelancer. Please update your plan.");
                }

                var task = new Tasks()
                {
                    Title = model.Title,
                    Description = model.Description,
                    IdUser = model.IdUser,
                    IdProject = model.IdProject,
                    IdFreelancer = freelancerId,
                    Status = TaskStatus.TO_BE_DONE,
                    UpdatedAt = DateTime.Now
                };

                _serviceTasks.Add(task);

                if (model.Attachs != null)
                {
                    model.Attachs.ForEach(attach =>
                    {
                        _serviceTasksAttach.Add(new TasksAttach()
                        {
                            URL = attach.URL,
                            Name = attach.Name,
                            TasksId = task.Id,
                        });
                    });                    
                }

                
                Email sendEmail = new Email();
                sendEmail.Send(
                    task.Freelancer.Email,
                    task.Freelancer.Name,
                    "Nova Tarefa para Orçamento",
                    "new-task-propose",
                    new Dictionary<string, string> {
                        { "DE", user.Name },
                        { "EMPRESA", String.IsNullOrEmpty(user.Customer.Trade) ? user.Customer.Name :  user.Customer.Trade},
                        { "TITLE", task.Title }
                    }
                );

                model.Id = task.Id;
                model.Status = task.Status;                                

                response = Request.CreateResponse(model);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("Get")]
        public HttpResponseMessage Get(int IdUser, int IdTask)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var user = _serviceUser.GetById(IdUser);
                
                if (user == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "User does not exist");

                var task = _serviceTasks.GetById(IdTask);

                if (task == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Task does not exist");

                if ((user.Role == RoleType.EMPLOYEE &&
                    user.Customer.Id != task.User.IdCustomer) ||
                    (user.Role == RoleType.FREELANCER && 
                    task.Freelancer.Id != user.Freelancer.Id))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Não autorizado");
                }
                var freelancerId = Criptografy.Encrypt(task.Freelancer.Id.ToString(), ConfigurationManager.AppSettings["cryptoPass"]);
                var ret = new TasksViewModel()
                {
                    Id = task.Id,
                    Title = task.Title,
                    Description = task.Description,
                    Project = new Models.Projects.ProjectViewModel()
                    {
                        Id = task.Project.Id,
                        Color = task.Project.Color,
                        Name = task.Project.Name
                    },
                    Freelancer = new FreelancerBriefingViewModel()
                    {
                        Id = freelancerId,
                        Name = task.Freelancer.Name,
                        Photo = task.Freelancer.Users.Count() != 0 ? task.Freelancer.Users.First().Photo : null,
                        Title = task.Freelancer.Title,
                        Code = task.Freelancer.Code
                    },
                    Status = task.Status,
                    User = new UserBriefingViewModel()
                    {
                        Id = task.User.Id,
                        Customer = new CustomerViewModel()
                        {
                            Id = task.User.Customer.Id,
                            Name = task.User.Customer.Name,
                            Logo = task.User.Customer.Logo
                        },
                        Name = task.User.Name,
                        Photo = task.User.Photo
                    },
                    DeliveryAt = task.DeliveryAt.HasValue ? String.Format("{0:s}", task.DeliveryAt.Value) : null,
                    Price = task.Price,
                    UpdatedAt = task.UpdatedAt.HasValue ? String.Format("{0:s}", task.UpdatedAt.Value) : null,
                    Active = task.Active,
                    FreelancerReview = task.FreelancerReview ?? false,
                    CustomerReview = task.CustomerReview ?? false
                };

                ret.Attachs = new List<TasksAttachViewModel>();

                task.Attachs.ToList().ForEach(attach =>
                {
                    ret.Attachs.Add(new TasksAttachViewModel()
                    {
                        URL = attach.URL,
                        Name = attach.Name,
                        IdTask = task.Id,
                    });
                });

                ret.Messages = new List<TasksMessageViewModel>();

                Briefing.Propose propose = null;
                if (string.IsNullOrEmpty(task.IdBriefing.ToString()))
                {
                    propose = _servicePropose.GetAll(x => x.TaskId == task.Id).OrderByDescending(x => x.CreatedAt).FirstOrDefault();
                }
                else
                {
                    propose = _servicePropose.GetAll(x => x.BriefingId == task.IdBriefing && x.Contracted).OrderByDescending(x => x.CreatedAt).FirstOrDefault();
                }

                if (task.Briefing != null)
                {
                    task.Briefing.Messages.Where(x => x.FreelancerId == task.IdFreelancer).ToList().ForEach(message =>
                    {
                        var freelancerMessageId = Criptografy.Encrypt(message.Freelancers.Id.ToString(), ConfigurationManager.AppSettings["cryptoPass"]);
                        var message_item = new TasksMessageViewModel()
                        {
                            BriefingId = task.IdBriefing,
                            CreatedAt = message.CreatedAt,
                            FreelancerId = freelancerMessageId,
                            Id = message.Id,
                            Read = message.Read,
                            TaskId = task.Id,
                            Text = message.Text,
                            UserId = message.UserId   ,
                            Freelancer = new FreelancerBriefingViewModel()
                            {
                                Id = freelancerMessageId,
                                Name = message.Freelancers.Name,
                                Photo = message.Freelancers.Users.Count() != 0 ? message.Freelancers.Users.First().Photo : null,
                                Title = message.Freelancers.Title,
                                Code = message.Freelancers.Code
                            }
                        };


                        if(message.UserId != null)
                        {
                            message_item.User = new UserBriefingViewModel()
                            {
                                Id = message.User.Id,
                                Customer = new CustomerViewModel()
                                {
                                    Id = message.User.Customer.Id,
                                    Name = message.User.Customer.Name,
                                    Logo = message.User.Customer.Logo
                                },
                                Name = message.User.Name,
                                Photo = message.User.Photo
                            };
                        }

                        if (propose != null)
                        {
                            message_item.Price = propose.Price;
                            message_item.DeadlineDays = propose.DeadlineDays;
                            message_item.ProposeId = propose.Id;
                            message_item.DeliveryAt = propose.UpdatedAt.HasValue ? propose.UpdatedAt.Value.AddDays(propose.DeadlineDays) : propose.CreatedAt.AddDays(propose.DeadlineDays);
                        }

                        ret.Messages.Add(message_item);
                    });
                }

                task.Messages.ToList().ForEach(message =>
                {
                    var freelancerMessageId = Criptografy.Encrypt(message.Freelancer.Id.ToString(), ConfigurationManager.AppSettings["cryptoPass"]);

                    var message_item = new TasksMessageViewModel()
                    {
                        BriefingId = task.IdBriefing,
                        CreatedAt = message.CreatedAt,
                        FreelancerId = freelancerMessageId,
                        Id = message.Id,
                        Read = message.Read,
                        TaskId = message.TaskId,
                        Text = message.Text,
                        UserId = message.UserId,
                        Freelancer = new FreelancerBriefingViewModel()
                        {
                            Id = freelancerMessageId,
                            Name = message.Freelancer.Name,
                            Photo = message.Freelancer.Users.Count() != 0 ? message.Freelancer.Users.First().Photo : null,
                            Title = message.Freelancer.Title,
                            Code = message.Freelancer.Code
                        }
                    };


                    if (message.UserId != null)
                    {
                        message_item.User = new UserBriefingViewModel()
                        {
                            Id = message.User.Id,
                            Customer = new CustomerViewModel()
                            {
                                Id = message.User.Customer.Id,
                                Name = message.User.Customer.Name,
                                Logo = message.User.Customer.Logo
                            },
                            Name = message.User.Name,
                            Photo = message.User.Photo
                        };
                    }

                    if (propose != null)
                    {
                        message_item.Price = propose.Price;
                        message_item.DeadlineDays = propose.DeadlineDays;
                        message_item.ProposeId = propose.Id;
                        message_item.DeliveryAt = propose.UpdatedAt.HasValue ? propose.UpdatedAt.Value.AddDays(propose.DeadlineDays) : propose.CreatedAt.AddDays(propose.DeadlineDays);
                    }

                    message_item.Attachs = new List<TasksMessageAttachViewModel>();
                    message.Attachs.ToList().ForEach(m =>
                    {
                        message_item.Attachs.Add(new TasksMessageAttachViewModel()
                        {
                            IdMessage = m.MessageTasksId,
                            Name = m.Name,
                            URL = m.URL
                        });
                    });


                    ret.Messages.Add(message_item);
                });

                if (propose != null)
                {
                    ret.SelectedPropose = new ProposeViewModel()
                    {
                        Contracted = propose.Contracted,
                        DeadlineDays = propose.DeadlineDays,
                        DeliveryAt = propose.UpdatedAt.HasValue ?
                                    propose.UpdatedAt.Value.AddDays(propose.DeadlineDays) : propose.CreatedAt.AddDays(propose.DeadlineDays),
                        Id = propose.Id,
                        Price = propose.Price
                    };
                }

                if (user.Role == RoleType.FREELANCER)
                    ret.UnreadMessages = task.Messages.Where(x => x.FreelancerId == user.IdFreelancer && !x.Read && x.User != null).Count();
                else
                    ret.UnreadMessages = task.Messages.Where(x => !x.Read && x.UserId != user.Id && x.Task.IdUser == user.Id).Count();


                response = Request.CreateResponse(ret);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        


        [HttpPost]
        [Route("ChangeStatus")]
        public HttpResponseMessage ChangeStatus(int TaskId, int UserId, TaskStatus CurrentStatus, TaskStatus NewStatus)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var task = _serviceTasks.GetById(TaskId);

                if (task == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Task does not exist");

                var user = _serviceUser.GetById(UserId);

                if (user == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "User does not exist");

                if (user.Role != RoleType.MASTER && (
                    ((user.Role == RoleType.CLIENT || user.Role == RoleType.EMPLOYEE) && task.User.IdCustomer != user.IdCustomer) ||
                    ((user.Role == RoleType.FREELANCER) && task.IdFreelancer != user.IdFreelancer))) {

                    return Request.CreateResponse(HttpStatusCode.Forbidden, "Unauthorized");
                }

                if (task.Status != CurrentStatus)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "The current status is different of provided.");

                if (
                    (CurrentStatus == TaskStatus.TO_BE_DONE && NewStatus != TaskStatus.DOING) ||
                    (CurrentStatus == TaskStatus.DOING && NewStatus != TaskStatus.WAITING) ||
                    (CurrentStatus == TaskStatus.WAITING && (NewStatus != TaskStatus.DOING && NewStatus != TaskStatus.DONE))
                )
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "This status change breaks the determined flow.");

                if(NewStatus == TaskStatus.DONE && 
                    (user.Role != RoleType.CLIENT &&
                     user.Role != RoleType.EMPLOYEE &&
                     user.Role != RoleType.MASTER))
                    return Request.CreateResponse(HttpStatusCode.Forbidden, "You do not have permission to change to this status.");

                if (NewStatus == TaskStatus.DOING &&
                    CurrentStatus == TaskStatus.WAITING &&
                    (user.Role == RoleType.FREELANCER))
                    return Request.CreateResponse(HttpStatusCode.Forbidden, "You do not have permission to change to this status.");

                if (NewStatus == TaskStatus.DOING && CurrentStatus == TaskStatus.TO_BE_DONE) {
                    Briefing.Propose propose = null;
                    propose = _servicePropose.GetAll(x => x.TaskId == task.Id && x.FreelancerId == task.IdFreelancer).OrderByDescending(x => x.CreatedAt).FirstOrDefault();                    

                    if (propose == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, "The current task does not have a propose.");
                    }

                    propose.Contracted = true;
                    propose.UserResponsibleId = UserId;
                    _servicePropose.Update(propose);
                    task.DeliveryAt = DateTime.Now.AddDays(propose.DeadlineDays);
                    task.Price = propose.Price;

                    Email sendEmail = new Email();
                    sendEmail.Send(
                            propose.Freelancer.Email,
                            propose.Freelancer.Name,
                            "Orçamento Aprovado!",
                            "selected-propose",
                            new Dictionary<string, string> {
                            { "DE", task.User.Name },
                            { "EMPRESA", String.IsNullOrEmpty(task.User.Customer.Trade)
                                ? task.User.Customer.Name :  task.User.Customer.Trade },
                            { "TITLE", propose.Task.Title }
                            });


                }

                try {
                    task.Status = NewStatus;

                    if (NewStatus == TaskStatus.WAITING)
                        task.ApprovalAt = DateTime.Today;

                    if (NewStatus == TaskStatus.DONE)
                        task.DueAt = DateTime.Today;

                    _serviceTasks.Update(task);

                    if (NewStatus == TaskStatus.WAITING && CurrentStatus == TaskStatus.DOING 
                        && (user.Role == RoleType.FREELANCER))
                    {
                        var userCustomer = _serviceUser.GetById(task.IdUser);
                        Email sendEmail = new Email();
                        sendEmail.Send(
                                task.User.Email,
                                task.User.Name,
                                "Tarefa Encaminhada para a Aprovação",
                                "propose-send-approvation",
                                new Dictionary<string, string> {
                                    { "TASK", task.Title },
                                    { "PROJECT", task.Project.Name },
                                    { "FREELANCER", task.Freelancer.Name }
                                });
                    }else if(NewStatus == TaskStatus.DOING && CurrentStatus == TaskStatus.WAITING)
                    {
                        Email sendEmail = new Email();
                        sendEmail.Send(
                                task.Freelancer.Email,
                                task.Freelancer.Name,
                                "Alteração de Tarefa",
                                "task-change",
                                new Dictionary<string, string> {
                                    { "TASK", task.Title}
                                });
                    }
                    else if (NewStatus == TaskStatus.DONE && CurrentStatus == TaskStatus.WAITING)
                    {
                        Email sendEmail = new Email();
                        sendEmail.Send(
                                task.Freelancer.Email,
                                task.Freelancer.Name,
                                "Job Aprovado!",
                                "task-conclused",
                                new Dictionary<string, string> {
                                    { "DE", task.User.Name },
                                    { "EMPRESA", String.IsNullOrEmpty(task.User.Customer.Trade)
                                        ? task.User.Customer.Name :  task.User.Customer.Trade },
                                    { "TASK", task.Title}
                                });

                    }
                }
                catch (Exception)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "An error ocurred on update the task status. Please try again later.");
                }

                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpPost()]
        [Route("AttachFile")]
        public HttpResponseMessage AttachFile()
        {
            int iUploadedCnt = 0;

            string folder = "/Content/images/tasks/";
            string sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/images/tasks/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            var path = "";
            var filename = "";

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];
                if (hpf.ContentLength > 10485760)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "File size exceed limit of 10MB");
                var fileName = Guid.NewGuid().ToString();
                var fileExtenstion = Path.GetExtension(hpf.FileName);
                if (hpf.ContentLength > 0)
                {
                    if (!File.Exists(sPath + Path.GetFileName(fileName + fileExtenstion)))
                    {
                        hpf.SaveAs(sPath + fileName + fileExtenstion);
                        filename = fileName + fileExtenstion;
                        path = sPath + fileName + fileExtenstion;
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt > 0)
            {
                return Request.CreateResponse(new { folder, filename });
            }
            else {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Cannot upload file");
            }
        }

        [HttpPost()]
        [Route("AttachMessageFile")]
        public HttpResponseMessage AttachMessageFile()
        {
            int iUploadedCnt = 0;

            string folder = "/Content/images/tasks/message/";
            string sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/images/tasks/message/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            var path = "";
            var filename = "";

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];
                if (hpf.ContentLength > 10485760)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "File size exceed limit of 10MB");
                var fileName = Guid.NewGuid().ToString();
                var fileExtenstion = Path.GetExtension(hpf.FileName);
                if (hpf.ContentLength > 0)
                {
                    if (!File.Exists(sPath + Path.GetFileName(fileName + fileExtenstion)))
                    {
                        hpf.SaveAs(sPath + fileName + fileExtenstion);
                        filename = fileName + fileExtenstion;
                        path = sPath + fileName + fileExtenstion;
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt > 0)
            {
                return Request.CreateResponse(new { folder, filename });
            }
            else {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Cannot upload file");
            }
        }

        [HttpPost]
        [Route("AddMessage")]
        public HttpResponseMessage AddMessage(TasksMessageViewModel model)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var freelancerId = int.Parse(Criptografy.Decrypt(model.FreelancerId, ConfigurationManager.AppSettings["cryptoPass"]));

                if (model.FreelancerId == null && freelancerId == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Freelancer does not exist");

                var freelancer = _serviceFreelancer.GetById(freelancerId);
                if (freelancer == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Freelancer does not exist");

                Email sendEmail = new Email();
                var task = _serviceTasks.GetById(model.TaskId.Value);
                _serviceTasks.Update(task);

                

                var message = new MessageTasks()
                {
                    TaskId = model.TaskId.Value,
                    UserId = model.UserId,
                    FreelancerId = freelancerId,
                    Text = model.Text,
                    Read = false,
                    CreatedAt = DateTime.Now
                };

                model.CreatedAt = message.CreatedAt;

                if(!string.IsNullOrEmpty(model.Text))
                    _serviceMessageTasks.Add(message);
                

                if (model.Attachs != null && model.Attachs.Count() != 0)
                {
                    model.Attachs.ForEach(attach =>
                    {
                        _serviceMessageTasksAttach.Add(new MessageTaskAttachs()
                        {
                            MessageTasksId = message.Id,
                            Name = attach.Name,
                            URL = attach.URL
                        });

                    });

                }

                if (model.Price != null && model.Price != 0 && model.DeadlineDays != null && model.DeadlineDays != 0)
                {
                    var propose = new Briefing.Propose()
                    {
                        TaskId = model.TaskId,
                        FreelancerId = freelancerId,
                        DeadlineDays = (int)model.DeadlineDays,
                        Price = (decimal)model.Price
                    };
                    _servicePropose.Add(propose);

                    sendEmail.Send(
                        task.User.Email,
                        task.User.Name,
                        "Você recebeu uma nova proposta",
                        "new-propose",
                        new Dictionary<string, string> {
                        { "CONFIRMEMAIL", String.Concat(ConfigurationManager.AppSettings["EmailconfirmadoURL"],"?hash=",task.User.ConfirmEmail.ToString()) },
                        { "DE", freelancer.Name },
                        { "TITLE", task.Title }
                    });
                }

                if (model.UserId != 0 && model.UserId != null)
                {
                    var user = _serviceUser.GetById((int)model.UserId);

                    sendEmail.Send(
                        freelancer.Email,
                        freelancer.Name,
                        "Nova mensagem",
                        "new-message-freelancer",
                        new Dictionary<string, string> {
                            { "CONFIRMEMAIL", String.Concat(ConfigurationManager.AppSettings["EmailconfirmadoURL"],"?hash=",freelancer.Users.First().ConfirmEmail.ToString()) },
                            { "NOME", freelancer.Name },
                            { "DE", user.Name },
                            { "EMPRESA", String.IsNullOrEmpty(user.Customer.Trade) ? user.Customer.Name :  user.Customer.Trade },
                            { "TASK", task.Title }
                        });
                }
                else
                {

                    if (!string.IsNullOrEmpty(model.Text) || ((model.Price == null || model.Price == 0) && (model.DeadlineDays == null || model.DeadlineDays == 0)))
                    {
                        sendEmail.Send(
                            task.User.Email,
                            task.User.Name,
                            "Nova mensagem",
                            "new-message-client",
                            new Dictionary<string, string> {
                                { "NOME", task.User.Name },
                                { "DE", freelancer.Name },
                                { "TASK", task.Title }
                        });
                    }

                }

                response = Request.CreateResponse(new { model });
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }

        [HttpPost]
        [Route("Archive")]
        public HttpResponseMessage Archive(int TaskId, bool Archive)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var task = _serviceTasks.GetById(TaskId);
                task.Active = !Archive;

                _serviceTasks.Update(task);

                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("List")]
        public IHttpActionResult List(int IdUser, TaskStatus Status, int IdProject = 0, int Page = 1, int PageSize = 10, OrderTask OrderTask = OrderTask.UPDATED, OrderType OrderType = OrderType.DESC)
        {
            return ListTasks(IdUser, Status, IdProject, Page, PageSize, true, OrderTask, OrderType);
        }

        [HttpGet]
        [Route("ListArchive")]
        public IHttpActionResult ListArchive(int IdUser, TaskStatus Status, int IdProject = 0, int Page = 1, int PageSize = 10, OrderTask OrderTask = OrderTask.UPDATED, OrderType OrderType = OrderType.DESC)
        {
            return ListTasks(IdUser, Status, IdProject, Page, PageSize, false, OrderTask, OrderType);
        }

        private IHttpActionResult ListTasks(int IdUser, TaskStatus Status, int IdProject, int Page, int PageSize, bool active, OrderTask OrderTask, OrderType OrderType)
        {
            IHttpActionResult response = null;

            if (ModelState.IsValid)
            {
                var user = _serviceUser.GetById(IdUser);

                if (user == null)
                    return Content(HttpStatusCode.NotFound, new { message = "User does not exist" });

                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
                var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;
                var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

                var tasks = _serviceTasks.GetAll(x => x.Status == Status, x => x.CreatedAt.ToString(), ascending: false);


                tasks = tasks.Where(x => x.Active);

                if (user.Role == RoleType.MASTER)
                {
                    tasks = tasks.ToList();
                }
                else if (user.Role == RoleType.EMPLOYEE || user.Role == RoleType.CLIENT)
                {
                    tasks = tasks.Where(x => x.User.Customer.Id == user.IdCustomer);
                }
                else
                {
                    tasks = tasks.Where(x => x.Freelancer.Id == user.IdFreelancer);
                }

                if (IdProject != 0)
                {
                    var project = _serviceProject.GetById(IdProject);

                    if (project == null)
                        return Content(HttpStatusCode.NotFound, "This project does not exist.");

                    if (requestUser.IdCustomer != project.IdCustomer && requestUser.Role != RoleType.MASTER)
                        return Content(HttpStatusCode.Forbidden, "You don't have permission to access this project.");

                    tasks = tasks.Where(x => x.IdProject == IdProject).ToList();
                }
                else
                    tasks = tasks.Where(x => x.Project.Active).ToList();

                switch (OrderTask)
                {
                    case OrderTask.DEADLINE:
                        if (OrderType == OrderType.ASC)
                            tasks = tasks.OrderBy(x => x.DeliveryAt).ToList();
                        else
                            tasks = tasks.OrderByDescending(x => x.DeliveryAt).ToList();
                        break;

                    case OrderTask.PRICE:
                        if (OrderType == OrderType.ASC)
                            tasks = tasks.OrderBy(x => x.Price).ToList();
                        else
                            tasks = tasks.OrderByDescending(x => x.Price).ToList();
                        break;

                    case OrderTask.FREELANCER:
                        if (OrderType == OrderType.ASC)
                            tasks = tasks.OrderBy(x => x.Freelancer.Name).ToList();
                        else
                            tasks = tasks.OrderByDescending(x => x.Freelancer.Name).ToList();
                        break;

                    case OrderTask.CREATED:
                        if (OrderType == OrderType.ASC)
                            tasks = tasks.OrderBy(x => x.CreatedAt).ToList();
                        else
                            tasks = tasks.OrderByDescending(x => x.CreatedAt).ToList();
                        break;
                    case OrderTask.UPDATED:
                        if (OrderType == OrderType.ASC)
                            tasks = tasks.OrderBy(x => x.UpdatedAt).ToList();
                        else
                            tasks = tasks.OrderByDescending(x => x.UpdatedAt).ToList();
                        break;
                }

                var skipCount = (Page - 1) * PageSize;
                tasks = tasks.Skip(skipCount).Take(PageSize).ToList();

                var model = new List<TasksViewModel>();

                tasks.ToList().ForEach(task =>
                {
                    Briefing.Propose propose = null;
                    var freelancerTaskId = Criptografy.Encrypt(task.Freelancer.Id.ToString(), ConfigurationManager.AppSettings["cryptoPass"]);
                    if (string.IsNullOrEmpty(task.IdBriefing.ToString()))
                    {
                        propose = _servicePropose.GetAll(x => x.TaskId == task.Id && x.Contracted).FirstOrDefault();
                    }
                    else
                    {
                        propose = _servicePropose.GetAll(x => x.BriefingId == task.IdBriefing && x.Contracted).FirstOrDefault();
                    }
                    ProposeViewModel selectedPropose = null;
                    if (propose != null)
                    {
                        selectedPropose = new ProposeViewModel()
                        {
                            DeadlineDays = propose.DeadlineDays,
                            Price = propose.Price,
                            Id = propose.Id,
                            Freelancer = new FreelancerBriefingViewModel()
                            {
                                Id = freelancerTaskId,
                                Name = propose.Freelancer.Name,
                                Photo = propose.Freelancer.Users.First().Photo,
                                Code = propose.Freelancer.Code
                            },
                            DeliveryAt = propose.UpdatedAt.Value.AddDays(propose.DeadlineDays)
                        };
                    }

                    var item = new TasksViewModel()
                    {
                        Id = task.Id,
                        Title = task.Title,
                        Description = task.Description,
                        Project = new Models.Projects.ProjectViewModel()
                        {
                            Id = task.Project.Id,
                            Color = task.Project.Color,
                            Name = task.Project.Name
                        },
                        Freelancer = new FreelancerBriefingViewModel()
                        {
                            Id = freelancerTaskId,
                            Name = task.Freelancer.Name,
                            Photo = task.Freelancer.Users.Count() != 0 ? task.Freelancer.Users.First().Photo : null,
                            Title = task.Freelancer.Title,
                            Code = task.Freelancer.Code
                        },
                        Status = task.Status,
                        User = new UserBriefingViewModel()
                        {
                            Id = task.User.Id,
                            Customer = new CustomerViewModel()
                            {
                                Id = task.User.Customer.Id,
                                Name = task.User.Customer.Name,
                                Logo = task.User.Customer.Logo
                            },
                            Name = task.User.Name,
                            Photo = task.User.Photo
                        },
                        DeliveryAt = task.DeliveryAt.HasValue ? String.Format("{0:s}", task.DeliveryAt.Value) : null,
                        Price = task.Price,
                        Attachs = new List<TasksAttachViewModel>(),
                        Messages = new List<TasksMessageViewModel>(),
                        Contracted = propose != null,
                        SelectedPropose = selectedPropose,
                        UpdatedAt = task.UpdatedAt.HasValue ? String.Format("{0:s}", task.UpdatedAt.Value) : null,
                        Active = task.Active,
                        FreelancerReview = task.FreelancerReview ?? false,
                        CustomerReview = task.CustomerReview ?? false
                    };

                    task.Attachs.ToList().ForEach(attach =>
                    {
                        item.Attachs.Add(new TasksAttachViewModel()
                        {
                            URL = attach.URL,
                            Name = attach.Name,
                            IdTask = task.Id,
                        });
                    });

                    if (task.Briefing != null)
                    {
                        task.Briefing.Messages.ToList().ForEach(message =>
                        {
                            var freelancerBriefingMsgId = Criptografy.Encrypt(message.Freelancers.Id.ToString(), ConfigurationManager.AppSettings["cryptoPass"]);
                            var message_item = new TasksMessageViewModel()
                            {
                                BriefingId = task.IdBriefing,
                                CreatedAt = message.CreatedAt,
                                FreelancerId = freelancerBriefingMsgId,
                                Id = message.Id,
                                Read = message.Read,
                                TaskId = task.Id,
                                Text = message.Text,
                                UserId = message.UserId,
                                Freelancer = new FreelancerBriefingViewModel()
                                {
                                    Id = freelancerBriefingMsgId,
                                    Name = message.Freelancers.Name,
                                    Photo = message.Freelancers.Users.Count() != 0 ? message.Freelancers.Users.First().Photo : null,
                                    Title = message.Freelancers.Title,
                                    Code = message.Freelancers.Code
                                }
                            };

                            if (message.UserId != null)
                            {
                                message_item.User = new UserBriefingViewModel()
                                {
                                    Id = message.User.Id,
                                    Customer = new CustomerViewModel()
                                    {
                                        Id = message.User.Customer.Id,
                                        Name = message.User.Customer.Name,
                                        Logo = message.User.Customer.Logo
                                    },
                                    Name = message.User.Name,
                                    Photo = message.User.Photo
                                };
                            }


                            item.Messages.Add(message_item);
                        });
                    }

                    task.Messages.ToList().ForEach(message =>
                    {
                        var freelancerTaskgMsgId = Criptografy.Encrypt(message.Freelancer.Id.ToString(), ConfigurationManager.AppSettings["cryptoPass"]);

                        var message_item = new TasksMessageViewModel()
                        {
                            BriefingId = task.IdBriefing,
                            CreatedAt = message.CreatedAt,
                            FreelancerId = freelancerTaskgMsgId,
                            Id = message.Id,
                            Read = message.Read,
                            TaskId = message.TaskId,
                            Text = message.Text,
                            UserId = message.UserId,
                            Freelancer = new FreelancerBriefingViewModel()
                            {
                                Id = freelancerTaskgMsgId,
                                Name = message.Freelancer.Name,
                                Photo = message.Freelancer.Users.Count() != 0 ? message.Freelancer.Users.First().Photo : null,
                                Title = message.Freelancer.Title,
                                Code = message.Freelancer.Code
                            }
                        };

                        if (message.UserId != null)
                        {
                            message_item.User = new UserBriefingViewModel()
                            {
                                Id = message.User.Id,
                                Customer = new CustomerViewModel()
                                {
                                    Id = message.User.Customer.Id,
                                    Name = message.User.Customer.Name,
                                    Logo = message.User.Customer.Logo
                                },
                                Name = message.User.Name,
                                Photo = message.User.Photo
                            };
                        }

                        if (propose != null)
                        {
                            message_item.Price = propose.Price;
                            message_item.DeadlineDays = propose.DeadlineDays;
                        }

                        message_item.Attachs = new List<TasksMessageAttachViewModel>();
                        message.Attachs.ToList().ForEach(m =>
                        {
                            message_item.Attachs.Add(new TasksMessageAttachViewModel()
                            {
                                IdMessage = m.MessageTasksId,
                                Name = m.Name,
                                URL = m.URL
                            });
                        });

                        item.Messages.Add(message_item);
                    });

                    if (user.Role == RoleType.FREELANCER)
                        item.UnreadMessages = task.Messages.Where(x => x.FreelancerId == user.IdFreelancer && !x.Read && x.User != null).Count();
                    else
                        item.UnreadMessages = task.Messages.Where(x => !x.Read && x.UserId != user.Id && x.Task.IdUser == user.Id).Count();


                    model.Add(item);
                });


                response = Ok(model);
            }
            else
            {
                response = BadRequest(ModelState);
            }

            return response;
        }


        [HttpPost]
        [Route("ReadMessage")]
        public HttpResponseMessage ReadMessage(int user_id, int message_id)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var message = _serviceMessageTasks.GetById(message_id);

                if(message == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Message does not exist");

                var user = _serviceUser.GetById(user_id);

                if (user == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "User does not exist");

                if (user.Role == RoleType.FREELANCER)
                {
                    if (user.Id == message.Freelancer.Users.First().Id)
                    {
                        message.Read = true;
                        _serviceMessageTasks.Update(message);
                        response = Request.CreateResponse(HttpStatusCode.OK);
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    if (user.Id == message.Task.IdUser)
                    {
                        message.Read = true;
                        _serviceMessageTasks.Update(message);
                        response = Request.CreateResponse(HttpStatusCode.OK);
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK);
                    }
                }
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }

        [HttpPost]
        [Route("EditTitle")]
        public HttpResponseMessage EditTitle(int task_id, string title)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var task = _serviceTasks.GetById(task_id);
                var handler = new JwtSecurityTokenHandler();

                var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
                var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

                var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

                if (task == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Task does not exist");

                if (requestUser.Customer.Id != task.User.Customer.Id && requestUser.Role != RoleType.MASTER)
                    return Request.CreateResponse(HttpStatusCode.Forbidden, "You don't have permission to change this task");

                var old_title = task.Title;

                task.Title = title;
                _serviceTasks.Update(task);

                SendAutomaticMessage(task, task.Title, old_title, "Título");

                response = Request.CreateResponse(HttpStatusCode.OK);
                   
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }

        private void SendAutomaticMessage(Tasks task, string new_text, string old_text, string property)
        {
            var message = new MessageTasks()
            {
                FreelancerId = task.IdFreelancer,
                TaskId = task.Id,
                Text = "*" + property + " da tarefa alterado de " + old_text + " para " + new_text + "*",
                UserId = task.IdUser
            };
            _serviceMessageTasks.Add(message);
        }


        [HttpPost]
        [Route("RemoveTask")]
        public HttpResponseMessage RemoveTask(int task_id)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var task = _serviceTasks.GetById(task_id);
                var handler = new JwtSecurityTokenHandler();

                var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
                var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

                var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

                if (task == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Task does not exist");

                if (requestUser.Customer.Id != task.User.Customer.Id && requestUser.Role != RoleType.MASTER)
                    return Request.CreateResponse(HttpStatusCode.Forbidden, "You don't have permission to change this task");

                task.Active = false;
                _serviceTasks.Update(task);

                SendAutomaticMessage(task, "cancelado", "ativo", "Status");

                response = Request.CreateResponse(HttpStatusCode.OK);

            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }
    }
}
