﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Services;

namespace API.Controllers
{
    [Authorize]
    [RoutePrefix("Search")]
    public class SearchController : BaseAPIController
    {
        private readonly IServiceUser _serviceUser;
        private readonly IServiceBase<Search> _serviceSearch;

        public SearchController(IServiceUser serviceUser, IServiceBase<Search> serviceSearch)
        {
            _serviceUser = serviceUser;
            _serviceSearch = serviceSearch;
        }
        // GET: api/Search
        [Route("")]
        public IEnumerable<string> Get(string tags)
        { 
            return new List<string>();
        }

        // GET: api/Search/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: Search
        [HttpPost]
        [Route("")]
        public HttpResponseMessage Post([FromUri]string tags)
        {
            if (!String.IsNullOrEmpty(tags))
            {

                var handler = new JwtSecurityTokenHandler();

                var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
                var uniqueName = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

                var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(uniqueName));

                var search = new Search()
                {
                    Active = true,
                    CreatedAt = DateTime.Now,
                    Tag = tags,
                    UserId = requestUser.Id
                };

                _serviceSearch.Add(search);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // PUT: api/Search/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Search/5
        public void Delete(int id)
        {
        }
    }
}
