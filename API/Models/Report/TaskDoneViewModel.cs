﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Report
{
    public class TaskDoneViewModel
    {
        public string FreelancerName { get; set; }
        public string FreelancerPhoto { get; set; }
        public string TaskName { get; set; }
        public string ProjectName { get; set; }
        public int ProjectId { get; set; }
        public string CustomerLogo { get; set; }
        public string CustomerName { get; set; }
        public string Date { get; set; }
        public decimal Price { get; set; }
        public string Color { get; set; }
        public string ResponsiblePhoto { get; set; }
        public string CustomerTrade { get; internal set; }
        public string ResponsibleName { get; internal set; }
    }
}