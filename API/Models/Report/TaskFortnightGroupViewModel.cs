﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Report
{
    public class TaskFortnightGroupViewModel
    {
        public string weekname { get; set; }
        public int weeknumber { get; set; }
        public int id { get; set; }
        public string title { get; set; }
        public decimal? price { get; set; }
        public string color { get; set; }
        public string project { get; set; }
        public DateTime updated_at { get; set; }
    }
}