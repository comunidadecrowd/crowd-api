﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class FreelancerAwardsViewModel
    {
        public int Id { get; set; }
        public int FreelancerId { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string URL { get; set; }
        public AwardType Type { get; set; }
    }
}
