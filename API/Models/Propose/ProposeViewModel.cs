﻿using System;

namespace API.Models.Propose
{
    public class ProposeViewModel
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public int DeadlineDays { get; set; }

        public bool Contracted { get; set; }

        public DateTime DeliveryAt { get; set; } 

        public FreelancerBriefingViewModel Freelancer { get; set; }
    }
}