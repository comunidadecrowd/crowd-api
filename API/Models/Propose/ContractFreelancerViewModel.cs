﻿namespace API.Models.Propose
{
    public class ContractFreelancerViewModel
    {
        public int ProposeId { get; set; }
        public int UserId { get; set; }
        public string FreelancerId { get; set; }
        public int BriefingId { get; set; }
    }
}