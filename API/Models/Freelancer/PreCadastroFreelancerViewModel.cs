﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Freelancer
{
    /// <summary>
    /// ViewModel do endpoint PreCadastro
    /// </summary>
    public class PreCadastroFreelancerViewModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public CategoriesViewModel Category { get; set; }
        public string Referrer { get; set; }
        public string PromoCode { get; set; }
        public bool AcceptTerms { get; set; }
    }
}
