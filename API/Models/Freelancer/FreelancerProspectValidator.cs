﻿using System;
using Backend.Crowd.Domain.Interfaces.Services;
using FluentValidation;
using Backend.Crowd.Domain.Util;

namespace API.Models.freelancer
{
    /// <summary>
    /// Valida PreCadastroViewModel
    /// </summary>
    public class FreelancerProspectValidator : AbstractValidator<Backend.Crowd.Domain.Entities.Freelancer>
    {
        private readonly IServiceBase<Backend.Crowd.Domain.Entities.Freelancer> _servicefreelancer;

        public FreelancerProspectValidator(IServiceBase<Backend.Crowd.Domain.Entities.Freelancer> servicefreelancer)
        {
            _servicefreelancer = servicefreelancer;

            RuleFor(freelancer => freelancer.Name).NotEmpty().WithMessage("Name,Razão social é obrigatória.");
            RuleFor(freelancer => freelancer.Title).NotEmpty().WithMessage("Title,Titulo é obrigatório.");
            RuleFor(freelancer => freelancer.Description).NotEmpty().WithMessage("Description,Sobre é obrigatório.");
            RuleFor(freelancer => freelancer.Skills).Must(s => s.Count > 0).WithMessage("Skills,Uma Habilidade é obrigatória.");
            RuleFor(freelancer => freelancer.Country).NotEmpty().WithMessage("Country,País é obrigatório.");
            RuleFor(freelancer => freelancer.State).NotEmpty().WithMessage("State,Estado é obrigatório.");
            RuleFor(freelancer => freelancer.City).NotEmpty().WithMessage("City,Cidade é obrigatória.");
            RuleFor(freelancer => freelancer.Price).NotEmpty().WithMessage("Price,Valor é obrigatório.");
            RuleFor(freelancer => freelancer.CategoryId).Must(s=>s > 0).WithMessage("Category,Categoria é obrigatória.");
            RuleFor(freelancer => freelancer.Availability).NotEmpty().WithMessage("Availability,Disponibilidade é obrigatória.");
            RuleFor(freelancer => freelancer.Segments).Must(s=> s.Count > 0).WithMessage("Segments,Um Segmento é obrigatório.");
            RuleFor(freelancer => freelancer.Languages).Must(s=> s.Count > 0).WithMessage("Languages,Um Idioma é obrigatório.");
            RuleFor(freelancer => freelancer.Experiences).Must(s=> s.Count > 0).WithMessage("Experiences,Experiencia é obrigatória.");

        }

    }
}
