﻿using System;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Util;
using FluentValidation;

namespace API.Models.Freelancer
{
    /// <summary>
    /// Valida PreCadastroFreelancerViewModel
    /// </summary>
    public class PreCadastroFreelancerViewModelValidator : AbstractValidator<PreCadastroFreelancerViewModel>
    {
        private readonly IServiceUser _serviceUser;

        public PreCadastroFreelancerViewModelValidator(IServiceUser serviceuser)
        {
            _serviceUser = serviceuser;

            RuleFor(preCadastro => preCadastro.Name).NotEmpty().WithMessage("Seu Nome é obrigatório.");
            RuleFor(preCadastro => preCadastro.Category).NotEmpty().WithMessage("Categoria é obrigatória.");
            RuleFor(preCadastro => preCadastro.Category.Id).NotEmpty().WithMessage("Categoria é obrigatória.");
            RuleFor(preCadastro => preCadastro.Category.Name).NotEmpty().WithMessage("Categoria é obrigatória.");
            RuleFor(preCadastro => preCadastro.Email).EmailAddress().WithMessage("Email inválido.");
            RuleFor(preCadastro => preCadastro.Phone).NotEmpty().WithMessage("Telefone Obrigatório.");
            RuleFor(preCadastro => preCadastro.Phone).Must(PhoneMatch).WithMessage("Telefone Inválido.");
            RuleFor(preCadastro => preCadastro.Email).Must(EmailnotExist).WithMessage("Email já cadastrado");
            RuleFor(preCadastro => preCadastro.Phone).Must(PhonenotExist).WithMessage("Telefone já cadastrado");
        }

        private bool PhoneMatch(string phone)
        {
            return phone.Length.IsBetweenII(10, 11);
        }

        private bool PhonenotExist(string phone)
        {
            var userExist = _serviceUser.GetByExpression(x => x.Phone == phone);

            return userExist == null;
        }

        private bool EmailnotExist(string email)
        {
            var userExist = _serviceUser.GetByExpression(x => x.Email == email);

            return userExist == null;
        }
    }
}
