﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class CustomerInterestedViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Trade { get; set; }
        public string CNPJ { get; set; }

        public PlanType PlanType { get; set; }

        public int QtyPersons { get; set; }

        public int State { get; set; }
        public int City { get; set; }

        public string Address { get; set; }
        public string Number { get; set; }
        public string Complement { get; set; }

        public string NameResponsible { get; set; }
        public string EmailResponsible { get; set; }

        public string Phone { get; set; }

        public bool Active { get; set; }

        public bool AcceptTerms { get; set; }

        public string Referrer { get; set; }
    }
}