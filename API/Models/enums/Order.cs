using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.enums
{
    public enum Order
    {
        CREATED = 0,
        PRICE = 1,
        RATING = 2
    }

    public enum OrderType
    {
        ASC = 1,
        DESC = 2
    }

    public enum OrderTask
    {
        DEADLINE = 1,
        PRICE = 2,
        FREELANCER = 3,
        CREATED = 4,
        UPDATED = 5,
    }
}
