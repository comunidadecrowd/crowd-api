﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class CustomerRatingSaveViewModel
    {
        public int taskId { get; set; }

        public int Approval { get; set; }
        public int Communicate { get; set; }
        public int Professionalism { get; set; }
        public string Comment { get; set; }
    }
}