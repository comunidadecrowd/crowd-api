﻿using Backend.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class FreelancerBankInfoViewModel
    {
        public string Id { get; set; }
        public string RazaoSocial { get; set; }
        public string CNPJ { get; set; }
        public string InscricaoEstadual { get; set; }
        public string InscricaoMunicipal { get; set; }
        public int? CompanyStateId { get; set; }
        public int? CompanyCityId { get; set; }
        public virtual StateViewModel CompanyState { get; set; }
        public virtual CityViewModel CompanyCity { get; set; }
        public string Address { get; set; }
        public string Number { get; set; }
        public string Complement { get; set; }

        /* Dados bancarios */
        public string BankFullName { get; set; }
        public string BankAgency { get; set; }
        public string BankAccount { get; set; }
        public string CPF { get; set; }
        public string Bank { get; set; }
    }
}