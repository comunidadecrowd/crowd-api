﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Backend.Crowd.Domain.Entities.Enum;

namespace API.Models
{
    public class BePayViewModel
    {
        public int IdCustomer { get; set; }
        public PlanType PlanType { get; set; }
        public string transactionId { get; set; }
        public string creditCardToken { get; set; }
        public string readableLine { get; set; } 
        public FinancialStatement financialStatement { get; set; }
    }

    public class AuthorizationDetails
    {
        public string number { get; set; }
    }

    public class FinancialStatement
    {
        public string status { get; set; }
        public AuthorizationDetails authorizationDetails { get; set; }
    }
}