﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class FreelancerAvailableViewModel
    {
        public string FreelancerId { get; set; }
        public bool Available { get; set; }
    }
}