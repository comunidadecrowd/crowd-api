﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Backend.Crowd.Domain.Entities.Enum;

namespace API.Models
{
    public class CoverViewModel
    {
        public FileType Type { get; set; }
        public string URL { get; set; }
        public string PhotoBase64 { get; set; }
        public string PhotoExtension { get; set; }
    }
}