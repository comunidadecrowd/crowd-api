﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace API.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MessageErrorViewModel
    {
        private int _code;
        private string _description;

        public MessageErrorViewModel(int code, string description)
        {
            this._code = code;
            this._description = description;
        }

        public int Code { get { return _code; } }
        public string Description { get { return _description; } }

        internal static HttpResponseMessage ReturnError(HttpStatusCode statusCode,int errorCode, string errorMessage)
        {
            return new HttpResponseMessage(statusCode) { Content = new MessageErrorViewModel(errorCode, errorMessage).Serialize() };
        }

        private HttpContent Serialize()
        {
            return new StringContent( Newtonsoft.Json.JsonConvert.SerializeObject(this), System.Text.Encoding.UTF8,"application/json");
        }
    }
}