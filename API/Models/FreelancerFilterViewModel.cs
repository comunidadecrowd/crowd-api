﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class FreelancerFilterViewModel
    {
        public int[] segmentsType { get; set; }
        public string[] terms { get; set; }
        public int uf { get; set; }
        public int city { get; set; }
        public int segment { get; set; }
        public int minPrice { get; set; }
        public int maxPrice { get; set; }
        public AvailabilityType availability { get; set; }
        public bool? Experience { get; set; }
        public bool? Portfolio { get; set; }
    }
}