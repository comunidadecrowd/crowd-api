﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Projects
{
    public class ProjectAddViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int IdUser { get; set; }
    }
}