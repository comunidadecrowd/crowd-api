﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Sms
{
    public class SmsBriefingStatusViewModel
    {
        public SmsBriefingStatus Status { get; set; }
        public int FreelancerId { get; set; }
        public int BriefingId { get; set; }
    }
}