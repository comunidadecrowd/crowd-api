﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class SmsUpdateViewModel
    {
        public SmsUpdateViewModel()
        {        
        }

        public int Id { get; set; }
        public string Sms { get; set; }
        public int Status { get; set; }
        public DateTime? DataEnvio { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string SourceId { get; set; }
    }
}