﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Sms
{
    public class SmsBriefingProposeViewModel : SmsBriefingStatusViewModel
    {
        public string Text { get; set; }
        public decimal? Price { get; set; }
        public int? DeadlineDays { get; set; }
    }
}