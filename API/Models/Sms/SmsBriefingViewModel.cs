﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Sms
{
    public class SmsBriefingViewModel
    {
        public int Id { get; }
        public int FreelancerId { get; }
        public string Name { get; }
        public string Role { get; }
        public string Resumo { get; }
        public string Title { get; }
        public string Description { get; }
        public string Company { get; }
        public string ServerUrl { get; }
        public string Project { get; }
        public string Logo { get; }
        public string Photo { get; }
        public List<AnexosViewModel> Attachs { get; }
        public List<SmsBriefingComplementViewModel> Complements { get; }

        public bool IsAttachs {
            get
            {
                return Attachs.Count > 0;
            }
        }

        public bool IsComplements
        {
            get
            {
                return Complements.Count > 0;
            }
        }

        public SmsBriefingViewModel(Briefing briefing, string baseUrl,int freelancerId)
        {
            this.Id = briefing.Id;
            this.Name = briefing.User.Name;
            this.Role = briefing.User.RoleCompany;
            this.Title = briefing.Title;

            if (briefing.User.Customer.PlanType == PlanType.FREE)
                this.Description = Validador.ClearText(briefing.Text);
            else
                this.Description = briefing.Text;

            this.Resumo = briefing.Excerpt;
            this.Company = briefing.User.Customer.Trade;
            this.Project = briefing.Project.Name;
            this.Logo = String.Concat(baseUrl, briefing.User.Customer.Logo);
            this.Photo = String.Concat(baseUrl, briefing.User.Photo);
            this.FreelancerId = freelancerId;
            
            this.Attachs = new List<AnexosViewModel>();

            this.Complements = new List<SmsBriefingComplementViewModel>();

            briefing.Attachs.ToList().ForEach(a => this.Attachs.Add(new AnexosViewModel(a)));
            briefing.Complements.ToList().ForEach(a => this.Complements.Add(new SmsBriefingComplementViewModel(a)));

            this.ServerUrl = baseUrl;

        }
    }
}