﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Backend.Crowd.Domain.Entities;

namespace API.Models
{ 
    public class SmsGetViewModel
    {
        public SmsGetViewModel(Backend.Crowd.Domain.Entities.Sms sms)
        {
            Id = sms.Id;
            From = sms.From;
            To = sms.To;
            Body = sms.Body;
            SourceId = sms.SourceId;
        }

        public int Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Body { get; set; }
        public string SourceId { get; set; }
    }
}