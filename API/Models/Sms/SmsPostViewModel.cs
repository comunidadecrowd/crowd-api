﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Sms
{
    public class SmsPostViewModel
    {
        public int IdUser { get;  set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Template { get; set; }
        public List<KeyValuePair<String,String>> Variables { get; set; }
    }
}