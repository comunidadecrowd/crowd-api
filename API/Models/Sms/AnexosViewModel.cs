﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Backend.Crowd.Domain.Entities;

namespace API.Models.Sms
{
    public class AnexosViewModel
    {
        public AnexosViewModel(ComplementAttach attach)
        {
            Link = attach.attachUrl;
            Name = attach.attachName;
        }

        public AnexosViewModel(Briefing.Attach attach)
        {
            Link = attach.attachUrl;
            Name = attach.attachName;
        }

        public string Link { get; }
        public string Name { get; }
    }
}