﻿using Backend.Crowd.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace API.Models.Sms
{
    public class SmsBriefingComplementViewModel
    {
        public string Description { get; }
        public List<AnexosViewModel> Attachs { get; }

        public bool IsAttachs
        {
            get
            {
                return Attachs.Count > 0;
            }
        }

        public SmsBriefingComplementViewModel(Briefing.Complement complement)
        {
            Description = complement.Text;

            Attachs = new List<AnexosViewModel>();

            complement.Attachs.ToList().ForEach(a => this.Attachs.Add(new AnexosViewModel(a)));
        }
    }
}