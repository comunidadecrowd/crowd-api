﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Sms
{
    public class SmsBriefingResponseViewModel
    {
        
        public string Title { get; }
        
        public string Company { get; }
        

        public SmsBriefingResponseViewModel(Briefing briefing)
        {
           
            this.Title = briefing.Title;
            this.Company = briefing.User.Customer.Trade;

        }
    }
}