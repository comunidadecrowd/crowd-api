﻿using Backend.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Login
{
    public class LoginResponseViewModel
    {
        public LoginResponseViewModel(LoggedUser loggedUser, string token)
        {
            this.loggedUser = loggedUser;
            this.token = token;
        }

        public LoggedUser loggedUser { get; set; }
        public string token { get; set; }
    }
}