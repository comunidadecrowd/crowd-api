﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class FreelancerRatingSaveViewModel
    {
        public int taskId { get; set; }

        public int Quality { get; set; }
        public int Responsability { get; set; }
        public int Agility { get; set; }
        public string Comment { get; set; }
    }
}