﻿using API.Models.Projects;
using API.Models.Propose;
using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Tasks
{
    public class TasksViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public UserBriefingViewModel User { get; set; }
        public ProjectViewModel Project { get; set; }
        public FreelancerBriefingViewModel Freelancer { get; set; }
        public TaskStatus Status { get; set; }
        public List<TasksAttachViewModel> Attachs { get; set; }

        public List<TasksMessageViewModel> Messages { get; set; }

        public int UnreadMessages { get; set; }

        public string DeliveryAt { get; set; }
        public decimal? Price { get; set; }

        public bool Contracted { get; set; }
        public ProposeViewModel SelectedPropose { get; set; }

        public string UpdatedAt { get; set; }

        public bool Active { get; set; }
        public bool FreelancerReview { get; set; }
        public bool CustomerReview { get; set; }
    }
}