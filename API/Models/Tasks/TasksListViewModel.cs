﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Tasks
{
    public class TasksListViewModel
    {
        public int IdUser { get; set; }
        public int IdProject { get; set; }
        public TaskStatus Status { get; set; }
    }
}