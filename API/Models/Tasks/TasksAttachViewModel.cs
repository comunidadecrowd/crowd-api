﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Tasks
{
    public class TasksAttachViewModel
    {
        public int IdTask { get; set; }
        public string URL { get; set; }
        public string Name { get; set; }
    }
}