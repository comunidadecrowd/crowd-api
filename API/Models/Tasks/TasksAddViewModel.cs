﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Tasks
{
    public class TasksAddViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int IdUser { get; set; }
        public int IdProject { get; set; }
        public string IdFreelancer { get; set; }
        public TaskStatus Status { get; set; }
        public List<TasksAttachViewModel> Attachs { get; set; }
    }
}