﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Tasks
{
    public class TasksGetViewModel
    {
        public int IdUser { get; set; }
        public int IdTask { get; set; }
    }
}