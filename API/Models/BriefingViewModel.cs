﻿using API.Models.Projects;
using API.Models.Propose;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Backend.Crowd.Domain.Entities;

namespace API.Models
{
    public class BriefingViewModel
    {
        public BriefingViewModel()
        {
            Freelancers = new List<FreelancerBriefingViewModel>();    
        }

        public BriefingViewModel(BriefingList briefing)
        {
            this.Id = briefing.Id;
            this.User = new UserBriefingViewModel
            {
                Name = briefing.UserName,
                Photo = briefing.Photo
            };
            this.Title = briefing.Title;
            this.Excerpt = briefing.Excerpt;
            this.CreatedAt = briefing.CreatedAt;
            this.UpdatedAt = briefing.UpdatedAt;
            this.Active = briefing.Active;
            this.Project = new ProjectViewModel
            {
                Id = briefing.IdProject,
                Name = briefing.NameProjeto,
                Color = briefing.Color
            };
            this.IdProject = briefing.IdProject;
            this.Messages = new List<MessageViewModel>();
        }

        public int Id { get; set; }
        public int IdUser { get; set; }

        public UserBriefingViewModel User { get; set; }

        public string Title { get; set; }
        public string Excerpt { get; set; }
        public string Text { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public bool Active { get; set; }

        public List<FreelancerBriefingViewModel> Freelancers { get; set; }
        public List<MessageViewModel> Messages { get; set; }

        public List<ComplementViewModel> Complements { get; set; }

        public List<BriefingAttachViewModel> Attachs { get; set; }

        public int MessagesCount { get; set; }

        public bool Bulletin { get; set; }

        public ProjectViewModel Project { get; set; }
        public int IdProject { get; set; }
                
        public ProposeViewModel SelectedPropose { get; set; }
        public bool Contracted { get; set; }
        public bool Read { get; set; }
        public bool? Hunting { get; set; }
        public bool? Sended { get; set; }
    }
}
