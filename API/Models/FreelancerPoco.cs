﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Newtonsoft.Json.Linq;
using Algolia.API;
using Backend.Crowd.Domain.Interfaces.Services;

namespace API.Models
{
    public class FreelancerPoco : IJsonParser
    {
        public string ObjectId => Id;
        public string Id { get; set; }
        public string Code { get; set; }
        public decimal Price { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string EmailSecondary { get; set; }
        public AvailabilityType Availability { get; set; }
        public string Phone { get; set; }
        public StateViewModel State { get; set; }
        public CityViewModel City { get; set; }
        public string Country { get; set; }
        public string Title { get; set; }
        public string Skype { get; set; }
        public bool Portfolio { get; set; }

        public bool FacebookUrl { get; set; }
        public bool TwitterUrl { get; set; }
        public bool DribbbleUrl { get; set; }
        public bool InstagramUrl { get; set; }
        public bool LinkedinUrl { get; set; }
        public bool BehanceUrl { get; set; }

        public string Photo { get; set; }
        public int UserId { get; set; }

        public string Description { get; set; }

        public DateTime? Birthday { get; set; }

        public CategoriesViewModel Category { get; set; }


        public string[] Segments { get; set; }//Name
        public string[] Skills { get; set; }//Name

        public ExperiencePoco Experiences { get; set; }//Description,Role, Company
        public string[] Awards { get; set; } //Title

        public bool Comments { get; set; }

        public double QualityRating { get; set; }
        public double ResponsabilityRating { get; set; }
        public double AgilityRating { get; set; }
        public double Rating { get; set; }

        public bool Active { get; set; }

        public string[] Languages { get; set; }
        public bool Available { get; set; }

        public bool Tasks { get; set; }
        public bool GaleriaPortfolio { get; set; }

        public bool Cover { get; set; }

        public DateTime CreatedAt { get; set; }
        public string Entities { get; private set; }

        public FreelancerPoco(Backend.Crowd.Domain.Entities.Freelancer freelancer)
        {
            Id = freelancer.Id.ToString();
            Code = freelancer.Code.ToString();
            Price = freelancer.Price;
            Name = freelancer.Name;
            Email = freelancer.Email;
            EmailSecondary = freelancer.EmailSecondary;
            Availability = freelancer.Availability;
            Phone = freelancer.Phone;
            if (freelancer.State != null)
                State = new StateViewModel
                {
                    Id = freelancer.State.Id,
                    Name = freelancer.State.Name,
                    UF = freelancer.State.UF
                };

            if (freelancer.City != null)
                City = new CityViewModel
                {
                    Id = freelancer.City.Id,
                    Name = freelancer.City.Name,
                    StateId = freelancer.City.StateId
                };
            Country = freelancer.Country;
            Title = freelancer.Title;
            Skype = freelancer.Skype;
            Portfolio = freelancer.Portfolios.Any();
            FacebookUrl = freelancer.Links.Any(x => x.Website == WebsiteType.Facebook);
            TwitterUrl = freelancer.Links.Any(x => x.Website == WebsiteType.Twitter);
            DribbbleUrl = freelancer.Links.Any(x => x.Website == WebsiteType.Dribbble);
            InstagramUrl = freelancer.Links.Any(x => x.Website == WebsiteType.Instagram);
            LinkedinUrl = freelancer.Links.Any(x => x.Website == WebsiteType.Linkedin);
            BehanceUrl = freelancer.Links.Any(x => x.Website == WebsiteType.Behance);
            Photo = freelancer.Users.Count != 0 ? freelancer.Users.First().Photo : "";
            UserId = freelancer.Users.Count != 0 ? freelancer.Users.First().Id : 0;
            Description = freelancer.Description;
            Birthday = freelancer.Birthday;
            Category = new CategoriesViewModel {Id = freelancer.CategoryId, Name = freelancer.Categorys.Name};
            Segments = freelancer.Segments.Select(s => s.Name).ToArray();
            Skills = freelancer.Skills.Select(s => s.Name).ToArray();
            Experiences = new ExperiencePoco
            {
                Companies = freelancer.Experiences.Select(e => e.Company).ToArray(),
                Descriptions = freelancer.Experiences.Select(e=> e.Description).ToArray(),
                Roles = freelancer.Experiences.Select(e=> e.Role).ToArray()
            };
            Awards = freelancer.Awards.Select(a => a.Title).ToArray();
            Comments = freelancer.Ratings.Any() && freelancer.Ratings.Count(r => !String.IsNullOrEmpty(r.Comment)) > 0;
            var ratingsactive = freelancer.Ratings.Where(r => r.Active).ToList();

            QualityRating = ratingsactive.Any() ? Math.Round(ratingsactive.Select(r => r.Quality).Average()) : 0;
            ResponsabilityRating = ratingsactive.Any() ? Math.Round(ratingsactive.Select(r => r.Responsability).Average()) : 0;
            AgilityRating = ratingsactive.Any() ? Math.Round(ratingsactive.Select(r => r.Agility).Average()): 0;
            Rating = Math.Round((QualityRating + ResponsabilityRating + AgilityRating) / 3);
            Active = freelancer.Active;
            Languages = freelancer.Languages.Select(l => l.Value).ToArray();
            Available = freelancer.Status == StatusUserType.Active;
            GaleriaPortfolio = freelancer.Portfolios.Any();
            Cover = freelancer.Cover != null;
            CreatedAt = freelancer.CreatedAt;
            Entities = String.Join(" ",freelancer.Entities.OrderByDescending(e => e.Salience).Select(e => e.Name).Distinct());
        }

        public JObject ToJsonBatch(string action, string indexname)
        {
            var jsonBatch = new JobjectBatch()
            {
                Action = action,
                IndexName = indexname,
                Body = ToJson()
            };

            return JObject.FromObject(jsonBatch);
        }

        public JObject ToJson()
        {
            return JObject.FromObject(this);
        }

        public static List<IJsonParser> GetAllForSearch(IServiceFreelancer serviceFreelancer, IServiceBase<Backend.Crowd.Domain.Entities.Tasks> serviceTasks)
        {
            return GetDateForSearch(DateTime.MinValue, serviceFreelancer, serviceTasks);
        }

        public static List<IJsonParser> GetDateForSearch(DateTime filterdata, IServiceFreelancer serviceFreelancer, IServiceBase<Backend.Crowd.Domain.Entities.Tasks> serviceTasks)
        {
            List<IJsonParser> jsonpoco = new List<IJsonParser>();
            var freelancers = serviceFreelancer.GetAll(f => (f.CreatedAt >= filterdata || f.UpdatedAt >= filterdata) && f.Status == StatusUserType.Active).ToList();
            freelancers.ForEach(f =>
            {
                jsonpoco.Add(new FreelancerPoco(f) { Tasks = serviceTasks.GetAll(t => t.IdFreelancer == f.Id).Any() });
            });

            return jsonpoco;
        }
    }

    public class ExperiencePoco
    {
        public string[] Roles { get; set; }
        public string[] Companies { get; set; }
        public string[] Descriptions { get; set; }
    }
}