﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class FreelancerSearchViewModel
    {
        public List<FreelancerViewModel> results { get; set; }
        public List<SkillsViewModel> relatedSkills { get; set; }        
    }
}