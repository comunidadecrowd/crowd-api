using System;

namespace API.Models
{
    public abstract class RatingTaskViewModel
    {
        public string Code { get; set; }
        public int CustomerId { get; set; }
        public string CustomerTrade { get; set; }
        public DateTime TaskDueDate { get; set; }
        public decimal? TaskPropose { get; set; }
        public string TaskTitle { get; set; }
        public string Photo { get; set; }
        public double Rating { get; set; }
        public int RatingCount { get; set; }
        
    }
}