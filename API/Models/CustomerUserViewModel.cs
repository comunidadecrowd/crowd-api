﻿using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class CustomerUserViewModel
    {
        public int ?Id { get; set; }
        public int IdCustomer { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public RoleType Role { get; set; }
        public string RoleCompany { get; set; }

        public string Photo { get; set; }
        public string PhotoBase64 { get; set; }
        public string PhotoExtension { get; set; }
        public string Phone { get; set; }
        public bool Active { get; set; }

        public bool AcceptTerms { get; set; }
    }
}