﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
	public class CustomerViewModel
	{
        public int Id { get; set; }
        public string Name { get; set; }
        public string Trade { get; set; }

        public PlanType PlanType { get; set; }

        public string Logo { get; set; }

        public string LogoBase64 { get; set; }
        public string LogoExtension { get; set; }

        public string Photo { get; set; }
        public string PhotoBase64 { get; set; }
        public string PhotoExtension { get; set; }

        public string CNPJ { get; set; }
        public int? QtyPersons { get; set; }

        public string InscricaoEstadual { get; set; }
        public string InscricaoMunicipal { get; set; }

        public StateViewModel State { get; set; }
        public CityViewModel City { get; set; }

        public string Address { get; set; }
        public string Number { get; set; }
        public string Complement { get; set; }
        public string CEP { get; set; }

        public string NameResponsible { get; set; }
        public string EmailResponsible { get; set; }

        public string Phone { get; set; }

        public bool Active { get; set; }

        public bool? Prospect { get; set; }

        public bool AcceptTerms { get; set; }

        public string Referrer { get; set; }

        public bool Trial { get; set; }

        public DateTime? TrialDate { get; set; }

        public bool? PassReset { get; set; }

        public string Password { get; set; }

        public string RoleCompany { get; set; }
    }
}