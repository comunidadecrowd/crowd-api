﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class MenuViewModel
    {
        public int id { get; set; }
        public string title { get; set; }
        public string href { get; set; }
        public string iconClass { get; set; }
    }
}