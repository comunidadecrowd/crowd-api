﻿using System;
using Backend.Crowd.Domain.Interfaces.Services;
using FluentValidation;
using Backend.Crowd.Domain.Util;

namespace API.Models.User
{
    /// <summary>
    /// Valida PreCadastroViewModel
    /// </summary>
    public class UserProspectValidator : AbstractValidator<Backend.Crowd.Domain.Entities.User>
    {

        public UserProspectValidator()
        {

            RuleFor(user => user.Password).NotEmpty().WithMessage("Password,Senha é obrigatória.");
            RuleFor(user => user.Photo).NotEmpty().WithMessage("Photo,Imagem é obrigatória.");

        }

    }
}
