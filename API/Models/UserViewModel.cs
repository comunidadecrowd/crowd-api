﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public int? IdCustomer { get; set; }
        public int? IdFreelancer { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public RoleType Role { get; set; }

        public string Photo { get; set; }
        public string RoleCompany { get; set; }

        public CustomerViewModel Customer { get; set; }
        public FreelancerViewModel Freelancer { get; set; }
    }
}