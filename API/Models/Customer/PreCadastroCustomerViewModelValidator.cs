﻿using System;
using System.Text.RegularExpressions;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Util;
using FluentValidation;

namespace API.Models.Customer
{
    /// <summary>
    /// Valida PreCadastroViewModel
    /// </summary>
    public class PreCadastroCustomerViewModelValidator : AbstractValidator<PreCadastroCustomerViewModel>
    {
        private readonly IServiceUser _serviceUser;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceuser"></param>
        public PreCadastroCustomerViewModelValidator(IServiceUser serviceuser)
        {
            _serviceUser = serviceuser;

            RuleFor(preCadastro => preCadastro.Phone).NotEmpty().Must(PhoneMatch).WithMessage("Telefone em formato inválido");
            RuleFor(preCadastro => preCadastro.Name).NotEmpty().WithMessage("Seu Nome é obrigatório.");
            RuleFor(preCadastro => preCadastro.Trade).NotEmpty().WithMessage("Nome da empresa é obrigatorio.");
            RuleFor(preCadastro => preCadastro.Email).EmailAddress().WithMessage("Email inválido.");
            RuleFor(preCadastro => preCadastro.PlanType).IsInEnum().WithMessage("Plano inválido.");
            RuleFor(preCadastro => preCadastro.Email).Must(EmailnotExist).WithMessage("Email já cadastrado");
        }

        private bool PhoneMatch(string phone)
        {
            return phone.Length.IsBetweenII(10,11);
        }

        private bool EmailnotExist(string email)
        {
            var userExist = _serviceUser.GetByExpression(x => x.Email == email);

            return userExist == null;
        }
    }
}
