﻿using System;
using Backend.Crowd.Domain.Interfaces.Services;
using FluentValidation;
using Backend.Crowd.Domain.Util;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;

namespace API.Models.Customer
{
    /// <summary>
    /// Valida PreCadastroViewModel
    /// </summary>
    public class CustomerProspectValidator : AbstractValidator<Backend.Crowd.Domain.Entities.Customer>
    {
        private readonly IServiceBase<Backend.Crowd.Domain.Entities.Customer> _servicecustomer;

        public CustomerProspectValidator(IServiceBase<Backend.Crowd.Domain.Entities.Customer> servicecustomer)
        {
            _servicecustomer = servicecustomer;

            RuleFor(customer => customer.Name).NotEmpty().WithMessage("Name,Razão social é obrigatória.");
            RuleFor(customer => customer.Trade).NotEmpty().WithMessage("Trade,Nome da empresa é obrigatório.");
            RuleFor(customer => customer.Address).NotEmpty().WithMessage("Address,Endereço é obrigatório.");
            RuleFor(customer => customer.CEP).NotEmpty().WithMessage("CEP,CEP é obrigatório.");
            RuleFor(customer => customer.City).NotEmpty().WithMessage("City,Cidade é obrigatória.");
            RuleFor(customer => customer.State).NotEmpty().WithMessage("State,Estado é obrigatório.");
            RuleFor(customer => customer.CNPJ).NotEmpty().WithMessage("CNPJ,CNPJ é obrigatório.");
            RuleFor(customer => customer.EmailResponsible).NotEmpty().WithMessage("EmailResponsible,Email é obrigatório.");
            RuleFor(customer => customer.NameResponsible).NotEmpty().WithMessage("NameResponsible,Nome do responsável é obrigatório.");
            RuleFor(customer => customer.Number).NotEmpty().WithMessage("Number,Número é obrigatório.");
            RuleFor(customer => customer.Phone).NotEmpty().WithMessage("Phone,Telefone é obrigatório.");
            //RuleFor(customer => customer.InscricaoEstadual).NotEmpty().WithMessage("InscricaoEstadualInscrição estadual é obrigatória.");
            //RuleFor(customer => customer.InscricaoMunicipal).NotEmpty().WithMessage("InscricaoMunicipal,Inscrição municipal é obrigatória.");
            RuleFor(customer => customer.Logo).NotEmpty().WithMessage("Logo,Logo da empresa é obrigatório.");

            RuleFor(customer => customer).Must(CNPJnotExist).WithMessage("CNPJ,CNPJ já cadastrado");
            RuleFor(customer => customer.CNPJ).Must(CNPJValid).WithMessage("CNPJ,CNPJ inválido");
            //RuleFor(customer => customer).Must(PaymentValid).WithMessage("Payment, Não há pagamento valido ou trial habilitado");
        }

        public CustomerProspectValidator()
        {
        }

        public bool PaymentValid(Backend.Crowd.Domain.Entities.Customer customer)
        {
            if (customer.Payment == null)
                return false;

            if (customer.Trial ?? false && (customer.TrialDate > DateTime.Today))
                return true;

            if (customer.Payment.Expires < DateTime.Today)
                return false;

            return (customer.Payment.Status == CustomerPaymentStatus.paid);
        }

        private bool CNPJValid(string cnpj)
        {
            return Validador.isCPFCNPJ(cnpj, false);
        }

        private bool CNPJnotExist(Backend.Crowd.Domain.Entities.Customer customer)
        {
            var cnpjExist = _servicecustomer.GetByExpression(x => x.CNPJ == customer.CNPJ && x.Id != customer.Id);

            return cnpjExist == null;
        }
    }
}
