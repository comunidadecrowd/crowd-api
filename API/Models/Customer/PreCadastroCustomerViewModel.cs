﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Customer
{
    /// <summary>
    /// ViewModel do endpoint PreCadastro
    /// </summary>
    public class PreCadastroCustomerViewModel
    {
        public string Trade { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public PlanType PlanType { get; set; }
        public string Referrer { get; set; }
        public string PromoCode { get; set; }
        public bool AcceptTerms { get; set; }
    }
}
