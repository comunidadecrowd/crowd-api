﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Backend.Crowd.Domain.Entities.Enum;

namespace API.Models.Portfolio
{
    public class PortfolioGaleriaViewModel
    {
        public FileType Type { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }
        public int StartSlide { get; set; }
    }
}