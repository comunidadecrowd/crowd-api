﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Portfolio
{
    public class PortfolioViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public FileType Type { get; set; }
        public string URL { get; set; }
        public string ClientName { get; set; }
        public string Media { get; set; }
        public string Thumb { get; set; }

        public string PortfolioURL { get; set; }

        public int FreelancerId { get; set; }
    }
}