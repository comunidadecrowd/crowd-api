﻿using Backend.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Backend.Crowd.Domain.Entities;
using System.IO;

namespace API.Models.Portfolio
{
    public class PortfolioOnLineViewModel
    {
        public string AmpHtml { get; set; }
        public string Canonical { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }
        public int Quality { get; set; }
        public int Responsability { get; set; }
        public int Rating { get; set; }
        public int Agility { get; set; }
        public string QualityStar { get; set; }
        public string ResponsabilityStar { get; set; }
        public string RatingStar { get; set; }
        public string AgilityStar { get; set; }
        public string Category { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Availability { get; set; }
        public string Price { get; set; }
        public IEnumerable<PortfolioExperienceViewModel> Experiences { get; set; }
        public IEnumerable<string> RatingsPhoto { get; private set; }
        public IEnumerable<PortfolioRatingsViewModel> Ratings { get; set; }
        public string FacebookUrl { get; private set; }
        public string TwitterUrl { get; private set; }
        public string DribbbleUrl { get; private set; }
        public string InstagramUrl { get; private set; }
        public string LinkedinUrl { get; private set; }
        public string BehanceUrl { get; private set; }
        public string GithubUrl { get; private set; }
        public IEnumerable<string> Skills { get; set; }
        public IEnumerable<string> Segments { get; set; }
        public IEnumerable<string> Languages { get; set; }


        public PortfolioOnLineViewModel(Backend.Crowd.Domain.Entities.Freelancer freelancer,string amphtml,string canonical,string baseUrl)
        {
            AmpHtml = amphtml;
            Canonical = canonical;
            Name = freelancer.Name;
            Title = freelancer.Title;
            Description = freelancer.Description;
            Email = freelancer.Email;
            Photo = String.Concat(baseUrl, freelancer.Users.Any() ? freelancer.Users.First().Photo : "");

            var ratings = freelancer.Ratings.Where(x => x.Active == true);
            var ratingsCount = ratings.Count();

            var QualityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Quality).Sum() / ratingsCount);
            var ResponsabilityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Responsability).Sum() / ratingsCount);
            var AgilityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Agility).Sum() / ratingsCount);
            var FullRating = ratingsCount == 0 ? 0 : Convert.ToInt32((QualityRating + ResponsabilityRating + AgilityRating) / 3);

            QualityStar = new String('★', Convert.ToInt32(QualityRating));
            ResponsabilityStar = new String('★', Convert.ToInt32(ResponsabilityRating));
            AgilityStar = new String('★', Convert.ToInt32(AgilityRating));
            RatingStar = new String('★', Convert.ToInt32(FullRating));
            Quality = Convert.ToInt32(QualityRating);
            Responsability = Convert.ToInt32(ResponsabilityRating);
            Agility = Convert.ToInt32(AgilityRating);
            Rating = Convert.ToInt32(FullRating);
            Category = freelancer.Categorys.Name;
            Skills = freelancer.Skills.Select(s => s.Name);
            Segments = freelancer.Segments.Select(s => s.Name);
            Languages = freelancer.Languages.Select(s => s.Value);
            State = freelancer.State != null ? freelancer.State.UF : string.Empty;
            City = freelancer.City != null ? freelancer.City.Name : string.Empty;
            Availability = freelancer.Availability.ToString();
            Price = freelancer.Price.ToString("G");
            Experiences = freelancer.Experiences.OrderByDescending(e => e.Id).Select(s => new PortfolioExperienceViewModel
            {
                Company = s.Company,
                Role = s.Role,
                StartDate = String.Format("{0:s}", s.StartDate),
                EndDate = String.Format("{0:s}", s.EndDate),
                Duration = String.Format("{0} - {1}", String.Format("{0:s}", s.StartDate), String.Format("{0:s}", s.EndDate)),
                Description = s.Description
            });
            if (freelancer.Ratings.Any())
            {
                RatingsPhoto = ratings.Select(s => s.User.Customer.Logo != null ? String.Concat(baseUrl, s.User.Customer.Logo) : String.Empty).Distinct();
                
                Ratings = ratings.Select(s => new PortfolioRatingsViewModel
                {
                    RatingStar = new String('★', Convert.ToInt32(FullRating)),
                    Rating = Convert.ToInt32(FullRating),
                    Logo = s.User.Customer.Logo != null ? String.Concat(baseUrl, s.User.Customer.Logo) : String.Empty,
                    TaskDate = s.Task != null ? s.Task.CreatedAt.ToString("MMM/yy") : String.Empty,
                    TaskTitle = s.Task != null ? s.Task.Title : String.Empty,
                    Photo = String.Concat(baseUrl, s.User.Photo),
                    Name = s.User.Name,
                    Company = s.User.Customer.Trade,
                    Description = s.Comment
                });
            }
            FacebookUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Facebook) != null
            ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Facebook).URL : String.Empty;
            TwitterUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Twitter) != null
            ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Twitter).URL : String.Empty;
            DribbbleUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Dribbble) != null
            ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Dribbble).URL : String.Empty;
            InstagramUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Instagram) != null
            ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Instagram).URL : String.Empty;
            LinkedinUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Linkedin) != null
            ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Linkedin).URL : String.Empty;
            BehanceUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Behance) != null
            ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Behance).URL : String.Empty;
            GithubUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Github) != null
            ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Github).URL : String.Empty;
        }

        private int GetRating(Backend.Crowd.Domain.Entities.Freelancer freelancer)
        {
            var ratings = freelancer.Ratings.Where(x => x.Active == true);
            var ratingsCount = ratings.Count();

            var QualityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Quality).Sum() / ratingsCount);
            var ResponsabilityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Responsability).Sum() / ratingsCount);
            var AgilityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Agility).Sum() / ratingsCount);
            return ratingsCount == 0 ? 0 : Convert.ToInt32((QualityRating + ResponsabilityRating + AgilityRating) / 3);

        }

        

        public string FirstName { get
            {
                return Name.IndexOf(" ") > -1
                  ? Name.Substring(0, Name.IndexOf(" "))
                  : Name;
            }
        

        }
    }

    public class PortfolioRatingsViewModel
    {
        public string Logo { get; internal set; }
        public string TaskDate { get; internal set; }
        public string TaskTitle { get; internal set; }
        public string Name { get; internal set; }
        public string Company { get; internal set; }
        public string Description { get; internal set; }
    public string Photo { get; internal set; }
        public int Rating { get; internal set; }
        public string RatingStar { get; internal set; }
    }

    public class PortfolioExperienceViewModel
    {
        public string Role { get; set; }
        public string Company { get; set; }
        public string Duration { get; set; }
        public string Description { get; set; }
        public String StartDate { get; internal set; }
        public String EndDate { get; internal set; }
    }
}