﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Backend.Crowd.Domain.Entities.Enum;
using CorePayment;

namespace API.Models.Payment
{
    public class PaymentTriggerViewModel
    {
        public string @event { get; set; }
        public string Id { get; set; }
        public string AccountId { get; set; }
        public string Status { get; set; }
        public string SubscriptionId { get; set; }

    }
}