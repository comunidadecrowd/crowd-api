﻿using System;
using Backend.Crowd.Domain.Interfaces.Services;
using FluentValidation;

namespace API.Models.Payment
{
    /// <summary>
    /// Valida PreCadastroFreelancerViewModel
    /// </summary>
    public class PaymentCustomerValidator : AbstractValidator<Backend.Crowd.Domain.Entities.Customer>
    {
        public PaymentCustomerValidator()
        {
            RuleFor(preCadastro => preCadastro.Name).NotEmpty().WithMessage("Nome é obrigatório.");
            RuleFor(preCadastro => preCadastro.CNPJ).NotEmpty().WithMessage("CNPJ é obrigatório.");
            RuleFor(preCadastro => preCadastro.EmailResponsible).NotEmpty().WithMessage("Email é obrigatório.");
            RuleFor(preCadastro => preCadastro.Address).NotEmpty().WithMessage("Endereco é obrigatório.");
            RuleFor(preCadastro => preCadastro.CEP).NotEmpty().WithMessage("CEP é obrigatório.");
            RuleFor(preCadastro => preCadastro.Number).NotEmpty().WithMessage("Numero é obrigatório.");
        }
    }
}
