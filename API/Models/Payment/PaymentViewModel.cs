﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Backend.Crowd.Domain.Entities.Enum;
using CorePayment;

namespace API.Models.Payment
{
    public class PaymentViewModel
    {
        public int IdCustomer { get; set; }
        public int Plan { get; set; }
        public string creditCardToken { get; set; }
        public int PaymentType { get; set; }
    }
}