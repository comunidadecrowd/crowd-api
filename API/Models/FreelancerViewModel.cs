﻿using API.Models.Tasks;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Util;
using SolrNet.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using API.Models.Portfolio;

namespace API.Models
{
    public class FreelancerViewModel
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public int[] SegmentsIds { get; set; }
        public int[] SkillsIds { get; set; }
        public decimal Price { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string EmailSecondary { get; set; }
        public AvailabilityType Availability { get; set; }
        public string Phone { get; set; }
        public StateViewModel State { get; set; }
        public CityViewModel City { get; set; }
        public string Country { get; set; }
        public string Title { get; set; }
        public string Skype { get; set; }
        public string Portfolio { get; set; }

        public string CNPJ { get; set; }

        public string FacebookUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string DribbbleUrl { get; set; }
        public string InstagramUrl { get; set; }
        public string LinkedinUrl { get; set; }
        public string BehanceUrl { get; set; }
        public string GithubUrl { get; set; }
                

        public string Photo { get; set; }
        public string PhotoBase64 { get; set; }
        public string PhotoExtension { get; set; }
        public int UserId { get; set; }
        public string Password { get; set; }

        public string Description { get; set; }

        public DateTime? Birthday { get; set; }

        public int CategoryId { get; set; }

        public string Referrer { get; set; }

        public CategoriesViewModel Category { get; set; }


        public List<SegmentsViewModel> Segments { get; set; }
        public List<SkillsViewModel> Skills { get; set; }

        public List<FreelancerExperienceViewModel> Experiences { get; set; }
        public List<FreelancerAwardsViewModel> Awards { get; set; }

        public List<string> Comments { get; set; }

        public double QualityRating { get; set; }
        public double ResponsabilityRating { get; set; }
        public double AgilityRating { get; set; }
        public double Rating { get; set; }

        public bool Active { get; set; }

        public string Languages { get; set; }
        public bool Available { get; set; }

        public bool Alocado { get; set; }

        public bool AcceptTerms { get; set; }
        public List<TasksViewModel> LastTasks { get; set; }
        public List<PortfolioGaleriaViewModel> GaleriaPortfolio { get; set; }

        public CoverViewModel Cover { get; set; }
        public string PortfolioOnline { get; set; }
    }
}