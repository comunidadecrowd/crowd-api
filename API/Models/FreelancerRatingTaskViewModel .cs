﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class FreelancerRatingTaskViewModel : RatingTaskViewModel
    {
        public string Portfolio { get; set; }

        public double Agility { get; set; }

        public int AgilityCount { get; set; }

        public double Responsability { get; set; }

        public int ResponsabilityCount { get; set; }

        public double Quality { get; set; }

        public int QualityCount { get; set; }

    }
}