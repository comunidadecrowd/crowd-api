﻿using Backend.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITracking
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="action"></param>
        /// <param name="actionArguments"></param>
        /// <returns></returns>
        List<Argument> Processor(string action, Dictionary<string, object> actionArguments);
    }
}
