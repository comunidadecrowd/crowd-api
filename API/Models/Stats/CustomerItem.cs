﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Stats
{
    public class CustomerItem
    {
        public int Id { get; set; }
        public string Trade { get; set; }
        public DateTime LastLogin { get; set; }
        public int QtyBriefings { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}