﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class CustomerRatingTaskViewModel : RatingTaskViewModel
    {
        public string Site { get; set; }

        public double Approval { get; set; }

        public int ApprovalCount { get; set; }

        public double Communicate { get; set; }

        public int CommunicateCount { get; set; }

        public double Professionalism { get; set; }

        public int ProfessionalismCount { get; set; }
    }
}