﻿namespace API.Models
{
    public class UserBriefingViewModel
    {
        public int Id { get; set; }
        public int? IdCustomer { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public string Photo { get; set; }

        public CustomerViewModel Customer { get; set; }

        public bool? Hunting { get; set; }
        public bool? Sended { get; set; }
    }
}