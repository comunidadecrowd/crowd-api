﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace API.Security
{
    public class SecurityConstants
    {
        public static readonly byte[] KeyForHmacSha256 = new byte[64];

        public static readonly string TokenIssuer = string.Empty;

        public static readonly string TokenAudience = string.Empty;

        public static readonly double TokenLifetimeMinutes = 720;

        static SecurityConstants()
        {
            RNGCryptoServiceProvider cryptoProvider = new RNGCryptoServiceProvider();
            cryptoProvider.GetNonZeroBytes(KeyForHmacSha256);

            TokenIssuer = "issuer";

            TokenAudience = "http://localhost:90";
        }
    }
}