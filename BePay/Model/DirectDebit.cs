﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BePay.Model
{
    public class DirectDebit
    {
        public string debtIdentifier { get; set; }
        public string accountingMethod { get; set; }
    }
}
