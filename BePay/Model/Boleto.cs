﻿namespace BePay.Model
{
    public class Boleto
    {
        public string bank { get; set; }
        public string shopperStatement { get; set; }
        public string accountingMethod { get; set; }
        public string dueDate { get; set; }
    }
}
