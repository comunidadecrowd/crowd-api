﻿namespace BePay.Model
{
    public class Sender
    {
        public Account account { get; set; }
        public Client client { get; set; }
    }
}
