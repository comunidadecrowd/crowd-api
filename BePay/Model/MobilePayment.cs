﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BePay.Model
{
    public class MobilePayment
    {
        public MobilePhone mobilePhone { get; set; }
        public TaxIdentifier taxIdentifier { get; set; }
        public string expiration { get; set; }
        public string recipientDescription { get; set; }
        public List<string> allowedPaymentTypes { get; set; }
    }
}
