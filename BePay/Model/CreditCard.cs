﻿namespace BePay.Model
{
    public class CreditCard
    {
        public string cardType { get; set; }
        public string cardNumber { get; set; }
        public string expirationMonth { get; set; }
        public string expirationYear { get; set; }
        public string cvv { get; set; }
        public string nameOnCard { get; set; }
        public HolderTaxId holderTaxId { get; set; }
        public string creditCardToken { get; set; }
        public int installment { get; set; }
        public string accountId { get; set; }
        public string accountHolderId { get; set; }
        public Address address { get; set; }
    }
}
