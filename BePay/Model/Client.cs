﻿using System.Collections.Generic;

namespace BePay.Model
{
    public class Client
    {
        public string status { get; set; }
        public string callbackSecretKey { get; set; }
        public string accountHolderId { get; set; }
        public TaxIdentifier taxIdentifier { get; set; }
        public List<MobilePhone> mobilePhones { get; set; }
        public MobilePhone mobilePhone { get; set; }
        public string email { get; set; }
        public Address mailAddress { get; set; }
        public string name { get; set; }
        public string representativeId { get; set; }
        public List<Representative> representatives { get; set; }
    }
}
