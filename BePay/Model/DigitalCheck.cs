﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BePay.Model
{
    public class DigitalCheck
    {
        public string document { get; set; }
        public string phoneNumber { get; set; }
        public string deviceUUID { get; set; }
        public int expectedValue { get; set; }
    }
}
