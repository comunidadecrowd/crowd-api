﻿namespace BePay.Model
{
    public class Order
    {
        public string orderId { get; set; }
        public string dateTime { get; set; }
        public string description { get; set; }
    }
}
