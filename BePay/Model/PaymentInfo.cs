﻿namespace BePay.Model
{
    public class PaymentInfo
    {
        public string transactionType { get; set; }
        public Address billingAddress { get; set; }
        public CreditCard creditCard { get; set; }
        public Boleto boleto { get; set; }
        public DirectDebit directDebit { get; set; }
        public MobilePayment mobilePayment { get; set; }
        public DigitalCheck digitalCheck { get; set; }
    }
}
