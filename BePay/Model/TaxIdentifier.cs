﻿namespace BePay.Model
{
    public class TaxIdentifier
    {
        public string taxId { get; set; }
        public string country { get; set; }
    }
}
