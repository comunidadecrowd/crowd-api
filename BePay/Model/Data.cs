﻿namespace BePay.Model
{
    public class Data
    {
        public string transactionId { get; set; }
        public string externalIdentifier { get; set; }
        public string senderAccountId { get; set; }
        public FinancialStatement financialStatement { get; set; }
    }
}
