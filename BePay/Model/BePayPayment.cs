﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BePay.Model
{

    public class BePayPayment
    {
        public string totalAmount { get; set; }
        public string currency { get; set; }
        public PaymentInfo paymentInfo { get; set; }
        public Sender sender { get; set; }
        public MyAccount myAccount { get; set; }
        public List<Recipient> recipients { get; set; }
        public Representative representative { get; set; }
        public string externalIdentifier { get; set; }
        public string callbackAddress { get; set; }
    }
}
