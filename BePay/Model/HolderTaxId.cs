﻿namespace BePay.Model
{
    public class HolderTaxId
    {
        public string taxId { get; set; }
        public string country { get; set; }
    }
}
