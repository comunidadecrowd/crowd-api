﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BePay.Model
{
    public class Error
    {
        public string code { get; set; }
        public string description { get; set; }
    }

    public class BePayError
    {
        public Error error { get; set; }
    }
}
