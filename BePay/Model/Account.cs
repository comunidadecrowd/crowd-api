﻿namespace BePay.Model
{
    public class Account
    {
        public string accountId { get; set; }
        public string account { get; set; }
        public string branch { get; set; }
        public MobilePhone mobilePhone { get; set; }
    }
}
