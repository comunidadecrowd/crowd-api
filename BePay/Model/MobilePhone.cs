﻿namespace BePay.Model
{
    public class MobilePhone
    {
        public string country { get; set; }
        public string phoneNumber { get; set; }
    }
}
