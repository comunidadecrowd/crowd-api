﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BePay.Model
{
    public class Representative
    {
        public string name { get; set; }
        public TaxIdentifier taxIdentifier { get; set; }
        public MobilePhone mobilePhone { get; set; }
        public string email { get; set; }
        public List<Document> documents { get; set; }
    }
}
