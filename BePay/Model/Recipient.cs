﻿namespace BePay.Model
{
    public class Recipient
    {
        public Account account { get; set; }
        public Order order { get; set; }
        public string amount { get; set; }
        public string mediatorFee { get; set; }
        public string currency { get; set; }
        public string senderComment { get; set; }
        public string recipientComment { get; set; }
        public string historyCodeSender { get; set; }
        public string historyCodeRecipient { get; set; }
        public string historyCodeFeeRecipient { get; set; }
        public string historyCodeFeeMediator { get; set; }
    }
}
