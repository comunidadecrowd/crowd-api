﻿using BePay.Model;

namespace BePay
{
    public interface IPayment
    {
        ResponseBePay Card(string hash,BePayPayment model);
        ResponseBePay Boleto(string hash, BePayPayment model);
    }
}