﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BePay.Model;
using RestSharp;
using Newtonsoft.Json;
using System.Net;
using System.Runtime.Serialization;

namespace BePay
{
    public class ServiceBePay : IPayment
    {
        private readonly RestClient _client;
        private readonly string _apiKey;

        public ServiceBePay(string apikey,string url)
        {
            var uri = new Uri(url);
            _client = new RestClient(uri);
            _apiKey = apikey;
        }

        public ResponseBePay Card(string hash,BePayPayment model)
        {
            IRestResponse response = Request(hash, model);

            if (response.StatusCode != HttpStatusCode.OK)
                Error(response);

            return JsonConvert.DeserializeObject<ResponseBePay>(response.Content);
        }

        public ResponseBePay Boleto(string hash, BePayPayment model)
        {
            IRestResponse response = Request(hash, model);

            if (response.StatusCode != HttpStatusCode.OK)
                Error(response);

            return JsonConvert.DeserializeObject<ResponseBePay>(response.Content);
        }

        private void Error(IRestResponse response)
        {
            var errorresponse = JsonConvert.DeserializeObject<BePayError>(response.Content);

            throw new BePayException(response.StatusCode,errorresponse);
        }

        private IRestResponse Request(string hash, BePayPayment model)
        {
            var request = new RestRequest("/v1/payments", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Transaction-Hash", hash);
            request.AddHeader("Api-Access-Key", _apiKey);
            request.AddBody(model);

            return _client.Execute(request);
        }

        

        

        
    }

    [Serializable]
    public class BePayException : Exception
    {
        private HttpStatusCode statusCode;
        private BePayError errorresponse;

        public BePayException()
        {
        }

        public BePayException(string message) : base(message)
        {
        }

        public BePayException(HttpStatusCode statusCode, BePayError errorresponse)
        {
            this.statusCode = statusCode;
            this.errorresponse = errorresponse;
        }

        public BePayException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BePayException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public HttpStatusCode StatusCode { get => statusCode; set => statusCode = value; }
        public BePayError ErrorResponse { get => errorresponse; set => errorresponse = value; }
    }
}
