﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Algolia.API;
using Autofac;
using Autofac.Core;
using Backend.Crowd.Domain.Interfaces.Repositories;
using Backend.Crowd.Domain.Interfaces.Services;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Infrasctructure.Database.Context;
using Backend.Crowd.Infrasctructure.Database.Repositories;
using NDesk.Options.Fork;

namespace ExportSearch
{
    class Program
    {
        private static List<Parameter> _searchParams;

        private static OptionSet Options = new OptionSet
        {
            { "di|datainicial=", "Periodo Inicial", v=> DataConvert("startDate",v)},
            { "df|datafinal=","Periodo Final", v => DataConvert("finishDate",v)}
        };

        private static void DataConvert(string name, string data)
        {
            DateTime returndata;
            if (DateTime.TryParse(data, out returndata))
              _searchParams.Add(new NamedParameter(name,returndata));
        }

        static void Main(string[] args)
        {
            ParseConsole(args);
            var container = ContainerConfig.Configure();
            using (var scope = container.BeginLifetimeScope())
            {
                var app = scope.Resolve<IApplication>(_searchParams);
                app.Run();
            }
        }

        private static void ParseConsole(string[] args)
        {
            try
            {
                _searchParams = new List<Parameter>();

                var unexpected = Options.Parse(args);

                if (_searchParams.Cast<NamedParameter>().All(p => p.Name != "startDate"))
                    _searchParams.Add(new NamedParameter("startDate", DateTime.MinValue));

                if (_searchParams.Cast<NamedParameter>().All(p => p.Name != "finishDate"))
                    _searchParams.Add(new NamedParameter("finishDate", DateTime.MaxValue));

                foreach (var arg in unexpected)
                {
                    Console.WriteLine($"Unknown argument: {arg}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static class ContainerConfig
        {
            public static IContainer Configure()
            {
                var builder = new ContainerBuilder();

                List<Parameter> parameters =
                    new List<Parameter>
                    {
                        new NamedParameter("appid", ConfigurationManager.AppSettings["AlgoliaId"]),
                        new NamedParameter("apikey", ConfigurationManager.AppSettings["AlgoliaApiKey"]),
                        new NamedParameter("indexname", ConfigurationManager.AppSettings["AlgoliaIndex"])
                    };

                builder.RegisterType<CrowdContext>();
                builder.RegisterGeneric(typeof(RepositoryBase<>)).As(typeof(IRepositoryBase<>));
                builder.RegisterGeneric(typeof(ServiceBase<>)).As(typeof(IServiceBase<>));
                builder.RegisterType<ServiceFreelancer>().As<IServiceFreelancer>();
                builder.RegisterType<ExportApplication>().As<IApplication>();
                builder.RegisterType<AlgoliaSearch>().As<ISearch>().WithParameters(parameters);
                
                // add other registrations here...

                return builder.Build();
            }
        }
    }
}
