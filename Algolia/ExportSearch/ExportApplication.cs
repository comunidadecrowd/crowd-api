﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algolia.API;
using API.Models;
using Autofac.Core;
using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Interfaces.Services;
using Newtonsoft.Json.Linq;

namespace ExportSearch
{
    public interface IApplication
    {
        void Run();
    }

    public class ExportApplication : IApplication
    {
        private readonly ISearch _search;
        private readonly IServiceFreelancer _serviceFreelancer;
        private IServiceBase<Tasks> _serviceTasks;
        private readonly DateTime _startDate;
        private readonly DateTime _finishDate;

        public ExportApplication(ISearch search, IServiceFreelancer serviceFreelancer, IServiceBase<Tasks> serviceTasks, DateTime startDate, DateTime finishDate)
        {

            _search = search;
            _serviceFreelancer = serviceFreelancer;
            _serviceTasks = serviceTasks;
            _startDate = startDate;
            _finishDate = finishDate;
        }

        public void Run()
        {
            var freelancers = _serviceFreelancer.GetAll(f => f.Status == StatusUserType.Active
                 && (f.UpdatedAt.HasValue ? f.UpdatedAt.Value >= _startDate : f.CreatedAt >= _startDate)
                 && (f.UpdatedAt.HasValue ? f.UpdatedAt.Value <= _finishDate : f.CreatedAt <= _finishDate)
                 //&& (f.Users.Count != 0 && f.Users.FirstOrDefault().ConfirmEmail != null)
                 ).ToList();

            Console.WriteLine("Exportando {0} registros",freelancers.Count);
            List<IJsonParser> jsonpoco = new List<IJsonParser>();
            var reg = 1;
            freelancers.ForEach(f =>
            {
                Console.Write("\rExportando Registro {0}", reg);
                jsonpoco.Add(new FreelancerPoco(f) {Tasks = _serviceTasks.GetAll(t => t.IdFreelancer == f.Id).Any()});
                reg++;
            });

            Console.Write("Pronto para subir");
            Console.ReadLine();
            _search.AddBatch(jsonpoco.ToList());

            _search.RunBatch();
        }
    }
}
