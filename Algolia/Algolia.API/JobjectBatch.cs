﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Algolia.API
{
    public class JobjectBatch
    {
        [JsonProperty("action",Order = 1)]
        public string Action { get; set; }

        [JsonProperty("indexName",Order = 2)]
        public string IndexName { get; set; }

        [JsonProperty("body", Order = 3)]
        public JObject Body { get; set; }
    }
}