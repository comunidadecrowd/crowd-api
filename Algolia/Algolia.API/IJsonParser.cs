﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Algolia.API
{
    public interface IJsonParser
    {
        [JsonProperty("objectID",Order=1)]
        string ObjectId { get; }
    
        JObject ToJsonBatch(string action,string indexname);
        JObject ToJson();
    }
}