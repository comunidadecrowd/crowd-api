﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Algolia.Search;
using Newtonsoft.Json.Linq;

namespace Algolia.API
{
    public class AlgoliaSearch : ISearch
    {
     
        private Index _index;
        private readonly AlgoliaClient _client;
        private readonly string _indexname;
        private List<JObject> _objs;

        public AlgoliaSearch(string appid, string apikey, string indexname)
        {
            _objs = new List<JObject>();
            _client = new AlgoliaClient(appid, apikey);
            _indexname = indexname;
            _index = _client.InitIndex(_indexname);
        }

        public void AddBatch(List<IJsonParser> obj)
        {
            obj.ForEach(o => _objs.Add(o.ToJsonBatch("addObject",_indexname)));
        }

        public void UpdateBatch(List<IJsonParser> obj)
        {
            obj.ForEach(o => _objs.Add(o.ToJsonBatch("updateObject", _indexname)));
        }

        public JObject RunBatch()
        {
            return _client.Batch(_objs);
        }

        public void Clear()
        {
            _index.ClearIndex();
        }

        public void Update(IJsonParser freelancer)
        {
            _index.SaveObject(freelancer.ToJson());
        }
    }
}