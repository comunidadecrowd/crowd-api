﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Algolia.API
{
    public interface ISearch
    {
        void AddBatch(List<IJsonParser> obj);
        void UpdateBatch(List<IJsonParser> obj);

        JObject RunBatch();
        void Clear();

        void Update(IJsonParser freelancerPoco);
    }
}
