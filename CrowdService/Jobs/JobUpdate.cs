﻿using Quartz;
using RestSharp;
using System.Configuration;
using System.Diagnostics;

namespace CrowdService
{
    internal class JobUpdate : IJob
    {
        const string sSource = "CrowdService";

        public void Execute(IJobExecutionContext context)
        {
            var apiaddress = ConfigurationManager.AppSettings["CrowdAPI"].ToString();
            var client = new RestClient(apiaddress);

            var request = new RestRequest("Professional/UpdateIndex", Method.POST);

            IRestResponse response = client.Execute(request);

            

            EventLog.WriteEntry(sSource, "Update Index Executado", EventLogEntryType.Information);

        }
    }
}