﻿using Dapper;
using Intercom;
using Quartz;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;

namespace CrowdService
{
    internal class JobTrial : IJob
    {
        const string sSource = "CrowdService";

        public void Execute(IJobExecutionContext context)
        {
            var intercom = new IntercomServer(ConfigurationManager.AppSettings["IntercomToken"].ToString());
            var connectionString = ConfigurationManager.ConnectionStrings["Connection"].ToString();
            using (var cn = new SqlConnection(connectionString))
            {
                var ids = cn.Query<int>(@"Select u.id from crowd_customers c 
                                            inner join crowd_users u on c.id = u.id_customer
                                            where trial = 1 and trial_at <= Getdate()+1");

                foreach (var id in ids)
                {
                    intercom.SendTrialEmail(id, DateTime.Now,true);
                }

                ids = cn.Query<int>(@"Select u.id from crowd_customers c 
                                        inner join crowd_users u on c.id = u.id_customer
                                        where c.trial = 0 and c.updated_at >= Getdate()-5");


                foreach (var id in ids)
                {
                    intercom.SendTrialEmail(id, DateTime.Now, false);
                }
            }

            EventLog.WriteEntry(sSource, "Email de Trial Executado", EventLogEntryType.Information);
        }
    }
}