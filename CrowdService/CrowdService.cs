﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;


namespace CrowdService
{
    public partial class CrowdService : ServiceBase
    {
        const string sSource = "CrowdService";
        const string sLog = "Application";

        public CrowdService()
        {
            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            InitializeComponent();
        }

        internal void Init()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail jobUpdateIndex = JobBuilder.Create<JobUpdate>().WithIdentity("jobupdate").Build();

            int updatetime = 0;
            Int32.TryParse(ConfigurationManager.AppSettings["UpdateTime"], out updatetime);

            ITrigger triggerUpdate = TriggerBuilder.Create().WithIdentity("triggeupdate").StartNow()
                   .WithSimpleSchedule(a => a.WithIntervalInMinutes(updatetime).RepeatForever())
                   .Build();

            IJobDetail jobTrial = JobBuilder.Create<JobTrial>().WithIdentity("jobtrial").Build();

            ITrigger triggerTrial = TriggerBuilder.Create().WithIdentity("triggetrial")
                                             .StartNow()//.WithCronSchedule("0 0 1 * * ?")
                                             .Build();

            scheduler.ScheduleJob(jobUpdateIndex, triggerUpdate);
            scheduler.ScheduleJob(jobTrial, triggerTrial);
        }

        protected override void OnStart(string[] args)
        {
            Init();
        }

        protected override void OnStop()
        {
        }
    }
}
