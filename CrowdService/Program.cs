﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace CrowdService
{
    static class Program
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        static void Main()
        {
#if DEBUG
            CrowdService s = new CrowdService();
                        s.Init(); // Init() is pretty much any code you would have in OnStart().
#else
                ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new CrowdService()
            };
            ServiceBase.Run(ServicesToRun);
#endif

        }
    }
}
