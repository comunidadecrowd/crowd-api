﻿using System.Collections.Generic;
using System.Net;

namespace Iugu
{
    public  class IuguErrorMessage
    {
        public HttpStatusCode StatusCode { get; set; }
        public string ReasonPhase { get; set; }
        public IuguMessage Message { get; set; }
    }

    public class IuguMessage
    {
        public dynamic Errors { get; set; }
    }
}