﻿using CorePayment;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iugu.net;
using iugu.net.Request;
using iugu.net.Entity;
using CorePayment.Model;
using Newtonsoft.Json;
using iugu.net.Response;
using System.Net;

namespace Iugu
{
    public class IuguServer : IPayment
    {
        public string _apikey;

        public IuguServer(string apikey)
        {
            _apikey = apikey;
        }

        public ReturnPayment Send(SendPayment pay)
        {
            var client = new CustomerModel { id = pay.Customer.Token };

            if (String.IsNullOrEmpty(pay.Customer.Token))
                client = SendClient(pay.Customer);

            var paymentmethod = new PaymentMethodModel();

            try
            {
                if (pay.PaymentType == PaymentType.CREDITCARD)
                    paymentmethod = SendPaymentMethod(client.id, pay.CreditCardToken, pay.PaymentType.ToString());
            }
            catch (Exception ex)
            {
                var message = ErrorResponseMessage.BuildAsync(ex.InnerException.Message).Result;

                return new ReturnPayment
                {
                    CustomerId = client.id,
                    Status = PaymentStatus.Error,
                    StatusCode = HttpStatusCode.BadRequest,
                    ErrorCode = message.StatusCode,
                    ErrorMessage = message.Errors.Aggregate("", (a, v) => a += String.Join(",", v.Errors))
                };
            }

            var subscription = new SubscriptionModel();

            try
            {
                if (String.IsNullOrEmpty(pay.Customer.Subscription))
                    subscription = SendSubscription(client.id, pay.Plan, pay.PaymentType);
                else
                {
                    var oldsubscription = GetSubscription(client.id);

                    if (oldsubscription.plan_identifier != pay.Plan)
                        SendChangePlan(client.id, pay.Plan);
                    else
                    return new ReturnPayment
                    {
                        CustomerId = client.id,
                        Status = PaymentStatus.Error,
                        StatusCode = HttpStatusCode.Conflict,
                        ErrorCode = 6444,
                        ErrorMessage = "Assinatura já existente"
                    };
                }
            }
            catch (Exception ex)
            {
                var message = ErrorResponseMessage.BuildAsync(ex.InnerException.Message).Result;              return new ReturnPayment
                {
                    CustomerId = client.id,
                    Status = PaymentStatus.Error,
                    StatusCode = HttpStatusCode.BadRequest,
                    ErrorCode = message.StatusCode,
                    ErrorMessage = message.Errors.Aggregate("", (a, v) => a += String.Join(",",v.Errors))
                };
            }

            if (pay.PaymentType == PaymentType.CREDITCARD && subscription.recent_invoices.Any() && subscription.recent_invoices.OrderByDescending(i => i.CreatedAt).First().Status != "paid")
                return new ReturnPayment
                {
                    CustomerId = client.id,
                    SubscriptionId = subscription.id,
                    Status = PaymentStatus.Suspended,
                    StatusCode = HttpStatusCode.PaymentRequired,
                    ErrorCode = 6402,
                    ErrorMessage = "Cartão Inválido"
                };

            return new ReturnPayment
            {
                CustomerId = client.id,
                SubscriptionId = subscription.id,
                Status = PaymentStatus.Ok
            };  
        }

        private SubscriptionModel SendChangePlan(string customerId, string planId)
        {
            var result = new SubscriptionModel();
            using (var apiClient = new iugu.net.Lib.Subscription())
            {
                var async = apiClient.ChangePlanAsync(customerId,planId);

                async.Wait();

                result = async.Result;
            };

            return result;
        }

        private SubscriptionModel SendSubscription(string customerId,string planId,PaymentType payment)
        {
            var subitem = new List<SubscriptionSubitem>();
            
            SubscriptionRequestMessage request = new SubscriptionRequestMessage(customerId)
            {
                IsCreditBased = false,
                PlanId = planId,
                PayableWith = (payment == PaymentType.CREDITCARD) ? "credit_card" : "bank_slip"
            };

            var result = new SubscriptionModel();
            using (var apiClient = new iugu.net.Lib.Subscription())
            {     
                var async = apiClient.CreateAsync(request);

                async.Wait();

                result = async.Result;
            };

            return result;
        }

        public SubscriptionModel GetSubscription(string customerId)
        {
            var result = new SubscriptionModel();
            using (var apiClient = new iugu.net.Lib.Subscription())
            {
                var async = apiClient.GetAsync<SubscriptionModel>(customerId);

                async.Wait();

                result = async.Result;
            };

            return result;
        }

        private CustomerModel SendClient(CustomerPayment customer)
        {
            var _customer = new CustomerRequestMessage
            {
                Name = customer.Name,
                Email = customer.Email,
                CpfOrCnpj = customer.CNPJ,
                ZipCode = customer.CEP,
                Number = Int32.Parse(customer.Number),
                Complement = customer.Complement
            };

            var result = new CustomerModel();
            using (var apiClient = new iugu.net.Lib.Customer())
            {
                var async = apiClient.CreateAsync(_customer, _apikey);

                async.Wait();

                result = async.Result;
            };

            return result;
        }

        private PaymentMethodModel SendPaymentMethod(string customerId,string token,string description)
        {
            var result = new PaymentMethodModel();
            using (var apiClient = new iugu.net.Lib.PaymentMethod(customerId))
            {
                try
                {
                    var async2 = apiClient.GetAsync(Constants.PaymentMethod.CREDIT_CARD);

                    async2.Wait();

                    result = async2.Result;
                }
                catch
                {
                    var async = apiClient.CreateAsync(description, null, null, token);

                    async.Wait();

                    result = async.Result;
                }
            };

            return result;
        }
        
    }
}
