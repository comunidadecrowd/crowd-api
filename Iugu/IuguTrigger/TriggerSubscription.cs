﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iugu.IuguTrigger
{
    public class TriggerSubscription : TriggerBase
    {
        public TriggerSubscription(NameValueCollection formBody) : base(formBody)
        {
            Id = formBody["data[id]"].ToString();
        }
    }
}
