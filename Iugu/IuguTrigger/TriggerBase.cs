﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Entities.Enum;
using Backend.Crowd.Domain.Util.Network;
using CorePayment;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iugu.IuguTrigger
{
    public class TriggerBase : ITrigger
    {
        public string Id { get; set; }
        private IuguServer _server;
        private IEmail _email;
        
        public TriggerBase(NameValueCollection formBody)
        {
            _server = new IuguServer(ConfigurationManager.AppSettings["PaymentApiKey"]);
            _email = new Email();
        }

        public void Execute(Customer customer)
        {
            var sub = _server.GetSubscription(Id);

            customer.Payment = new Customer.PaymentCustomer
            {
                Active = sub.active,
                CreatedAt = DateTime.Parse(sub.created_at),
                CustomerId = customer.Id,
                Expires = DateTime.Parse(sub.expires_at.ToString()),
                SubscriptionId = sub.id,
                Status = sub.recent_invoices.Any() ? (CustomerPaymentStatus)Enum.Parse(typeof(CustomerPaymentStatus), sub.recent_invoices.OrderByDescending(i => i.CreatedAt).First().Status) : CustomerPaymentStatus.pending,
                UpdatedAt = DateTime.Parse(sub.updated_at),
                Invoices = new List<Customer.CustomerInvoices>(),
                Logs = new List<Customer.CustomerPaymentLog>()
            };

            sub.recent_invoices.ForEach(i=> customer.Payment.Invoices.Add(new Customer.CustomerInvoices{
                DueDate = DateTime.Parse(i.DueDate),
                Id = i.ID,
                Status = (CustomerPaymentStatus)Enum.Parse(typeof(CustomerPaymentStatus), i.Status),
                SubscriptionId = sub.id,
                Total = Convert.ToDecimal(i.Total.Replace("R$ ","").Replace(" BRL",""))
            }));

            sub.logs.ForEach(l => customer.Payment.Logs.Add(new Customer.CustomerPaymentLog
            {
               CreatedAt = DateTime.Parse(l.created_at),
               Description = l.description,
               SubscriptionId =sub.id,
               Notes = l.notes,
               Id = l.id
            }));
        }
    }
}
