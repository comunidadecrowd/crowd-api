﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iugu.IuguTrigger
{
    public class TriggerInvoice : TriggerBase
    {
        public TriggerInvoice(NameValueCollection formBody) : base(formBody)
        {
            Id = formBody["data[subscription_id]"].ToString();
        }
    }
    
}
