﻿using CorePayment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using Iugu.IuguTrigger;

namespace Iugu
{
    public class TriggerPaymentFactory
    {
        public static ITrigger Instance(NameValueCollection formBody)
        {
            ITrigger retorno = null;

            switch (formBody["event"])
            {
                case "invoice.created" :
                case "invoice.status_changed" :
                case "invoice.refund":
                case "invoice.payment_failed":
                case "invoice.dunning_action":
                case "invoice.due":
                case "invoice.installment_released":
                case "invoice.released":
                    retorno = new TriggerInvoice(formBody); 
                    break;
                case "subscription.suspended":
                case "subscription.activated":
                case "subscription.created":
                case "subscription.renewed":
                case "subscription.expired":
                case "subscription.changed":
                    retorno = new TriggerSubscription(formBody);
                    break;
            }

            return retorno;
        }
    }
}
