﻿using Dapper;
using Intercom;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Intercom.Data;

namespace ServiceIntercom
{
    class Program
    {
        private static IntercomServer intercom;
        private static string connectionString;
        private static string photopath;
        private static Dictionary<String, object> custom_attributes_user;
        private static Dictionary<String, object> custom_attributes_company;

        static void Main(string[] args)
        {
            intercom = new IntercomServer(ConfigurationManager.AppSettings["IntercomToken"].ToString());

            connectionString = ConfigurationManager.ConnectionStrings["Connection"].ToString();

            photopath = ConfigurationManager.AppSettings["PhotoPath"].ToString();

            custom_attributes_user = new Dictionary<string, object> {
                { "hash", null },
                { "idFreelancer", null},
                { "idCustomer", null },
                { "defaultURL", null },
                { "contratou_task", null },
                { "publicou_task", null },
                { "aprovou_entrega", null },
                { "contratou", null },
                { "job_title", null },
                { "trial-expire", null },
                { "trial", null },
                { "enviou briefing", null },
                { "search", null },
                { "updated_at", null },
                { "roleCompany", null },
                { "acceptTerms", null },
                { "role", null }
            };

            custom_attributes_company = new Dictionary<string, object> {
                { "email", null },
                { "responsible", null},
                { "cont_users", null },
                { "trial-expire", null },
                { "trial", null },
                { "creation_source", null },
                { "Publicou_orçamento", null },
                { "Contratou", null },
                { "Search", null },
                { "responsibleId", null },
                { "last_change_plan", null },
                { "updated_at", null },
                { "session_count", null },
                { "user_count", null },
                { "type", null }
            };

            //ClearLead();

            ClearUser();

            //ClearCompany();
        }

        private static void ClearUser()
        {
            var scroll = intercom.ListUsers();

            for (int page = 1; page <= scroll.pages.total_pages; page++)
            {
                var users = intercom.ListUsers(new Dictionary<string, string>() { { "page", page.ToString() } });

                foreach (var user in users.users)
                {
                    if (user.last_request_at != null)
                    {
                        if (DateTimeOffset.FromUnixTimeSeconds(user.last_request_at.Value).LocalDateTime < DateTime.Now.AddMonths(-3).AddDays(-2))
                          intercom.DeleteUser(user.id);
                        else
                            UpdateUser(user);
                    }  
                }
            }
        }

        private static void ClearCompany()
        {
            var scroll = intercom.ListCompanies();

            for (int page = 1; page <= scroll.pages.total_pages; page++)
            {
                var companies = intercom.ListCompanies(new Dictionary<string, string>() { { "page", page.ToString() } });

                foreach (var company in companies.companies)
                {
                    UpdateCompany(company);
                }
            }
        }

        private static void UpdateUser(User iuser)
        {
            using (var cn = new SqlConnection(connectionString))
            {
                var email = iuser.email;

                dynamic user = cn.QueryFirstOrDefault(@"Select 
                                            u.*,f.title,c.trial,c.trial_at,
                                            (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = u.id_customer and t.status > 1 and t.active = 1 and t.id_user = u.id) as contratou,
	                                        (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = u.id_customer and t.status > 1 and t.active = 1 and t.id_user = u.id and t.id_briefing is null) as contratou_task,
                                            (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = u.id_customer and t.active = 1 and t.id_user = u.id and t.id_briefing is null) as publicou_task,
                                            (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = u.id_customer and t.status > 3 and t.active = 1 and t.id_user = u.id) as aprovou_entrega,
                                            (Select Count(*) from crowd_briefings b inner join crowd_users ub on b.id_user = ub.id where ub.id = u.id and b.active = 1 ) as contbriefings,
                                            (Select Count(*) from crowd_users_search s where s.user_id = u.id) as contsearch
                                            from crowd_users u
                                            left join crowd_freelancer f on u.id_freelancer = f.id
                                            left join crowd_customers c on u.id_customer = c.id
                                            where u.email = @email", new { email });

                if (user == null || user.id == 0 || user.id == null)
                {
                    intercom.DeleteUser(iuser);
                    return;
                }

                if (iuser.social_profiles.Any())
                    UpdateSocial(user.id, iuser.social_profiles);

                iuser.avatar.image_url = String.Concat(photopath, user.photo);
                iuser.name = user.name;
                iuser.phone = user.phone;
                iuser.user_id = user.id.ToString();
                iuser.custom_attributes = UpdateAttr(user, iuser.custom_attributes);

                intercom.UpdateUser(iuser);
            }
        }

        private static void UpdateSocial(int user_id, List<SocialProfile> social_profiles)
        {
            var list = new List<Social>();

            social_profiles.ForEach(sp => list.Add(new Social(user_id,sp)));
            
            using (var cn = new SqlConnection(connectionString))
            {
                cn.Execute(@"
                           INSERT INTO crowd_freelancer_social
                            (
                            user_id,
                            type,
                            url
                            )
                            VALUES
                            (@UserId, 
                            @Type,
                            @Url
                            )"
                            , list);
            }
        }

        private static void UpdateCompany(Company icompany)
        {
            using (var cn = new SqlConnection(connectionString))
            {
                var id = icompany.company_id;

                dynamic company = cn.QueryFirstOrDefault(@"Select 
	                                        c.id as Id,
	                                        plan_type as PlanId,
	                                        p.name as [Plan],
	                                        c.Trade,
	                                        c.email_responsible as EmailResponsible,
	                                        c.name_responsible as Responsible,
											u.id as ResponsibleId,
	                                        c.trial as Trial,
	                                        c.trial_at as TrialDate,
											c.last_change_plan,
											c.updated_at,
											(Select Count(*) from crowd_users_log l inner join crowd_users u on l.user_id = u.id where u.id_customer = c.id) as contSession,
											(Select Count(*) from crowd_users_search s inner join crowd_users u on s.user_id = u.id where u.id_customer = c.id) as search,
	                                        (Select Count(*) from crowd_users where id_customer = c.id) as contUsers,
	                                        (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = c.id) as contJobs,
	                                        (Select Count(*) from crowd_briefings b inner join crowd_projects p on b.id_project = p.id where p.id_customer = c.id) as contBriefings
                                        from crowd_customers c
                                        inner join crowd_customers_plan p on c.plan_type = p.id
										left join crowd_users u on u.email = c.email_responsible
                                        where c.active = 1 and c.id = @id", new { id });

                if (company == null || company.Id == 0 || company.Id == null)
                {
                    return;
                }

                icompany.plan = new Intercom.Data.Plan
                {
                    id = company.PlanId.ToString(),
                    name = company.Plan.ToString(),
                };
                icompany.name = company.Trade;

                icompany.custom_attributes = UpdateAttrCompany(company, icompany.custom_attributes);

                intercom.UpdateCompany(icompany);
            }
        }

        private static Dictionary<string, object> UpdateAttrCompany(dynamic company, Dictionary<string, object> custom_attributes)
        {
            var attributes = new Dictionary<string, object>(custom_attributes_company);

            if (company.EmailResponsible != null)
            {
                attributes["email"] = company.EmailResponsible.ToString();
                custom_attributes.Remove("email");
            }

            attributes["responsible"] = company.Responsible;
            custom_attributes.Remove("responsible");

            attributes["cont_users"] = company.contUsers;
            custom_attributes.Remove("cont_users");

            if (company.TrialDate != null)
            {
                attributes["trial-expire"] = company.TrialDate;
                custom_attributes.Remove("trial-expire");
            }
            else attributes.Remove("trial-expire");

            if (company.Trial != null)
            {
                attributes["trial"] = company.Trial;
                custom_attributes.Remove("trial");
            }
            else attributes.Remove("trial");

            attributes["creation_source"] = "api";

            attributes["Publicou_orçamento"] = company.contBriefings;
            custom_attributes.Remove("Publicou_orçamento");

            attributes["Contratou"] = company.contJobs;
            custom_attributes.Remove("Contratou");

            attributes["Search"] = company.search;
            custom_attributes.Remove("Search");

            attributes["responsibleId"] = company.ResponsibleId;
            custom_attributes.Remove("responsibleId");

            if (company.last_change_plan != null)
            {
                attributes["last_change_plan"] = company.last_change_plan;
                custom_attributes.Remove("last_change_plan");
            } else attributes.Remove("last_change_plan");

            if (company.updated_at != null)
            {
                attributes["updated_at"] = company.updated_at;
                custom_attributes.Remove("updated_at");
            }

            attributes["session_count"] = company.contSession;
            custom_attributes.Remove("session_count");

            attributes["user_count"] = company.contUsers;
            custom_attributes.Remove("user_count");

            attributes["type"] = "company";

            return attributes;
        }


        private static Dictionary<string, object> UpdateAttr(dynamic user, Dictionary<string, object> custom_attributes)
        {
            var attributes = new Dictionary<string,object>(custom_attributes_user);

            if (user.confirm_email != null)
            {
                attributes["hash"] = user.confirm_email.ToString();
                custom_attributes.Remove("hash");
            }

            if (user.id_freelancer > 0)
            {
                attributes["idFreelancer"] = user.id_freelancer.ToString();
                custom_attributes.Remove("idFreelancer");
                attributes["defaultURL"] = ConfigurationManager.AppSettings["freelancerPath"];
                custom_attributes.Remove("defaultURL");

                attributes["job_title"] = user.title;
                custom_attributes.Remove("job_title");
            }

            if (user.id_customer > 0)
            {
                attributes["idCustomer"] = user.id_customer.ToString();
                custom_attributes.Remove("idCustomer");
                attributes["defaultURL"] = ConfigurationManager.AppSettings["customerPath"];
                custom_attributes.Remove("defaultURL");

                attributes["contratou_task"] = user.contratou_task;
                custom_attributes.Remove("contratou_task");

                attributes["publicou_task"] = user.publicou_task;
                custom_attributes.Remove("publicou_task");

                attributes["aprovou_entrega"] = user.aprovou_entrega;
                custom_attributes.Remove("aprovou_entrega");

                attributes["contratou"] = user.contratou;
                custom_attributes.Remove("contratou");

                attributes["trial-expire"] = user.trial_at;
                custom_attributes.Remove("trial-expire");

                attributes["trial"] = user.trial;
                custom_attributes.Remove("trial");

                attributes["enviou briefing"] = user.contbriefings;
                custom_attributes.Remove("enviou briefing");

                attributes["search"] = user.contsearch;
                custom_attributes.Remove("search");
            }

            attributes["updated_at"] = DateTime.Now;
            custom_attributes.Remove("updated_at");

            if (user.role_company != null)
            {
                attributes["roleCompany"] = user.role_company;
                custom_attributes.Remove("roleCompany");
            }

            if (user.accept_terms != null)
            {
                attributes["acceptTerms"] = user.accept_terms;
                custom_attributes.Remove("acceptTerms");
            }

            if (user.role != null)
            {
                attributes["role"] = user.role;
                custom_attributes.Remove("role");
            }

            return attributes;
        }

        private static bool ValidateUserFreelancer(Dictionary<string, object> attributes)
        {
            if (!attributes.Keys.Contains("IDFREELANCER"))
                return false;

            if (!Int32.TryParse(attributes["IDFREELANCER"].ToString(), out int id))
                return false;

            using (var cn = new SqlConnection(connectionString))
            {
                dynamic freela = cn.Query(@"Select * from crowd_freelancer where id = @id", id);

                if (freela == null) return false;

                if (freela.prospect == 0 && freela.active == 1 && freela.status == 1) return true;
            }

            return false;
        }

        private static void ClearLead()
        {
            var scroll = intercom.ListLeads();

            for (int page = 1; page <= scroll.pages.total_pages; page++)
            {
                var leads = intercom.ListLeads(new Dictionary<string, string>() { { "page", page.ToString() } });

                foreach (var lead in leads.contacts)
                {
                    if (DateTimeOffset.FromUnixTimeSeconds(lead.created_at.Value).LocalDateTime < DateTime.Now.AddMonths(-1).AddDays(-2))
                    {
                        if (ValidateLeadFreelancer(lead.custom_attributes) ||
                            ValidateLeadCustomer(lead.custom_attributes))
                            ChangeLeadToUser(lead);
                        else
                            intercom.DeleteLead(lead.id);
                    }
                }
            }
        }

        private static void ChangeLeadToUser(Contact lead)
        {
              
        }

        private static bool ValidateLeadCustomer(Dictionary<string, object> attributes)
        {
            if (!attributes.Keys.Contains("IDCUSTOMER"))
                return false;

            if (!Int32.TryParse(attributes["IDCUSTOMER"].ToString(), out int id))
                return false;

            using (var cn = new SqlConnection(connectionString))
            {
                dynamic customer = cn.Query(@"Select * from crowd_customer where id = @id", id);

                if (customer == null) return false;

                if (customer.prospect == 0 && customer.active == 1) return true;
            }

            return false;
        }

        private static bool ValidateLeadFreelancer(Dictionary<string,object> attributes)
        {
            if (!attributes.Keys.Contains("IDFREELANCER"))
                return false;

            if (!Int32.TryParse(attributes["IDFREELANCER"].ToString(), out int id))
                return false;

            using (var cn = new SqlConnection(connectionString))
            {
                dynamic freela = cn.Query(@"Select * from crowd_freelancer where id = @id", id);

                if (freela == null) return false;

                if (freela.prospect == 0 && freela.active == 1 && freela.status == 1) return true;
            }

            return false;
        }

        private static void AddHashinUser(IntercomServer intercom, SqlConnection cn)
        {
            var scroll = intercom.ListUsers();

            for (int page = 1; page <= scroll.pages.total_pages; page++)
            {
                var users = intercom.ListUsers(new Dictionary<string, string>() { { "page", page.ToString()} });

                foreach (var user in users.users)
                {
                    var hash = cn.Query(@"Select id as Id,confirm_email as Hash from crowd_users where email = @email",new { email = user.email });

                    if (hash.Count() > 0)
                    {
                        //if (user.user_id == null)
                        //user.user_id = hash.First().Id.ToString();
                        
                        if (!user.custom_attributes.ContainsKey("hash"))
                          user.custom_attributes.Add("hash", hash.First().Hash);

                        intercom.UpdateUser(user);
                    }
                }
            }
        }

        private static void Lead(IntercomServer intercom, SqlConnection cn)
        {
            var leads = cn.Query(@"Select 
	                                    c.id as IdCustomer,
	                                    u.email as Email,
	                                    p.id as IdPlan,
	                                    p.name as [Plan],
	                                    c.Trade as Trade,
	                                    c.email_responsible as EmailResponsible,
	                                    c.name_responsible as Responsible,
	                                    c.last_change_plan as LastChangePlan,
	                                    u.name as Name,
	                                    u.phone as Phone,
	                                    f.id as IdFreelancer
                                        from crowd_users u
                                        left join crowd_customers c on u.id_customer = c.id
                                        left join crowd_freelancer f on u.id_freelancer = f.id
	                                    left join crowd_customers_plan p on p.id = c.plan_type
                                        where u.active = 1 
                                        and ((c.id is not null and c.prospect = 1)
                                        or (f.id is not null and f.prospect = 1))");

            foreach (var lead in leads)
            {
                if (lead.IdCustomer > 0)
                    LeadCustomer(lead,intercom);
                else
                    LeadFreelancer(lead,intercom);
            }
        }

        private static void LeadFreelancer(dynamic lead,IntercomServer intercom)
        {
            var contact = intercom.GetLead(lead.Email);

            if (contact == null)
            {
                var user = intercom.GetUser(lead.Email);

                if (user != null)
                    intercom.DeleteUser(user.id);

                contact = new Contact()
                {
                    name = lead.Name,
                    phone = lead.Phone,
                    email = lead.Email
                };

                contact.custom_attributes = new Dictionary<string, object>();

                contact.custom_attributes.Add("IdFreelancer", lead.IdFreelancer);

                contact = intercom.SendNewLead(contact);
            }
        }

        private static void LeadCustomer(dynamic lead, IntercomServer intercom)
        {
            var contact = intercom.GetLead(lead.Email);

            if (contact == null)
            {
                var user = intercom.GetUser(lead.Email);

                if (user != null)
                    intercom.DeleteUser(user.Id);

                var company = new Intercom.Data.Company
                {
                    company_id = lead.IdCustomer.ToString(),
                    plan = new Intercom.Data.Plan
                    {
                        id = lead.IdPlan.ToString(),
                        name = lead.Plan.ToString(),
                    },
                    name = lead.Trade,
                    custom_attributes = new Dictionary<string, object>
                    {
                        { "email", lead.EmailResponsible },
                        { "responsible", lead.Responsible },
                        { "last_change_plan", lead.LastChangePlan ?? DateTime.Now }
                    }
                };

                company = intercom.SendCompany(company);

                contact = new Contact()
                {
                    name = lead.Name,
                    phone = lead.Phone,
                    email = lead.Email
                };

                contact.companies = new List<Intercom.Data.Company>();

                contact.companies.Add(company);

                contact.custom_attributes = new Dictionary<string, object>();

                contact.custom_attributes.Add("IdCustomer", lead.IdCustomer);

                contact = intercom.SendNewLead(contact);
            }
        }

        private static void Users(IntercomServer intercom, SqlConnection cn)
        {
            var usercustomers = cn.Query(@"Select 
                                            u.id as Id,
                                            u.name,
                                            u.Email,
                                            u.Phone,
                                            (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = u.id_customer and t.status > 1 and t.active = 1 and t.id_user = u.id) as contContratacoes,
	                                        (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = u.id_customer and t.status > 1 and t.active = 1 and t.id_user = u.id and t.id_briefing is null) as contContratacoestask,
                                            (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = u.id_customer and t.active = 1 and t.id_user = u.id and t.id_briefing is null) as contPublicacoestask,
                                            (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = u.id_customer and t.status > 3 and t.active = 1 and t.id_user = u.id) as contAprovadoEntrega
                                        from crowd_users u
                                        where u.id_customer > 1 and u.active = 1 and u.id_customer = 944");

            foreach (var usercustomer in usercustomers)
            {
                int id = usercustomer.Id;
                User intcustomer = intercom.GetUser(id);

                if (intcustomer != null)
                {
                    intcustomer.user_id = usercustomer.Id;
                    intcustomer.email = usercustomer.Email;
                    intcustomer.custom_attributes["contratou"] = usercustomer.contContratacoes;
                    intcustomer.custom_attributes["aprovou_entrega"] = usercustomer.contAprovadoEntrega;
                    intcustomer.custom_attributes["publicou_task"] = usercustomer.contPublicacoestask;
                    intcustomer.custom_attributes["contratou_task"] = usercustomer.contContratacoestask;

                    intercom.UpdateUser(intcustomer);
                }
                else
                {
                    intcustomer.user_id = usercustomer.Id;
                    intcustomer.email = usercustomer.Email;
                    intcustomer.phone = usercustomer.Phone;
                    intcustomer.custom_attributes["contratou"] = usercustomer.contContratacoes;
                    intcustomer.custom_attributes["aprovou_entrega"] = usercustomer.contAprovadoEntrega;
                    intcustomer.custom_attributes["publicou_task"] = usercustomer.contPublicacoestask;
                    intcustomer.custom_attributes["contratou_task"] = usercustomer.contContratacoestask;
                }
            }
        }

        private static void Customers(IntercomServer intercom, string connectionString, SqlConnection cn)
        {
            var customers = cn.Query(@"Select 
	                                            c.id as Id,
	                                            c.Trade,
	                                            c.name,
	                                            c.created_at,
                                                p.Id as PlanId,
	                                            p.name as [Plan],
	                                            c.trial as Trial,
	                                            (Select Count(*) from crowd_users where id_customer = c.id) as contUsers,
	                                            (Select Count(l.id) from crowd_users_log l inner join crowd_users u on u.id = l.user_id where u.id_customer = c.id) as contLogs,
	                                            (Select Max(l.created_at) from crowd_users_log l inner join crowd_users u on u.id = l.user_id where u.id_customer = c.id) as lastLogs,
	                                            (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = c.id and t.active = 1 and t.id_briefing is null) as contTasks,
	                                            (Select Count(*) from crowd_briefings b inner join crowd_projects p on b.id_project = p.id where p.id_customer = c.id) as contBriefings,
	                                            (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = c.id and t.status > 1 and t.active = 1) as contContratacoes,
	                                            (Select Sum(t.price) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = c.id and t.status > 1 and t.active = 1) as Value,
	                                            (Select Count(*) from crowd_users_search s inner join crowd_users u on u.id = s.user_id and u.id_customer = c.id) as contSearch,
	                                            (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = c.id and t.status > 2 and t.active = 1) as contRecebeuAprovacao,
	                                            (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = c.id and t.status > 3 and t.active = 1) as contAprovadoEntrega
                                            from crowd_customers c
                                            inner join crowd_customers_plan p on c.plan_type = p.id
                                            where active = 1 and c.id > 1");

            foreach (var customer in customers)
            {
                int id = customer.Id;
                var intcustomer = intercom.GetCompany(id);

                if (intcustomer == null)
                    intcustomer = Sendcompany(id, connectionString, intercom);

                if (intcustomer != null)
                {
                    intcustomer.custom_attributes["Publicou_orçamento"] = customer.contBriefings;
                    intcustomer.custom_attributes["Search"] = customer.contSearch;
                    intcustomer.custom_attributes["Contratou"] = customer.contContratacoes;
                    intcustomer.custom_attributes["Aprovou_entrega"] = customer.contRecebeuAprovacao;
                    intcustomer.custom_attributes["Recebeu_para_aprovar"] = customer.contAprovadoEntrega;

                    intcustomer.company_id = customer.Id.ToString();
                    intcustomer.plan = new Intercom.Data.Plan
                    {
                        id = customer.PlanId.ToString(),
                        name = customer.Plan.ToString(),
                    };
                    intcustomer.name = customer.Trade;

                    intercom.UpdateCompany(intcustomer);
                }

                SendUsers(id, cn, intercom);
            }
        }

        private static void SendUsers(int id, SqlConnection cn, IntercomServer intercom)
        {
            var company = intercom.GetCompany(id);
            var users = cn.Query(@"Select 
                                            u.id as Id,
                                            u.name,
                                            u.Email,
                                            u.Phone,
                                            (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = u.id_customer and t.status > 1 and t.active = 1 and t.id_user = u.id) as contContratacoes,
	                                        (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = u.id_customer and t.status > 1 and t.active = 1 and t.id_user = u.id and t.id_briefing is null) as contContratacoestask,
                                            (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = u.id_customer and t.active = 1 and t.id_user = u.id and t.id_briefing is null) as contPublicacoestask,
                                            (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = u.id_customer and t.status > 3 and t.active = 1 and t.id_user = u.id) as contAprovadoEntrega
                                        from crowd_users u where id_customer = @customer and active = 1 ", new { customer = id });

            foreach (var item in users)
            {
                var user = intercom.GetUser(item.Id);

                if (user != null && !String.IsNullOrEmpty(user.user_id))
                {
                    if (user.companies == null)
                        user.companies = new List<Intercom.Data.Company>();

                    user.companies.Add(company);

                    intercom.UpdateUser(user);
                }
                else
                {
                    var intuser = new Intercom.Data.User();

                    if (intuser.companies == null)
                        intuser.companies = new List<Intercom.Data.Company>();

                    intuser.companies.Add(company);

                    intuser.user_id = item.Id.ToString();
                    intuser.email = item.Email;
                    intuser.phone = item.Phone;
                    intuser.custom_attributes = new Dictionary<string, object>();
                    intuser.custom_attributes["contratou"] = item.contContratacoes;
                    intuser.custom_attributes["aprovou_entrega"] = item.contAprovadoEntrega;
                    intuser.custom_attributes["publicou_task"] = item.contPublicacoestask;
                    intuser.custom_attributes["contratou_task"] = item.contContratacoestask;

                    intercom.SendUser(intuser);

                }
            }
        }

        private static Company Sendcompany(int id, string connectionString, IntercomServer intercom)
        {
            using (var cn = new SqlConnection(connectionString))
            {
                var customer = cn.QueryFirst(@"Select 
	                                        c.id as Id,
	                                        plan_type as PlanId,
	                                        p.name as [Plan],
	                                        c.Trade,
	                                        c.email_responsible as EmailResponsible,
	                                        c.name_responsible as Responsible,
	                                        c.trial as Trial,
	                                        c.trial_at as TrialDate,
	                                        (Select Count(*) from crowd_users where id_customer = c.id) as contUsers,
	                                        (Select Count(*) from crowd_tasks t inner join crowd_projects p on t.id_project = p.id where p.id_customer = c.id) as contJobs,
	                                        (Select Count(*) from crowd_briefings b inner join crowd_projects p on b.id_project = p.id where p.id_customer = c.id) as contBriefings
                                        from crowd_customers c
                                        inner join crowd_customers_plan p on c.plan_type = p.id
                                        where active = 1 and c.id = @id", new { id });


                var users = cn.Query<int>(@"Select id from crowd_users where id_customer = @customer and active = 1 ", new { customer = id });
                var company = new Intercom.Data.Company
                {
                    company_id = customer.Id.ToString(),
                    plan = new Intercom.Data.Plan
                    {
                        id = customer.PlanId.ToString(),
                        name = customer.Plan.ToString(),
                    },
                    name = customer.Trade,
                    custom_attributes = new Dictionary<string, object>
                    {
                        { "email", customer.EmailResponsible },
                        { "responsible", customer.Responsible },
                        { "cont_users", customer.contUsers },
                        { "cont_jobs", customer.contJobs },
                        { "cont_briefings", customer.contBriefings}

                    }
                };

                if (customer.TrialDate != null)
                    company.custom_attributes.Add("trial-expire", customer.TrialDate);

                if (customer.Trial != null)
                    company.custom_attributes.Add("trial", customer.Trial);

                company = intercom.SendCompany(company);

                foreach (var userId in users)
                {
                    var user = intercom.GetUser(userId);

                    if (user != null && !String.IsNullOrEmpty(user.user_id))
                    {
                        if (user.companies == null)
                            user.companies = new List<Intercom.Data.Company>();

                        user.companies.Add(company);

                        intercom.UpdateUser(user);
                    }
                }

                return company;
            }
        }

        private class Social
        {
            private SocialProfile sp;

            public Social(int userid,SocialProfile sp)
            {
                UserId = userid;
                if (sp.url.Contains("facebook"))
                    Type = 1;
                else if (sp.url.Contains("twitter"))
                    Type = 2;
                else if (sp.url.Contains("dribbble"))
                    Type = 3;
                else if (sp.url.Contains("instagram"))
                    Type = 4;
                else if (sp.url.Contains("linkedin"))
                    Type = 5;
                else if (sp.url.Contains("behance"))
                    Type = 6;
                else if (sp.url.Contains("github"))
                    Type = 7;
                else Type = 0;   
                
                Url = sp.url;
            }

            public int UserId { get; set; }
            public int Type { get; set; }
            public string Url { get; set; }
        }
    }
}
