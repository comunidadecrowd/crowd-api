﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment
{
    public enum PaymentType
    {
        CREDITCARD = 1,
        BOLETO = 2,
        DEBITO = 3
    }
}
