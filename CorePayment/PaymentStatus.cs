﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment
{
    public enum PaymentStatus
    {
        Ok = 0,
        Error = 1,
        Suspended = 2
    }
}
