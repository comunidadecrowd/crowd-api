﻿namespace CorePayment
{
    public class CustomerPayment
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string CNPJ { get; set; }
        public string Token { get; set; }
        public string Address { get; set; }
        public string Number { get; set; }
        public string Complement { get; set; }
        public string CEP { get; set; }
        public string Subscription { get; set; }
    }
}