﻿namespace CorePayment
{
    public class SendPayment
    {
        public string CreditCardToken { get; set; }
        public CustomerPayment Customer { get; set; }
        public PaymentType PaymentType { get; set; }
        public string Plan { get; set; }
    }
}