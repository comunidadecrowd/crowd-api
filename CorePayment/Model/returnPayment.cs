﻿using System.Net;

namespace CorePayment
{
    public class ReturnPayment
    {
        public string CustomerId { get; set; }
        public string SubscriptionId { get; set; }
        public PaymentStatus Status { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}