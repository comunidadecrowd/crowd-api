﻿using Backend.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment
{
    public interface ITrigger
    {
        string Id { get; set; }

        void Execute(Customer customer);
    }
}
