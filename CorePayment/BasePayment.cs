﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment
{
    public abstract class BasePayment
    {
        protected readonly RestClient _client;
        protected readonly string _apiKey;

        public BasePayment(string apikey, string url)
        {
            var uri = new Uri(url);
            _client = new RestClient(uri);
            _apiKey = apikey;
        }

        protected abstract IRestResponse Request<T>(string resource, Method method, T model);
    }
}
