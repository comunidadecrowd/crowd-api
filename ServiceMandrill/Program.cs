﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Services;
using Backend.Crowd.Domain.Util.Network;
using Backend.Crowd.Infrasctructure.Database.Context;
using Backend.Crowd.Infrasctructure.Database.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceMandrill
{
    class Program
    {
        static void Main(string[] args)
        {
            var crowdContext = new CrowdContext();
            var repository = new RepositoryBriefing(crowdContext);
            var serviceBriefing = new ServiceBriefing(repository);
            var ids = new int[] { 957, 958, 959, 960, 961, 962, 963, 964, 966, 967, 968 };
            var briefings = serviceBriefing.GetAll(b => ids.Any(i => i == b.Id));

            foreach (var briefing in briefings)
            {
                var sendEmail = new Email();
                briefing.Freelancers.ToList().ForEach(x =>
                {
                    sendEmail.Send(x.Email, x.Name, "Novo Briefing: " + briefing.Title, "new-briefing",
                            new Dictionary<string, string> {
                                { "DE", briefing.User.Name },
                                { "EMPRESA", String.IsNullOrEmpty(briefing.User.Customer.Trade) ? briefing.User.Customer.Name :  briefing.User.Customer.Trade},
                                { "EXCERPT", briefing.Excerpt }
                            }
                    );
                });
            }
        }
    }
}
