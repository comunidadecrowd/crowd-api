﻿/*using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Urlshortener.v1;
using Google.Apis.Urlshortener.v1.Data;*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace RebrandlyApi
{
    public class UrlShortener
    {
        public UrlShortener(string baseUrl)
        {
        }

        public string Insert(string urlToShorten)
        {
            WebClient client = new WebClient();
            client.Headers.Add("Content-Type", "application/json");
            client.Headers.Add("apikey", "254a7e1b19814602827514aca498574f");
            string ret = string.Empty;
            string body = "{\"destination\":\"_LINK_\",\"domain\":{\"fullName\":\"rebrand.ly\"}}";
            body = body.Replace("_LINK_", urlToShorten);
            try
            {
                string response = client.UploadString("https://api.rebrandly.com/v1/links", "POST", body);
                var jvs = new JavaScriptSerializer().DeserializeObject(response);
                ret = ((Dictionary<string, object>)jvs)["shortUrl"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ret;
        }

    }
}
