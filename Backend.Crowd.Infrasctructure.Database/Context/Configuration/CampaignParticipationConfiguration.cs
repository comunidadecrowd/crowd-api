﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{

    public class CampaignParticipationConfiguration : BaseEntityConfiguration<CampaignParticipation>
    {
        public CampaignParticipationConfiguration()
            : base("crowd_campaigns_participation")
        {

            HasRequired(x => x.Freelancer)
                 .WithMany()
                 .HasForeignKey(x => x.FreelancerId);

            HasRequired(x => x.Campaign)
                 .WithMany()
                 .HasForeignKey(x => x.CampaignId);


            Property(x => x.Text)
                .HasColumnName("text")
                .HasColumnType("text");

            Property(x => x.CampaignId)
                .HasColumnName("id_campaign")
                .HasColumnType("int");

            Property(x => x.FreelancerId)
                .HasColumnName("id_freelancer")
                .HasColumnType("int");

        }
    }
}


