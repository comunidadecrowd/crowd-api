﻿using Backend.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class SentimentConfiguration : BaseEntityConfiguration<Sentiment>
    {
        public SentimentConfiguration()
            : base("crowd_freelancer_sentiment")
        {
            Property(x => x.Language)
                .HasColumnName("language")
                .HasMaxLength(255)
                .IsOptional();

            Property(x => x.Magnitude)
                .HasColumnName("magnitude")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.Score)
                .HasColumnName("score")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .HasColumnType("int")
                .IsOptional();


            HasOptional(x => x.Freelancer)
                .WithMany()
                .HasForeignKey(x => x.FreelancerId);
        }
    }

    public class SentenceConfiguration : BaseEntityConfiguration<Sentence>
    {
        public SentenceConfiguration()
            : base("crowd_freelancer_sentence")
        {
            Property(x => x.Content)
                .HasColumnName("content")
                .IsOptional();

            Property(x => x.BeginOffset)
                .HasColumnName("begin_offset")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.Magnitude)
                .HasColumnName("magnitude")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.Score)
                .HasColumnName("score")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.SentimentId)
                .HasColumnName("sentiment_id")
                .HasColumnType("int")
                .IsOptional();

        }
    }
}
