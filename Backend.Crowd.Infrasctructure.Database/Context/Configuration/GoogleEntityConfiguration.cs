﻿using Backend.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class GoogleEntityConfiguration : BaseEntityConfiguration<GoogleEntity>
    {
        public GoogleEntityConfiguration()
            : base("crowd_freelancer_entity")
        {
            Property(x => x.Name)
                .HasColumnName("name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsOptional();

            Property(x => x.Type)
                .HasColumnName("type")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.Salience)
                .HasColumnName("salience")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .HasColumnType("int")
                .IsOptional();
        }
    }
}
