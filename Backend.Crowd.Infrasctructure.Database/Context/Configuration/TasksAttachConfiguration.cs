﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class TasksAttachConfiguration : BaseEntityConfiguration<TasksAttach>
    {
        public TasksAttachConfiguration()
            : base("crowd_tasks_attachs")
        {

            Property(x => x.TasksId)
                .HasColumnName("tasks_id")
                .HasColumnType("int");

            Property(x => x.URL)
                .HasColumnName("attach_url")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Name)
                .HasColumnName("attach_name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

        }
    }
}
