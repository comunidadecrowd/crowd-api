﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class CityConfiguration : BaseEntityConfiguration<City>
    {
        public CityConfiguration()
            :base("crowd_city")
        {
            /*HasRequired(x => x.State)
                 .WithMany()
                 .HasForeignKey(x => x.StateId);*/

            Property(x => x.Name)
                .HasColumnName("name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.StateId)
               .HasColumnName("id_state")
               .HasColumnType("int");
        }
    }
}
