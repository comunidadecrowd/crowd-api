﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class ComplementAttachConfiguration : BaseEntityConfiguration<ComplementAttach>
    {
        public ComplementAttachConfiguration()
            : base("crowd_complements_attachs")
        {

            Property(x => x.complementId)
                .HasColumnName("complement_id")
                .HasColumnType("int");

            Property(x => x.attachUrl)
                .HasColumnName("attach_url")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.attachName)
                .HasColumnName("attach_name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();
        }
    }
}
