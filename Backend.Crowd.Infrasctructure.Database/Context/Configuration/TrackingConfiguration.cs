﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class TrackingConfiguration : BaseEntityConfiguration<Tracking>
    {
        public TrackingConfiguration()
            : base("crowd_tracking")
        {
           
            Property(x => x.IdUser)
                .HasColumnName("user_id")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.Action)
                .HasColumnName("action")
                .HasColumnType("varchar");

            Property(x => x.Controller)
                .HasColumnName("controller")
                .HasColumnType("varchar");

            Property(x => x.State)
               .HasColumnName("state")
               .HasColumnType("varchar");

            Property(x => x.Token)
                .HasColumnName("token")
                .HasColumnType("varchar");

            Property(x => x.Environment)
                .HasColumnName("environment")
                .HasColumnType("varchar");
        }
    }

    public class ArgumentsConfiguration : BaseEntityConfiguration<Argument>
    {
        public ArgumentsConfiguration()
            : base("crowd_tracking_arguments")
        {
            HasRequired(x => x.Tracking)
                .WithMany(x => x.Arguments)
                .HasForeignKey(x => x.TrackingId);

            Property(x => x.TrackingId)
                .HasColumnName("tracking_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Name)
                .HasColumnName("name")
                .HasColumnType("varchar");

            Property(x => x.Value)
                .HasColumnName("value")
                .HasColumnType("varchar");

            Property(x => x.Type)
                .HasColumnName("type")
                .HasColumnType("varchar");
        }
    }
}
