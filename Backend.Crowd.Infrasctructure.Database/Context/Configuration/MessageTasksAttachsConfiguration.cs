﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class MessageTasksAttachsConfiguration : BaseEntityConfiguration<MessageTaskAttachs>
    {
        public MessageTasksAttachsConfiguration()
            : base("crowd_tasks_messages_attachs")
        {

            Property(x => x.MessageTasksId)
                .HasColumnName("messagetasks_id")
                .HasColumnType("int");

            Property(x => x.URL)
                .HasColumnName("attach_url")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Name)
                .HasColumnName("attach_name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

        }
    }
}
