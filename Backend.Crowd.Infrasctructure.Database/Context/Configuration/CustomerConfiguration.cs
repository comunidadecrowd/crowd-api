﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class CustomerConfiguration : BaseEntityConfiguration<Customer>
    {
        public CustomerConfiguration()
            : base("crowd_customers")
        {
            HasOptional(x => x.State)
                .WithMany()
                .HasForeignKey(x => x.StateId);

            HasOptional(x => x.City)
                .WithMany()
                .HasForeignKey(x => x.CityId);

            Property(x => x.Name)
                .HasColumnName("name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsOptional();

            Property(x => x.Trade)
                .HasColumnName("trade")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Logo)
                .HasColumnName("logo")
                .HasColumnType("varchar")
                .HasMaxLength(255);

            Property(x => x.CNPJ)
                .HasColumnName("cnpj")
                .HasColumnType("varchar")
                .HasMaxLength(20)
                .IsOptional();

            Property(x => x.QtyPersons)
                .HasColumnName("qty_persons")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.StateId)
                .HasColumnName("id_state")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.CityId)
                .HasColumnName("id_city")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.NameResponsible)
                .HasColumnName("name_responsible")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.EmailResponsible)
                .HasColumnName("email_responsible")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.InscricaoEstadual)
                .HasColumnName("inscricao_estadual")
                .HasColumnType("varchar")
                .HasMaxLength(255);

            Property(x => x.InscricaoMunicipal)
                .HasColumnName("inscricao_municipal")
                .HasColumnType("varchar")
                .HasMaxLength(255);

            Property(x => x.Address)
                .HasColumnName("address")
                .HasColumnType("varchar")
                .HasMaxLength(255);

            Property(x => x.Number)
                .HasColumnName("number")
                .HasColumnType("varchar")
                .HasMaxLength(50);

            Property(x => x.CEP)
                .HasColumnName("cep")
                .HasColumnType("varchar")
                .HasMaxLength(9);

            Property(x => x.Complement)
                .HasColumnName("complement")
                .HasColumnType("varchar")
                .HasMaxLength(255);

            Property(x => x.Prospect)
                .HasColumnName("prospect")
                .HasColumnType("bit")
                .IsOptional();

            Property(x => x.Phone)
                .HasColumnName("phone")
                .HasColumnType("varchar")
                .HasMaxLength(20)
                .IsOptional();

            Property(x => x.LastChangeActivation)
                .HasColumnName("last_change_activation");

            Property(x => x.LastChangePlan)
                .HasColumnName("last_change_plan");

            Property(x => x.PlanType)
                .HasColumnName("plan_type")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Referrer)
                .HasColumnName("referrer")
                .HasColumnType("varchar")
                .HasMaxLength(2000)
                .IsOptional();

            Property(x => x.Trial)
                .HasColumnName("trial")
                .HasColumnType("bit")
                .IsOptional();

            Property(x => x.TrialDate)
                .HasColumnName("trial_at")
                .IsOptional();

            Property(x => x.PaymentToken)
                .HasColumnName("payment_token")
                .HasColumnType("varchar")
                .IsOptional();

            Property(x => x.PaymentSubscription)
                .HasColumnName("payment_subscription")
                .HasColumnType("varchar")
                .IsOptional();

            Property(x => x.PromoCode)
               .HasColumnName("promocode")
               .HasColumnType("varchar")
               .IsOptional();

            HasOptional(x => x.Payment)
                .WithMany()
                .HasForeignKey(x => x.PaymentSubscription);
        }
    }

    public class CustomerTrackingConfiguration : BaseEntityConfiguration<Customer.CustomerTracking>
    {
        public CustomerTrackingConfiguration()
            : base("crowd_customers_tracking")
        {
            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.UserId)
                .HasColumnName("user_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.FieldRequested)
                .HasColumnName("field_requested")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.SearchTerm)
                .HasColumnName("search_terms")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsOptional();

            Property(x => x.PlanType)
                .HasColumnName("plan_type")
                .HasColumnType("int")
                .IsRequired();
        }
    }

    public class RatingCustomerConfiguration : BaseEntityConfiguration<Customer.RatingCustomer>
    {
        public RatingCustomerConfiguration()
            : base("crowd_customers_rating")
        {

            HasRequired(x => x.Customer)
                .WithMany(x => x.Ratings)
                .HasForeignKey(x => x.CustomerId);

            HasRequired(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.UserId);

            HasOptional(x => x.Task)
                .WithMany()
                .HasForeignKey(x => x.TaskId);

            Property(x => x.CustomerId)
                .HasColumnName("customer_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.UserId)
                .HasColumnName("user_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Approval)
                .HasColumnName("approval")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Communicate)
                .HasColumnName("communicate")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Professionalism)
                .HasColumnName("professionalism")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Comment)
                .HasColumnName("comment")
                .HasColumnType("varchar");

            Property(x => x.TaskId)
                .HasColumnName("task_id")
                .HasColumnType("int")
                .IsOptional();
        }
    }

    public class PaymentCustomerConfiguration : EntityTypeConfiguration<Customer.PaymentCustomer>
    {
        public PaymentCustomerConfiguration()
        {
            ToTable("crowd_customers_payment");

            HasKey(x => x.SubscriptionId)
                .HasEntitySetName("subscription_id");

            Property(x => x.SubscriptionId)
                .HasColumnName("subscription_id")
                .HasColumnType("varchar")
                .IsRequired();

            HasRequired(x => x.Customer)
                .WithMany()
                .HasForeignKey(x => x.CustomerId);

            Property(x => x.CustomerId)
               .HasColumnName("customer_id")
               .HasColumnType("int")
               .IsRequired();

            Property(x => x.Status)
                .HasColumnName("status")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Expires)
                .HasColumnName("expires_at")
                .HasColumnType("datetime")
                .IsOptional();

            Property(x => x.CreatedAt)
                .HasColumnName("created_at")
                .IsRequired();

            Property(x => x.UpdatedAt)
                .HasColumnName("updated_at");
        }
    }

    public class CustomerInvoicesConfiguration : EntityTypeConfiguration<Customer.CustomerInvoices>
    {
        public CustomerInvoicesConfiguration()
        {
            ToTable("crowd_customers_payment_invoices");

            HasKey(x => x.Id)
                .HasEntitySetName("id");

            Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("varchar")
                .IsRequired();

            Property(x => x.DueDate)
               .HasColumnName("due_date")
               .HasColumnType("datetime")
               .IsOptional();

            Property(x => x.SubscriptionId)
                .HasColumnName("subscription_id")
                .HasColumnType("varchar")
                .IsRequired();

            Property(x => x.Status)
                .HasColumnName("status")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Total)
                .HasColumnName("total")
                .HasColumnType("decimal")
                .IsOptional();
        }
    }

    public class CustomerPaymentLogConfiguration : EntityTypeConfiguration<Customer.CustomerPaymentLog>
    {
        public CustomerPaymentLogConfiguration()
        {

            ToTable("crowd_customers_payment_log");

            HasKey(x => x.Id)
                .HasEntitySetName("id");

            Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("varchar")
                .IsRequired();

            Property(x => x.CreatedAt)
               .HasColumnName("created_at")
               .HasColumnType("datetime")
               .IsOptional();

            Property(x => x.SubscriptionId)
                .HasColumnName("subscription_id")
                .HasColumnType("varchar")
                .IsRequired();

            Property(x => x.Description)
                .HasColumnName("description")
                .HasColumnType("varchar")
                .IsOptional();

            Property(x => x.Notes)
                .HasColumnName("notes")
                .HasColumnType("varchar")
                .IsOptional();
        }
    }

}
