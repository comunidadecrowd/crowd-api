﻿using Backend.Crowd.Domain.Entities;
using static Backend.Crowd.Domain.Entities.Briefing;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class BriefingConfiguration : BaseEntityConfiguration<Briefing>
    {
        public BriefingConfiguration()
            :base("crowd_briefings")
        {

            HasMany(x => x.Freelancers)
                .WithMany(x => x.Briefings)
                .Map(x =>
                {
                    x.MapLeftKey("briefing_id");
                    x.MapRightKey("freelancer_id");
                    x.ToTable("crowd_briefings_freelancers");
                });

            HasRequired(x => x.User)
               .WithMany()
               .HasForeignKey(x => x.IdUser);

            Property(x => x.IdUser)
                .HasColumnName("id_user")
                .HasColumnType("int");


            HasOptional(x => x.Project)
               .WithMany()
               .HasForeignKey(x => x.IdProject);

            Property(x => x.IdProject)
                .HasColumnName("id_project")
                .HasColumnType("int");

            Property(x => x.Title)
                .HasColumnName("title")
                .HasColumnType("varchar(max)")
                //.HasMaxLength(255)
                .IsRequired();

            Property(x => x.Excerpt)
                .HasColumnName("excerpt")
                .HasColumnType("varchar")
                .IsRequired();

            Property(x => x.Text)
                .HasColumnName("text")
                .HasColumnType("text")
                .IsMaxLength();

            Property(x => x.Sended)
                .HasColumnName("sended")
                .HasColumnType("bit")
                .IsRequired();

            Property(x => x.Hunting)
               .HasColumnName("hunting")
               .HasColumnType("bit")
               .IsOptional();

            

        }
    }

    public class TokenFreelancerConfiguration : BaseEntityConfiguration<TokenFreelancer>
    {
        public TokenFreelancerConfiguration() : base("crowd_briefings_freelancers_token")
        {

            Property(x => x.BriefingId)
               .HasColumnName("briefing_id")
               .HasColumnType("int");

            Property(x => x.FreelancerId)
               .HasColumnName("freelancer_id")
               .HasColumnType("int");

            Property(x => x.Token)
                    .HasColumnName("token")
                    .HasColumnType("uniqueidentifier");

            Property(x => x.Status)
                    .HasColumnName("status")
                    .HasColumnType("int");

            Property(x => x.UrlShortener)
                    .HasColumnName("url")
                    .HasColumnType("varchar");

            Property(x => x.Visualizations)
           .HasColumnName("vizualizations")
           .HasColumnType("int");

            Property(x => x.SeeAt)
           .HasColumnName("see_at")
           .HasColumnType("datetime");


        }
    }

    public class BriefingAttachConfiguration : BaseEntityConfiguration<Briefing.Attach>
    {
        public BriefingAttachConfiguration()
            : base("crowd_briefings_attachs")
        {
            HasRequired(x => x.Briefing)
                .WithMany(x => x.Attachs)
                .HasForeignKey(x => x.briefingId);

            Property(x => x.briefingId)
                .HasColumnName("briefing_id")
                .HasColumnType("int");

            Property(x => x.attachUrl)
                .HasColumnName("attach_url")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.attachName)
                .HasColumnName("attach_name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

        }
    }

    public class ComplementConfiguration : BaseEntityConfiguration<Briefing.Complement>
    {
        public ComplementConfiguration()
            : base("crowd_complements")
        {
            HasRequired(x => x.Briefing)
                .WithMany(x => x.Complements)
                .HasForeignKey(x => x.BriefingId);

            HasOptional(x => x.Task)
                .WithMany(x => x.Complements)
                .HasForeignKey(x => x.TaskId);

            Property(x => x.BriefingId)
                .HasColumnName("id_briefing")
                .HasColumnType("int");

            Property(x => x.TaskId)
                .HasColumnName("id_task")
                .HasColumnType("int");

            Property(x => x.Text)
                .HasColumnName("text")
                .HasColumnType("text");
        }
    }

    public class ReadBriefingConfiguration : BaseEntityConfiguration<Briefing.ReadBriefing>
    {
        public ReadBriefingConfiguration()
            : base("crowd_read_briefing")
        {
            HasRequired(x => x.Briefings)
                .WithMany(x => x.Read)
                .HasForeignKey(x => x.BriefingId);

            HasRequired(x => x.Freelancer)
                .WithMany()
                .HasForeignKey(x => x.FreelancerId);

            Property(x => x.BriefingId)
                .HasColumnName("briefing_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Read)
                .HasColumnName("read")
                .HasColumnType("bit")
                .IsRequired();
        }
    }

    public class ProposeConfiguration : BaseEntityConfiguration<Briefing.Propose>
    {
        public ProposeConfiguration()
            : base("crowd_proposes")
        {
            HasRequired(x => x.Freelancer)
                .WithMany()
                .HasForeignKey(x => x.FreelancerId);

            HasOptional(x => x.Briefing)
                .WithMany()
                .HasForeignKey(x => x.BriefingId);

            HasOptional(x => x.Task)
                .WithMany()
                .HasForeignKey(x => x.TaskId);

            HasOptional(x => x.UserResponsible)
                .WithMany()
                .HasForeignKey(x => x.UserResponsibleId);

            Property(x => x.BriefingId)
                .HasColumnName("id_briefing")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.TaskId)
                .HasColumnName("id_task")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.FreelancerId)
                .HasColumnName("id_freelancer")
                .HasColumnType("int");

            Property(x => x.UserResponsibleId)
                .HasColumnName("id_responsible_user")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.Price)
                .HasColumnName("price")
                .HasColumnType("decimal")
                .IsRequired();

            Property(x => x.DeadlineDays)
                .HasColumnName("deadline_days")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Contracted)
                .HasColumnName("contracted")
                .HasColumnType("bit")
                .IsRequired();
        }
    }

    public class MessageConfiguration : BaseEntityConfiguration<Briefing.Message>
    {
        public MessageConfiguration()
            : base("crowd_messages")
        {

            HasOptional(x => x.User)
                //.WithMany(x => x.Messages)
                .WithMany()
                .HasForeignKey(x => x.UserId);

            HasOptional(x => x.Freelancers)
                .WithMany()
                .HasForeignKey(x => x.FreelancerId);

            HasRequired(x => x.Briefings)
                .WithMany(x => x.Messages)
                .HasForeignKey(x => x.BriefingId);

            Property(x => x.BriefingId)
                .HasColumnName("id_briefing")
                .HasColumnType("int");

            Property(x => x.UserId)
                .HasColumnName("id_user")
                .HasColumnType("int");

            Property(x => x.FreelancerId)
                .HasColumnName("id_freelancer")
                .HasColumnType("int");

            Property(x => x.Text)
                .HasColumnName("text")
                .HasColumnType("text");

            Property(x => x.Read)
                .HasColumnName("read")
                .HasColumnType("bit")
                .IsRequired();
        }
    }
}
