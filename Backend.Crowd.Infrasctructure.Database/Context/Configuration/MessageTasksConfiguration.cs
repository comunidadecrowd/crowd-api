﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{

    public class MessageTasksConfiguration : BaseEntityConfiguration<MessageTasks>
    {
        public MessageTasksConfiguration()
            : base("crowd_tasks_messages")
        {

            HasOptional(x => x.User)
                //.WithMany(x => x.MessageTasks)
                .WithMany()
                .HasForeignKey(x => x.UserId);

            HasOptional(x => x.Freelancer)
                .WithMany()
                .HasForeignKey(x => x.FreelancerId);

            HasRequired(x => x.Task)
                .WithMany(x => x.Messages)
                .HasForeignKey(x => x.TaskId);

            Property(x => x.TaskId)
                .HasColumnName("id_task")
                .HasColumnType("int");

            Property(x => x.UserId)
                .HasColumnName("id_user")
                .HasColumnType("int");

            Property(x => x.FreelancerId)
                .HasColumnName("id_freelancer")
                .HasColumnType("int");

            Property(x => x.Text)
                .HasColumnName("text")
                .HasColumnType("text");

            Property(x => x.Read)
                .HasColumnName("read")
                .HasColumnType("bit")
                .IsRequired();
        }
    }
}
