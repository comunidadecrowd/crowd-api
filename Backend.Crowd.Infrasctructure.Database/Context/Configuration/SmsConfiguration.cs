﻿using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class SmsConfiguration : BaseEntityConfiguration<Sms>
    {
        public SmsConfiguration()
            : base("crowd_sms")
        {
            Property(x => x.UserId)
                .HasColumnName("user_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Body)
                 .HasColumnName("body")
                 .HasColumnType("varchar")
                 .IsOptional();

            HasRequired(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.UserId);

            Property(x => x.SentAt)
                .HasColumnName("sent_at")
                .IsOptional();

            Property(x => x.ErrorCode)
                 .HasColumnName("error_code")
                 .HasColumnType("varchar")
                 .IsOptional();

            Property(x => x.ErrorMessage)
                 .HasColumnName("error_message")
                 .HasColumnType("varchar")
                 .IsOptional();

            Property(x => x.From)
                 .HasColumnName("from")
                 .HasColumnType("varchar")
                 .IsOptional();

            Property(x => x.To)
                 .HasColumnName("to")
                 .HasColumnType("varchar")
                 .IsOptional();

            Property(x => x.SmsId)
                 .HasColumnName("sms_id")
                 .HasColumnType("varchar")
                 .IsOptional();

            Property(x => x.SourceId)
                 .HasColumnName("source_id")
                 .HasColumnType("varchar")
                 .IsOptional();

            Property(x => x.Status)
                .HasColumnName("status")
                .HasColumnType("int")
                .IsRequired();
        }
    }
}
