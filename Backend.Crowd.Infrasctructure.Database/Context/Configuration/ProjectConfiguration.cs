﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class ProjectConfiguration : BaseEntityConfiguration<Project>
    {
        public ProjectConfiguration()
            : base("crowd_projects")
        {
            HasRequired(x => x.Customer)
               .WithMany()
               .HasForeignKey(x => x.IdCustomer);

            Property(x => x.IdCustomer)
                .HasColumnName("id_customer")
                .HasColumnType("int");

            Property(x => x.Name)
                .HasColumnName("name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Color)
                .HasColumnName("color")
                .HasColumnType("varchar")
                .HasMaxLength(20)
                .IsRequired();

            Ignore(x => x.TaskCount);
        }
    }
}
