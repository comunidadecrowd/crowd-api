﻿using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class FreelancerConfiguration : BaseEntityConfiguration<Freelancer>
    {
        public FreelancerConfiguration()
            : base("crowd_freelancer")
        {
            HasMany<Freelancer.Skill>(x => x.Skills)
                .WithMany()
                .Map(x =>
                {
                    x.MapLeftKey("freelancers_id");
                    x.MapRightKey("skill_id");
                    x.ToTable("crowd_freelancers_skills");
                });

            HasMany<Freelancer.Segment>(x => x.Segments)
                .WithMany()
                .Map(x =>
                {
                    x.MapLeftKey("freelancers_id");
                    x.MapRightKey("segments_id");
                    x.ToTable("crowd_freelancers_segments");
                });

            HasRequired(x => x.Categorys)
                .WithMany()
                .HasForeignKey(x => x.CategoryId);

            HasOptional(x => x.State)
               .WithMany()
               .HasForeignKey(x => x.StateId);

            HasOptional(x => x.City)
                .WithMany()
                .HasForeignKey(x => x.CityId);

            HasOptional(x => x.Sentiment)
               .WithMany()
               .HasForeignKey(x => x.SentimentId);

            HasOptional(x => x.SafeSearch)
               .WithMany()
               .HasForeignKey(x => x.SafeSearchId);

            HasOptional(x => x.WebInformation)
               .WithMany()
               .HasForeignKey(x => x.WebInformationId);

            Property(x => x.CategoryId)
                .HasColumnName("category_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Name)
                .HasColumnName("name")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Description)
                .HasColumnName("description")
                .HasColumnType("text")
                .IsMaxLength();

            Property(x => x.Email)
                .HasColumnName("email")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.EmailSecondary)
                .HasColumnName("email_secondary")
                .HasMaxLength(255)
                .IsOptional();            

            Property(x => x.Phone)
                .HasColumnName("phone")
                .HasMaxLength(25);

            Property(x => x.Country)
                .HasColumnName("country")
                .HasMaxLength(100)
                .IsOptional();

            Property(x => x.Title)
                .HasColumnName("title")
                .HasMaxLength(255)
                .IsOptional();

            Property(x => x.Skype)
                .HasColumnName("skype")
                .HasMaxLength(100)
                .IsOptional();

            Property(x => x.Price)
                .HasColumnName("price")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.PortfolioURL)
               .HasColumnName("portfolio")
               .HasMaxLength(255);

            Property(x => x.StateId)
                .HasColumnName("state_id")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.CityId)
                .HasColumnName("city_id")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.SentimentId)
                .HasColumnName("sentiment_id")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.SafeSearchId)
               .HasColumnName("safesearch_id")
               .HasColumnType("int")
               .IsOptional();

            Property(x => x.WebInformationId)
               .HasColumnName("webinformation_id")
               .HasColumnType("int")
               .IsOptional();

            Property(x => x.Availability)
                .HasColumnName("Availability");

            Property(x => x.Status)
                .HasColumnName("status")
                .IsRequired();

            Property(x => x.Code)
                .HasColumnName("code")
                .HasColumnType("uniqueidentifier");

            Ignore(x => x.Rating);

            Property(x => x.Birthday)
                .HasColumnName("birthday");

            Property(x => x.Code)
                .HasColumnName("code")
                .HasColumnType("uniqueidentifier");

            Property(x => x.Cover)
                .HasColumnName("cover")
                .HasColumnType("varchar")
                .IsMaxLength();

            Property(x => x.CoverType)
                .HasColumnName("cover_type")
                .HasColumnType("int").IsOptional();

            Property(x => x.Referrer)
                .HasColumnName("referrer")
                .HasColumnType("varchar")
                .HasMaxLength(2000)
                .IsOptional();

            Property(x => x.PromoCode)
               .HasColumnName("promocode")
               .HasColumnType("varchar")
               .IsOptional();

            Property(x => x.Prospect)
                .HasColumnName("prospect")
                .HasColumnType("bit")
                .IsOptional();

            Property(x => x.Alocado)
                .HasColumnName("alocado")
                .HasColumnType("bit")
                .IsOptional();

            Property(x => x.PortfolioOnLine)
                .HasColumnName("portfolio_online")
                .HasColumnType("varchar")
                .HasMaxLength(2000)
                .IsOptional();
        }
    }

    public class SkillConfiguration : BaseEntityConfiguration<Freelancer.Skill>
    {
        public SkillConfiguration()
            : base("crowd_skill")
        {
            Property(x => x.Name)
                .HasColumnName("name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();
        }
    }

    public class SegmentConfiguration : BaseEntityConfiguration<Freelancer.Segment>
    {
        public SegmentConfiguration()
            : base("crowd_segment")
        {
            Property(x => x.Name)
                .HasColumnName("name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();
        }
    }

    public class CategoryConfiguration : BaseEntityConfiguration<Freelancer.Category>
    {
        public CategoryConfiguration()
            : base("crowd_category")
        {
            Property(x => x.Name)
                .HasColumnName("name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();
        }

    }

    public class ExperienceConfiguration : BaseEntityConfiguration<Freelancer.Experience>
    {
        public ExperienceConfiguration()
            : base("crowd_freelancer_experience")
        {

            HasRequired(x => x.Freelancer)
                .WithMany(x => x.Experiences)
                .HasForeignKey(x => x.FreelancerId);

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Role)
                .HasColumnName("role")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Company)
                .HasColumnName("company")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Description)
                .HasColumnName("description")
                .HasColumnType("text");

            Property(x => x.StartDate)
                .HasColumnName("start_date")
                .IsRequired();

            Property(x => x.EndDate)
                .HasColumnName("end_date")
                .IsOptional();

        }
    }

    public class RatingFreelancerConfiguration : BaseEntityConfiguration<Freelancer.RatingFreelancer>
    {
        public RatingFreelancerConfiguration()
            : base("crowd_freelancer_rating")
        {

            HasRequired(x => x.Freelancer)
                .WithMany(x => x.Ratings)
                .HasForeignKey(x => x.FreelancerId);

            HasRequired(x => x.User)
                 .WithMany()
                 .HasForeignKey(x => x.UserId);

            HasOptional(x => x.Task)
               .WithMany()
               .HasForeignKey(x => x.TaskId);

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.UserId)
                .HasColumnName("user_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Responsability)
                .HasColumnName("responsability")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Agility)
                .HasColumnName("agility")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Quality)
                .HasColumnName("quality")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Comment)
                .HasColumnName("comment")
                .HasColumnType("varchar");

            Property(x => x.TaskId)
                .HasColumnName("task_id")
                .HasColumnType("int")
                .IsOptional();

           
        }
    }

    public class AwardsConfiguration : BaseEntityConfiguration<Freelancer.Award>
    {
        public AwardsConfiguration()
            : base("crowd_freelancer_award")
        {
            HasRequired(x => x.Freelancer)
                .WithMany(x => x.Awards)
                .HasForeignKey(x => x.FreelancerId);

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Title)
                .HasColumnName("title")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.URL)
                .HasColumnName("url")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsOptional();

            Property(x => x.Date)
                .HasColumnName("date")
                .IsRequired();

            Property(x => x.Type)
                .HasColumnName("type")
                .HasColumnType("int")
                .IsRequired();

        }
    }

    public class PortfolioConfiguration : BaseEntityConfiguration<Freelancer.Portfolio>
    {
        public PortfolioConfiguration()
            : base("crowd_freelancer_portfolio")
        {
            HasRequired(x => x.Freelancer)
                .WithMany(x => x.Portfolios)
                .HasForeignKey(x => x.FreelancerId);

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Title)
                .HasColumnName("title")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Media)
                .HasColumnName("media_url")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.URL)
                .HasColumnName("url")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsOptional();

            Property(x => x.Description)
                .HasColumnName("description")
                .HasColumnType("text")
                .IsOptional();

            Property(x => x.ClientName)
                .HasColumnName("client_name")
                .HasColumnType("varchar")
                .HasMaxLength(50)
                .IsOptional();

            Property(x => x.Type)
                .HasColumnName("type")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Order)
                .HasColumnName("order")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Thumb)
                .HasColumnName("thumb")
                .HasColumnType("varchar")
                .IsOptional();
        }

    }

    public class PaymentConfiguration : BaseEntityConfiguration<Freelancer.Payment>
    {
        public PaymentConfiguration()
            : base ("crowd_freelancer_payment")
        {
            HasRequired(x => x.Freelancer)
                .WithMany(x => x.Payments)
                .HasForeignKey(x => x.FreelancerId);

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .IsRequired();

            Property(x => x.Bank)
                .HasColumnName("bank")
                .IsRequired();

            Property(x => x.FullName)
                .HasColumnName("full_name")
                .IsRequired();

            Property(x => x.Agency)
                .HasColumnName("agency")
                .IsRequired();

            Property(x => x.Account)
                .HasColumnName("account")
                .IsRequired();

            Property(x => x.CPF)
                .HasColumnName("cpf")
                .IsRequired();
        }
    }

    public class LinkConfiguration : BaseEntityConfiguration<Freelancer.Link>
    {
        public LinkConfiguration()
            : base ("crowd_freelancer_link")
        {
            HasRequired(x => x.Freelancer)
                .WithMany(x => x.Links)
                .HasForeignKey(x => x.FreelancerId);

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .IsRequired();

            Property(x => x.Website)
                .HasColumnName("website")
                .HasColumnType("int");

            Property(x => x.URL)
                .HasColumnName("url")
                .HasColumnType("varchar")
                .IsMaxLength();
        }
    }

    public class LanguageConfiguration: BaseEntityConfiguration<Freelancer.Language>
    {
        public LanguageConfiguration()
            : base("crowd_freelancer_language")
        {
            HasRequired(x => x.Freelancer)
                .WithMany(x => x.Languages)
                .HasForeignKey(x => x.FreelancerId);

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .IsRequired();

            Property(x => x.Value)
                .HasColumnName("value")
                .HasColumnType("varchar")
                .IsMaxLength()
                .IsRequired();
        }
    }
}
