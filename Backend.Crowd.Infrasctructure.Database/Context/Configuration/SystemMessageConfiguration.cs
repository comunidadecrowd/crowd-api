﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class SystemMessageConfiguration : BaseEntityConfiguration<SystemMessage>
    {
        public SystemMessageConfiguration()
            : base("crowd_system_messages")
        {

            HasMany<User>(x => x.Users)
                .WithMany()
                .Map(x =>
                {
                    x.MapLeftKey("system_message_id");
                    x.MapRightKey("user_id");
                    x.ToTable("crowd_system_messages_users");
                });

            Property(x => x.Text)
                .HasColumnName("text")
                .HasColumnType("text");

            Property(x => x.Excerpt)
                .HasColumnName("excerpt")
                .HasColumnType("text");

            Property(x => x.Title)
                .HasColumnName("title")
                .HasColumnType("varchar");

            HasRequired(x => x.UserSent)
               .WithMany()
               .HasForeignKey(x => x.IdUserSent);

            Property(x => x.IdUserSent)
                .HasColumnName("id_user_sent")
                .HasColumnType("int");

            Property(x => x.ToAllCustomers)
                .HasColumnName("to_all_customers")
                .HasColumnType("bit")
                .IsRequired();

            Property(x => x.ToAllFreelancers)
                .HasColumnName("to_all_freelancers")
                .HasColumnType("bit")
                .IsRequired();

        }
    }

    public class ReadSystemMessageConfiguration : BaseEntityConfiguration<SystemMessage.ReadSystemMessage>
    {
        public ReadSystemMessageConfiguration()
            : base("crowd_system_messages_read")
        {
            HasRequired(x => x.SystemMessage)
                .WithMany(x => x.Read)
                .HasForeignKey(x => x.SystemMessageId);

            HasRequired(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.UserId);

            Property(x => x.SystemMessageId)
                .HasColumnName("system_message_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.UserId)
                .HasColumnName("user_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Read)
                .HasColumnName("read")
                .HasColumnType("bit")
                .IsRequired();
        }
    }
}
