﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Infrasctructure.Database.Context.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Backend.Crowd.Domain.Entities.WebInformation;

namespace Backend.Crowd.Infrasctructure.Database.Context
{
    public class FacesConfiguration : BaseEntityConfiguration<Face>
    {
        public FacesConfiguration() : base("crowd_freelancer_face")
        {
            Property(x => x.DetectionConfidence)
                .HasColumnName("detection_confidence")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.SorrowLikelihood)
                .HasColumnName("sorrow")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.JoyLikelihood)
                .HasColumnName("joy")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.AngerLikelihood)
               .HasColumnName("anger")
               .HasColumnType("int")
               .IsOptional();

            Property(x => x.UnderExposedLikelihood)
                .HasColumnName("under_exposed")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.BlurredLikelihood)
               .HasColumnName("blurred")
               .HasColumnType("int")
               .IsOptional();

            Property(x => x.HeadwearLikelihood)
                .HasColumnName("head_wear")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.SurpriseLikelihood)
               .HasColumnName("surprise")
               .HasColumnType("int")
               .IsOptional();

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .HasColumnType("int")
                .IsOptional();


        //    HasOptional(x => x.Freelancer)
        //        .WithMany()
        //        .HasForeignKey(x => x.FreelancerId);
        }
    }

    public class LabelsConfiguration : BaseEntityConfiguration<Label>
    {
        public LabelsConfiguration() : base("crowd_freelancer_label")
        {
            Property(x => x.Description)
                .HasColumnName("description")
                .HasColumnType("varchar")
                .IsOptional();

            Property(x => x.Score)
                .HasColumnName("score")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .HasColumnType("int")
                .IsOptional();


            //HasOptional(x => x.Freelancer)
            //    .WithMany()
            //    .HasForeignKey(x => x.FreelancerId);
        }
    }

    public class SafeSearchConfiguration : BaseEntityConfiguration<SafeSearch>
    {
        public SafeSearchConfiguration() : base("crowd_freelancer_safesearch")
        {
            Property(x => x.Adult)
                .HasColumnName("adult")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.Spoof)
                .HasColumnName("spoof")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.Medical)
                .HasColumnName("medical")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.Violence)
                .HasColumnName("violence")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .HasColumnType("int")
                .IsOptional();

            //HasOptional(x => x.Freelancer)
            //    .WithMany()
            //    .HasForeignKey(x => x.FreelancerId);
        }
    }

    public class WebInformationConfiguration : BaseEntityConfiguration<WebInformation>
    {
        public WebInformationConfiguration() : base("crowd_freelancer_webinformation")
        {
             

            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .HasColumnType("int")
                .IsOptional();
        }
    }

    public class WebEntityConfiguration : EntityTypeConfiguration<WebEntity>
    {
        public WebEntityConfiguration() 
        {
            ToTable("crowd_freelancer_webinformation_webentity");

            HasKey(x => x.Id)
                .HasEntitySetName("id");

            Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Score)
                .HasColumnName("score")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.Description)
                .HasColumnName("description")
                .HasColumnType("varchar")
                .IsOptional();

            Property(x => x.WebInformationId)
                .HasColumnName("webinformation_id")
                .HasColumnType("int")
                .IsOptional();

            //HasOptional(x => x.WebInformation)
            //    .WithMany()
            //    .HasForeignKey(x => x.WebInformationId);
        }
    }

    public class WebFullConfiguration : EntityTypeConfiguration<WebFull>
    {
        public WebFullConfiguration()
        {
            ToTable("crowd_freelancer_webinformation_webfull");

            HasKey(x => x.Id)
                .HasEntitySetName("id");

            Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Score)
                .HasColumnName("score")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.Url)
                .HasColumnName("url")
                .HasColumnType("varchar")
                .IsOptional();

            Property(x => x.WebInformationId)
                .HasColumnName("webinformation_id")
                .HasColumnType("int")
                .IsOptional();

            //HasOptional(x => x.WebInformation)
            //    .WithMany()
            //    .HasForeignKey(x => x.WebInformationId);
        }
    }

    public class WebPartialConfiguration : EntityTypeConfiguration<WebPartial>
    {
        public WebPartialConfiguration()
        {
            ToTable("crowd_freelancer_webinformation_webpartial");

            HasKey(x => x.Id)
                .HasEntitySetName("id");

            Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Score)
                .HasColumnName("score")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.Url)
                .HasColumnName("url")
                .HasColumnType("varchar")
                .IsOptional();

            Property(x => x.WebInformationId)
                .HasColumnName("webinformation_id")
                .HasColumnType("int")
                .IsOptional();

            //HasOptional(x => x.WebInformation)
            //    .WithMany()
            //    .HasForeignKey(x => x.WebInformationId);
        }
    }

    public class WebSimilarConfiguration : EntityTypeConfiguration<WebSimilar>
    {
        public WebSimilarConfiguration()
        {
            ToTable("crowd_freelancer_webinformation_websimilar");

            HasKey(x => x.Id)
                .HasEntitySetName("id");

            Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Score)
                .HasColumnName("score")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.Url)
                .HasColumnName("url")
                .HasColumnType("varchar")
                .IsOptional();

            Property(x => x.WebInformationId)
                .HasColumnName("webinformation_id")
                .HasColumnType("int")
                .IsOptional();

            //HasOptional(x => x.WebInformation)
            //    .WithMany()
            //    .HasForeignKey(x => x.WebInformationId);
        }
    }

    public class WebPageConfiguration : EntityTypeConfiguration<WebPage>
    {
        public WebPageConfiguration()
        {
            ToTable("crowd_freelancer_webinformation_webpage");

            HasKey(x => x.Id)
                .HasEntitySetName("id");

            Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Score)
                .HasColumnName("score")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.Url)
                .HasColumnName("url")
                .HasColumnType("varchar")
                .IsOptional();

            Property(x => x.WebInformationId)
                .HasColumnName("webinformation_id")
                .HasColumnType("int")
                .IsOptional();

            //HasOptional(x => x.WebInformation)
            //    .WithMany()
            //    .HasForeignKey(x => x.WebInformationId);
        }
    }

}
