﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class UserConfiguration : BaseEntityConfiguration<User>
    {
        public UserConfiguration()
            :base("crowd_users")
        {
            Ignore(x => x.NewPassword);

            HasOptional(x => x.Customer)
                .WithMany()
                .HasForeignKey(x => x.IdCustomer);

            HasOptional(x => x.Freelancer)
                .WithMany(x => x.Users)
                .HasForeignKey(x => x.IdFreelancer);

            Property(x => x.IdCustomer)
                .HasColumnName("id_customer")
                .HasColumnType("int");

            Property(x => x.IdFreelancer)
                .HasColumnName("id_freelancer")
                .HasColumnType("int");

            Property(x => x.Name)
                .HasColumnName("name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Email)
                .HasColumnName("email")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Password)
                .HasColumnName("password")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsOptional();

            Property(x => x.RecoverPassword)
                .HasColumnName("recover_password")
                .HasColumnType("uniqueidentifier");

            Property(x => x.Role)
                .HasColumnName("role")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Photo)
                .HasColumnName("photo")
                .HasColumnType("varchar")
                .IsMaxLength();

            Property(x => x.Phone)
                .HasColumnName("phone")
                .HasColumnType("varchar")
                .IsMaxLength();

            Property(x => x.RoleCompany)
                .HasColumnName("role_company")
                .HasColumnType("varchar")
                .IsMaxLength();

            Property(x => x.AcceptTerms)
                .HasColumnName("accept_terms")
                .HasColumnType("bit");

            Property(x => x.AcceptTermsAt)
                .HasColumnName("accept_terms_at");

            Property(x => x.ConfirmEmailAt)
                .HasColumnName("confirm_email_at");

            Property(x => x.ConfirmEmail)
                .HasColumnName("confirm_email")
                .HasColumnType("uniqueidentifier");

            Property(x => x.IsResetting)
                .HasColumnName("reset")
                .HasColumnType("bit")
                .IsOptional();
        }
    }

    public class ActionLogConfiguration : BaseEntityConfiguration<User.ActionLog>
    {
        public ActionLogConfiguration()
            : base("crowd_users_log")
        {
            Property(x => x.UserId)
                .HasColumnName("user_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Type)
                .HasColumnName("type")
                .HasColumnType("int")
                .IsRequired();
        }
    }
}
