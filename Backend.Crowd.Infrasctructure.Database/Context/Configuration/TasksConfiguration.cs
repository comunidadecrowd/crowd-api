﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Backend.Crowd.Domain.Entities;


namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class TasksConfiguration : BaseEntityConfiguration<Tasks>
    {
        public TasksConfiguration()
            : base("crowd_tasks")
        {

            HasRequired(x => x.User)
               .WithMany()
               .HasForeignKey(x => x.IdUser);

            Property(x => x.IdUser)
                .HasColumnName("id_user")
                .HasColumnType("int");

            HasRequired(x => x.Project)
               .WithMany()
               .HasForeignKey(x => x.IdProject);

            Property(x => x.IdProject)
                .HasColumnName("id_project")
                .HasColumnType("int");

            HasRequired(x => x.Freelancer)
               .WithMany()
               .HasForeignKey(x => x.IdFreelancer);

            Property(x => x.IdFreelancer)
                .HasColumnName("id_freelancer")
                .HasColumnType("int");

            HasOptional(x => x.Briefing)
               .WithMany()
               .HasForeignKey(x => x.IdBriefing);

            Property(x => x.IdBriefing)
                .HasColumnName("id_briefing")
                .HasColumnType("int")
                .IsOptional();

            Property(x => x.Title)
                .HasColumnName("title")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Description)
                .HasColumnName("description")
                .HasColumnType("text")
                .IsMaxLength();

            Property(x => x.Status)
                .HasColumnName("status")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.DeliveryAt)
                .HasColumnName("delivery_at");

            Property(x => x.ApprovalAt)
                .HasColumnName("approval_at");

            Property(x => x.DueAt)
                .HasColumnName("due_at");

            Property(x => x.Price)
                .HasColumnName("price")
                .HasColumnType("decimal")
                .IsOptional();

            Property(x => x.FreelancerReview)
                .HasColumnName("review_freelancer")
                .HasColumnType("bit")
                .IsOptional();

            Property(x => x.CustomerReview)
                .HasColumnName("review_customer")
                .HasColumnType("bit")
                .IsOptional();
        }
    }
}
