﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Crowd.Domain.Entities;

namespace Backend.Crowd.Infrasctructure.Database.Context.Configuration
{

    public class CampaignConfiguration : BaseEntityConfiguration<Campaign>
    {
        public CampaignConfiguration()
            : base("crowd_campaigns")
        {

            Property(x => x.Text)
                .HasColumnName("text")
                .HasColumnType("text");

            Property(x => x.Title)
                .HasColumnName("title")
                .HasColumnType("varchar");

            Property(x => x.LimitParticipation)
                .HasColumnName("limitParticipation")
                .HasColumnType("int");


            
        }
    }
}
