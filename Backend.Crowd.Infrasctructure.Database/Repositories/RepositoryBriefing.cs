﻿using Backend.Crowd.Domain.Entities;
using Backend.Crowd.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;

using Backend.Crowd.Infrasctructure.Database.Context;
using System.Linq;

namespace Backend.Crowd.Infrasctructure.Database.Repositories
{
    public class RepositoryBriefing : RepositoryBase<Briefing>,IRepositoryBriefing
    {
        public RepositoryBriefing(CrowdContext context) : base(context)
        {
        }

        public void ExecuteCommand(string query)
        {
            Connection.Database.ExecuteSqlCommand(query);
        }

        public Briefing Buscar(int id)
        {
            return  Connection.Briefing
                .Where(x => x.Id == id)
                .FirstOrDefault();
        }

        public IEnumerable<BriefingList> GetList(string query, params object[] parameters)
        {
            return Connection.Database.SqlQuery<BriefingList>(query,parameters);
        }
    }
}
