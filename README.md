## Iniciando o projeto (front-end): ##
Acessos para deploy no ambiente de homologação
```
FTP homolog.comunidadecrowd.com.br/	
developer
TPN55fDZKWQsbg4G

banco de dados Homolog
dev	$dev_crowd#2016
```

**Abra o navegador em: localhost:8080**

### **Instalar** o git-flow, ou usar alguma GUI compatível *(ex.: GitKraken)* ###
https://danielkummer.github.io/git-flow-cheatsheet/index.pt_BR.html

## Estrutura das branches: ##

### Principais ###
*   **Master** - Produção
*   **Develop** - Homologação

### Gitflow ###
*   **Feature** - Desenvolvimento
*   **Hotfix** - Bugs em produção
*   **Release** - Versão RC/Beta

O fluxo é abrir de Master uma nova **feature/hotfix** branche somente para a tarefa atual, testar e concluir realizando um **merge** em uma **release branche** também de master, realizar um **teste de implementação** subindo para o ambiente de homologação, por fim enviar um **pull request** da sua **release branche**.
O desenvolvimento deve acontecer localmente em sua máquina, inclusive todos os testes que não forem de implementação da release.
Você pode subir no ambiente de homologação para testar sua branche quando em **momento de teste de implementação e Pull Request**.
**Todos** os envios para as branches Develop e Master são através de **Pull Request**.